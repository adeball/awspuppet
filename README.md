# Ux1 AWS

Ux1 Ltd AWS infrastructure 
Contains CloudFormation templates, scripts, client configuration directories and the puppet source tree for client infrastructure builds.

## Basic Structure

* etc/*_table: Top-level configuration
* Client directories/template_client: CF templates and customised build scripts
* puppet
  * hieradata: Local and client configuration data
  * manifests: Main Puppet configuration
  * modules: Standard, servertype_* & local_* modules
* scripts: Build scripts (start with common.sh)

## Outline instructions for building a new infrastructure

Note: There is full documentation available, but it still contains client information - it is therefore not publically available at present.

* Edit etc/infrastructure_table as required
* Set up AWS credentials
* Create a client directory (see scripts/create_client_directory.sh)
* Run the client-directory/infra.sh script to initiate the CloudFormation infrastructure build

