# 
# common.pp
# 
# Everything here is used on every node
# Rather than defining each node by name, use the
# info from cloudformation to include the appropriate
# servertype module
#


### Code below here applies to EVERY node...

### Install standard packages and files
#include common
### 2017-09-01: Now done in hiera


### Baseline firewall configuration

resources { 'firewall':
  purge => true,
}

Firewall {
  before  => Class['common-fw::post'],
  require => Class['common-fw::pre'],
}

class { ['common-fw::pre', 'common-fw::post']: }
class { 'firewall': }


### No need for individual node definitions, as everything should have a servertype

node default {
  case $cloudservertype {
    'admin':        { include servertype_admin }
    'testbuild':    { include servertype_testbuild }
    'maximo':       { include servertype_maximo }
    'db2':          { include servertype_db2 }
    'nodejs':       { include servertype_nodejs }
    'couchdb':      { include servertype_couchdb }
    'nginx':        { include servertype_nginx }
    'cognos':       { include servertype_cognos }
    'vpn':          { include servertype_vpn }
    'websphere':    { include servertype_websphere }
    'webspheremgr': { include servertype_webspheremgr }
    'proxy':        { include servertype_proxy }
    'icinga2':      { include servertype_icinga2 }
    'lamp':         { include servertype_lamp }
  }

}


