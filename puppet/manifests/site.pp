
# /etc/puppet/manifests/site.pp

hiera_include ('classes')


# Import every file in /etc/puppet/manifests/nodes/
import 'nodes/*.pp'

# Get rid of annoying warnings...
  Package {
    allow_virtual => true
  }

