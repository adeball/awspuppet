#!/usr/bin/env bash

if [[ $# -ne 1 ]] ; then
    echo "Usage: $0 modulename"
    exit 1
fi

module=$1

### Get e.g. 'local' or 'servertype'
firstpart=${module%%_*}

cd $(dirname $0)

if [[ -d "$module" ]] ; then
    echo "$module already exists"
    exit 1
fi

mkdir -p $module/manifests
sed -e "s/modulename/$module/g" 0README.md.template > $module/README.md

if [[ "$firstpart" == "servertype" ]] ; then
    cat > $module/manifests/init.pp << EOF
class $module {

  \$version = hiera(common::version)
  case \$version {
    '1': { include $module::v1 }
    '2': { include $module::v2 }
    'recover': { include $module::recover }
  }

}
EOF

    cat > $module/manifests/v1.pp << EOF
class $module::v1 {

        file {'/tmp/version':
                content => '1
',
        }

        include $module::storage
        include $module::firewall
}
EOF

    cat > $module/manifests/v2.pp << EOF
class $module::v2 {
        file {'/tmp/version':
                content => '2
',
        }
}
EOF

    cat > $module/manifests/recover.pp << EOF
class $module::recover {

}
EOF

    cat > $module/manifests/firewall.pp << EOF
class $module::firewall {

}
EOF

    cat > $module/manifests/storage.pp << EOF
class $module::storage {

  ### Disk device setup

  class { 'lvm':
    volume_groups => {
      'vg_data' => {
        physical_volumes => \$common::lvmdisks,
        logical_volumes  => {
          ### 4GB minus one 4MB extent
          'swap' => { 'size' => '4092M', fs_type => swap, },
        },
      },
    },
  }

}
EOF

else

    cat > $module/manifests/init.pp << EOF
class $module {

}

EOF

fi
