#!/usr/bin/env bash

ls | grep '-'| while read module ; do
    echo $module
    newmod=$(echo "$module" | sed -e 's/-/_/g')
    mkdir -p $newmod
    cd $module
    find . -type d | cpio -pdmv ../$newmod
    find . -type f | while read f ; do
        sed -e "s/$module/$newmod/g" "$f" > ../$newmod/$f
    done
    cd ..
done
