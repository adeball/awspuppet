
class apache::amazon_systemd_fix {

  ### Amazon Linux 2: httpd.service does not work by default, replace with this
  if $operatingsystem == "Amazon" {
    file { '/usr/lib/systemd/system/httpd.service':
      owner   => root,
      content => '

[Unit]
Description=The Apache HTTP Server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
EnvironmentFile=/etc/sysconfig/httpd
ExecStart=/usr/sbin/httpd $OPTIONS -k start
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
ExecStop=/usr/sbin/httpd $OPTIONS -k graceful-stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target

      '
    }

  }
}