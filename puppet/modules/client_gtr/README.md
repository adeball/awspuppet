# client_gtr

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [client_gtr]](#setup)
    * [What [client_gtr] affects](#what-[client_gtr]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [client_gtr]](#beginning-with-[client_gtr])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module applies GTR specific changes, i.e. functionality that will never be required in any other client account.

GTR access is via a dedicated VPN, which is only available to systems within the GTR AWS account.  
For support purposes it is a requirement to access certain GTR Windows systems via RDP, which cannot be achieved directly from the management network.
This module creates a script to set up SSH tunnels to enable this access, creates and runs a service to ensure that the tunnels are always available.

### Setup Requirements 

Any host to which this module is applied must have access to the GTR network via the VPN.
Hosts must have firewall rules and security groups configured to allow access to the inbound ports specified.

### Beginning with [common]	

Include the module (include client_gtr).  (Use Heira classes to apply only to the GTR )

## Usage

Set the parameters $envallowed & $servertypeallowed to 'true' 

## Reference

Parameters:
* envallowed: default 'false'
* servertypeallowed: default 'false'

Set both to true (use Hiera) to enable on specific hosts without having to name them (i.e. only PRD and webspheremgr).
  
## Limitations

This is a very specific, environment-limited module.  Only use on GTR.

## Development

Against the Puppet philosophy, this module is hard-coded.  It will never be reused.
Make any changes to the init.pp and included file(s) directly.

