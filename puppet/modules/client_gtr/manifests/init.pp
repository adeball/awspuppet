class client_gtr (
  $envallowed = 'false',
  $servertypeallowed = 'false',
){

  if $envallowed == 'true' and $servertypeallowed == 'true' {

    file {'/etc/init.d/gtr_ssh_tunnels':
      source => 'puppet:///modules/client_gtr/etc/init.d/gtr_ssh_tunnels',
      owner => root,
      mode => 0755,
    } ->
    exec {'Set up SSH tunnel to GTR as a service':
      path => '/usr/bin:/usr/sbin',
      command => 'chkconfig --add gtr_ssh_tunnels',
      creates => '/etc/rc3.d/S99gtr_ssh_tunnels',
    } ->
    service {'gtr_ssh_tunnels':
      ensure => running,
    }

  }

}
