# common-fw

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [common-fw]](#setup)
    * [What [common-fw] affects](#what-[common-fw]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [common-fw]](#beginning-with-[common-fw])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up iptables with the base rules for Linux instances.
Essentially, allow SSH, block everything else.

NB: You really don't need to touch this module.  If you make a mistake, it is quite possible to block all access to every network service on all instances in one fell swoop.  This is generally regarded as a Bad Thing and a "career limiting move".  If access is blocked to all services, reverting the change in Puppet will not fix the problem, as the client will not be able to communicate with the Puppet Master.  You would have to stop every instance, detach the root filesystem, attach elsewhere, fix and restart.  Not a fun activity.

Note: All other modules with dashes as part of the name were converted to the equivalent using underscores.  This one can be left - it could be done, with a load of work/testing.  The risk of a problem is small, but the impact if it went wrong is enormous.  One for later when there's a test infrastructure available.

## Setup

### Beginning with [common-fw]	

include common-fw (Use Hiera)

## Usage


## Reference

## Limitations

## Development

See description.  Only make changes if thoroughly tested in a sandbox/altenative environment prior to updating.  This is a potentially very dangerous place to be working.  Additional firewall rules should be defined outside of this module (typically in servertype-*::firewall)


