# common

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [common]](#setup)
    * [What [common] affects](#what-[common]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [common]](#beginning-with-[common])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

The common module is inherited by all non-specific AWS/Linux build systems (i.e. it applies to most systems, with the exception of specialised servers built from marketplace AMIs, e.g. RancherOS servers).

This is the place to add any packages, configuration changes, standard scripts (i.e. those to be deployed and made available on the servers, e.g. in /usr/local/bin) which need to be applied across all servers.

## Setup

include common (or add to Hiera classes)

### What [common] affects 

* Standard package installation
* Standard links required
* Various standard configuration changes from default
* Standard scripts -> /usr/local/bin
* motd setup
* Security changes (e.g. password quality)
* Makes host file entries consistent


### Beginning with [common]	

include common (or add to Hiera classes)

## Usage

## Reference

Two variables are defined in common, though not used directly in the module itself:

1. $lvmdisks (default: '/dev/xvdd'): An array of disks (often just one) - this must match the actual external devices used on an AWS instance, as Puppet will expect it/them to be available.  Normally xvdd is sufficient, but it is best to define in Hiera, then specific hosts or groups of systems may have different devices defined as requirements change over time.

2. $releasever (default: '7'): This is the version number for the OS, as used by YUM.  It cannot be parameterised in YUM as it should be, because the AWS Linux build gets this wrong.  Defining here enables /etc/yum/repos.d/ files to be configured correctly.

## Limitations

## Development

