#!/bin/bash
#
# aws_variables.sh
#
# Sets default region (used by AWS utilities), and other useful info
#

tmpf=$(mktemp)

curl -s http://169.254.169.254/latest/dynamic/instance-identity/document > $tmpf

export AWS_DEFAULT_REGION=$(awk -F\" '/region/ {print $4}' $tmpf )
export AWS_AZ=$(awk -F\" '/availabilityZone/ {print $4}' $tmpf )
export AWS_INSTANCE_ID=$(awk -F\" '/instanceId/ {print $4}' $tmpf )
export AWS_INSTANCE_TYPE=$(awk -F\" '/instanceType/ {print $4}' $tmpf )

export AWS_BACKUP=s3://myco-backup-${CLOUDCLIENT,,}/${CLOUDENV,,}/${CLOUDSERVERTYPE}-$CLOUDINSTANCE


rm -f $tmpf
