#!/bin/ksh
#
# cpio_del_month - Ade - 04/2002
#
# Takes a month (01-12) and year (ccyy) as arguments
# finds all files modified in that month and creates a cpio file
# ccyy_mm.cpio, then removes original files if no errors occurred
#
# To extract all files in an archive e.g.: zcat 2002-02.cpio.gz | cpio -iv
#

[[ $(uname) == "Linux" ]] && AWK=/usr/bin/awk || AWK=/usr/bin/nawk


trap bombout ERR

bombout()
{
	echo "Encountered some error - stopping now, before I do something I might regret..."
	exit 1
}

usage() 
{
    echo "usage $0 year month"
    echo "year=CCYY, eg 2001, month=1-12"
    exit 1
}

[ $# -ne 2 ] && usage
[ $1 -lt 1970 -o $1 -gt 2038 ] && usage
year=$1
shift
[ $1 -lt 1 -o $1 -gt 12 ] && usage
month=`echo $1| $AWK '{printf "%2.2d\n", $1}'`

logfile=$(mktemp)

nextmonth=`echo $month | $AWK '{printf "%2.2d\n", $1+1}'`
nextyear=$year
if [ $nextmonth -eq 13 ] ; then
    nextmonth="01"
    nextyear=`expr $year + 1`
fi

startfile=$(mktemp)
stopfile=$(mktemp)
touch -am -t $year${month}010000 $startfile
touch -am -t $nextyear${nextmonth}010000 $stopfile

echo "pwd=$(pwd)" > $logfile
ls -l $startfile $stopfile >> $logfile

arcfile=${year}_${month}.cpio
echo "arcfile=$arcfile" >> $logfile

echo "Creating/adding to cpio archive $arcfile..."

if [ -f $arcfile.gz ] ; then
	echo gunzipping $arcfile, so that we can append
	gunzip $arcfile.gz
fi

if [ -f $arcfile.Z ] ; then
	echo uncompressing $arcfile, so that we can append
	uncompress $arcfile.Z
fi

append=""
if [ -f $arcfile ] ;then 
	### Test the toc file for emptiness, as we cannot append to an empty cpio file
	cpio -itv < $arcfile > $arcfile.toc
	[ -s $arcfile.toc ] && append="-A"
fi


find . -newer $startfile ! -newer $stopfile ! -name "*cpio*" | \
	cpio -ovC65536 -c $append -O $arcfile
if [ $? -ne 0 ] ; then
    echo "AAAAAARGH - this didn't work for some reason, not deleting original files"
    rm $startfile $stopfile
    exit 1
fi

echo "Creating TOC file..."
cpio -itv < $arcfile > $arcfile.toc

### If the toc file contains nothing, then so does the archive - 
### we can safely delete them and give up now

if [ ! -s $arcfile.toc ] ; then
	echo "No files in $arcfile.cpio, removing..."
	rm $arcfile $arcfile.toc
	exit 0
fi


### Only remove files that have made it to the cpio archive - can't make it any
### safer than this...

echo "Removing these files..."
for file in $($AWK '{print $NF}' $arcfile.toc) ; do
	echo $file
	[ ! -d $file ] && rm -f $file
done

cat $logfile >> /tmp/cpio_del_month.log
rm $startfile $stopfile $logfile

touch -am -t $year${month}010000 $arcfile $arcfile.toc
echo "gzipping $arcfile (background)"
gzip $arcfile &

