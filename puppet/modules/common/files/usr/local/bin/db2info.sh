#!/usr/bin/env bash
#
# Print some basic info about a DB2 installation
#


### If there's no ctginst1 user, then exit
id ctginst1 >/dev/null 2>&1
[[ $? -ne 0 ]] && exit

suctginst1()
{
        echo "$@" | sudo -i -u ctginst1
}


### Obtain a list of local databases (not remote connections)
dbs=$(suctginst1 db2 list database directory | awk '
        /Database name/         {dbname=$NF}
        /Directory entry type/  {if ($NF=="Indirect") { print dbname }}
')

fmt="%-12s : %s\n"

printf "$fmt" "Install path" "$(db2ls | awk '/^\// {print $1}')"
printf "$fmt" "Level" "$(db2ls | awk '/^\// {print $2}')"
printf "$fmt" "Fix pack" "$(db2ls | awk '/^\// {print $3}')"

tcpport=$(suctginst1 "db2 get dbm config" | awk '/\(SVCENAME\)/ {print $NF}')
printf "$fmt" "TCP Port" $tcpport

for db in $dbs ; do
    users=$(suctginst1 "db2 connect to $db > /dev/null ; db2 -x select GRANTEE from syscat.dbauth" \
        | awk '{printf "%s ", $1} END {print ""}' | sed -e 's/ $//')
    printf "$fmt" "Database" "$db ($users)"
done

