class common (
  $lvmdisks = ['/dev/xvdd',],
  $releasever = '7',
)
{

  if $osfamily == "Debian" {
    package { [ git, ksh, unzip, strace, xauth, xterm, htop, jq, ]: ensure => present }
  } else {
    package { [ git, bind-utils, nc, ksh, unzip, strace, xauth, xterm, htop, jq, 'epel-release', ]: ensure => present }
  }

  file {'/usr/bin/pip-python': ensure => link, target => '/usr/bin/pip', }
  file {'/dev/shm': mode => 1777 }


  ### SNMP - see Hiera for installation/setup of the module itself.
  firewall { '161 Allow inbound SNMP':
    dport    => 161,
    proto    => udp,
    action   => accept,
  }

  ### Disable local postfix daemon except on admin servers
  ### RWGMROSI-483
  if $cloudservertype != "admin" {
    service {'postfix': ensure => 'stopped'}
  }

  service {'sshd': ensure => 'running'}

  ### AWS Linux needs root login with keys enabling, and the Postfix package

  if $cloudos == "linux" {
    exec { 'Permit root login with keys':
      path => '/usr/bin:/bin',
      command => 'sed -i -e "s/^PermitRootLogin forced/#PermitRootLogin forced-commands-only/;
      s/^#PermitRootLogin yes/PermitRootLogin yes/" /etc/ssh/sshd_config',
      unless => 'grep -q "^PermitRootLogin yes" /etc/ssh/sshd_config',
      notify => Service['sshd'],
    }
    ensure_packages(['postfix',])
  }


  # ### Argos is an anamoly, otherwise this should be in Hiera
  # case $cloudservertype {
  #   /(db2|websphere|proxy|maximo)/: {
  #     unless $hostname == 'argos' { include users_ibm }
  #   }
  # }


  ### Copy /usr/local/bin scripts

  file { '/usr/local/bin':
    path    => '/usr/local/bin',
    source  => 'puppet:///modules/common/usr/local/bin',
    recurse => true,
    mode    => 755,
    owner   => 0,
    group   => 0,
  }

  file {'/etc/cron.hourly/mk_motd':
    ensure => link,
    target => '/usr/local/bin/mk_motd',
  }

  ### Don't do anything with these files, just ensure Puppet tracks them (for monitoring software updates)
  file {'/cloudinit/etc/host_table': ensure => present }
  file {'/cloudinit/etc/host_table_monitoronly': ensure => present }

  ### This file is created by the install scripts, but we need to define for a dependency (in case it is changed)
  file {'/etc/profile.d/cloud.sh':
    ensure => present,
    owner => root,
    group => root,
    mode => 0755,
  }

  ### Puppet agent needs the cloud env stuff setting up as environment variables

  exec { 'Create puppet agent environment variables':
#    creates => '/etc/sysconfig/puppet',
    unless => 'grep CLOUD /etc/sysconfig/puppet',
    command => 'mkdir -p /etc/sysconfig; sed -e \'s/export //\' /etc/profile.d/cloud.sh > /etc/sysconfig/puppet; service puppet restart',
    path    => '/usr/bin:/usr/sbin:/bin:/sbin',
    subscribe => File['/etc/profile.d/cloud.sh'],
  }

    if $cloudos == "linux" {
      ### Amazon Linux upstart ignores /etc/sysconfig, so replace the init script with one which sources /etc/profile
      file { '/etc/init.d/puppet':
        ensure => present,
        owner  => root,
        group  => root,
        mode   => 0755,
        source => 'puppet:///modules/common/etc/init.d/puppet',
      }
    }

  if $cloudos == "ubuntu" {
    file { '/etc/init.d/puppet':
      ensure => present,
      owner  => root,
      group  => root,
      mode   => 0755,
      source => 'puppet:///modules/common/etc/init.d/puppet_ubuntu',
    }
  }

  ### Password quality
  file {'/etc/security/pwquality.conf':
    source => 'puppet:///modules/common/etc/security/pwquality.conf',
    mode => 0644,
    owner => root,
  }


  ### Set up some AWS related environment variables
  ### NB: make sure it's alphabetically after cloud.sh

  file {'/etc/profile.d/variables_aws.sh':
    source => 'puppet:///modules/common/etc/profile.d/variables_aws.sh',
    mode => 0755,
    owner => root,
  }

  file { '/shared': ensure => directory }


  ### Switch SELinux to permissive mode

  file {'/etc/selinux/config':
    ensure => present,
    owner => root,
    mode => 0644,
    content => 'SELINUX=permissive
SELINUXTYPE=targeted
',
  }


  ### Ensure consistent /etc/hosts entries

  host { 'localhost':
    ip           => '127.0.0.1',
    host_aliases => ['localhost.localdomain', 'localhost4', 'localhost4.localdomain4'],
    comment      => 'Managed by puppet',
  }
  host { 'localhost6':
    ip           => '::1',
    host_aliases => ['localhost6.localdomain4'],
    comment      => 'Managed by puppet',
  }
  host { $fqdn:
    ip           => $ipaddress,
    host_aliases => [$hostname, "$hostname.myco.com"],
    comment      => 'Managed by puppet',
  }

}
