class hieratest (
 $testvar1 = 'default value from class def',
  $testvar2 = 'default value from class def',
) {

 file {'/tmp/hieratest':
   ensure => present,
   content => template("hieratest/hieratest.erb"),
 }
}