# local_audit

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_audit]](#setup)
    * [What [local_audit] affects](#what-[local_audit]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_audit]](#beginning-with-[local_audit])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Ensures SELinux messages are directed to /var/log/audit/* by setting up the config file and ensuring auditd is running.       

## Setup

### What [local_audit] affects **OPTIONAL**

SELinux
Updates /etc/setroubleshoot/setroubleshoot.conf

### Beginning with [local_audit]	

include local_audit

## Usage

## Reference

## Limitations

Use on RHEL7, not applicible to Amazon Linux as SELinux is disabled by default.

## Development

