class local_audit {

	### Ensure SELinux messages go to /var/log/audit/* by running auditd
	service {'auditd': ensure => running }

	### Decrease verbosity
  file {'/etc/setroubleshoot/setroubleshoot.conf':
		source => "puppet:///modules/$name/etc/setroubleshoot/setroubleshoot.conf",
		owner => root,
	}

}
