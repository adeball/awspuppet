# local_clamav

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_clamav]](#setup)
    * [What [local_clamav] affects](#what-[local_clamav]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_clamav]](#beginning-with-[local_clamav])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs and configures ClamAV Anti-virus scanning & online detection software.

ClamAV is free non-proprietory Unix/Linux AV software.  Unfortunately, it is notoriously awkward to install ClamAV on RHEL due to broken dependencies, so the software was compiled and made available as part of the Puppet module.
See https://confluence.myco.com/display/EC/Anti-virus for more details.

## Setup

### What [local_clamav] affects 

* The software is self-contained in /opt/clamav
* Packages required to support clamav: pcre 
* The user and group clamav are configured
* The service clamav is set up and executed
* Logs are stored in /var/log
* Cron is configured to download new virus definitions daily
* Cron is configured to execute a full scan weekly

### Setup Requirements 

Amazon Linux pcre uses a different library version (libpcre.so.0) to the one ClamAV was compiled with (libpcre.so.1).
Rather than recompiling, this was tested using a link, which works fine.  The link is automatically created by Puppet if on Amazon Linux.

### Beginning with [local_clamav]	

include local_clamav

## Usage

The module was written some time ago, so needs parameterising.  For now, changes must be made by editing the config files in Puppet.  The most important file is files/opt/clamav/etc/clamd.conf.template.


## Reference

### files/opt/clamav/etc/clamd.conf.template
This file is used to generate the deployed /opt/clamav/etc/clamd.conf file.
In particular, the 'OnAccessExcludePath' and 'ExcludePath' directives tell the clamd daemon and clamscan programmes, respectively, where to avoid scanning.

### files/opt/clamav/clamscan.sh
This script is executed weekly from cron, it will perform a full scan on all files (with the exception of excluded ones, see above).

### files/opt/clamav/detected.sh
Executed if an AV signature is detected.  Moves the affected file to /var/lib/clamav/infected and logs a message.

### files/opt/clamav/freshclam.sh
Executed nightly at 3am - if provided with '-s secs' it waits a random time from zero to 'secs' seconds (default two hours).  This means that hosts will stagger their downloads.

### files/opt/clamav/mkclamdconf.sh
Creates /opt/clamav/etc/clamd.conf from the template.  Essentially copies the template, then appends the source of '/shared/maximo' to the 'on access' scan paths.  On Access scanning, only works from the host making the write request (i.e. the NFS server will not scan files it has been asked to write, only the reqeusting host - so all hosts with the NFS mounted /shared/maximo must scan their own files as they are written there).

## Limitations

Note the comment re: Amazon Linux in 'Setup Requirements'.  This is not really a limitation, as clamav could be compiled for any flavour of Linux in case of real trouble with library dependencies.

The clamscan programme uses quite a bit of RAM, and does not behave very well when there is not enough available.  The clamscan.sh script (run from cron) will therefore check for a minimum of 800MB free RAM (which seemed to be a safe limit in testing) before running.  If it fails to find this amount, it will try dropping cached memory and restarting the Puppet agent, then recheck.  If it still fails, the script will abort and log a failure message.

## Development


