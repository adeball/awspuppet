#!/usr/bin/env bash
#

fail()
{
    echo
    echo "Failure: $@"
    echo
    logger -p user.err "Error: $@"
    exit 1
}

LOGF=/var/log/clamscan.log
INFDIR=/var/lib/clamav/infected
MINMEM=800
MINDISK=1024

### Output to stdout & logfile
exec > >(tee $LOGF)
exec 2>&1

echo
echo "Started: $(date)"

freedisk=$(df -m / | awk '/^\// {print $4}')

if [[ "$freedisk" -lt "$MINDISK" ]] ; then
    fail "Free space on root (${freedisk}MB) is too low to safely run clamscan, needs ${MINDISK}MB"
fi

### Awesome, 'free -m' output is in different formats >:-(
if [[ "$CLOUDOS" == "linux" ]] ; then
    freemem=$(free -m | awk '/buffers\/cache/ {print $NF}')
else
    freemem=$(free -m | awk '/^Mem:/ {print $NF}')
fi

if [[ "$freemem" -lt "$MINMEM" ]] ; then
    echo "Free memory (${freemem}MB) is too low to safely run clamscan, needs ${MINMEM}MB"
    echo "Trying drop_caches & puppet restart..."
    echo 3 > /proc/sys/vm/drop_caches
    [[ "$CLOUDOS" == "linux" ]] && service puppet restart || systemctl restart puppet
    freemem2=$(free -m | awk '/^Mem:/ {print $NF}')
    if [[ "$freemem2" -lt "$MINMEM" ]] ; then
        fail "Free memory (${freemem}MB, ${freemem2}MB after drop_caches/puppet restart) is still too low to safely run clamscan, needs ${MINMEM}MB"
    fi
fi


mkdir -p $INFDIR

### Don't use --log as it overwrites, better to append so we retain the history
(time nice /opt/clamav/bin/clamscan -r -i --move=$INFDIR --exclude-dir="^/proc|^/sys|^$INFDIR" /)
echo "Completed: $(date)"
echo
