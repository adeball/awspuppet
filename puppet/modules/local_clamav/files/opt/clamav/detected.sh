#!/bin/bash

#echo "Signature detected: $CLAM_VIRUSEVENT_VIRUSNAME in $CLAM_VIRUSEVENT_FILENAME"

export PATH=/bin:/usr/bin
INFDIR=/var/lib/clamav/infected
mkdir -p $INFDIR
echo "Moving $CLAM_VIRUSEVENT_FILENAME to $INFDIR" >> /var/log/clamd_detected.log
mv "$CLAM_VIRUSEVENT_FILENAME" $INFDIR
