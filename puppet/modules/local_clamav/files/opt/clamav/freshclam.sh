#!/usr/bin/env bash
#
# Call with -s seconds to sleep a random number of seconds
#

fail()
{
    echo
    echo "Failure: $@"
    echo
    exit 1
}

getopts s: s
secs=$OPTARG

. /etc/profile

if [[ ! -z ${secs} ]] ; then
    rnd=$(( $RANDOM * $secs / 32768 + 1 ))
    echo "Sleeping for $rnd seconds..."
    sleep $rnd
fi

/opt/clamav/bin/freshclam
