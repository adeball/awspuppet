#!/bin/bash
#
#

. /etc/profile
export PATH=/bin:$PATH

chome=/opt/clamav

### The template file does not include filesystems to monitor.
### Adding non-existent filesystems causes clamd to silently fail, so
### ensure that we use a list from the system itself

cp $chome/etc/clamd.conf.template $chome/etc/clamd.conf


fsf=$(mktemp)

### This line would add all local filesystems, however, performance can be *severely* impacted
#mount | awk '$5 ~ "^ext|^xfs$" {printf "OnAccessMountPath %s\n", $3}' | grep -v '/shared' > $fsf

### Instead, just protect the source of the /shared/maximo share as an OnAccess activity
### The other filesystems will be scanned as scheduled tasks instead.
mount | awk '$5 ~ "^ext|nfs4|^xfs$" {printf "OnAccessMountPath %s\n", $3}' | egrep  '/shared/maximo' > $fsf


### Append the filesystem list for OnAccess scanning to the copy of the template conf file
### The sort is in case there are duplicates (sometimes happens with NFS mounts on Linux :-S )
sort -u $fsf >> $chome/etc/clamd.conf

rm $fsf
