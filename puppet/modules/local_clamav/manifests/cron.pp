
class local_clamav::cron {

  file {'/opt/clamav/freshclam.sh':
    ensure => present,
    source => 'puppet:///modules/local_clamav/opt/clamav/freshclam.sh',
    mode => 0755,
  }

  file {'/opt/clamav/clamscan.sh':
    ensure => present,
    source => 'puppet:///modules/local_clamav/opt/clamav/clamscan.sh',
    mode => 0755,
  }

  ### Distribute over a couple of hours randomly
  ### If this becomes a problem, we
  cron { 'Anti-virus definition update':
    command => '/opt/clamav/freshclam.sh -s 7200',
    user    => 'root',
    hour    => '03',
    minute  => '00',
  }

  cron {'Full AV scan':
    command => '/opt/clamav/clamscan.sh',
    user    => 'root',
    hour    => '05',
    minute  => '00',
    weekday     => '06',
  }

}
