class local_clamav {

  group { 'clamav': ensure => present, gid => 4004 } ->
    user { 'clamav': ensure => present, uid => 4006, gid => clamav, home => '/home/clamav', managehome => true, }

#  if $cloudenv != 'PRD' {
    include local_clamav::install
    include local_clamav::cron

    service { 'clamav':
      ensure => running,
    }
#  }

  file {['/var/log/freshclam.log', '/var/log/clamd.log', '/var/log/clamscan.log',]:
    ensure => present,
    owner => clamav,
    group => root,
    mode => 0644,
  }

  ### I compiled ClamAV on RHEL7.  Libraries on Amazon Linux are older, but it seems to work happily with a link.
  ### It's only for regexps anyway.  If this cause problems, it will need to be recompiled and copied for Amazon Linux
  if $cloudos == "linux" {
    file{'/lib64/libpcre.so.1':
      ensure => link,
      target => './libpcre.so.0',
    }
    ### Amazon Linux uses upstart
    file { '/etc/init/clamav.conf':
      ensure => present,
      owner  => root,
      mode   => 0755,
      source => 'puppet:///modules/local_clamav/etc/init/clamav.conf',
    }
  }



}
