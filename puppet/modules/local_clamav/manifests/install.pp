class local_clamav::install {

  file {'/opt/clamav':
    ensure => directory,
    source => 'puppet:///modules/local_clamav/opt/clamav',
    recurse => true,
    purge => false,
    mode => 0755,
  }

  file {['/opt/clamav/share/clamav', '/var/lib/clamav', ] :
    ensure => 'directory',
    owner => 'clamav',
    mode => 0755,
  } ->
  exec {'Download initial ClamAV definitions':
    path => '/usr/bin:/usr/sbin',
    command => '/opt/clamav/bin/freshclam',
    creates => '/var/lib/clamav/main.cvd',
  } ->
  file {'/opt/clamav/etc/clamd.conf.template':
    ensure => present,
    source => 'puppet:///modules/local_clamav/opt/clamav/etc/clamd.conf.template',
    mode => 0644,
    notify => Service ['clamav'],
  }
  file {'/opt/clamav/mkclamdconf.sh':
    ensure => present,
    source => 'puppet:///modules/local_clamav/opt/clamav/mkclamdconf.sh',
    mode => 0755,
    notify => Service ['clamav'],
  } ->
  exec {'Create clamd.conf':
    path => '/usr/bin:/usr/sbin',
    command => '/opt/clamav/mkclamdconf.sh',
    subscribe => [ File ['/opt/clamav/etc/clamd.conf.template'], File ['/opt/clamav/mkclamdconf.sh'], ],
    refreshonly => true,
  } ->
  file {'/etc/init.d/clamav':
    ensure => present,
    mode => 0755,
    source => 'puppet:///modules/local_clamav/etc/init.d/clamav',
  } ->
  exec {'Add ClamAV to system start up':
    path => '/bin:/sbin:/usr/bin:/usr/sbin',
    command => 'chkconfig --add clamav',
    creates => '/etc/rc3.d/S99clamav',
  }

  file {'/opt/clamav/detected.sh':
    ensure => present,
    source => 'puppet:///modules/local_clamav/opt/clamav/detected.sh',
    mode => 0755,
  }

}
