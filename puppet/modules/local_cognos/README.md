# local_cognos

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_cognos]](#setup)
    * [What [local_cognos] affects](#what-[local_cognos]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_cognos]](#beginning-with-[local_cognos])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

IBM Cognos is a reporting tool which integrates with Maximo.  This module installs and configures the software in a mostly automated fashion.  The set up is very complicated, with many dependencies, pitfalls, points of failure and less than helpful error messages.  This module constitutes many hours of work and pain in overcoming a lot of these obstacles.

See https://confluence.myco-group.com/display/EC/Cognos+BI+Server+Installation for full details.
The module installs dependencies, user accounts, sets up paths, library paths etc, installs and configures DB2 and Cognos software, and goes as far as it can in configuring the integration with Maximo (further steps are required on other servers).


## Setup

### Setup Requirements 

* There should be a /data/vol01 filesystem (or capacity under that path) for the Cognos Management database installation.  (See servertype-cognos, this is where it would normally be set up).

* Firewall rules (and AWs security groups where applicable) should allow traffic to ports:
    * 50005 (for DB2)
    * 80 (for the web service)
    * 9300 (for Cognos itself)
    
* The Cognos software is expected to be available under /shared/install/cognos/linuxi38664h

* The version installed is 10.2.1 (Fix packs caused issues, so are currently not added).  

### Beginning with [local_cognos]	

include local_cognos

## Usage

### Variables - default values 

* $cognosroot    = '/opt/ibm/cognos',
* $cognosinst    = '/opt/ibm/cognos/c10_64',
* $maxdbuser     = 'db2admin',
* $maxdbpassword = '%602;wall;NORTHERN;plants;7%',
* $maxdbserver   = 'db2',
* $cognosextdns  = 'dummyvalue',
* $installawstats = 'yes',

Mostly these should work as-is (the password is likely to be over-ridden of course).
The cognosextdns parameter, if left as 'dummyvalue', will be calculated from the hostname, environment, client and zone name.

## Reference

https://confluence.myco-group.com/display/EC/Cognos+BI+Server+Installation 

## Limitations

This module should only be installed on a supported IBM OS (i.e. this effectively means RHEL when on AWS).  

## Development

