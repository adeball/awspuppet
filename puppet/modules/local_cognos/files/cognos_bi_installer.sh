#!/bin/bash

sucog()
{
    echo "$@" | sudo -i -u cognos
}

fail()
{
    echo "Failure: $@"
    exit 1
}


[[ $# -ne 4 ]] && fail "Usage: $0 cognosroot cognosinst cognosextdns mediabucket"

installroot="/shared/install"
apppath=cognos
installhome="$installroot/$apppath/linuxi38664h"

cognosroot=$1
cognosinst=$2
cognosextdns=$3
mediabucket=$4


mpt=$(df $installroot | awk 'NR==2 {print $NF}')
[[ "$mpt" != "$installroot" ]] && fail "$installroot is not mounted"


if [[ ! -d $installhome ]] ; then
    mkdir -p $installroot/$apppath
    cd $installroot/$apppath
    aws s3 cp --recursive $mediabucket/Cognos/linux .
    tar xvfz bi_svr_10.2.1_l86_ml.tar.gz
fi

cd $installhome || fail "Cannot cd $installhome"

sed -e "s/I Agree=n/I Agree=y/ ; s-APPDIR=-APPDIR=${cognosinst}-" response.ats > /tmp/response.ats
sucog "cd $installhome ; ./issetup -s /tmp/response.ats"

### No external address set in hiera, so work out the internal address
if [[ "$cognosextdns" == "dummyvalue" ]] ; then
    cognosextdns="${CLOUDSERVERTYPE}-${CLOUDINSTANCE}.${CLOUDENV,,}.${CLOUDCLIENT,,}.${CLOUDDOMAIN}"
fi

sed -e "s/MYFQDN/$cognosextdns/g;
        s/MYDB/${CLOUDCLIENT}CM${CLOUDENV}/" \
    $cognosroot/cogstartup.xml.template > $cognosinst/configuration/cogstartup.xml

#fphome=$installhome/../bifixpack12/linuxi38664h
#cd $fphome || fail "Cannot cd $fphome to install fixpack"
#
#sucog "cd $fphome; ./issetup -s /tmp/response.ats"


