#!/usr/bin/env bash
#
# If Cognos is timing out, it will show in the httpd error log
# This probably needs fixing, but as it happens very infrequently, an effective
# resolution is simply stop and restart the Cognos service
#
# This script finds the date of the last time out error, and the date of the
# Cognos service starting.
#
# If the last error is newer than the service *and* less than 15 minutes old (to give the
# service a chance to start up) it will restart the service.
#
# NB: This could potentially cause constant restarting of the service, so put a
# very obvious message in /var/log/messages for monitoring (or a human troubleshooter)
# to pick up/report on
#

tmpf=$(mktemp)


logit()
{
        echo "$@"
        logger -p user.error "$@"
}

### Find the latest occurrence of the time out message
cat $(ls -rt /var/log/httpd/*error.log*) | awk -F[ '/timed out before returning headers/ {print $2}' | sed -e 's/] $//' | tail -1 > $tmpf
lasterrdate=$(cat $tmpf)

### Find the start time of the cognos bootstrap process
sdate=$(ps -eo stime,args | awk '!/awk/ && /cogbootstrap/ {print $1}')

### No timeouts found, so just exit quietly
[[ -z "$lasterrdate" ]] && exit


lasterrtime=$(date -d "$lasterrdate")
lasterrtimesecs=$(date -d "$lasterrdate" +%s)

stime=$(date -d "$sdate")
stimesecs=$(date -d "$sdate" +%s)


diff=$(( $lasterrtimesecs - $stimesecs ))


if [[ $diff -gt 900 ]] ; then

        myname=$(pwd)/$(basename $0)

        logit "Cognos timeouts recorded in httpd error log.  The latest timeout was at $lasterrtime"
        logit "Restarting Cognos services.  If this repeats quickly, please disable $myname in cron and investigate fully."

        /etc/init.d/cognos stop
        sleep 5
        /etc/init.d/cognos start

fi


rm $tmpf

