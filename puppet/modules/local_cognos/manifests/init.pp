class local_cognos (
  $cognosroot    = '/opt/ibm/cognos',
  $cognosinst    = '/opt/ibm/cognos/c10_64',
  $maxdbuser     = 'db2admin',
  $maxdbpassword = '%602;wall;NORTHERN;plants;7%',
  $maxdbserver   = 'db2',
  $cognosextdns  = 'dummyvalue',
  $installawstats = 'yes',
  #$mediabucket    = hiera('common::mediabucketname'),
  ### Temporary hack
  $mediabucket    = 's3://myco-cloud-media',
)
  {

    include local_cognos::template_update

    ### Installation Manager uses lowercase, others use uppercase as default
    file { '/opt/ibm':
      ensure => link,
      target => '/opt/IBM',
    } ->

      file { "$cognosroot":
        ensure  => directory,
        owner   => cognos,
        group   => cognos,
        require => File ['/opt/IBM'],
      }

    ### Prerequisites (NB: ODBC might not be needed, but is harmless)
    ensure_packages(['motif', 'motif.i686', 'unixODBC', ],)

    ### Cognos user environment setup
    file { '/home/cognos/.bashrc':
      owner   => root,
      mode    => 0755,
      content => "#!/bin/bash
# Source global definitions
if [ -f /etc/bashrc ]; then
    . /etc/bashrc
fi

COGNOS=$cognosinst
export LD_LIBRARY_PATH=\$COGNOS/bin64:/opt/IBM/db2/V10.5/lib32
export PATH=\$PATH:\$COGNOS/bin64

"
    }


    ### Base install

    file { '/usr/local/bin/cognos_bi_installer.sh':
      ensure => present,
      mode   => 0755,
      source => 'puppet:///modules/local_cognos/cognos_bi_installer.sh',
    }
    exec { 'Install Cognos BI Server':
      path    => '/usr/bin:/usr/sbin',
      creates => "$cognosinst",
      command => "/usr/local/bin/cognos_bi_installer.sh $cognosroot $cognosinst $cognosextdns $mediabucket",
      require => [ File['/usr/local/bin/cognos_bi_installer.sh'], File["$cognosroot/cogstartup.xml.template"], Exec[
        'Install JRE 7.0.7'], ],
    }
    file { "$cognosroot/cogstartup.xml.template":
      ensure => present,
      source => 'puppet:///modules/local_cognos/cogstartup.xml.template',
    }

    ### Tried linking, but Tomcat was unhappy with this, so copy
    # exec {'Copy DB2 JDBC files':
    #   path => '/usr/bin:/bin',
    #   creates => "$cognosinst/webapps/p2pd/WEB-INF/lib/db2jcc.jar",
    #   command => "cd /opt/ctginst1/sqllib/java; cp db2jcc.jar db2jcc_license_cu.jar $cognosinst/webapps/p2pd/WEB-INF/lib/",
    # }

    ### Maximo integration - use these jar files instead, they are more recent
    file { "$cognosinst/webapps/p2pd/WEB-INF/lib/db2jcc.jar":
      ensure  => present,
      source  => 'puppet:///modules/local_cognos/db2jcc.jar_1.6.0',
      mode    => 0775,
      owner   => cognos,
      group   => cognos,
      require => Exec['Install Cognos BI Server'],
    }
    file { "$cognosinst/webapps/p2pd/WEB-INF/lib/db2jcc_license_cu.jar":
      ensure  => present,
      source  => 'puppet:///modules/local_cognos/db2jcc_license_cu.jar_1.6.0',
      mode    => 0775,
      owner   => cognos,
      group   => cognos,
      require => Exec['Install Cognos BI Server'],
    }
    file { "$cognosinst/webapps/p2pd/WEB-INF/lib/CAM_AAA_MXCSP.jar":
      ensure  => present,
      source  => 'puppet:///modules/local_cognos/CAM_AAA_MXCSP.jar',
      mode    => 0775,
      owner   => cognos,
      group   => cognos,
      require => Exec['Install Cognos BI Server'],
    }

    ### Maximo integration
    file { "$cognosinst/configuration/mxcognosdatasources.properties":
      ensure  => present,
      mode    => 0664,
      owner   => cognos,
      group   => cognos,
      content => "
maximoDataSource.url=jdbc:db2://${maxdbserver}:50005/${cloudclient}MX${cloudenv}
maximoDataSource.driver=com.ibm.db2.jcc.DB2Driver
maximoDataSource.username=$maxdbuser
maximoDataSource.password=$maxdbpassword
maximoDataSource.schemaowner=MAXIMO

",
      require => Exec['Install Cognos BI Server'],
    }


    file { ['/home/cognos/projects', '/home/cognos',]:
      ensure => directory,
      owner  => cognos,
      group  => cognos,
      mode   => 0775,
    }

    ### Oracle Java 7.0.7 JRE
    file { '/root/jre-7u79-linux-x64.rpm':
      source => 'puppet:///modules/local_cognos/jre-7u79-linux-x64.rpm'
    }
    exec { 'Install JRE 7.0.7':
      path    => '/usr/bin:/usr/sbin',
      command => 'rpm -i /root/jre-7u79-linux-x64.rpm',
      require => File['/root/jre-7u79-linux-x64.rpm'],
      creates => '/usr/java/jre1.7.0_79',
    }


    ### Script to deal with occasional timeouts (where the service needs restarting)
    file { '/usr/local/bin/cognos_bi_timeout_check.sh':
      ensure => present,
      mode   => 0755,
      source => 'puppet:///modules/local_cognos/cognos_bi_timeout_check.sh',
    }
    ### Link in /etc/cron.hourly (old - so remove)
    file {'/etc/cron.hourly/cognos_bi_timeout.sh':
      ensure => absent,
    }
    ### Run every fifteen minutes
    cron {'Check for Cognos timeouts':
      command => "/usr/local/bin/cognos_bi_timeout_check.sh",
      user    => 'root',
      minute  => [10,25,40,55],
    }


    ### Web server configuration

    ## Images directory - also see apache configuration below
    $imagedir="$cognosinst/webcontent/shared_images"
    file {"$imagedir":
      ensure => directory,
      owner  => cognos,
      group  => apache,
      mode => 0775,
    }

    class { 'apache': }
    apache::vhost { [ "${cloudservertype}-${cloudinstance}.${cloudenv}.${cloudclient}.${clouddomain}",
      "$cognosextdns", ]:
      port    => 80,
      docroot => "$cognosinst/webcontent",
      access_log_format => 'forwarded',
      aliases => [
        {
          scriptalias => '/ibmcognos/cgi-bin',
          path        => "$cognosinst/cgi-bin",
        },
        {
          alias => '/ibmcognos',
          path  => "$cognosinst/webcontent",
        },
      ],
      directories => [
        {
          path => "$imagedir",
          dav  => 'on',
          options => ['Indexes', 'Multiviews',],
          order => 'allow, deny',
          allow => 'from all',
        }
      ],
      require => Exec['Install Cognos BI Server'],
    }


    ### Make the maximo database server name available to the db2 install script
    file{'/usr/local/etc/db2cognos.cfg':
      ensure => present,
      owner => root,
      mode => 0755,
      content => template("local_cognos/db2cognos.cfg.erb"),
    }


    ### Start/stop script + service

    file{'/etc/init.d/cognos':
      ensure => present,
      owner => root,
      mode => 0755,
      content => template("local_cognos/cognos.erb"),
      require => Exec['Install Cognos BI Server'],
    } ->
    exec {'Create and configure cognos service':
      path => '/usr/bin:/usr/sbin',
      command => 'chkconfig --add cognos; systemctl enable cognos; systemctl start cognos',
      unless => 'systemctl | grep -q cognos',
    } ->
    service {'cognos': ensure => 'running'}

    ### awstats
    if $installawstats == 'yes' {
      $loadplugin     = ['decodeutfkeys', 'geoip GEOIP_STANDARD /usr/share/GeoIP/GeoIP.dat']
      $internal_hosts = 'REGEX[^192\.168\.] REGEX[^172\.16\.] REGEX[^10\.]'

      apache::vhost { ["$fqdn","localhost","$hostname",]:
        port          => '81',
        docroot       => '/usr/share/awstats/wwwroot',
        serveraliases => ['awstats'],
        aliases => [
          { alias => '/awstatsclasses', path => '/usr/share/awstats/wwwroot/classes/' },
          { alias => '/awstatscss', path => '/usr/share/awstats/wwwroot/css/' },
          { alias => '/awstatsicons', path => '/usr/share/awstats/wwwroot/icon/' },
        ],
        scriptaliases => [
          { alias => '/awstats/', path => '/usr/share/awstats/wwwroot/cgi-bin/' },
        ],
        directories   => [{
          path     => '/usr/share/awstats/wwwroot',
          provider => 'directory',
          options  => 'None',
          order    => 'Allow,Deny',
          allow    => 'from all',
          #deny    => 'from all',
        }],
        setenv        => ['PERL5LIB /usr/share/awstats/lib:/usr/share/awstats/plugins'],
      }

      class { '::awstats':
        config_dir_purge => true,
        enable_plugins   => [ 'DecodeUTFKeys', 'GeoIP' ],
      }

      # this ordering is needed for both the docroot path and so that the
      # awstats package provided apache configuration snippet is purged on the
      # first run
      Class['::awstats'] -> Class['::apache']

      # apache log
      awstats::conf { "$fqdn":
        options => {
          'SiteDomain'        => "${cloudservertype}-${cloudinstance}.${cloudenv}.${cloudclient}.${clouddomain}",
          'LogFile'           => "cat /var/log/httpd/${cloudservertype}-${cloudinstance}.${cloudenv}.${cloudclient}.${clouddomain}_access.log* |",
          'AllowFullYearView' => 3,
          'DNSLookup'         => 1,
          'LogFormat'         => '%other %host %other %time1 %methodurl %code %bytesd %otherquot %uaquot',
          #'SkipHosts'         => $internal_hosts,
          'LoadPlugin'        => $loadplugin,
        },
      }

      ### Update every hour
      cron {'Update awstats':
        command => "/usr/share/awstats/wwwroot/cgi-bin/awstats.pl -config=$fqdn -update",
        user    => 'root',
        hour    => '*',
        minute  => '50',
      }

    }

  }
