
class local_cognos::template_update (
  $resdir='/opt/ibm/cognos/c10_64/webcontent/pat/res'
) {

  file {'/usr/local/bin/cognos_template_update.sh':
    content => template("local_cognos/cognos_template_update.sh.erb"),
    mode => 0755,
    owner => root,
  } ->
  exec {'Update Cognos templates.xml':
    path => '/sbin:/bin:/usr/sbin:/usr/bin',
    command => '/usr/local/bin/cognos_template_update.sh',
    creates => "$resdir/templates.xml.backup",
  }

}
