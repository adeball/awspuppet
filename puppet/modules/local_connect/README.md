# local_connect

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_connect]](#setup)
    * [What [local_connect] affects](#what-[local_connect]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_connect]](#beginning-with-[local_connect])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Performs the minimal configuration required for the NodeJS 'connect' application. 

## Setup

### Beginning with [local_connect]	

Requires: local-nodejs
include local_connect

## Usage

## Reference

* Creates the 'connect' user and group.
* Sets up the SSH key for that user to connect to the MYCO repository
* Sets up the firewall rules required (TCP 8181 & 8443)


## Limitations

## Development

