
### NodeJS "Connect" application.  Contact: Jade Beesley

class local_connect {

  $sshprivatekey = hiera('reporokey')

  group { 'connect':    ensure => present, gid => 4001 }
  user { 'connect':  ensure => present, uid => 4001, gid => connect, home => '/home/connect', managehome => true, } ->
  file { '/home/connect/.ssh': ensure => directory, owner => connect, mode => 0700} ->
  file {'/home/connect/.ssh/id_rsa':
    ensure => present,
    owner => connect,
    mode => 0600,
    content => "$sshprivatekey",
  }

  $key=hiera('local_jenkinsslave::sshpublickey')
  ssh_authorized_key { "connect@jenkins": user => "connect", type => 'ssh-rsa', key => $key }

  firewall { '8181 Allow inbound BRAID Connect':
    dport    => 8181,
    proto    => tcp,
    action   => accept,
  }

  firewall { '8443 Allow inbound OHub test':
    dport    => 8443,
    proto    => tcp,
    action   => accept,
  }

}
