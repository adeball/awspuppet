# local_couchdb

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_couchdb]](#setup)
    * [What [local_couchdb] affects](#what-[local_couchdb]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_couchdb]](#beginning-with-[local_couchdb])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the Apache CouchDB application

There are many package dependencies which must be met before CouchDB will successfully install.  This module ensure that these are met, sets up the couchdb user account and group, configures the required firewall rules, and also configures CouchDB as a service.

## Setup

### Setup Requirements

* A /home filesystem
* Preferable SELinux is in permissive mode (or disabled)

### Beginning with [local_couchdb]	

include local_couchdb

## Usage

## Reference

* Ensures the multiple documented dependency packages are installed: 'autoconf', 'autoconf-archive', 'automake', 'curl-devel', 'erlang-asn1', 'erlang-erts', 'erlang-eunit', 'erlang-os_mon', 'erlang-xmerl', 'help2man', 'js-devel-1.8.5', 'libicu-devel', 'libtool', 'perl-Test-Harness'
* Ensures that the *undocumented* dependency packages are also installed: 'erlang-erl_interface','erlang-reltool','gcc-c++'
* Installs couchdb user & group
* Copies the install script /root/install_couchdb.sh, and executes it
* Configures the local.ini file for basic operation
* Configures and starts the couchdb service
* Configures access to ports 5984 & 6984 in iptables (firewall)

## Limitations

## Development

