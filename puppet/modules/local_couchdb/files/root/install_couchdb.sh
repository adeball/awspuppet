#!/bin/bash

. /etc/profile

fail()
{
    echo >&2
    echo "Failed: $@" >&2
    echo >&2
    exit 1

}


### No idea why the installer breaks if HOME is not set (which is the case when running through the Puppet agent),
### but it does, so set it...
export HOME=/root

cd

### NB: couchdb is now in repositories for RHEL etc, so it may be easiest to rewrite this in future.

### Original source download, as per instructions - 2.0.0 no longer available at this location
#curl -sL http://mirror.ox.ac.uk/sites/rsync.apache.org/couchdb/source/2.0.0/apache-couchdb-2.0.0.tar.gz | tar xvfz -
#cd apache-couchdb-2.0.0

### ... so clone from github instead, and switch branch to the version we currently use
git clone https://github.com/apache/couchdb.git
cd couchdb
git checkout 2.0.x || fail "Cannot switch to 2.0.x branch"

### Pointless, as it doesn't work - but may do in future.
pip install -U Sphinx


./configure

### Total hack, sphinx-build does not want to work, so create empty files to satisfy deps, as we can live without
### this stuff.
mkdir -p src/docs/build/latex  ; touch src/docs/build/latex/CouchDB.pdf
mkdir -p src/docs/build/man    ; touch src/docs/build/man/apachecouchdb.1
mkdir -p src/docs/build/texinfo; touch src/docs/build/texinfo/CouchDB.info
mkdir -p src/docs/build/html   ;


make release || fail "Make failed"
cd rel/couchdb || fail "Cannot cd rel/couchdb"
cd ..
find couchdb | cpio -pdumv /home/couchdb
mv /home/couchdb/couchdb/etc/local.ini /home/couchdb/couchdb/etc/local.ini.save

chown -R couchdb:couchdb /home/couchdb/couchdb
find /home/couchdb/couchdb -type d -exec chmod 0770 {} \;
chmod 0644 /home/couchdb/couchdb/etc/*

cat > /etc/init.d/couchdb << EOF
#!/bin/bash
#
# Very basic stop/start script for couchdb v2
#
# chkconfig: 2345 99 15
# description: Apache CouchDB
# config: /home/couchdb/couchdb/etc/local.ini
#

case \$1 in
    start)
        echo "Starting CouchDB..."
        sudo -i -u couchdb couchdb/bin/couchdb > /var/log/couchdb.log 2>&1 &
        ;;
    stop)
        # This is flaky...
        echo "Stopping CouchDB..."
        ps -ef | awk '/couchdb\/bin\/couchdb/ && !/awk/ {print \$2}' | xargs kill
        pkill epmd
        ;;
 esac

EOF

chmod a+x /etc/init.d/couchdb
chkconfig --add couchdb
chkconfig couchdb on

