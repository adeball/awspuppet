
### Ref: http://docs.couchdb.org/en/2.0.0/install/unix.html

class local_couchdb {

  ### Documented dependencies (http://docs.couchdb.org/en/2.0.0/install/unix.html)
  ensure_packages(['autoconf', 'autoconf-archive', 'automake', 'curl-devel', 'erlang-asn1', 'erlang-erts', 'erlang-eunit',
    'erlang-os_mon', 'erlang-xmerl', 'help2man', 'js-devel-1.8.5', 'libicu-devel', 'libtool', 'perl-Test-Harness'])

  ### Undocumented dependencies >:-(
  ensure_packages(['erlang-erl_interface','erlang-reltool','gcc-c++', 'npm', 'python-pip', ])

  group {'couchdb': ensure => present, gid => 4002, } ->
  user { 'couchdb':  ensure => present, uid => 4002, gid => couchdb, home => '/home/couchdb', managehome => true, } ->
  file { '/root/install_couchdb.sh':
    ensure => present,
    source => 'puppet:///modules/local_couchdb/root/install_couchdb.sh',
    mode => 0755,
  } ->
  exec {'Install CouchDB':
    command => '/root/install_couchdb.sh',
    path => '/bin:/usr/bin:/usr/sbin:/usr/local/bin',
    creates => '/etc/init.d/couchdb',
    subscribe => Package['erlang-reltool'],
    timeout => '1200',
  } ->
  file {'/home/couchdb/couchdb/etc/local.ini':
    ensure => present,
    owner => couchdb,
    group => couchdb,
    replace => no,
    source => 'puppet:///modules/local_couchdb/home/couchdb/couchdb/etc/local.ini',
  } ->
  service {'couchdb': ensure => running }

  ### Sure this should be covered by 'manage_home', but it was owned by root, so no history etc
  file {'/home/couchdb': ensure => directory, owner => couchdb }


  file {'/etc/rc3.d/S99couchdb': ensure => link, target => '/etc/init.d/couchdb'}
  file {'/etc/rc2.d/K01couchdb': ensure => link, target => '/etc/init.d/couchdb'}


  firewall { '5984 Allow inbound CouchDB':
    dport    => [5984, 6984],
    proto    => tcp,
    action   => accept,
  }

}
