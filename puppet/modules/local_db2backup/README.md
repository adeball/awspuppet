# local_db2backup

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_db2backup]](#setup)
    * [What [local_db2backup] affects](#what-[local_db2backup]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_db2backup]](#beginning-with-[local_db2backup])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module performs a handful of functions, the primary one being to schedule online backups of DB2 databases to online storage (with the expectation that another process will secure the backup to offline storage).
       
Secondary functions are:

* Provides a script (and schedule) to check that the most recent online backup is no more than a certain number of hours old (default 48 for warning and 168 for critical).
* Incorporates a script (and schedule) to execute db2 exports (which works for databases without transaction logging, and also is importable to different platforms)
* Incorporates a script (and schedule) to prune archive logs

Documentation: https://confluence.myco-group.com/display/EC/DB2+backups

## Setup

### What [local_db2backup] affects

* Scripts are created in /usr/local/bin
* Some are linked in /etc/cron.* (e.g. daily), others set up explicitly in cron

### Setup Requirements

Just executing the backup script is not sufficient to secure a backup for disaster recovery purposes (they are held online, on the same server).  The expectation is that the files saved will be secured offline by another process - this was written with local-s3backup in mind, but another process will work just as well.

### Beginning with [local_db2backup]	

Assuming a standard db2 server build, with /data/vol03(/backup) available and writable by the instance owner, all that is needed is to 'include local_db2backup'.

## Usage

The scripts can be run manually:

db2backup.sh : Run an online backup, if applicable
db2_backupcheck.sh: Check the date of the latest backup, log via syslog if not in range
db2export.sh: Run a DB2 export, and (optionally) upload to S3
db2prunearchivelogs.sh: Remove archived transaction logs over 90 (default) days old

## Reference

### Parameters

Shown with default values
* $retention = 3 : Number of days online backups to retain
* $location  = '/data/vol03/backup' : Where to backup online copies to
* $schedule  = 'daily' : 'daily' or 'weekly'
* $online    = 'true' : 'true' or 'false' - not much point if false TBH
* $emsawsid  = '40403ac262f9120d910d7dd0c76883799d1b656c085f5fc213fc34e6570d369e' : required for S3 access from the MYCO account (used for the exports -> docker)
* $retainarclogs = 90 : Number of days to retain archive logs when pruning

## Limitations

## Development

## Release Notes/Contributors/Etc.

The db2export.sh script was contributed by Istvan Debrody.
