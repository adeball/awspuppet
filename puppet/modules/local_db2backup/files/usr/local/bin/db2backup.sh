#!/bin/bash
#
# db2backup.sh
#

fail()
{

	echo
	echo "Failure: $@"
	echo
	logger -p local0.info "Error: backup failed: $@"
	exit 1
}


### db2 command run under sudo
sudb2()
{
	echo "$@" | sudo -i -u db2admin db2	
}



### Set up environment
. /etc/profile

### CFG file sets location and retention
CFG=/usr/local/etc/db2backup.cfg
[[ -f $CFG ]] && . $CFG || fail "Cannot read $CFG"

[[ -d $location ]] || fail "$location does not exist, or is not a directory"
[[ -w $location ]] || fail "$location is not writeable"


### Obtain a list of databases (not remote connections)
dbs=$(sudb2 list database directory | awk '
	/Database name/ 	{dbname=$NF}
	/Directory entry type/	{if ($NF=="Indirect") { print dbname }}
')

echo
echo "------------"
echo
echo "Backing up database(s): $dbs"
echo "To directory: $location"
echo "Retaining $retention backups on disk when completed"
echo "Online: $online"
echo "NB: S3 offline copies will be available in $AWS_BACKUP/*${location}.tar.gz"
echo "(Where '*' = latest, daily/yyyy-mm-dd, weekly/yyyy-mm-dd etc)"
echo
echo "------------"
echo

for dbname in $dbs ; do

    ### Check if this is a Standby HADR database (backup not needed)
    status=$(sudb2 get snapshot for database on $dbname | awk '/Database status/ {print $NF}')

    if [[ "$status" == "Standby" ]] ; then
        echo
        echo "$dbname is an HADR Standby database, not backing up"
        echo
    else

        echo
        echo "Backing up $dbname"
        echo

        if [[ "$online" != "true" ]] ; then

            echo
            echo "Performing offline backup"
            echo
            sudb2 backup database $dbname to $location \
                with 2 buffers buffer 1024 parallelism 1 compress \
                without prompting || (
                    echo
                    echo
                    sudb2 list application
                    echo
                    fail "Cannot perform an offline backup whilst applications are connected."
                )

        else

            echo
            echo "Performing online backup"
            echo
            sudb2 backup database $dbname online to $location \
                with 2 buffers buffer 1024 parallelism 1 compress \
                include logs without prompting || fail "Did not backup successfully"
        fi

        cd $location
        rm -f $(ls $dbname* | head -n -${retention})

        ### And copy the most recent to the S3 server's latest directory top-level
        latest=$(ls $dbname* | tail -1)
        aws s3 cp $latest $AWS_BACKUP/latest/${dbname}.backup

        mount | grep -q "/shared/tmp"
        if [[ $? -eq 0 ]] ; then
            mkdir -p "/shared/tmp/db2backups"
            cp $location/$latest /shared/tmp/db2backups
            chmod 644 /shared/tmp/db2backups/$latest
            cd /shared/tmp/db2backups || fail "Cannot cd /shared/tmp/db2backups"
            rm -f $(ls $dbname* | head -n -1)
        fi

    fi

done
