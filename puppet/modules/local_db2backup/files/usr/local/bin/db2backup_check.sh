#!/bin/bash
#
# db2backup_check.sh
#

WARN=48
PANIC=168

fail()
{
    echo
    echo "Failure: $@"
    echo
    logger -p user.error "DB2 Backup check critical: $@"
    exit 1
}

warn()
{
    echo
    echo "Warning: $@"
    echo
    logger -p user.warning "DB2 Backup check warning: $@"
    exit 1
}

### db2 command run under sudo
sudb2()
{
	echo "$@" | sudo -i -u db2admin db2
}


### Set up environment
. /etc/profile

### CFG file sets location and retention
CFG=/usr/local/etc/db2backup.cfg
[[ -f $CFG ]] && . $CFG || fail "Cannot read $CFG"

[[ -d $location ]] || fail "$location does not exist, or is not a directory"



##############
############## Ultimately, we may have DB2 backups of non-prod
############## for now, they won't run (see confluence docs)
############## So only check production systems
############## (If non-prod does get backed up, use the schedule (weekly/daily) to adjust the
##############  warning and panic thresholds)
##############

[[ "$CLOUDENV" != "PRD" ]] && exit






### Obtain a list of databases (not remote connections)
dbs=$(sudb2 list database directory | awk '
	/Database name/ 	{dbname=$NF}
	/Directory entry type/	{if ($NF=="Indirect") { print dbname }}
')


cd $location || fail "Cannot cd $location"
nowstamp=$(date --utc +%s)

for db in $dbs ; do

    ### Check if this is a Standby HADR database (backup not needed)
    status=$(sudb2 get snapshot for database on $db | awk '/Database status/ {print $NF}')

    if [[ "$status" != "Standby" ]] ; then
        latest=$(ls -t ${db}*|head -1)
        latestdate=$(stat $latest | awk '/^Modify/ {print $2,$3}')
        lateststamp=$(date --utc --date "$latestdate" +%s)

        hoursdiff=$(( ( $nowstamp - $lateststamp ) / 3600 ))

        if [[ $hoursdiff -gt $PANIC ]] ; then
            fail "Latest backup is > $PANIC hours old"
        elif [[ $hoursdiff -gt $WARN ]] ; then
            warn "Latest backup is > $WARN hours old"
        fi
    fi

done

