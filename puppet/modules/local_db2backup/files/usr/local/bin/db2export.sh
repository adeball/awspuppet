#!/bin/bash

. /etc/profile

### db2 commands run under sudo, so the script can be run as root
sudb2()
{
  echo "$@" | sudo -i -u ${INSTOWNER} db2
}
sudb2move()
{
  echo "cd ${EXPDIR}; db2move $@" | sudo -i -u ${INSTOWNER}
}
sudb2look()
{
  echo "cd ${EXPDIR}; db2look $@" | sudo -i -u ${INSTOWNER}
}


usage() {
    cat <<EOM
    Usage:
    	$(basename $0) [params]
    Description:
    	The script exports the maximo schema using db2move and it also exports the schema ddl zips it up
	and optionally uploads it to S3
    Parameters:
	-d|--dbname <DBNAME>: 
		you can use this paramter to specify the dbname the default is 
		specified using the CLOUDCLIENT|CLOUDENV env variables.
		if you use this parameter you will have to add an 
		output file (-o) and an s3path (-s) if you want to upload
		In this environment: ${CLOUDCLIENT}MX${CLOUDENV}
	-o|--output <Zip File Name>:
		the name of the zip file the process should generate, default is
		specified using the CLOUDCLIENT|CLOUDENV env variables.
		In this environment: ${CLOUDCLIENT,,}-${CLOUDENV,,}-export.zip
	-s|--s3path <S3 Path>:
		the s3 path for the aws bucket where this file should be uploaded
		it requires the --upload parameter to be declared otherwise it won't 
		do the upload.
		It expects a directory and will add a trailing slash if it is missing.
		Default is bases on the CLOUDCLIENT env variable
		In this environemnt: s3://myco-cloud-media/downloads/clients/${CLOUDCLIENT,,}/latest_db_export/
	-t|--targetdb <DB Name>:
		this string is used to change the dbname within the exported files
		Default value is maximo
	-w|--workdir <PATH>:
		change the default work dir where the temporary db2export directory will be generated and
		where the zip file will be created
	--upload:
		tells the script to upload to the generated zip file to the S3PATH
		if the parameter is not defined it won't upload it.
	-h|--help:
		display this message
    Examples:
	To export db MAXDEMO, target zip file maxdemo.zip and upload it to s3://example-bucket/demo/max/
	./$(basename $0) -d MAXDEMO -o maxdemo.zip -s s3://example-bucket/demo/max/ --upload 
EOM
    exit 0
}

### CFG file sets location and retention
CFG=/usr/local/etc/db2backup.cfg
[[ -f $CFG ]] && . $CFG || fail "Cannot read $CFG"

# You need to run this batch as the db2 instance owner or a user that can export the database
# the script assumes that the db2profile has been initialised
WORKDIR=$location/..
DBNAME=${CLOUDCLIENT}MX${CLOUDENV}
DBSCHEMA=maximo
TARGETDBNAME=MAXIMO
FILEPREFIX=maximo
INSTOWNER=ctginst1
ZIPFILENAME=${CLOUDCLIENT,,}-${CLOUDENV,,}-export.zip
S3PATH=s3://myco-cloud-media/downloads/clients/${CLOUDCLIENT,,}/latest_db_export/
UPLOAD=false
userDefDB=false
userDefZip=false
userDefS3=false
# check if any parameters have been declared and will set the parameters accordingly
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -d|--dbname)
    DBNAME="$2"
    userDefDB=true
    shift # past argument
    ;;
    -s|--s3path)
    S3PATH="$2"
    userDefS3=true
    shift # past argument
    ;;
    -w|--workdir)
    WORKDIR="$2"
    shift # past argument
    ;;
    -o|--output)
    ZIPFILENAME="$2"
    userDefZip=true
    shift # past argument
    ;;
    -t|--targetdb)
    TARGETDBNAME="$2"
    shift # past argument
    ;;
    -h|--help)
    #print usage:
	usage
	exit 0
    shift # past argument
    ;;
    --upload)
    UPLOAD=true
    ;;
    *)
            # unknown option
	echo "[ERROR]: Unknown parameter: $key"
	usage
	exit 1
    ;;
esac
shift # past argument or value
done

# if dbname is user defined, need to check if the other parameters are declared
if [ "$userDefDB" = true ] && ( [ "$userDefZip" = false ] || [ "$userDefZip" = true ] && [ "$userDefS3" = false ] ); then
	echo "[ERROR]: When DB Name (-d|--dbname) is defined you need to define an output file (-o) and an S3 Path (-s) in case of upload"
	usage
	exit 1
fi
# if the CLOUDENV or CLOUDCLIENT environment variable doesn't exists we need a dbname to be defined
if ( [ "$CLOUDENV" = "" ] || [ "$CLOUDCLIENT" = "" ] ) && [ "$userDefDB" = false ]; then
	echo "[ERROR]: Either the CLOUDENV or the CLOUDCLIENT env variable does not exists, you need to define a dbname (-d) for this process to work"
	usage
	exit 1
fi
# db2export will be the directory to be used for the export
# delete the previous directory if exists using hardcoded values deliberately so we don't delete anything else by accident
if [ -d $WORKDIR/db2export ]; then 
	echo Removing directory $WORKDIR/db2export ...
	/bin/rm -r $WORKDIR/db2export
fi

EXPDIR=$WORKDIR/db2export
if [ "${S3PATH: -1}" != "/" ] ; then
	S3PATH="${S3PATH}/"
	echo "[WARN]: Added the trailing slash to the S3PATH: $S3PATH"
fi
#Check if this db is not a standby database - if it is the script will exit without error.
status=$(sudb2 get snapshot for database on $DBNAME | awk '/Database status/ {print $NF}')
if [ "$status" = "Standby" ]; then
	echo "[WARN]: $DBNAME is a standby database; please run the command on the primary active database"
	exit 0;
fi
echo Creating ${EXPDIR} directory
mkdir -p ${EXPDIR} ; chown ${INSTOWNER} ${EXPDIR}
currDir=`pwd`
cd ${EXPDIR}
echo Generating the export files
# set -e to exit if there is an error with any of the lines from now on
set -e
sudb2move $DBNAME export -sn maximo -l lob -aw -sn maximo && sudb2look -d $DBNAME -e -z $DBSCHEMA \
    -o ${EXPDIR}/$FILEPREFIX.ddl && sudb2look -d $DBNAME -m -z $DBSCHEMA -o ${EXPDIR}/${FILEPREFIX}_stats.ddl

echo Replacing the db name connection in the ddls from $DBNAME to $TARGETDBNAME
sed -i "s|CONNECT TO $DBNAME|CONNECT TO $TARGETDBNAME|g" $EXPDIR/${FILEPREFIX}.ddl
sed -i "s|CONNECT TO $DBNAME|CONNECT TO $TARGETDBNAME|g" $EXPDIR/${FILEPREFIX}_stats.ddl

echo Zipping files into $WORKDIR/$ZIPFILENAME
/bin/rm -f $WORKDIR/$ZIPFILENAME
zip -r $WORKDIR/$ZIPFILENAME *
cd $currDir
if [ "$UPLOAD" = true ] ; then
	echo Uploading $WORKDIR/$ZIPFILENAME to $S3PATH
	echo "aws s3 cp $WORKDIR/$ZIPFILENAME $S3PATH --grants=full=id=$emsawsid" | sudo -i -u ${INSTOWNER}
fi
