#!/bin/bash
#
# Generic version of Istvan's db2export.sh script
#
# Simply exports all databases to the default backup location
# (allows import on a recovered host)
#

. /etc/profile

### db2 commands run under sudo, so the script can be run as root
sudb2()
{
  echo "$@" | sudo -i -u ${INSTOWNER} db2
}
sudb2move()
{
  echo "cd ${EXPDIR}; db2move $@" | sudo -i -u ${INSTOWNER}
}
sudb2look()
{
  echo "cd ${EXPDIR}; db2look $@" | sudo -i -u ${INSTOWNER}
}

### CFG file sets location and retention
CFG=/usr/local/etc/db2backup.cfg
[[ -f $CFG ]] && . $CFG || fail "Cannot read $CFG"

# You need to run this batch as the db2 instance owner or a user that can export the database
# the script assumes that the db2profile has been initialised
INSTOWNER=ctginst1

dbs=$(sudb2 list database directory | awk '
	/Database name/ 	{dbname=$NF}
	/Directory entry type/	{if ($NF=="Indirect") { print dbname }}
')


for DBNAME in $dbs ; do

    EXPDIR=$location/$DBNAME

    ### Check if this db is not a standby database - if it is the script will exit without error.
    status=$(sudb2 get snapshot for database on $DBNAME | awk '/Database status/ {print $NF}')
    if [ "$status" = "Standby" ]; then
        echo "[WARN]: $DBNAME is a standby database; please run the command on the primary active database"
        exit 0;
    fi

    echo Creating ${EXPDIR} directory
    mkdir -p ${EXPDIR} ; chown ${INSTOWNER} ${EXPDIR}
    currDir=`pwd`
    cd ${EXPDIR}
    echo Generating the export files
    # set -e to exit if there is an error with any of the lines from now on
    set -e
    sudb2move $DBNAME export -l lob -aw
    sudb2look -d $DBNAME -e -o ${EXPDIR}/$DBNAME.ddl
    sudb2look -d $DBNAME -m -o ${EXPDIR}/${DBNAME}_stats.ddl

done
