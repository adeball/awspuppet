#!/usr/bin/env bash
#
# This script is managed by puppet
# local_db2backup module
#

fail()
{
	echo
	echo "Failure: $@"
	echo
	logger -p local0.info "Error: $@"
	exit 1
}


### ctginst1 command run under sudo
suctginst1()
{
	echo "$@" | sudo -i -u ctginst1
}



### Set up environment
. /etc/profile

### CFG file sets retention period as $retainarclogs
CFG=/usr/local/etc/db2backup.cfg
[[ -f $CFG ]] && . $CFG || fail "Cannot read $CFG"


dbnames=$(suctginst1 db2 list database directory | awk '
        /Database name/ {dbname=$NF}
        /Directory entry type/  {if ($NF!="Remote") {print dbname}}
        ')

### Use temp file as DB2 queries behave weirdly when piped/embedded
### (works on the command line, but not in a script, go figure...)
tmpf=$(mktemp)

for dbname in $dbnames ; do

        suctginst1 "db2 connect to $dbname ; db2 get db config" > $tmpf
        archivelogloc=$(awk '/LOGARCHMETH1.*DISK:/ {split($NF,a,":") ; print a[2]}' $tmpf)

        if [[ -z "$archivelogloc" ]] ; then
                echo "Cannot determine archive log location for $dbname"
        else

            logf=/var/log/db2prunearchivelogs-${dbname}-$(date +%Y-%m-%d)
            exec > >(tee $logf)
            echo
            echo "$(date) : Pruning archive logs for $dbname"
            echo "Archive log location: $archivelogloc"
            echo
            cd $archivelogloc || fail "Cannot cd $archivelogloc"

            find . -type f -path *${dbname}* -name *LOG -mtime +$retainarclogs -ls -exec rm {} \;
            echo
        fi
done

rm $tmpf
