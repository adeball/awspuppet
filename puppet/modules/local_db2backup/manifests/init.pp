class local_db2backup (
  $retention = 3,
  $location  = '/data/vol03/backup',
  $schedule  = 'weekly',
  $online    = 'false',
  $emsawsid  = '40403ac262f9120d910d7dd0c76883799d1b656c085f5fc213fc34e6570d369e',
  $retainarclogs = 90,
  $runexport = 'no',
) {

  file { '/usr/local/bin/db2backup.sh':
    ensure => present,
    owner  => root,
    mode   => 0755,
    source => 'puppet:///modules/local_db2backup/usr/local/bin/db2backup.sh',
  }

  file { '/usr/local/bin/db2backup_check.sh':
    ensure => present,
    owner  => root,
    mode   => 0755,
    source => 'puppet:///modules/local_db2backup/usr/local/bin/db2backup_check.sh',
  }

  file { '/usr/local/bin/db2prunearchivelogs.sh':
    ensure => present,
    owner  => root,
    mode   => 0755,
    source => 'puppet:///modules/local_db2backup/usr/local/bin/db2prunearchivelogs.sh',
  }

  file { '/usr/local/etc/db2backup.cfg':
    ensure  => present,
    owner   => db2admin,
    mode    => 0644,
    content => template("local_db2backup/db2backup.cfg.erb"),
  }

  file { '/usr/local/bin/db2export.sh':
    ensure => present,
    owner  => root,
    mode   => 0755,
    source => 'puppet:///modules/local_db2backup/usr/local/bin/db2export.sh',
  }

  file { '/usr/local/bin/db2genericexport.sh':
    ensure => present,
    owner  => root,
    mode   => 0755,
    source => 'puppet:///modules/local_db2backup/usr/local/bin/db2genericexport.sh',
  }

  ### Schedule backups to run at 11pm each night (or Sunday night) - no data is transferred to S3
  ### until the s3backup job is executed early morning.
  cron { 'DB2 backup':
    command => '/usr/local/bin/db2backup.sh',
    user    => 'root',
    hour    => '23',
    minute  => '0',
    weekday => $schedule ? {
      "daily"   => '*',
      "weekly"  => '7',
    },
  }

  ### Schedule export to run at 11:30pm, needs switching on with $runexport='yes'
  if $runexport == 'yes' {
    cron { 'DB2 export':
      command => '/usr/local/bin/db2export.sh --upload',
      user    => 'root',
      hour    => '23',
      minute  => '30',
      weekday => '*',
    }
  }

  file {'/etc/cron.hourly/db2backup_check.sh':
    ensure => link,
    target => '/usr/local/bin/db2backup_check.sh',
  }

  file {'/etc/cron.monthly/db2prunearchivelogs.sh':
    ensure => link,
    target => '/usr/local/bin/db2prunearchivelogs.sh',
  }

}
