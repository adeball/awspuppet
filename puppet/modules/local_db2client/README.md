# local_db2client

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_db2client]](#setup)
    * [What [local_db2client] affects](#what-[local_db2client]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_db2client]](#beginning-with-[local_db2client])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the DB2 client software.

Performs a silent installation of the DB2 client, along with the common drivers it might need to connect.

## Setup

### Setup Requirements 

Requires the OS to be pre-configured to support the IBM software stack.  This may be achieved by loading the module local-ibmsupport.
This also requires the software to be available in s3://myco-cloud-media/IBM/DB2 10.5.8 (needs parameterising).

### Beginning with [local_db2client]	

include local_db2client

## Usage

## Reference

The installation script installs DB2 10.5.8 and the following drivers:
* jdbc_sqlj
* odbc_cli
* rtcl

## Limitations

RHEL only

## Development

