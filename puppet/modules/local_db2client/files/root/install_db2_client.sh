#!/bin/bash


fail()
{
    echo "Failed: $@"
    exit 1
}

pkgs="client|dsdriver|jdbc_sqlj|odbc_cli|rtcl"
installd=/shared/install/db2client
mkdir -p $installd
cd $installd || fail "Cannot cd to $installd"

s3loc="s3://myco-cloud-media/IBM/DB2 10.5.8"

aws s3 ls "$s3loc"/ | egrep "$pkgs" | awk '{print $NF}' | while read f ; do
	echo "Copying $f"
	aws s3 cp "$s3loc"/$f - | tar xfz -
done

### Install the client
### NB: install location is in the .rsp file.  Set to /opt/IBM/db2/V10.5
rm -rf /opt/db2inst1/sqllib
cd $installd/client
./db2setup -r ~/db2client.rsp

driverd=/opt/IBM/db2/V10.5/dsdriver
cd $installd/dsdriver
rsync -avx . $driverd
cd $installd
rsync -avx jdbc_sqlj $driverd
rsync -avx odbc_cli $driverd
rsync -avx rtcl $driverd
cd $driverd
./installDSDriver

