
class local_db2client {

  require local_ibmsupport

  file {'/root/install_db2_client.sh':
    ensure => present,
    mode => 0755,
    source => 'puppet:///modules/local_db2client/root/install_db2_client.sh',
  } ->

  file {'/root/db2client.rsp':
    ensure => present,
    mode => 0644,
    source => 'puppet:///modules/local_db2client/root/db2client.rsp',
  } ->

  exec {'Install DB2 client':
    creates => '/opt/IBM/db2',
    path => '/bin:/usr/bin',
    command => '/root/install_db2_client.sh',
    onlyif => 'mount | grep -q " /opt/IBM "',
  }

}
