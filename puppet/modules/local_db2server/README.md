# local_db2server

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_db2server]](#setup)
    * [What [local_db2server] affects](#what-[local_db2server]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_db2server]](#beginning-with-[local_db2server])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs and configures DB2 server software.

Essentially deploys and executes a DB2 installation script, which itself calls a second script designed to configured DB2 for a specific database (e.g. Maximo or Cognos).  This mechanism is intended to replace the code in servertype-db2.

## Setup

### Setup Requirements

* Requires access to the /shared/install NFS mount (see local-nfssharedclient), which must contain the DB2 software (see servertype-maximo).
* Requires IBM support (users, groups, filesystems, permissions etc) - see local-ibmsupport

### Beginning with [local_db2server]

include local_db2server

## Usage

## Reference

Currently this module is not parameterised (which needs doing), so uses environment variables to determine the environment and servertype, which then determines which DB2 setup is applied.

### Scripts

The module deploys (currently) four scripts:

* install_db2_server.sh: The main installation script, which installs a base DB2 system, sets up permissions, paths, users etc using a silent response file.  At the end of the script it optionally runs an additional script as the instance owner - this second script sets up database(s) as required by the servertype it is executed on (e.g. one for cognos, another for maximo).
* db2_cognos_config.sh: Sets up a Cognos database (called from install_db2_server.sh)
* db2_maximo_config.sh: Sets up a Maximo database (called from install_db2_server.sh)
* wipe_db2.sh: Not used by the module, provides a reliable way to completely remove DB2 from a system - useful when testing installation scripts.


## Limitations

* Use on RHEL (IBM supported OS)

## Development


