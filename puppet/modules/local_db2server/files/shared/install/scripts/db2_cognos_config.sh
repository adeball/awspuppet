#!/usr/bin/env bash
#
# db2_cognos_config.sh
#
# Based on https://confluence.myco-group.com/display/HD/Installing+DB2+on+Amazon+Linux
#

if [[ ${USER} != "ctginst1" ]] ; then
    echo
    echo "This script must be executed as the ctginst1 user"
    echo
fi

DBNAME=${CLOUDCLIENT}CM${CLOUDENV}
DBPATH=/data/vol01/db/${DBNAME}
ARCHPATH=/data/vol01/archive/${DBNAME}

cd

db2start
db2 update dbm config using SVCENAME 50005 DEFERRED

### Oracle compatibility mode - has to be set before creating the DB
#db2set DB2_COMPATIBILITY_VECTOR=ORA
#db2set DB2_DEFERRED_PREPARE_SEMANTICS=YES
#db2set DB2_WORKLOAD=COGNOS

db2set DB2AUTOSTART=YES

db2stop
db2set DB2COMM=tcpip
db2start

mkdir -p $DBPATH

db2 create db ${DBNAME} on ${DBPATH} ALIAS ${DBNAME} using codeset UTF-8 territory US pagesize 32 K

db2 CREATE DATABASE ${DBNAME} on $DBPATH ALIAS ${DBNAME} USING CODESET UTF-8 TERRITORY US;
db2 CHANGE DATABASE ${DBNAME} COMMENT WITH \'IBM Cognos Content Store\';
db2 CONNECT TO ${DBNAME};
db2 UPDATE DATABASE CONFIGURATION USING APPLHEAPSZ 1024 DEFERRED;
db2 UPDATE DATABASE CONFIGURATION USING LOCKTIMEOUT 240 DEFERRED;
db2 CONNECT RESET;

db2 CONNECT TO ${DBNAME};
db2 CREATE BUFFERPOOL ${DBNAME}_08KBP IMMEDIATE SIZE 1000 PAGESIZE 8K;
db2 CREATE BUFFERPOOL ${DBNAME}_32KBP IMMEDIATE SIZE 1000 PAGESIZE 32K;
db2 CONNECT RESET;

db2 CONNECT TO ${DBNAME};
db2 CREATE SYSTEM TEMPORARY TABLESPACE TSN_SYS_${DBNAME} IN DATABASE PARTITION GROUP IBMTEMPGROUP PAGESIZE 32K BUFFERPOOL ${DBNAME}_32KBP;
db2 CREATE USER TEMPORARY TABLESPACE TSN_USR_${DBNAME} IN DATABASE PARTITION GROUP IBMDEFAULTGROUP PAGESIZE 8K BUFFERPOOL ${DBNAME}_08KBP;
db2 CREATE REGULAR TABLESPACE TSN_REG_${DBNAME} IN DATABASE PARTITION GROUP IBMDEFAULTGROUP PAGESIZE 8K BUFFERPOOL ${DBNAME}_08KBP;
db2 CONNECT RESET;

db2 CONNECT TO ${DBNAME};
db2 CREATE SCHEMA db2COGNOS AUTHORIZATION cognos;
db2 COMMENT ON SCHEMA db2COGNOS IS \'IBM Cognos Content Store\';
db2 GRANT CREATETAB,BINDADD,CONNECT,IMPLICIT_SCHEMA ON DATABASE  TO USER cognos;
db2 GRANT CREATEIN,DROPIN,ALTERIN ON SCHEMA DB2COGNOS TO USER cognos WITH GRANT OPTION;
db2 GRANT USE OF TABLESPACE TSN_USR_${DBNAME} TO user cognos;
db2 GRANT USE OF TABLESPACE TSN_REG_${DBNAME} TO user cognos;
db2 CONNECT RESET;




if [[ $CLOUDENV == "PRD" ]] ; then
   mkdir -p $ARCHPATH
   db2 update db cfg for ${DBNAME} using logarchmeth1 "disk:$ARCHPATH"
   db2 update db cfg for ${DBNAME} using auto_del_rec_obj on
   db2 update db cfg for ${DBNAME} using rec_his_retentn 10
   db2 update db cfg for ${DBNAME} using num_db_backups 10
   db2 update db cfg for ${DBNAME} using logarchcompr1 off

   db2stop force
   db2start

   db2 backup database ${DBNAME} to /data/vol01/backup compress

fi


### Normally, the query database is on the live db2 server, so use the
### alias 'db2' in the current domain, override if defined in hiera (and
### thus set in the cfg file below)
maxdbserver="db2"
[[ -f /usr/local/etc/db2cognos.cfg ]] && . /usr/local/etc/db2cognos.cfg
db2ip=$(getent hosts $maxdbserver | awk 'NR==1 {print $1}')

### Set up the link to the query database
db2 catalog tcpip node db2 remote $db2ip server 50005
db2 catalog database ${CLOUDCLIENT}MX${CLOUDENV} at node db2
