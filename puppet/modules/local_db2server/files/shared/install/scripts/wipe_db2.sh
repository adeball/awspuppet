#!/bin/bash

echo "db2stop" | su - ctginst1

for u in dasusr1 ctginst1 ; do
    pkill -u $u
    sleep 3
    pkill -9 -u $u
done

for user in ctginst1 dasusr1 db2admin; do
	userdel $user
done

rm -rf /opt/IBM/db2 /var/ibm /etc/.ibm /opt/ctginst1 /opt/db2admin /etc/profile.d/db2.sh
rm -rf /home/dasusr1 /tmp/TPAEAutomation /tmp/db2* /tmp/das* /data/vol01/db/ctginst1



