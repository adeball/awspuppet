
class local_db2server {


  if $cloudenv == 'PRD' {
    class {'local_db2backup':
      schedule => 'daily',
      online   => 'true',
    }
  } else {
    class {'local_db2backup':
      schedule => 'weekly',
      online   => 'false',
    }
  }


  ### Stop/start script

  file { '/etc/init.d/db2':
    ensure => present,
    owner  => root,
    mode   => 700,
    source => 'puppet:///modules/local_db2server/etc/init.d/db2',
  }
  file { '/etc/rc3.d/S99db2': ensure => link, target => '/etc/init.d/db2', }
  file { '/etc/rc2.d/K01db2': ensure => link, target => '/etc/init.d/db2', }


  file {'/shared/install/scripts':
    source => 'puppet:///modules/local_db2server/shared/install/scripts',
    owner => root,
    recurse => true,
    purge => true,
    mode => 0755,
    require => Class['local_ibmsupport'],
  } ->
  file {'/shared/install/SilentResponseFiles':
      source => 'puppet:///modules/local_db2server/shared/install/SilentResponseFiles',
      owner => root,
      recurse => true,
  } ->
  exec {'Install DB2':
    path => '/usr/bin:/usr/sbin',
    command => 'bash /shared/install/scripts/install_db2_server.sh',
    creates => '/opt/IBM/db2',
    timeout => '900',
  }

  file {'/opt/db2admin': ensure => directory, owner => db2admin, group => db2iadm1, mode => 0750 }
  file {'/opt/db2fenc1': ensure => directory, owner => db2fenc1, group => db2iadm1, mode => 0750 }
  file {'/opt/maximo':   ensure => directory, owner => maximo,   group => db2user,  mode => 0755 }
  file {'/opt/mxsibmif': ensure => directory, owner => mxsibmif, group => db2user,  mode => 0755 }
  file {'/opt/mxsibui':  ensure => directory, owner => mxsibui,  group => db2user,  mode => 0755 }

}
