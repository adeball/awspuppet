# local_eyaml

#### Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with local_eyaml](#setup)
    * [What local_eyaml affects](#what-local_eyaml-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with local_eyaml](#beginning-with-local_eyaml)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Overview

Installs hiera-eyaml and sets up initial keys if not already available.

## Module Description

eyaml (encrypted YAML - see https://github.com/voxpupuli/hiera--eyaml) enables PKI encryption of data within YAML.  If this is integrated into Hiera and used in Puppet, this effectively means that passwords, keys and any other sensitive data may be stored in a Puppet Hiera repository without danger of that information being exposed in plain text.  This module is only required on the Puppet Master (admin server).

## Setup

### What local_eyaml affects

By default, the keys directory will be created in /etc/pki/eyaml.  The files created within the keys directory will be called private_key.pkcs7.pem & public_key.pkcs7.pem.  If the files already exist in this location with these names, new keys will not be generated.

### Beginning with local_eyaml

include local_eyaml

## Usage

There is one parameter, $keydir, which is the directory into which the 'keys' directory will be installed.  This defaults to /etc/pki/eyaml, which is suitable default in most cases.

## Reference

For full information, please read the documentation on Github.
A brief view of how it works is:

* Keys are created in /etc/pki/eyaml 
* The Puppet server is already configured (see /etc/puppet/hiera.conf) to use eyaml as a backend to Hiera
* Any eyaml files in Hiera are automatically decrypted by the Puppet server using the keys in /etc/pki/eyaml
* To edit files, you must have a copy of the eyaml utility and the keys (either copy from the Puppet server, or from other secure storage).  NB: If the keys are lost, none of the information may be decrypted.
* See https://puppet.com/blog/encrypt-your-data-using-hiera-eyaml for a good discussion on how to set all this up, including how to edit eyaml files.

## Limitations

## Development


