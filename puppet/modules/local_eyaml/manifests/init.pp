
class local_eyaml (
  $keydir = '/etc/pki/eyaml',
){

  ### Install eyaml
  package {'hiera-eyaml':
    ensure => present,
    provider => 'gem',
  }

  file {$keydir:
    ensure => directory,
    owner => puppet,
    group => puppet,
    mode => 0500,
  } ->

  exec {'Create eyaml keys':
    path => '/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin',
    command => "cd $keydir; eyaml createkeys",
    unless => "test -d $keydir/keys",
  } ->

  file {"$keydir/keys":
    ensure => directory,
    owner => puppet,
    group => puppet,
    mode => 0500,
    recurse => true,
  }

}
