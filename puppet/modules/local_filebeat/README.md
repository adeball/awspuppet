# local_filebeat

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_filebeat]](#setup)
    * [What [local_filebeat] affects](#what-[local_filebeat]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_filebeat]](#beginning-with-[local_filebeat])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs Filebeat with a minimal configuration.  Used as a proof-of-concept.    

Filebeat strmyco logs to a Logstash server.  MYCO is investigating Graylog, so this module was developed in order to test this out.  It needs parameterising and tidying up.

## Setup

### What [local_filebeat] affects 

* Filebeat is not in the standard yum repositories, so one is set up prior to installing the software.
* The software installation does not set up a service for filebeat, so one is created.

### Setup Requirements 

### Beginning with [local_filebeat]	

include filebeat
The current PoC version has hard-coded the environment/client/servertype (GTR prod websphere(mgr)) and the recipient logstash server.  A rewrite is needed to parameterise this, should this move from PoC.

## Usage

## Reference

* The filebeat.yml.template file includes the hard-coded recipient server: graylog2.graylog.tools.myco.cloud:5044.  This would be easy enough to switch to a ruby template and use Hiera data.  For now, you need to change the file.
* The string XXXPROSPECTORSXXX is replaced by the log files to stream.  This is not easy to parameterise as they are not in (totally) standard locations, so this information is generated on a per-host basis when the mk_filebeat_conf.sh script is run.  This script works out where the websphere logs are, and injects this into the filebeat.yml file which is deployed.


## Limitations

This is a PoC module, and requires further development if it is to be used in anger.

## Development

