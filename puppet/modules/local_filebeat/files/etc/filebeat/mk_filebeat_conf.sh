#!/bin/bash

### Normally /opt, this is an MYCO configuration
BASE=/opt/maxapps

PROFILEDIR=$BASE/IBM/WebSphere/AppServer/profiles
cd $PROFILEDIR

logfiles=$(find $PROFILEDIR | grep -i systemout.log$ | xargs grep -i "total memory" | awk -F: '{print $1}' | sort -u)

tmpf=$(mktemp)
for logfile in $logfiles ; do
	prospector=$(dirname $logfile)
	printf "    - $prospector/*\n" >> $tmpf
done

sed -e '/XXXPROSPECTORS/ {' -e "r $tmpf" -e 'd' -e '}' /etc/filebeat/filebeat.yml.template > /etc/filebeat/filebeat.yml

rm $tmpf
