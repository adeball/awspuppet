class local_filebeat {

  if ( $cloudservertype == "webspheremgr" or $cloudservertype == "websphere" ) and
    $cloudenv == 'PRD' and $cloudclient == 'GTR' {

    exec { 'Install Elastic.co repository GPG key':
      command => 'rpm --import https://packages.elastic.co/GPG-KEY-elasticsearch',
      path    => '/usr/bin:/bin',
      unless  => 'rpm -qa gpg-pubkey* | xargs rpm -qi | grep Elasticsearch',
    } ->
      file { '/etc/yum.repos.d/elastic.repo':
        ensure  => present,
        content => '[elastic-5.x]
name=Elastic repository for 5.x packages
baseurl=https://artifacts.elastic.co/packages/5.x/yum
gpgcheck=1
gpgkey=https://artifacts.elastic.co/GPG-KEY-elasticsearch
enabled=1
autorefresh=1
type=rpm-md',
        owner   => root,
        mode    => 0644,
      } ->
    package {'filebeat':
      ensure => present,
    } ->

    exec {'Enable filebeat service':
      command => 'chkconfig --add filebeat',
      path => '/usr/bin:/usr/sbin',
      unless => 'chkconfig --list filebeat',
    } ->

    service {'filebeat':
      ensure => running,
    }


    file{'/etc/filebeat/filebeat.yml.template':
      ensure => present,
      source => 'puppet:///modules/local_filebeat/etc/filebeat/filebeat.yml.template',
      owner => root,
      mode => 0644,
    } ->
    file{'/etc/filebeat/mk_filebeat_conf.sh':
      ensure => present,
      source => 'puppet:///modules/local_filebeat/etc/filebeat/mk_filebeat_conf.sh',
      owner => root,
      mode => 0755,
    }
    exec{'Make filebeat configuration file':
      path => '/usr/bin:/usr/sbin',
      command => '/etc/filebeat/mk_filebeat_conf.sh',
      notify => Service['filebeat'],
      subscribe => [
        File['/etc/filebeat/mk_filebeat_conf.sh'],
        File['/etc/filebeat/filebeat.yml.template'],
      ],
      refreshonly => true,
    }
  } else {
    service {'filebeat':
      ensure => stopped,
    }
  }

}
