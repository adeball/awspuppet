# local_ibminstall

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_ibminstall]](#setup)
    * [What [local_ibminstall] affects](#what-[local_ibminstall]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_ibminstall]](#beginning-with-[local_ibminstall])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the IBM Installation Manager software and (conditionally) unpacks IBM software zip files.

## Setup

### What [local_ibminstall] affects

* Creates and executes /root/ibmbasecopy.sh script, which
    * Optionally unpacks IBM software
      * On DB2 servers (i.e. those with servertype=db2), IBM software zip files stored in S3://myco-cloud-media/ will be unpacked into the local filesystem /data/install.  NB: This is required for the DB2 installation which is embedded in servertype-db2 (deprecated).  When that has been switched to use local-db2server, this behaviour can be removed.
      * On Maximo admin servers (which are the source for the NFS share /shared/install), the zip files will be unpacked into /shared/install, so that all other servers can access the single copy.
    * Installs the IBM Installation Manager software
    * Sets up a rudimentary response file and imports all repository.config files

### Setup Requirements 

* Requires /shared/install to be mounted (see local-nfssharedclient).
* You should install local-ibmsupport in conjunction with this module.

### Beginning with [local_ibminstall]	

include local_ibminstall

## Usage

## Reference

## Limitations

Only installed and tested on RHEL7

## Development

