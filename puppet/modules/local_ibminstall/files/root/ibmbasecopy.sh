#!/bin/bash

INSTALLDIR=/shared/install

[[ $CLOUDSERVERTYPE == "db2" ]] && INSTALLDIR=/data/install



fail()
{
    echo "Failed: $@" ; exit 1
}

df -k ${INSTALLDIR} > /dev/null 2>&1 || fail "${INSTALLDIR} does not exist"
[[ -f /etc/profile.d/cloud.sh ]] && . /etc/profile.d/cloud.sh || fail "Environment not set up"

if [[ $CLOUDSERVERTYPE == "maximo" || $CLOUDSERVERTYPE == "db2" ]] ; then

mpt=$(df -k ${INSTALLDIR} | tail -1 | awk '{print $NF}')
[[ $mpt == "${INSTALLDIR}" ]] || fail "${INSTALLDIR} is not a separate mounted filesystem"

mkdir -p ${INSTALLDIR}/zips || fail "Cannot make ${INSTALLDIR}/zips"


cd ${INSTALLDIR}/zips


case $CLOUDSERVERTYPE in

    maximo)
        cd ..
        for f in MAM_7.6.0.0_LINUX64.tar.gz Middl_Inst_DB2_105_Linux_x86-64.tar.gz ; do
            [[ ! -f $f ]] && aws s3 cp s3://myco-cloud-media/Maximo/7.6/$f .
            tar xfz $f
        done
        aws s3 cp s3://myco-cloud-media/GTR/braid-license-gtrproduction-s1.0.zip .
        cd zips
        aws s3 cp s3://myco-cloud-media/downloads/ibm/im/1.8.5/agent.installer.linux.gtk.x86_64_1.8.5001.20161016_1705.zip .
        aws s3 sync s3://myco-cloud-media/Braid/Baseline/3.1.2/ .
        aws s3 sync s3://myco-cloud-media/Braid/Advance/5.0.2/ .
        aws s3 sync s3://myco-cloud-media/Braid/FAM/4.1.0/ .
        aws s3 sync s3://myco-cloud-media/GTR/Releases/1.0.1/ .
        aws s3 cp s3://myco-cloud-media/Maximo/7.6/Fixpacks/MAMMTFP7606IMRepo.zip .
        aws s3 cp s3://myco-cloud-media/Maximo/7.6/MAM_Multitenancy_7.6.0.0_WIN64.zip .

        # WAS
        aws s3 cp s3://myco-cloud-media/Maximo/7.6/WASND_v8.5.5_1of3.zip .
        aws s3 cp s3://myco-cloud-media/Maximo/7.6/WASND_v8.5.5_2of3.zip .
        aws s3 cp s3://myco-cloud-media/Maximo/7.6/WASND_v8.5.5_3of3.zip .
        cd .. ; aws s3 sync s3://myco-cloud-media/SSL/2017/ SSL

        # Proxy
        for f in Max_Transportation_762.zip Max_Asset_Health_Insights_760.zip 7.6.2.3-TIV-TRN-FP0003.zip MAM_Scheduler_7.6.4.zip ; do
            aws s3 cp s3://myco-cloud-media/Maximo/7.6/Addons/$f .
        done

        ;;

    ### DB2 is a pain, so leave in /data/install
    db2)
        cd ..
        for f in MAM_7.6.0.0_LINUX64.tar.gz Middl_Inst_DB2_105_Linux_x86-64.tar.gz ; do
            [[ ! -f $f ]] && aws s3 cp s3://myco-cloud-media/Maximo/7.6/$f .
            tar xfz $f
        done
        ### The correct one will be put in place by Puppet, so we can use this as a semaphore
        rm ${INSTALLDIR}/SilentResponseFiles/Installer/Unix/ResponseFile_DB2_Install_Unix.xml
        ;;

esac


### Unzip one by one, and remove the zip file if successful

for group in agent  WASND SUPPL SDK TIV-TRN MAMMTFP MAM_Scheduler \
                    Max_Asset_Health_Insights Max_Transportation MAM_Multitenancy \
                    brda brdb brdf mxr license ; do
    mkdir -p ${INSTALLDIR}/${group}
    cd ${INSTALLDIR}/${group}
    for z in ../zips/*${group}* ; do
        echo "Unzipping $z in ${INSTALLDIR}/${group}"
        unzip -o $z && rm -f $z
    done
done


### This one is superfluous
rm ${INSTALLDIR}/zips/Install_Mgr*

### Get rid of empty directories, including zips
find ${INSTALLDIR} -maxdepth 3 -type d -exec rmdir {} \; 2>/dev/null

fi

### Set ulimits, as we may be running at initial creation time
if [[ -f /etc/profile.d/ibmsupport.sh ]] ; then
    . /etc/profile.d/ibmsupport.sh
else
    fail "ulimit script not yet in place"
fi


### Install the IBM installation manager
cd ${INSTALLDIR}/Install/IM/installer.linux.x86_64
./installc -acceptLicense


if [[ "$CLOUDSERVERTYPE" == "db2" ]] ; then
    exit
fi



# Finds all the top-level repository.config files in /data/install
# Creates a response file to import these
# Imports/connects them
#
# Note: To create a manual response file , use imcl -c -record <responsefile.xml>
#

IM=/opt/IBM/InstallationManager/eclipse/tools/imcl

responsefile=$INSTALLDIR/${CLOUDSERVERTYPE}_response.xml

cat << EOF > $responsefile
<?xml version='1.0' encoding='UTF-8'?>
<agent-input>
  <server>
EOF

find $INSTALLDIR -maxdepth 3 -name repository.config | grep -v "/agent" | while read rf ; do
	dir=$(dirname $rf)
	printf "    <repository location='%s'/>\n" $dir >> $responsefile
done

cat << EOF >> $responsefile
  </server>
</agent-input>
EOF

$IM -input $responsefile


### Can't put this script in $INSTALLDIR directly, as it might not exist at the time
### But now we're here, we can at least link it.  Don't assume the mycoscripts directory
### already exists if dependencies have not all been met yet.

mkdir -p $INSTALLDIR/mycoscripts ; cd $INSTALLDIR/mycoscripts ; ln -s /root/ibmbasecopy.sh

cat > $INSTALLDIR/README << EOF

/shared/install
Source location is on the Maximo admin server
/shared/install/mycoscripts & mycoconfig is copied in the nightly backup - other directories are created
from S3, so are not backed up.

The directory is initially populated by /root/ibmbasecopy.sh (local_ibminstall Puppet module)

EOF
