class local_ibminstall {

    file { '/root/ibmbasecopy.sh':
      ensure => present,
      source => 'puppet:///modules/local_ibminstall/root/ibmbasecopy.sh',
      owner  => 'root',
      mode   => 755,
    } ->

      ### And run it when it's there...
      exec { 'Copying WebSphere software and installing IBM installation manager':
        command   => '/root/ibmbasecopy.sh',
        creates   => '/opt/IBM/InstallationManager',
        path      => '/usr/bin:/usr/sbin:/bin',
        logoutput => true,
        timeout   => 1800,
      }

    file { '/var/ibm': ensure => directory, } ->
      file { '/var/ibm/InstallationManager': ensure => directory, } ->
      file { '/var/ibm/InstallationManager/secure_storage':
        ensure => present,
        source => 'puppet:///modules/local_ibminstall/var/ibm/InstallationManager/secure_storage',
        owner  => root,
        mode   => 644,
      }


}
