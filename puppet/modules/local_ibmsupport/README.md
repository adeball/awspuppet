# local_ibmsupport

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_ibmsupport]](#setup)
    * [What [local_ibmsupport] affects](#what-[local_ibmsupport]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_ibmsupport]](#beginning-with-[local_ibmsupport])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs dependency packages and system configuration required to support the IBM software stack.

The class local_ibmsupport::truetype will optionally install TrueType fonts (required by Websphere servers running Maximo, for certain reports).  

## Setup

### What [local_ibmsupport] affects

* SELinux
* JAVA_HOME
* ulimits
* Packages

### Setup Requirements 

None

### Beginning with [local_ibmsupport]	

* For the main module: include local_ibmsupport
* Optionally: include local_ibmsupport::truetype (if TrueType fonts are required)

## Usage

One parameter is configured: $javahome = '/usr/lib/jvm/jre-1.8.0'
Override when including the module, or configure in Hiera if needed.

## Reference

### local_ibmsupport

* Sets SELinux to permissive
* Links /Launchpad to /shared/install (so installers can find software without changes to the defaults)
* Configures /etc/profile.d/ibmsupport.sh & /etc/security/limits.d/30-nofile.conf to set ulimits
* Installs Java 1.7.0, (and Java 1.8.0 for Maximo & Cognos servertypes)
* Installs several package dependencies
* Links /usr/bin/seamonkey to firefox, as this would otherwise fail dependency checks

### local_ibmsupport::truetype

* Copies OpenSans-* fonts into /usr/share/fonts/truetype

## Limitations

This module has only been installed/tested on RHEL7.

## Development

