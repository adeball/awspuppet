
class local_ibmsupport (
  $javahome = '/usr/lib/jvm/jre-1.8.0',
)
  {

  ### IBM Software does not like SELinux
  exec { 'Set SELinux to permissive mode':
    command => 'setenforce 0',
    path    => '/usr/bin:/usr/sbin:/bin',
    unless  => 'test $(getenforce) == "Permissive" '
  }

  file {'/Launchpad':
    ensure => 'link',
    target => '/shared/install',
  }

  file {'/etc/profile.d/ibmsupport.sh':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => 755,
    content => "#!/bin/bash
# This file is managed by Puppet - local_ibmsupport
export JAVA_HOME=${javahome}
ulimit -n 8192
ulimit -f unlimited
"
  }
  file {'/etc/security/limits.d/30-nofile.conf':
    ensure => present,
    owner  => root,
    group  => root,
    mode   => 755,
    content => "# This file is managed by Puppet - local_ibmsupport module
*       soft    nofile       4096
*       hard    nofile       63356
"
  }

  ### Packages
  ### https://www.ibm.com/support/knowledgecenter/en/SSAW57_8.5.5/com.ibm.websphere.installation.nd.doc/ae/tins_linuxsetup_rhel7.html

  ### From failed pre-reqs
  ensure_packages([bc, 'java-1.7.0-openjdk-devel.x86_64', ])

  if $cloudservertype == "maximo" or $cloudservertype == "cognos" { ensure_packages(['java-1.8.0-openjdk-devel.x86_64', ]) }

  ### Specified explicitly
  #ensure_packages([gtk2, 'libXtst.x86_64', xorg-x11-fonts-Type1, psmisc, 'compat-libstdc++-33.x86_64','firefox',])
  ensure_packages([gtk2, 'libXtst.x86_64', xorg-x11-fonts-Type1, psmisc, 'compat-libstdc++-33.x86_64',])

  exec {'Configure yum for optional RPMs':
    command => 'yum-config-manager --enable rhui-REGION-rhel-server-extras rhui-REGION-rhel-server-optional ; touch /tmp/rhui.flag',
    unless  => 'test -f /tmp/rhui.flag',
    path    => '/usr/bin:/usr/sbin:/bin',
  } ->
    package {['glibc.i686', 'libXft.i686', 'libXtst.i686', 'gtk2.i686', 'libgcc.i686', 'compat-libstdc++-33.i686', 'pam.i686']:
      ensure => present,
    }

  ### Strangely, seamonkey is a pre-req for the IBM products, but the installer is buggy.
  ### We therefore need to fool the IBM pre-req checker.
  file {'/usr/bin/seamonkey':
    ensure => 'link',
    target => '/usr/bin/firefox',
  }


}

