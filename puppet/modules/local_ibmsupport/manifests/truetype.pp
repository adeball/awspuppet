class local_ibmsupport::truetype {
  file {'/usr/share/fonts/truetype':
    ensure => directory,
    source => 'puppet:///modules/local_ibmsupport/usr/share/fonts/truetype',
    recurse => true,
    purge => false,
    mode => 0755,
  }
}
