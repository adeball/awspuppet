# local_icinga2

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_icinga2]](#setup)
    * [What [local_icinga2] affects](#what-[local_icinga2]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_icinga2]](#beginning-with-[local_icinga2])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs an Icinga2 monitoring client and connects to an Icinga2 server.

Automated installation for monitoring.

## Setup

### Beginning with [local_icinga2]	

include local_icinga2 is enough to install the client and dependencies, but is unlikely to connect to a server correctly - see next section.

## Usage

Four parameters are included, listed below with default values, their names should make them fairly self-explanatory:

* $icinga2_master = 'icinga2',
* $icinga2_port = 5665,
* $icinga2_user = 'icinga',
* $icinga2_password = 'password',

You will almost certainly need to change these, which is best done in Hiera.

## Reference

The module:
* Deploys and executes a script: /usr/local/bin/install_icinga2_client.sh, which
  * Connects to the icinga2 server with the username and password specified, on the port specified.
  * Obtains a ticket from the server
  * Sets up the client certificates and node configuration
  * Starts the service

## Limitations

## Development

