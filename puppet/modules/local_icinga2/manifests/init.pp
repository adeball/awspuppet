
class local_icinga2(
  $icinga2_master = 'icinga2',
  $icinga2_port = 5665,
  $icinga2_user = 'icinga',
  $icinga2_password = 'password',
) {

  package {[nagios-plugins, nagios-plugins-all]: ensure => present}

  exec { 'Install icinga2 source rpm':
    command => "yum install -y --nogpgcheck https://packages.icinga.com/epel/7/release/noarch/icinga-rpm-release-7-1.el7.centos.noarch.rpm",
    creates => "/etc/yum.repos.d/ICINGA-release.repo",
    path => "/usr/bin:/usr/sbin"
  } ->
  package {'icinga2': ensure => present} ->

  file {'/usr/local/bin/install_icinga2_client.sh':
    ensure => present,
    content => template("local_icinga2/install_icinga2_client.sh.erb"),
    owner => 'root',
    mode => 755,
  } ->
    file {'/usr/lib64/nagios/plugins/check_mem.pl':
      ensure => present,
      source => 'puppet:///modules/local_icinga2/usr/lib64/nagios/plugins/check_mem.pl',
      owner => 'root',
      mode => 755,
    } ->

  exec {'Configure icinga2 client':
    command => '/usr/local/bin/install_icinga2_client.sh',
    path => '/usr/bin:/usr/sbin',
    creates => '/etc/icinga2/pki',
    subscribe => [
      File["/usr/local/bin/install_icinga2_client.sh"],
    ],
  }

  file {'/sbin/icinga2':
    ensure => present,
    owner  => root,
    source => 'puppet:///modules/local_icinga2/sbin/icinga2',
    mode   => 0755,
  }

}
