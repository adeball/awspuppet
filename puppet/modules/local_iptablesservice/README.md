# local_iptablesservice

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_iptablesservice]](#setup)
    * [What [local_iptablesservice] affects](#what-[local_iptablesservice]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_iptablesservice]](#beginning-with-[local_iptablesservice])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Configures iptables and ip6tables as a service for systems which do not have this built in.

This is required for Puppet to manage IPTables.


## Setup

### Beginning with [local_iptablesservice]	

include local_iptablesservice

## Usage

## Reference

## Limitations

Only required on Amazon Linux 2 (where IPTables exists, but there is no service)

## Development

