class local_iptablesservice {

  file {'/etc/init.d/iptables':
    source => 'puppet:///modules/local_iptablesservice/iptables',
    owner => root,
    group => root,
    mode => 0755,
  }
  file { '/etc/init.d/ip6tables':
    source => 'puppet:///modules/local_iptablesservice/ip6tables',
    owner  => root,
    group  => root,
    mode   => 0755,
  }

}

