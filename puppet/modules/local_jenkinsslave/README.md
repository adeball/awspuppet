# local_jenkinsslave

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_jenkinsslave]](#setup)
    * [What [local_jenkinsslave] affects](#what-[local_jenkinsslave]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_jenkinsslave]](#beginning-with-[local_jenkinsslave])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the Jenkins swarm client and connects to a Jenkins server.

This enables instances to automatically become Jenkin slaves where appropriate.  This enables any instances with this module installed to be used by Jenkins for application deployments.

https://confluence.myco-group.com/display/EC/Jenkins+Integration 

## Setup

### Beginning with [local_jenkinsslave]	

include local_jenkinsslave is sufficient to install the software and configure it, but it is unlikely to successfully connect to a Jenkins server with the default parameter values.

## Usage

Seven parameters are included, listed below, with default values:

* $localuser = 'jenkins',
* $localgroup = 'jenkins',
* $localsshpublickey = 'sshpublicrsakey',
* $jenkinsserver = 'jenkins',
* $jenkinsserverip = '10.0.0.1',
* $jenkinsuser = 'jenkinsserverlogin',
* $jenkinspassword = 'secretpassword',

Most are self-explanatory.  Two need further explanation:

* localsshpublickey: This is no longer used, but could be useful in an emergency.  See https://confluence.myco-group.com/display/EC/Jenkins+Integration - Manual Setup section.  This is the public key which goes with the Jenkins server private key, so will allow the Jenkins server to execute commands over SSH if the swarm client does not work for whatever reason, it just needs the node to be registered manually.
* jenkinsserverip: This should not be needed, as the DNS entry should suffice.  During testing, DNS was unreliable, so this was included so that the entry could be added to the hosts file explicitly.  It is probably ok to remove it, but this needs to be tested.

Because a username and password is needed, take care not to store the password insecurely (use eyaml).

## Reference

The module:

* Sets up the 'jenkins' (or defined) user, and makes the home directory /var/jenkins
* Add the jenkins user to the admin group, so that it has sudo rights to run deployment scripts etc
* Copies the swarm client JAR file
* Configures swarm_slave.cfg
* Configures the 'jenkins-slave' service and starts it

## Limitations

Tested on RHEL7 and Amazon Linux

## Development

