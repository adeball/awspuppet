#!/bin/bash

fail()
{
	echo "Failed: $@"
	exit 1
}


. /var/jenkins/swarm_slave.cfg || fail "Cannot read configuration file"
[[ $LOGNAME == "$LOCALUSER" ]] || fail "Must be run as the $LOCALUSER user"
cd || fail "Cannot cd to home directory"

java -jar swarm-client-3.4.jar \
	-master $JURL \
	-fsroot /var/jenkins \
	-name $SLAVE_NAME \
	-description "$COMMENT" \
	-disableSslVerification \
	-disableClientsUniqueId \
	-labels "$LABELS" \
	-username $JUSER \
	-passwordEnvVariable JPASS

