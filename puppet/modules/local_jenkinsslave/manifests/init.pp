
class local_jenkinsslave (
  $localuser = 'jenkins',
  $localgroup = 'jenkins',
  $localsshpublickey = 'sshpublicrsakey',
  $sshprivatekey = 'sshprivatekey',
  $jenkinsserver = 'jenkins',
  $jenkinsserverip = '10.0.0.1',
  $jenkinsuser = 'jenkinsserverlogin',
  $jenkinspassword = 'secretpassword',
){

  ### NB: Tried out the puppetlabs jenkins module, but had trouble with dependencies
  ###     abandoned for now

  ensure_packages(['java-1.8.0', ])

  file {'/var/jenkins':
    ensure => directory,
    owner => $localuser,
    group => $localgroup,
  }

  group { "$localgroup":
    ensure => present,
    gid => 4005
  } ->
  user { "$localuser":
    ensure => present,
    uid => 4007,
    gid => "$localgroup",
    groups => ['admin', ],
    home => '/var/jenkins',
    managehome => true,
  } ->
  file { "/var/jenkins/.ssh/id_rsa":
    ensure  => present,
    owner   => "$localuser",
    mode    => 0600,
    content => "$sshprivatekey\n"
  }


  ssh_authorized_key { "${localuser}@jenkinsslave": user => "$localuser", type => 'ssh-rsa', key => $localsshpublickey, }



  # DNS is flaky, so make sure we can resolve the Jenkins server
  host { "$jenkinsserver":
    ip      => "$jenkinsserverip",
    comment => 'Managed by puppet',
  }

  file {'/var/jenkins/swarm-client-3.4.jar':
    owner => "$localuser",
    ensure => present,
    source => 'puppet:///modules/local_jenkinsslave/var/jenkins/swarm-client-3.4.jar',
  }

  file {'/var/jenkins/swarm_slave.sh':
    owner => "$localuser",
    ensure => present,
    source => 'puppet:///modules/local_jenkinsslave/var/jenkins/swarm_slave.sh',
    mode => 0755,
    notify => Service['jenkins-slave'],
  }

  file {'/var/jenkins/swarm_slave.cfg':
    owner => "$localuser",
    group => "$localgroup",
    mode => 0440,
    content => "

### This file is managed by Puppet

LOCALUSER=$localuser
JURL=http://$jenkinsserver/
JUSER=$jenkinsuser
export JPASS=$jenkinspassword
env=\$(echo $cloudenv | tr [A-Z] [a-z])
client=\$(echo $cloudclient | tr [A-Z] [a-z])
SLAVE_NAME=${cloudclient}-$cloudservername
COMMENT=\"$cloudservername - \$client \$env ${cloudservertype}-$cloudinstance\"
LABELS=\"linux rhel7 $cloudservertype \$client \$env\"

",
    notify => Service['jenkins-slave'],
  }

  ### Amazon Linux uses upstart
  if $cloudos == "linux" {
    file { '/etc/init/jenkins-slave.conf':
      ensure => present,
      owner  => root,
      mode   => 0755,
      source => 'puppet:///modules/local_jenkinsslave/etc/init/jenkins-slave.conf',
    }
  }

  file {'/etc/init.d/jenkins-slave':
    ensure => present,
    owner => root,
    mode => 0755,
    source => 'puppet:///modules/local_jenkinsslave/etc/init.d/jenkins-slave',
  } ->
    exec {'Add Jenkins slave to system start up':
      path => '/sbin:/usr/bin:/usr/sbin',
      command => 'chkconfig --add jenkins-slave',
      creates => '/etc/rc3.d/S99jenkins-slave',
    } ->
    service {'jenkins-slave':
      ensure => running,
    }
}
