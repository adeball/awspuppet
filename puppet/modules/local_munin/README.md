# local_munin

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_munin]](#setup)
    * [What [local_munin] affects](#what-[local_munin]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_munin]](#beginning-with-[local_munin])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the munin performance monitoring client (and server, if specified).

Munin is a comprehensive OS monitoring tool, which captures detailed statistics suitable for diagnosing OS performance issues.  

## Setup

### What [local_munin] affects

#### Clients

* munin-node service created

#### Servers

* Creates /etc/httpd/conf.d/munin.conf for Apache
* Uses /dev/shm (memory based filesystem) to hold temporary html data
* Sets up rrdcached (as a service) for improved performance
* cron jobs are created 

### Setup Requirements 

* The munin server requires apache & rsync.
* The mk_munin_nodes.sh script requires /cloudinit and the set of functions from common.sh (getclients, getenvironments etc)
* The client listens on TCP 4949, which is set up in IPTables by the module.  However, external firewall/security groups settings will also need to allow access to this port.

### Beginning with [local_munin]	

include local_munin
For the client to communicate with a server, it will need the $munin_server_ips parameter configuring - this is an array.  The $munin_server_ips parameter is to allow inbound TCP access, as the server polls the clients.

## Usage

There are three parameters configured:

* $munin_servers=['munin'] : List of servers - not used
* $munin_server_ips=['10.0.0.1'] : List of server IP addresses.  Used to configure inbound TCP access
* $munin_type='client' : client|server - the client is always installed, if 'server' is used, then both the client and server packages will be installed and configured.

## Reference


## Limitations

Tested on RHEL6/7 and Amazon Linux

## Development

