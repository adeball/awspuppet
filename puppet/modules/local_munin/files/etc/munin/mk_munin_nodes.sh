#!/bin/bash
#
# This script is managed by puppet
#
#


. /etc/profile

BASE=/cloudinit
muninhome=/etc/munin
noded=$muninhome/conf.d
semaphore=$muninhome/munin_config_created

cd $BASE/scripts
. ./common.sh
export ECPSCRIPTDIR=$BASE/scripts
export ECPETCDIR=$ECPSCRIPTDIR/../etc

touch $semaphore --date="2 minutes ago"
### Make sure all the new files are more recent than the semaphore

#client=$(echo $CLOUDCLIENT | tr [A-Z] [a-z])
#servers=$(getservers $client)
#[[ $client == "ems" ]] && servers=$(getservers)


for client in $(getclients) ; do
for env in $(getenvironments $client) ; do
for server in $(getservers $client $env); do
        servertype=$(getservertype $server)
        zone=$env.$client.$CLOUDDOMAIN
        node=$server.$zone
        nodef=$noded/${node}.conf
#        if [[ $(getmonitor $server) == "yes" ]] ; then
            echo "[$node]" > $nodef
            echo "  address $node" >> $nodef
            echo >> $nodef
            #echo "  use_node_name yes" >> $nodef
#        fi
done
done
done


echo "rrdcached_socket /run/munin/rrdcached.sock" > $noded/RRDCACHED.conf

### Removes old node definitions, if any
cd $noded
find . ! -newer $semaphore -exec rm {} \;

systemctl restart munin-node
