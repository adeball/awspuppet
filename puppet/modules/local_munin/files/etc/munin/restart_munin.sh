#!/usr/bin/env bash

systemctl stop munin-node
/etc/init.d/munin_rrdcached stop
sleep 5
pkill -u munin
sleep 5
pkill -9 -u munin

/etc/init.d/munin_rrdcached start
systemctl start munin-node

