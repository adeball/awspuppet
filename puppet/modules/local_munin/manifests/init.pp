# == Class: munin
#
# This class deploys the munin client. 
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
# # Using the default exxxlinman01 & exvmuninprd01
# include munin
# or
# class { munin: }
#
# # Or for a different server (e.g.)
# class { munin: $munin_servers=> ['someotherserver'],
#                $munin_server_ips => ['1.1.1.1'],
#                $munin_type => 'client|server' }
#
# === Author
#
# Ade Ball <adrian.ball@myco.com>
#

class local_munin (
  $munin_servers=['munin'],
  $munin_server_ips=['10.0.0.1'],
  $munin_type='client'
) {
                

  package { ['munin-common','munin-node', ]:
    ensure  => present,
  }
  
  if $munin_type == 'server' {

    #package { ['munin','munin-cgi','munin-async','rsync']:
    package { ['munin','munin-cgi','rsync']:
      ensure  => present,
    }

    file { "/etc/httpd/conf.d/munin-cgi.conf": ensure => "absent" }

    file { "/etc/httpd/conf.d/munin.conf":
      path  =>  "/etc/httpd/conf.d/munin.conf",
      owner => root,
      group => root,
      mode  => 644,
      content => template("local_munin/munin.conf.erb"),
      notify  => Service[httpd],
    }
    
    # file { "/var/www/html/index.html":
    #   path => "/var/www/html/index.html",
    #   content => template("local_munin/index.html.erb"),
    #   owner   => root,
    #   group   => root,
    #   mode    => 644,
    # }

    firewall { '04949 Munin + httpd server ':
      dport  => ['80', '443', '4949'],
      proto  => 'tcp',
      action => 'accept',
    }

    file { '/etc/munin/mk_munin_nodes.sh':
      ensure => present,
      source => 'puppet:///modules/local_munin/etc/munin/mk_munin_nodes.sh',
      mode   => '0755'
    }

    exec { 'Create munin hosts config':
      command => '/etc/munin/mk_munin_nodes.sh',
      path    => '/usr/bin:/usr/sbin',
      notify => Service['munin-node'],
      subscribe => [File['/cloudinit/etc/host_table'],File['/etc/munin/mk_munin_nodes.sh'],],
      refreshonly => true,
    }

    file { '/etc/munin/munin.conf':
      ensure => present,
      source => 'puppet:///modules/local_munin/etc/munin/munin.conf',
      mode => 0644,
    }

    file { '/etc/cron.d/munin':
      ensure => present,
      source => 'puppet:///modules/local_munin/etc/cron.d/munin',
      mode => 0644,
    }

    file { '/usr/bin/munin-graph':
      ensure => present,
      source => 'puppet:///modules/local_munin/usr/bin/munin-graph',
      mode => 0755,
    }

    file { '/usr/bin/munin-cron':
      ensure => present,
      source => 'puppet:///modules/local_munin/usr/bin/munin-cron',
      mode => 0755,
    }

    file {'/dev/shm/munin': ensure => directory, owner => munin, mode => 0755, }
    file {'/var/www/html/munin': ensure => link, target => '/dev/shm/munin', }

    ### RRDcached (EC-531)
    file {'/var/lib/munin/rrdcached-journal':
      ensure => directory,
      owner => munin,
      mode => 0755,
    } ->
      file { '/etc/init.d/munin_rrdcached':
        ensure => present,
        source => 'puppet:///modules/local_munin/etc/init.d/munin_rrdcached',
        owner => root,
        mode => 0755,
      } ->
    exec {'Set up rrdcached service':
      path => '/usr/bin:/usr/sbin:/bin:/sbin',
      command => 'chkconfig --add munin_rrdcached',
      unless => 'service munin_rrdcached status',
    } ->
    service {'munin_rrdcached': ensure => running }

    ### Memory leak in rrdcached
    file {'/etc/munin/restart_munin.sh':
      source => 'puppet:///modules/local_munin/etc/munin/restart_munin.sh',
      owner => root,
      group => root,
      mode => 0755,
    }
    file {'/etc/cron.weekly/restart_munin.sh':
      ensure => link,
      target => '/etc/munin/restart_munin.sh',
    }

  } else {

    firewall { '04949 Allow inbound Munin':
      dport    => 4949,
      proto    => tcp,
      action   => accept,
    }
  }
  
  service { 'munin-node':
    ensure     => running,
    enable     => true,
    name       => 'munin-node',
    hasstatus  => true,
    hasrestart => true,
  }
  
  file { "/etc/munin/munin-node.conf":
    path  =>  "/etc/munin/munin-node.conf",
    owner =>  root,
    group =>  root,
    mode  =>  644,
    content => template("local_munin/munin-node.conf.erb"),
    notify  => Service[munin-node], 
    require  => Package['munin-node'],
  }

}
