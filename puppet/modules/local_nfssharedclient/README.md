# local_nfssharedclient

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_nfssharedclient]](#setup)
    * [What [local_nfssharedclient] affects](#what-[local_nfssharedclient]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_nfssharedclient]](#beginning-with-[local_nfssharedclient])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Creates and mounts /shared/maximo, /shared/install and /shared/tmp on relevant hosts.

Enables file sharing amongst Maximo instances, shared online installation media and a shared 'scratchspace', useful for transferring data between hosts in the same environment.

## setup

### what [local_nfssharedclient] affects

Creates mountpoints and mounts NFSv4 filesystems.

### Setup Requirements 

The NFS servers should be in place before this module is used, though the only impact will be Puppet errors as the agent tries to mount non-existent exports.  The NFS servers are:

* The Maximo administration server for the client (in the sys environment): /shared/install & /shared/tmp
* The DB2 live server for the client/environment: /shared/maximo

### Beginning with [local_nfssharedclient]	

include local_nfssharedclient

## Usage

## Reference

See servertype-[db2|maximo] for the export configurations.
See local-xymon for details of the NFS mount test, which ensures no hanging mounts.

## Limitations

Tested on RHEL7

## Development

This module needs parameterising, the current version is bespoke.  


