class local_nfssharedclient {

  case $cloudservertype {
    'websphere', 'webspheremgr', 'proxy': {

      case $hostname {
        'sparta': { }   ### Special case - GTR test server created outside of the cloud build system
        default: { ### db2 should always be the primary db2 server in the environment
          file { '/shared/maximo': ensure => directory } ->
            mount { "/shared/maximo":
              device  => "db2:/maximo",
              fstype  => "nfs4",
              ensure  => "mounted",
              options => "soft,intr,timeo=450,retry=1,sync",
              atboot  => true,
            }
        }
      }

    }
    default: { }
  }


  $cc = downcase($cloudclient)
  if $cloudservertype != "maximo" and $cloudclient != "ems" {

  file { '/shared/install': ensure => directory } ->
    mount { "/shared/install":
      device => "maximo-001.sys.$cc.$clouddomain:/install",
      fstype => "nfs4",
      ensure => "mounted",
      options => "soft,intr,timeo=450,retry=1,sync",
      atboot => true,
    }

    file { '/shared/tmp': ensure => directory } ->
      mount { "/shared/tmp":
        device => "maximo-001.sys.$cc.$clouddomain:/tmp",
        fstype => "nfs4",
        ensure => "mounted",
        options => "soft,intr,timeo=450,retry=1,sync",
        atboot => true,
      }
    }

}
