# local_nodejs

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_nodejs]](#setup)
    * [What [local_nodejs] affects](#what-[local_nodejs]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_nodejs]](#beginning-with-[local_nodejs])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs NodeJS, dependencies and the PM2 NodeJS application.

NodeJS is a JavaScript runtime designed to build scalable network applications.  PM2 (a process manager) enables applications to run in a load-balanced configuration.

## Setup

### Beginning with [local_nodejs]	

include local_nodejs

## Usage

One parameter is used: $nodejs_major_version
This defaults to '7', but may be set to '8' or '9' (at the time of writing).  The most recent LTS version within the major version number will be installed.

## Reference

* Installs: nodejs, gcc-c++ & make from the standard repositories
* Uses the instructions from the NodeJS web site to download and run the installer script.
* Usen npm (node package manager) to install PM2

## Limitations

Tested on RHEL7

## Development

