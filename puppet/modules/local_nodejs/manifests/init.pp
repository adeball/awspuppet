
class local_nodejs (
  $nodejs_major_version = '7',
)
  {

  ensure_packages(['nodejs', 'gcc-c++', 'make', 'java-1.8.0', ])

  exec { 'Download and run NodeJS installer script':
    command => "yum clean all ; curl -sL https://rpm.nodesource.com/setup_${nodejs_major_version}.x | bash - ; yum -y update nodejs",
    path => '/bin:/usr/bin:/usr/sbin',
    unless => "rpm -q nodejs | grep -q \"nodejs-${nodejs_major_version}\"",
  } ->

  exec {'Install pm2':
    command => 'npm install -g pm2',
    path => '/usr/bin:/usr/sbin',
    unless => 'npm list -g pm2',
  }

}
