# local_ohubbuild

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_ohubbuild]](#setup)
    * [What [local_ohubbuild] affects](#what-[local_ohubbuild]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_ohubbuild]](#beginning-with-[local_ohubbuild])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs the NodeJS applications required to support the MYCO OHub application.

## Setup

### Setup Requirements 

Requires NodeJs (which includes npm).
This dependency is satisfied by installing the local-nodejs module.

### Beginning with [local_ohubbuild]	

include local_ohubbuild

## Usage

## Reference

The module installs three NodeJS applications:

* cordova 7.0.1
* ionic 3.3.0
* electron 1.6.2

And configures cordova to switch off telemetry.

## Limitations

Tested on RHEL7

## Development

