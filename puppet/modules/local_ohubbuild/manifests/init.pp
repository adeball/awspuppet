
class local_ohubbuild {

  exec {'Install cordova@7.0.1':
    command => 'npm install -g cordova@7.0.1',
    path => '/usr/bin:/usr/sbin',
    unless => 'npm list -g cordova@7.0.1',
  } ->
  ### There's a weird error using "cordova telemetry off" within puppet
  ### And there's no status command, so check/modify the config file directly instead.
  exec {'Switch cordova telemetry off':
    command => "sed -i -e 's/false/true/' /root/.config/configstore/insight-cordova.json",
    path => '/usr/bin:/usr/sbin',
    unless => 'grep optOut.*true /root/.config/configstore/insight-cordova.json',
  }

  exec {'Install ionic@3.3.0':
    command => 'npm install -g ionic@3.3.0',
    path => '/usr/bin:/usr/sbin',
    unless => 'npm list -g ionic@3.3.0',
  }

  exec {'Install electron@1.6.2':
    command => 'npm install -g electron@1.6.2',
    path => '/usr/bin:/usr/sbin',
    unless => 'npm list -g electron@1.6.2',
  }

}
