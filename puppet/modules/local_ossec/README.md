# local_ossec

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_ossec]](#setup)
    * [What [local_ossec] affects](#what-[local_ossec]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_ossec]](#beginning-with-[local_ossec])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs OSSEC-HIDS, a free, open source host intrusion detection system (https://ossec.github.io/)

OSSEC monitors key directories, files and system areas for evidence of attempted or actual intrusion.  It has a comprehensive database of patterns and configurations to look for and report on.  It also also able to actively block connections from IP addresses which have been found to initiate suspicious activity.

## Setup

### What [local_ossec] affects 

local_ossec:

* Sets up users and groups
* Sets up the repository configuration to enable OSSEC installation
* Installs the ossec-hids and ossec-hids-server packages
* Configures the OSSEC system
* Configures 'ossec-hids' as a service and ensures it is running


### Setup Requirements 

Although not required for operation, OSSEC is made more valuable when it has its actions and messages (recorded in log files) monitored and alerted on by third party monitoring software.  OSSEC is able to send emails, but these are a very blunt instrument, reporting on many false-positives.

### Beginning with [local_ossec]	

include local_ossec

## Usage

Customisation is by modification and generation of files within the 'files' directory of the module.  See next section for details.

## Reference

For a complete discussion of the operation of OSSEC, please refer to: https://ossec.github.io/  The following instructions explain how to make local modifications with Puppet, for distribution to managed servers.

### Modifying rules

One of the key files is files/var/ossec/rules/local_rules.xml (there is also a syslog_rules.xml file in the same location, but this is not as important, and is unlikely to need further modification).

##### local_rules.xml

The local_rules.xml file will mostly be used to prevent the recording of false-positive web-based attacks.  The Maximo application clients typically produce what would appear to be unusual requests, and if not filtered, these are not only recorded, but the IP address from which they originate will be blocked. 

If an HTTP request (or series) has been logged as an attack, check the logs to see if the IP address has been blocked.  If it has, and it is a legitimate client, then you should add a rule to this file.  E.g.

    <rule id="100015" level="0">
        <if_sid>31122</if_sid>
        <match>/meaweb</match>
        <description>Ignore /meaweb* POST requests </description>
    </rule>

Where:
* The rule id is an unused number (so increment from the latest entry).
* The 'sid' identifies the rule which matched the request(s) in question - this should be logged if you need to look it up
* match is a glob style string to match in the request

##### mkconf, mk_ossec_conf.sh and conf/*.cfg

If files (with the extension .cfg) are created in the conf directory, these are concatenated to create the main ossec.conf file.  The order of addition is:
* defaults.cfg
* fqdn, servertype, env, client, env.client, servertype.env, servertype.client, servertype.env.client (so e.g.  tst.rwg.cfg will apply to all RWG test servers)
See the mk_ossec_conf.sh script for details.  This script should be automatically run whenever any config files (or the script itself) is updated and deployed to the Puppet server.

## Limitations

Tested on RHEL7 & Amazon Linux

## Development

