#!/bin/bash 

sourcedir=/var/ossec/mkconf/conf
outf=/var/ossec/etc/ossec.conf


### Header
cat > $outf << EOF

<!-- OSSEC config -->

<ossec_config>

<!-- The email here gets *everything*, but if it's removed, no one else gets anything -->
<!-- So, send to the local ossec user -->
  <global>
     <email_notification>no</email_notification>
     <smtp_server>127.0.0.1</smtp_server>
     <email_to>ossec@$(hostname)</email_to>
     <email_from>ossec@$(hostname)</email_from>

     <white_list>81.139.49.26</white_list>   <!-- MYCO public IP -->
     <white_list>10.0.0.0/8</white_list>     <!-- MYCO client IP range, and GTR via IPsec VPN -->
     <white_list>14.0.0.0/8</white_list>     <!-- MYCO internal IP range -->
     <white_list>85.255.232.0/21</white_list> <!-- GTR mobile training 2017-06-15 -->

     <white_list>195.11.246.0/24</white_list> <!-- RWG #1 There are more, and it needs coding in, but for now this covers most-->
     <white_list>195.11.163.0/24</white_list> <!-- RWG #2 -->
     <white_list>216.118.201.0/24</white_list> <!-- RWG #3 -->

   </global>

   <!--email_alerts-->
     <!-- email_to>adrian.ball@myco.com</email_to-->
     <!-- level>7</level-->
   <!--/email_alerts-->


EOF


cat $sourcedir/defaults.cfg >> $outf

client=$(echo $ClOUDCLIENT | tr [A-Z] [a-z])
env=$(echo $ClOUDENV | tr [A-Z] [a-z])
servertype=$CLOUDSERVERTYPE
zone=$CLOUDDOMAIN
fqdn=$host.$env.$client.$zone

for f in $fqdn.cfg $servertype.cfg $env.cfg $client.cfg $env.$client.cfg $servertype.$env.cfg \
    $servertype.$client.cfg $servertype.$env.$client.cfg ; do
    [[ -f $sourcedir/$f ]] && cat $sourcedir/$f >> $outf
done

echo >> $outf

### Footer
cat >> $outf << EOF
</ossec_config>
EOF

semanage fcontext -a -t var_log_t '/var/ossec/logs/ossec.log'
semanage fcontext -a -t var_log_t '/var/ossec/logs/active-responses.log'

touch /var/ossec/ossec_config_created
systemctl restart ossec-hids
