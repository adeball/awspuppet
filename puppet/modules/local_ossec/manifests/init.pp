
class local_ossec {

  $releasever=hiera('common::releasever')

  group { 'ossec': gid => 2004 } ->
  user { 'ossec':   ensure => present, uid => 2202, gid => ossec, home => '/var/ossec', managehome => true, } ->
  user { 'ossecr':  ensure => present, uid => 2204, gid => ossec, home => '/var/ossec', managehome => true, } ->
  user { 'ossecm':  ensure => present, uid => 2205, gid => ossec, home => '/var/ossec', managehome => true, } ->
  user { 'ossece':  ensure => present, uid => 2206, gid => ossec, home => '/var/ossec', managehome => true, } ->



  file {'/etc/yum.repos.d/ossec.repo': ensure => absent } ->
  file {'/etc/yum.repos.d/atomic.repo':
    ensure => present,
    content => template("local_ossec/etc/yum.repos.d/atomic.repo"),
    owner => root,
    mode => 644,
  } ->
  file {'/etc/pki/rpm-gpg/RPM-GPG-KEY.art.txt':
    ensure => present,
    source => 'puppet:///modules/local_ossec/etc/pki/rpm-gpg/RPM-GPG-KEY.art.txt',
    owner => root,
    mode => 644,
  } ->
  file {'/etc/pki/rpm-gpg/RPM-GPG-KEY.atomicorp.txt':
    ensure => present,
    source => 'puppet:///modules/local_ossec/etc/pki/rpm-gpg/RPM-GPG-KEY.atomicorp.txt',
    owner => root,
    mode => 644,
  } ->
  package {['ossec-hids','ossec-hids-server']: ensure => present } ->
  file {'/var/ossec/mkconf': ensure => directory } ->
  file {'/var/ossec/mkconf/conf':
    ensure => directory,
    source => 'puppet:///modules/local_ossec/var/ossec/mkconf/conf',
    recurse => true,
  } ->
  file {'/var/ossec/mkconf/mk_ossec_conf.sh':
    ensure => present,
    source => 'puppet:///modules/local_ossec/var/ossec/mkconf/mk_ossec_conf.sh',
    owner => root,
    mode => 755,
  } ->
    file {'/var/ossec/rules/syslog_rules.xml':
      ensure => present,
      source => 'puppet:///modules/local_ossec/var/ossec/rules/syslog_rules.xml',
      owner => root,
      mode => 644,
    } ->
    file {'/var/ossec/rules/local_rules.xml':
      ensure => present,
      source => 'puppet:///modules/local_ossec/var/ossec/rules/local_rules.xml',
      owner => root,
      mode => 644,
    } ->
  exec {'Create OSSEC configuration':
    path => '/usr/bin:/usr/sbin:/bin',
    command => '/var/ossec/mkconf/mk_ossec_conf.sh',
    subscribe => [File['/var/ossec/mkconf/conf'],File['/var/ossec/mkconf/mk_ossec_conf.sh'],],
    refreshonly => true,
  } ->

  ### This link doesn't seem to get backed up to S3, so if a system is restored, we need
  ### To make sure it's created
  file {'/var/ossec/bin/ossec-control':
    ensure => link,
    target => '/var/ossec/bin/ossec-server.sh',
  } ->

  service {'ossec-hids':
    ensure => running,
    enable => true,
    require => Package['ossec-hids', 'ossec-hids-server'],
    subscribe => File['/var/ossec/rules/local_rules.xml'],
  }


  ### This is purely to stop Xymon complaining if a host hasn't created this log
  file {'/var/ossec/logs/active-responses.log':
    ensure => present,
    owner => root,
    group => ossec,
    mode => 0644,
    require => Package['ossec-hids'],
  }

}
