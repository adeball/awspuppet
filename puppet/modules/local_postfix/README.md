# local_postfix

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_postfix]](#setup)
    * [What [local_postfix] affects](#what-[local_postfix]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_postfix]](#beginning-with-[local_postfix])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Configures the postfix mail service.

On RHEL7, postfix is installed by default, but not configured.  This module configures it to work within the AWS environment, using the appropriate AWS relay host, user and password.  It requires parameterisation.

## Setup

### What [local_postfix] affects 

Various other packages are required, this module ensures they are installed:

* cyrus-sasl
* cyrus-sasl-plain
* cyrus-sasl-md5
* cyrus-imapd
* openssl
* openssl-devel

If the module is installed, it ensures the postfix service stays running.


### Setup Requirements 

### Beginning with [local_postfix]	

include local_postfix

## Usage

The AWS host, user and password are hard-coded - these need parameterising.

## Reference

## Limitations

Tested on RHEL7

## Development

