
class local_postfix {

  service {'postfix':
    ensure => running,
  }

  ensure_packages(['cyrus-sasl', 'cyrus-sasl-plain', 'cyrus-sasl-md5',
    'cyrus-imapd', 'openssl', 'openssl-devel', ])

  file {'/etc/postfix':
    ensure => directory,
    purge => false,
    recurse => true,
    source => 'puppet:///modules/local_postfix/etc/postfix',
    notify => Service ['postfix'],
  }
}
