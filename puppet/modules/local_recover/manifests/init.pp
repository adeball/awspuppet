class local_recover {

  exec {'Recover from S3 backup':
    path => '/sbin:/bin:/usr/bin:/usr/sbin',
    command => '/usr/local/bin/s3restore.sh',
    unless => 'test -f /puppet_ssl.tar',
  } ->
  exec {'Recover databases from export':
    path => '/sbin:/bin:/usr/bin:/usr/sbin',
    command => '/usr/local/bin/db2restore.sh',
    unless => 'test -f /puppet_ssl.tar',
  }

}

