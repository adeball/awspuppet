# local_s3backup

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_s3backup]](#setup)
    * [What [local_s3backup] affects](#what-[local_s3backup]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_s3backup]](#beginning-with-[local_s3backup])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module backs up an EC2 instance to AWS S3 storage

Documentation here: https://confluence.myco-group.com/display/EC/S3+server+backups


## Setup

### What [local_s3backup] affects

The module installs and configures a number of scripts and files under /usr/local, and sets up a cron jobs back up the server daily, and also perform a check that the latest backup is not out-of-date.

### Setup Requirements 

The EC2 instance should have AWS CLI access, and access to the relevant S3 client bucket.  If the instance was built in the standard way, this should be already configured.

### Beginning with [local_s3backup]	

include local_s3backup

## Usage

The module applies suitable defaults, except for the S3 bucket, which should be defined.

  backupbase = 'somes3bucket' (S3 bucket, which the instance should have r/w access to)
  $starthour = '0' (When to start the backup, hour)
  $startminute = '0' (When to start the backup, minute)
  $randomwait = '10800' (#seconds to wait before starting, default is three hours, this allows mulitple instances to be scheduled the same way, but each start at a different time)
  

## Reference

## Limitations

Tested on RHEL7 and Amazon Linux

## Development

