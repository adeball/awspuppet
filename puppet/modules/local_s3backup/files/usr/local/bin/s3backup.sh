#!/usr/bin/env bash
#
# Call with -s seconds to sleep a random number of seconds
#

fail()
{
    echo
    echo "Failure: $@"
    echo
    exit 1
}

getopts s: s
secs=$OPTARG

. /etc/profile

if [[ ! -z ${secs} ]] ; then
    rnd=$(( $RANDOM * $secs / 32768 + 1 ))
    echo "Sleeping for $rnd seconds..."
    sleep $rnd
fi

CFGDIR=/usr/local/etc/backup
fqdn=$(hostname)
latestdir="latest"               # Preferred name for the most recent backup directory
today=$(date +%Y-%m-%d)
client=$(echo $CLOUDCLIENT | tr '[A-Z]' '[a-z]')
env=$(echo $CLOUDENV | tr '[A-Z]' '[a-z]')
servertype=$CLOUDSERVERTYPE
backupbase=$(cat $CFGDIR/s3_location)
bucketname=${backupbase}-$client
hostname=$(hostname -s)
BUCKET=s3://$bucketname
TARGET=$BUCKET/$env/${CLOUDSERVERTYPE}-$CLOUDINSTANCE
current=${TARGET}/$latestdir      # Most recent backup directory, S3 URL


### How many of each frequency to keep
case $env in
    "prd")
        NDAILY=5
        NWEEKLY=4
        NMONTHLY=6
        NYEARLY=3
     ;;
     *)
        NDAILY=3
        NWEEKLY=2
        NMONTHLY=0
        NYEARLY=0
     ;;
esac


### Check for a previous backup, and rotate/purge if it exists
aws s3 ls $current > /dev/null
if [[ $? -eq 0 ]] ; then


    currentdate=$( aws s3 cp $TARGET/$latestdir/metadata.txt - | \
        awk -F: '/datetime/ {split ($2, a, "-"); printf "%s-%s-%s\n",a[1],a[2],a[3]}')
    ### Daily copies - move the latest backup (hopefully yesterday's) to daily/date
    ### and purge older versions

    aws s3 sync $current ${TARGET}/daily/$currentdate
    aws s3 ls ${TARGET}/daily/ | head -n -${NDAILY} | awk '{print $2}' | while read d ; do
        aws s3 rm ${TARGET}/daily/$d --recursive
    done


    ### Weekly copies - move the latest backup (hopefully yesterday's) to weekly/date
    ### and purge older versions

    if [[ $(date +%a) == "Fri" && $NWEEKLY -gt 0 ]] ; then
        aws s3 sync $current ${TARGET}/weekly/$currentdate
        aws s3 ls ${TARGET}/weekly/ | head -n -${NWEEKLY} | awk '{print $2}' | while read d ; do
            aws s3 rm ${TARGET}/weekly/$d --recursive
        done
    fi


    ### Monthly copies - move the latest backup (hopefully yesterday's) to monthly/date
    ### and purge older versions

    if [[ $(date +%d) == "12" && $NMONTHLY -gt 0 ]] ; then
        aws s3 sync $current ${TARGET}/monthly/$currentdate
        aws s3 ls ${TARGET}/monthly/ | head -n -${NMONTHLY} | awk '{print $2}' | while read d ; do
            aws s3 rm ${TARGET}/monthly/$d --recursive
        done
    fi


    ### Yearly copies - move the latest backup (hopefully yesterday's) to yearly/date
    ### and purge older versions

    if [[ $(date +%d) == "12" && $(date +%m) == "05" && $NYEARLY -gt 0 ]] ; then
        aws s3 sync $current ${TARGET}/yearly/$currentdate
        aws s3 ls ${TARGET}/yearly/ | head -n -${NYEARLY} | awk '{print $2}' | while read d ; do
            aws s3 rm ${TARGET}/yearly/$d --recursive
        done
    fi

fi


### Create list of files and directories to be backed up
filelist=$(mktemp) ; > $filelist

cd $CFGDIR

echo
echo "Backing up $(hostname) - $(date)"
echo
echo "Obtaining backup list from $CFGDIR"
echo

for f in all $hostname $fqdn $servertype $env $client $env.$client \
            $servertype.$env $servertype.$client $servertype.$env.$client ; do

    echo Checking for $f.cfg
    if [[ -f ${f}.cfg ]] ; then
        echo "Appending $f.cfg to filelist:$filelist"
        cat $f.cfg >> $filelist
    fi

done

echo
echo "Backing up the following files and directories..."
echo
cat $filelist


### Perform the backup
log=$(mktemp)

echo > $log
echo "Backing up to: $current" >> $log
echo >> $log

while read t ; do

    ### We can use tar to safely save perms/ownerships/links etc, as it's GNU tar
    ### Don't need to worry about slashes in the filenames either as S3 will simply
    ### create the "folder" in the path to the file.
    ### NB: because paths start with "/", don't add an extra one in the path to the
    ### tarfile.

    ### Even tar individual files, should they be listed, to save perms
    ### (it's simpler than making them a special case and using the s3 ACL save option).

    echo "Backing up $t - $(date)" >> $log
    nice -n 10 tar cvfzSp 2>> $log - "$t" | aws s3 cp - ${current}"$t".tar.gz

done < $filelist

### Save some metadata
metadata=$(mktemp)
echo "host:$fqdn" > $metadata
echo "dayno:$(($(date --utc --date "$1" +%s)/86400))" >> $metadata
echo "datetime:$(date +%Y-%m-%d-%H-%M)" >> $metadata

aws s3 cp $log ${current}/backup.lst
aws s3 cp $metadata ${current}/metadata.txt
echo | aws s3 cp - ${current}/this_is_a_backup_of_$hostname


rm $filelist $log $metadata
