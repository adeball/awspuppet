#!/bin/bash
#
# Checks that the most recent S3 backup is no more than many hours old
# default, 48 hours for warning, 168 hours (a week) for panic
#

. /etc/profile

WARN=48
PANIC=168

fail()
{
    echo
    echo "Failure: $@"
    echo
    logger -p user.error "S3 Backup check critical: $@"
    exit 1
}

warn()
{
    echo
    echo "Warning: $@"
    echo
    logger -p user.warning "S3 Backup check warning: $@"
    exit 1
}

CFGDIR=/usr/local/etc/backup
backupbase=$(cat $CFGDIR/s3_location)
bucketname=${backupbase}-$client

### Backups
listfile=$AWS_BACKUP/latest/backup.lst 
latestdate=$(aws s3 ls $listfile|awk '{print $1, $2}')

[[ -z "$latestdate" ]] && fail "Cannot find the date for $listfile"

lateststamp=$(date --utc --date "$latestdate" +%s)
nowstamp=$(date --utc +%s)

hoursdiff=$(( ( $nowstamp - $lateststamp ) / 3600 ))

if [[ $hoursdiff -gt $PANIC ]] ; then
    fail "Latest backup is > $PANIC hours old"
elif [[ $hoursdiff -gt $WARN ]] ; then
    warn "Latest backup is > $WARN hours old"
fi
