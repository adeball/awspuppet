#!/usr/bin/env bash
#

fail()
{
    echo
    echo "Failure: $@"
    echo
    exit 1
}

. /etc/profile


fqdn=$(hostname)
today=$(date +%Y-%m-%d)
client=$(echo $CLOUDCLIENT | tr [A-Z] [a-z])
env=$(echo $CLOUDENV | tr [A-Z] [a-z])
servertype=$CLOUDSERVERTYPE
hostname=$(hostname -s)
TARGET=$AWS_BACKUP
BUCKET=$(echo $AWS_BACKUP | awk -F/ '{print $3}')

latestdir="latest"               # Preferred name for the most recent backup directory
current=${TARGET}/$latestdir      # Most recent backup directory, S3 URL

### Prevent login problems, set SELinux to permissive mode
setenforce 0

### Save current puppet certificate
tar cvf /puppet_ssl.tar /var/lib/puppet/ssl

### Recover everything to their original locations
cd /

for tarfile in $(aws s3 ls $current --recursive | awk ' /.tar.gz$/ {print $NF}' | while read tf; do echo s3://$BUCKET/$tf ; done) ; do

	echo "Recovering $tarfile"
	aws s3 cp $tarfile - | tar xvfz -

done


### This needs recreating - no idea why!
mkdir -p /var/run/munin ; chown munin /var/run/munin

### Syslog needs kicking after the restore
systemctl restart rsyslog

### Stop puppet, reinstate the certs, restart
/etc/init.d/puppet stop
tar xvf /puppet_ssl.tar
/etc/init.d/puppet start

echo "Now configure the system with a version != 'recover'"

