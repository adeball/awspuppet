#!/usr/bin/env bash
#

fail()
{
    echo
    echo "Failure: $@"
    echo
    exit 1
}

. /etc/profile


### Ensure LVM2 is available
yum -y install lvm2

### Disable, even though it won't be working at the moment due to mismatched certs
puppet agent --disable "Recovering from backup"

#BUCKET=s3://myco-cloud-backup
fqdn=$(hostname)
#TARGET=$BUCKET/$fqdn
today=$(date +%Y-%m-%d)
client=$(echo $CLOUDCLIENT | tr [A-Z] [a-z])
env=$(echo $CLOUDENV | tr [A-Z] [a-z])
servertype=$CLOUDSERVERTYPE
hostname=$(hostname -s)
BUCKET=s3://myco-cloud-backup-$client
TARGET=$BUCKET/$env/${CLOUDSERVERTYPE}-$CLOUDINSTANCE
latestdir="latest"               # Preferred name for the most recent backup directory
current=${TARGET}/$latestdir      # Most recent backup directory, S3 URL

### Set up any LVM volumes/filesystems before recovering
puppet apply --modulepath=/cloudinit/puppet/modules -e "include servertype-${CLOUDSERVERTYPE}::storage"

echo "If the above did not create LVM volumes, hit ctrl-C now and fix it"
sleep 30

### Prevent login problems, set SELinux to permissive mode
setenforce 0

### Recover everything to their original locations
cd /

for tarfile in $(aws s3 ls $current --recursive | awk ' /.tar.gz$/ {print $NF}') ; do

	recopath=$(echo $tarfile | sed -e 's-.*latest/-/- ; s/.tar.gz$//')
	echo "Recovering $BUCKET/$tarfile to $recopath"

	aws s3 cp $BUCKET/$tarfile - | tar xvfz -

done


### This needs recreating - no idea why!
mkdir -p /var/run/munin ; chown munin /var/run/munin

### Syslog needs kicking after the restore
systemctl restart rsyslog

### Stop and restart puppet, as the running agent won't have inherited the
### CLOUD* facts (so Puppet will be unaware of environment, servertype etc)
systemctl restart puppetagent

### Reenable puppet and run a handful of times
puppet agent --enable

