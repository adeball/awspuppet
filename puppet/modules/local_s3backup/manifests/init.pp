
class local_s3backup(
  $backupbase = hiera('common::backupbase'),
  $starthour = '0',
  $startminute = '0',
  $randomwait = '10800'
){

  file {'/usr/local/bin/s3backup.sh':
    ensure => present,
    owner => root,
    mode => 0755,
    source => 'puppet:///modules/local_s3backup/usr/local/bin/s3backup.sh'
  }

  file {'/usr/local/bin/s3restore.sh':
    ensure => present,
    owner => root,
    mode => 0755,
    source => 'puppet:///modules/local_s3backup/usr/local/bin/s3restore.sh'
  }

  file { '/usr/local/bin/s3backup_check.sh':
    ensure => present,
    owner  => root,
    mode   => 0755,
    source => 'puppet:///modules/local_s3backup/usr/local/bin/s3backup_check.sh'
  }

  file {'/usr/local/etc/backup':
    ensure => directory,
    owner => root,
    recurse => true,
    purge => true,
    source => 'puppet:///modules/local_s3backup/usr/local/etc/backup',
  } ->
  file {'/usr/local/etc/backup/s3_location':
    ensure => present,
    owner => root,
    mode => 0644,
    content => "$backupbase\n",
  }

  ### Schedule backups to run at a random time up to three hours after midnight (default)
  ### This will help to stagger S3 access
  cron {'S3 file backup':
    command => "/usr/local/bin/s3backup.sh -s $randomwait",
    user => 'root',
    hour => "$starthour",
    minute => "$startminute",
  }


  ### Moved from hourly to daily
  file {'/etc/cron.hourly/s3backup_check.sh':
    ensure => absent,
  }
  file {'/etc/cron.daily/s3backup_check.sh':
    ensure => link,
    target => '/usr/local/bin/s3backup_check.sh',
  }

}
