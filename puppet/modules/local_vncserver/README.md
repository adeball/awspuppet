# local_vncserver

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_vncserver]](#setup)
    * [What [local_vncserver] affects](#what-[local_vncserver]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_vncserver]](#beginning-with-[local_vncserver])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs and configures VNC server, and X11 software to support it.

See: https://confluence.myco-group.com/display/EC/Login+access for more information.
This enables a GUI interface to be used, which is useful when setting up/troubleshooting IBM software.  The module also includes a mechanism which kills old VNC sessions (based on last use time), as they tend to hang around and can therefore present a security risk if left.

## Setup

### What [local_vncserver] affects 

On RHEL7, a reduced GNOME desktop is installed, along with dependencies.  Amazon Linux has no window manager available.

A default root VNC password is applied (as a binary file) - the text password is in LastPass

### Setup Requirements

### Beginning with [local_vncserver]	

include local_vncserver

## Usage


## Reference


## Limitations

* Tested and works fine on RHEL7
* Tested and works fine on Amazon Linux, but there is no Window manager available, so it is of very limited use here.

## Development

