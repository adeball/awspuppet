class local_vncserver (
  $maxroot = 86400,
  $maxuser = 604800
) {

  ### VNC server
  firewall { '05901 allow VNC clients to connect':
    dport  => ['5901-5909'],
    proto  => 'tcp',
    action => 'accept',
  }

  if $operatingsystem != "Amazon" {

    exec { 'Install reduced Gnome desktop environment':
      command => '/usr/bin/yum -y --disableexcludes=all groupinstall "X Window System" ;
                  yum -y install nfs-utils PackageKit-command-not-found',
      onlyif  => '/usr/bin/yum grouplist "X Window System" | /bin/grep "^Available"',
      timeout => 600,
    }
    exec { 'Remove full Gnome desktop environment':
      command => '/usr/bin/yum -y group remove "Server with GUI" ;
                  yum -y install nfs-utils PackageKit-command-not-found',
      onlyif  => '/usr/bin/yum grouplist "Server with GUI" | /bin/grep "^Installed"',
      timeout => 600,
    }

    #ensure_packages(['lvm2', 'gnome-classic-session', 'gnome-terminal', 'firefox', 'control-center',
    ensure_packages(['lvm2', 'gnome-classic-session', 'gnome-terminal', 'control-center',
      'liberation-mono-fonts', 'zip', 'nss', 'wget', 'terminus-fonts', ])
    file { '/etc/setroubleshoot': ensure => directory, }

    # {
    #   exec { 'yum groupinstall Server with GUI':
    #     command => '/usr/bin/yum -y --disableexcludes=all groupinstall "Server with GUI"',
    #     unless  => '/usr/bin/yum grouplist "Server with GUI" | /bin/grep "^Installed"',
    #     timeout => 600,
    #   }
    # }

  }

  package { ['tigervnc-server', 'tigervnc', 'xorg-x11-font-utils', 'xorg-x11-fonts-100dpi',
    'xorg-x11-fonts-75dpi', ]:
    ensure => present
  }

  file { '/root/.vnc': ensure => directory, }
  file { '/root/.vnc/passwd':
    ensure => present,
    source => 'puppet:///modules/local_vncserver/root/.vnc/passwd',
    owner  => root,
    mode   => 600,
  }

  ### Automate pruning of old sessions
  file { '/usr/local/bin/prunevnc.sh':
    ensure  => present,
    mode    => 0755,
    content => template("local_vncserver/prunevnc.sh.erb"),
  }

  ### Used to do this hourly, but now every 10 minutes for less chance of missing a connection
  file { '/etc/cron.hourly/prunevnc.sh': ensure => absent, }
  cron { 'Prune VNC sessions':
    command => '/usr/local/bin/prunevnc.sh',
    user    => 'root',
    hour    => '*',
    minute  => '*/10',
  }

}

