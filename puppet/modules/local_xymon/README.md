# local_xymon

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_xymon]](#setup)
    * [What [local_xymon] affects](#what-[local_xymon]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_xymon]](#beginning-with-[local_xymon])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module installs the Xymon monitoring software client (and optionally, the server).

Some configuration documentation is available here: https://confluence.myco-group.com/display/EC/Xymon+Configuration


## Setup

### What [local_xymon] affects 

#### Client

* Creates xymon user & group
* Sets up the yum repository to install xymon
* Installs the client
* Configures the client as a service
* Starts the client service
* Sets up IPTables
* Sets up permissions on /var/log/messages
* Installs client extension scripts

#### Server

If the 'xymon_type' parameter is set to 'server', then the following is also executed:

* The xymon server package is installed
* The default gifs are replaced by a rectangular set
* /run & /run/xymon are created if they do not exist
* The 'mkconf' directory is transferred
* The repository host_table and mkconf directory is used to build the configurations
* Various links and directories are created under /etc and /usr/local

### Setup Requirements

* A Xymon server requires the host_table to be available under /cloudinit/etc (this needs updating).
* The hostname 'xymon' shoudl resolve and point to the xymon server in the client environment.
* The clients should be able to connect to the server on TCP port 1984

### Beginning with [local_xymon]	

'include local_xymon' will install the client software, and communicate with the server 'xymon'.

## Usage

See the documentation at: https://confluence.myco-group.com/display/EC/Xymon+Configuration 
When installing the server, set the $xymon_type parameter to 'server' (default is 'client').

## Reference

## Limitations

Tested on RHEL7 & Amazon Linux

## Development

This needs tidying and parameterising - it should be getting replaced by Icinga2 in due course, so effort should be kept to a minimum.

