#!/bin/bash
#
# connect.sh
#
# Directly tests connection from NodeJS servers to the connect URL
#

. /etc/profile


[[ $CLOUDENV == "PRD" ]] && cloudenv="" || cloudenv=$(echo $CLOUDENV | tr '[A-Z]' '[a-z]')
cloudclient=$(echo $CLOUDCLIENT | tr '[A-Z]' '[a-z]')
TLD="myco-cloud.com"

URL="https://${cloudclient}ohub${cloudenv}.myco-cloud.com/nodejs/meta"


COLUMN=connect              # Name of the column

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"

statf=$(mktemp)
msgf=$(mktemp)
echo "green" > $statf

echo "Check $URL" > $msgf

tf=$(mktemp)
curl -si $URL > $tf
echo >> $tf

http_response=$(awk '/^HTTP/ {print $2}' $tf)
if [[ "$http_response" != 200 ]] ; then
    echo "yellow" > $statf

    echo                                                     >> $msgf
    echo "HTTP response is not ok (response=$http_response)" >> $msgf
    echo                                                     >> $msgf
    echo "Output below"                                      >> $msgf
    echo                                                     >> $msgf
    cat $tf                                                  >> $msgf
else
    echo "HTTP response ok ($http_response)" >> $msgf
fi

COLOR=$(cat $statf)

# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

$(cat $msgf)
"

rm $msgf $statf
exit 0

