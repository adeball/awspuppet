#!/bin/bash
#
# Xymon graphs are not massively flexible, and always auto-scale to the largest value
# Log scales cannot be used, unless the value is stored as a log.
#
# As some values are in the tens, and some potentially sit low, but peak to several thousands,
# use a log2 scale for all.
# NB: We're not expecting values below one, so fudge it and add one before taking the log
#     not strictly accurate, but shows zeroes as zero on the graph.
#


COLUMN=db2              # Name of the column
LOCKWARN=5000
LOCKPANIC=5000000

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"

statf=$(mktemp)
snapf=$(mktemp)
msgf=$(mktemp)
echo "green" > $statf
> $msgf

dbs=$(echo "list db directory" | sudo -i -u db2admin db2 | awk '/Database name/ {print $NF}')

MAXDB=${CLOUDCLIENT}MX${CLOUDENV}

for db in $dbs ; do

    echo "get snapshot for database on $db" | sudo -i -u db2admin db2 > $snapf

    ### Application connections
    appsconn=$(awk '/Applications connected currently/ {print $NF}' $snapf)
    appsconnlog=$(echo $appsconn | awk '{print log($1 + 1)/log(2)}')
    echo ${db}appconnlog: $appsconnlog >> $msgf

    ### Locks held
    locksheld=$(awk '/Locks held currently/ {print $NF}' $snapf)
    locksheldlog=$(echo $locksheld | awk '{print log($1 + 1)/log(2)}')
    echo ${db}locksheldlog: $locksheldlog >> $msgf

    if [[ $locksheld -ge $LOCKPANIC ]] ; then
        echo "red" > $statf
        echo "Alert: ${db} #locks held is > $LOCKPANIC" >> $msgf
    elif [[ $locksheld -ge $LOCKWARN ]] ; then
        echo "yellow" > $statf
        echo "Warning: ${db} #locks held is > $LOCKWARN" >> $msgf
    fi

    ### Memory
    MemMB=$(awk '/Current size/ {s+=$NF} END {print s/1024/1024}' $snapf)
    MemMBlog=$(echo $MemMB | awk '{print log($1 + 1)/log(2)}')
    echo ${db}MemMBlog: ${MemMBlog} >> $msgf


    ### #Maximo Users, if we host that DB
    if [[ "$db" == "$MAXDB" ]] ; then
      maxusers=$(echo "db2 connect to $MAXDB ; db2 select MAXSESSIONUID from MAXIMO.LOGINTRACKING where ATTEMPTDATE \
        between \(current date - 1 day \) and current date" | \
        sudo -i -u db2admin | \
        sort -n | \
        uniq -c | \
        awk '$1==1' | \
        wc -l )
        maxuserslog=$(echo $maxusers | awk '{print log($1 +1)/log(2)}')

        echo ${db}MaximoUserslog: $maxuserslog >> $msgf

    fi

done

COLOR=$(cat $statf)

# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

DB2 stats
NB: to make the Y-axis scale work, values are reported as log2(value+1)
Convert e.g. 12.345 with e.g. echo 12.345 | awk '{print 2^$1-1}'

$(cat $msgf)
"

rm $msgf $statf $snapf
exit 0

