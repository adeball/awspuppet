#!/bin/sh

COLUMN=vpn              # Name of the column

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"

msgf=$(mktemp)
statf=$(mktemp)
logf=$(mktemp)
tmpf=$(mktemp)

sitedown=0

printf "%s" "$(date)" > $logf

echo > $msgf

while read vpn reqlinkcount sitename ; do

	printf ",%s" "$sitename" >> $logf


	### Sometimes the VPN is briefly reported down, so try another couple of times

	loopcount=0
	linkcount=-1

	while [[ $linkcount -lt $reqlinkcount && $loopcount -le 3 ]] ; do
		aws ec2 describe-vpn-connections --region=eu-west-2 --output=text --vpn-connection-ids=$vpn > $tmpf
		linkcount=$(grep -c "UP" $tmpf)
		loopcount=$(($loopcount + 1))
		sleep 1
	done

	egrep "TAGS|TELEMETRY"  $tmpf >> $msgf
	echo "Required link count=$reqlinkcount, actual=$linkcount" >> $msgf

	if [[ $linkcount -lt $reqlinkcount ]] ; then
		sitedown=$(($sitedown + 1))	
		echo "$sitename VPN tunnel is degraded!" >> $msgf
		echo >> $msgf
		printf "%s" ",DOWN" >> $logf
	else
		printf "%s" ",OK" >> $logf
	fi
	echo >> $msgf
	echo $sitedown > $statf
done << EOF
vpn-a39f5794 1 Go-Ahead House
vpn-8d9f57ba 0 Monument Place
EOF

fmt="%-6s - Connection to %-40s (%-15s) on TCP port %-3s is %s\n"

sitedown=$(cat $statf)
while read server port value sitename ; do
               nc -v -w10 $server $port < /dev/null 2>&1 | grep -q Connected
               if [[ $? -eq 0 ]] ; then
                    printf "$fmt" "OK" "$sitename" $server $port "responding" >> $msgf
                    #echo "OK - Connection to $sitename ($server) on TCP port $port is responding" >> $msgf
                    printf "%s" ",UP  " >> $logf
               else
                    printf "$fmt" "NOT OK" "$sitename" $server $port "not responding" >> $msgf
                    #echo "NOT OK - Connection to $sitename ($server) on TCP port $port is not responding" >> $msgf
                    sitedown=$(( $sitedown + $value ))
                    printf "%s" ",DOWN " >> $logf
               fi
        echo $sitedown > $statf
done << EOF
10.194.221.9  636 1 Monument Place AD Server
10.194.2.18   636 1 Go-Ahead House AD Server
10.194.1.70   21  1 Test Oracle Integration FTP server
10.194.1.165  21  2 Production Oracle Integration FTP server
EOF


### Not required from 2017-07-24
#vpn-8d9f57ba 10.194.221.9 Monument Place
#vpn-a39f5794 10.194.2.18 Go-Ahead House

echo >> $logf

case $(cat $statf) in 
	0)
	COLOR="green"
	;;
	1)
	COLOR="yellow"
	;;
	2)
	COLOR="red"
	;;
esac

if [[ ${COLOR} == "red" || ${COLOR} == "yellow" ]] ; then
        cat >> $msgf << EOF


NB: The VPN telemetry sometimes reports as down, when it is not.  If any of the servers are responding
on the ports tested, then it is obviously ok.  Monument place VPN is not used since Aug 2017, but is still in place.
Go-Ahead house VPN requires only one of the two links to work at any given time.
Check https://confluence.myco-group.com/display/GTRMYCO/VPN+Integration for information.

See also $(hostname -s):/var/log/xymon/gtrvpn_status.log

EOF
fi



MSG="GTR VPN tunnel and end-to-end AD server connection status

$(cat $msgf)
"

# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

${MSG}
"

cat $logf >> /var/log/xymon/gtrvpn_status.log
rm $msgf $statf $logf $tmpf

exit 0

