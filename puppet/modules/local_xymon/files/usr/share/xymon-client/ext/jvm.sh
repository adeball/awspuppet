#!/bin/bash

COLUMN=jvm              # Name of the column
WARN=25
PANIC=10

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"

statf=$(mktemp)
msgf=$(mktemp)
wasf=$(mktemp)
jvmf=$(mktemp)
echo "green" > $statf

### Get the base directory (BASEDIR), WASJVMIGNORE and WASJVMDONOTRUN variables
sudo cat /usr/local/etc/websphere.cfg > $wasf
. $wasf


PROFILEDIR=$BASEDIR/IBM/WebSphere/AppServer/profiles
cd $PROFILEDIR

PROFILES=$(ls)

### mtime -1 not strictly needed, but it can speed up the find when there are loads of files
logfiles=$(find $PROFILEDIR -mtime -1 | grep -i systemout.log$ | xargs grep -i "total memory" | awk -F: '{print $1}' | sort -u)

for logfile in $logfiles ; do
	name=$(echo $logfile | awk -F/ '{print $(NF - 1)}')
	pctram=$(grep "total memory" $logfile | tail -1 | awk '{printf "%0.0d\n", $NF * 100 / $(NF - 6)}')
	[[ $pctram -le $WARN ]] && echo "yellow" > $statf
	[[ $pctram -le $PANIC ]] && echo "red" > $statf
	echo "$name: $pctram" >> $msgf
done

### How much real RAM is *not* used by JVMs in total (so it's consistent with the JVM "available" scale)
### i.e. very low=bad
name="nonJVMrss"
javarss=$(ps -eo rss,cmd | awk '/java/ {s+=$1} END {print s}')
realram=$(free | awk '/Mem/ {print $2}')
pctram=$(echo $javarss $realram | awk '{printf "%0.0d\n", 100 - $1/$2*100}')
[[ $pctram -le $WARN ]] && echo "yellow" > $statf
[[ $pctram -le $PANIC ]] && echo "red" > $statf
echo "$name: $pctram" >> $msgf

COLOR=$(cat $statf)

### IBM output format is annoying with split lines, the awk just puts them together where required.
sudo /etc/init.d/websphere status | awk '
    BEGIN                   { s=0 ; b=""     }
    s==1 && /^[A-Z]/        { print b ; b=$0 }
    s==1 && /^ /            { gsub(/^ */, "", $0) ; print b" "$0 ; s=0 }
    s==0 && /^[A-Z]/        { s=1 ; b=$0 }
    END                     {print}
' > $wasf

servers=$(awk -F: '/Server name/ {print $3}' $wasf | sort)
fmt="%-20s - %s\n"
for server in $servers ; do

    serverstatus=$(awk -F \" -v s="$server" \
    '/The Application Server|The Node Agent|The Deployment Manager|The Web server/ && $2==s {print $3}' $wasf)


    ### Filter the ones which are ok to be down by adding the string 'ignored' to the status
    echo $server | egrep -q "$WASJVMIGNORE|$WASJVMDONOTRUN"
    if [[ $? -eq 0 ]] ; then
        serverstatus="$serverstatus - ignored by monitoring config"
    fi


    ### Normally running JVMs
    echo $serverstatus | grep -v "ignored" | grep -iq "stopped"
    if [[ $? -eq 0 ]] ; then
        [[ "$COLOR" == "green" ]] && COLOR="yellow"
        serverstatus="$serverstatus - yellow alert"
    fi


    ### Finally, warn if a JVM is running, which should not be (e.g. staging servers)
    echo $server | egrep -q "$WASJVMDONOTRUN"
    if [[ $? -eq 0 ]] ; then
        echo $serverstatus | egrep -iq "started|running"
        if [[ $? -eq 0 ]] ; then
            serverstatus=" RUNNING - should not be - red alert"
            COLOR="red"
        fi
    fi


    printf "$fmt" "$server" "$serverstatus" >> $jvmf

done


# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

WAS JVM Server status

$(cat $jvmf)


JVM %memory available

$(cat $msgf)
"

rm $msgf $statf $wasf $jvmf
exit 0

