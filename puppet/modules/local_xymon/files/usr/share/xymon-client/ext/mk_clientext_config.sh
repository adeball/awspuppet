#!/bin/bash

sourcedir=/usr/share/xymon-client/ext
cd $sourcedir
fqdn=$(hostname)

client=$(echo $CLOUDCLIENT | tr [A-Z] [a-z])
env=$(echo $CLOUDENV | tr [A-Z] [a-z])

while read expression scriptname interval ; do

	for f in $fqdn \
	    $CLOUDSERVERTYPE \
	    $env \
	    $client \
	    $env.$client \
	    $CLOUDSERVERTYPE.$env \
		$CLOUDSERVERTYPE.$client \
		$CLOUDSERVERTYPE.$env.$client \
	    $CLOUDSERVERTYPE-$CLOUDINSTANCE.$env \
		$CLOUDSERVERTYPE-$CLOUDINSTANCE.$client \
		$CLOUDSERVERTYPE-$CLOUDINSTANCE.$env.$client \
		; do

		
		if [[ "$f" == "$expression" ]] ; then
			chmod a+x $scriptname
            base=$(basename $scriptname .sh)
            outf=../etc/client.d/$base.cfg
            echo "[$base]" > $outf
            echo "  ENVFILE /etc/xymon-client/xymonclient.cfg" >> $outf
            echo "  CMD \$XYMONCLIENTHOME/ext/$scriptname" >> $outf
            echo "  LOGFILE \$XYMONCLIENTHOME/logs/$base.log" >> $outf
            echo "  INTERVAL $interval" >> $outf
		fi

	done 

done < clientext_table

