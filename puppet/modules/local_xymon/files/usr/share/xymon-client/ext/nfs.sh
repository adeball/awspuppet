#!/bin/bash
#
# This script checks NFS mounted filesystems, if they are hanging (i.e. the server is inaccessible)
# it will unmount them.
# Puppet will eventually remount them after 30 minutes
#
# Run from (and reports to) Xymon
#

COLUMN=nfs              # Name of the column

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"

statf=$(mktemp)
msgf=$(mktemp)
echo "green" > $statf

echo "Mounted nfs4 filesystems" > $msgf
>> $msgf
for fs in $(mount | awk '$5 == "nfs4" {print $3}') ; do

	echo "$fs" >> $msgf

	df $fs > /dev/null &
	dfpid=$!

	sleep 5
	ps -ef -q $dfpid > /dev/null
	if [[ $? -eq 0 ]] ; then
		echo $fs has hung >> $msgf
		echo "yellow" > $statf

		sudo umount $fs &
		sleep 2

		sudo umount -f $fs &
		sleep 2

		sudo umount -l $fs &
		sleep 2

		mount | grep -q $fs
		if [[ $? -eq 0 ]] ; then
			echo "$fs could not be unmounted" >> $msgf
			echo "red" > $statf
		else
			echo "$fs unmounted" >> $msgf
		fi
	fi
done

COLOR=$(cat $statf)
[[ "$COLOR" == "green" ]] && echo "Ok" >> $msgf

MSG="NFS Client Status

$(cat $msgf)
"

# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

${MSG}
"

rm -f $msgf $statf

exit 0

