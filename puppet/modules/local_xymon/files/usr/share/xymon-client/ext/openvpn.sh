#!/bin/sh

COLUMN=vpn              # Name of the column

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"

statf=$(mktemp)
msgf=$(mktemp)
echo "green" > $statf

### Check tun0 exists and is up
ifconfig tun0 | grep -q "UP"
if [[ $? -eq 0 ]] ; then
	echo "tun0 is up" >> $msgf
else
	echo "tun0 is not up" >> $msgf
	echo >> $msgf
	ifconfig tun0 >> $msgf
	echo >> $msgf
	echo "Last 20 lines of /var/log/openvpn*.log" >> $msgf
	tail -20 /var/log/openvpn*.log >> $msgf
	echo "red" > $statf
fi


### We could run a ping here, but actually, as the Xymon traffic is going over the VPN
### we probably know about this already, as all the hosts on the other side of the
### tunnel will appear down.


COLOR=$(cat $statf)

MSG="OpenVPN Status

$(cat $msgf)
"

# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

${MSG}
"

exit 0

