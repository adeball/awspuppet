#!/bin/bash
#
# oraint.sh
#
# Xymon test for files without their equivalent '.go' counterpart, or vice versa.
# Ade <ade@ux1.co.uk>
#
# This file is manage by Puppet
#

### If we're not running through xymon, just echo
[[ -z ${XYMON} ]] && XYMON="echo"
COLUMN=oraint


tmpf=$(mktemp); warnf=$(mktemp); alarmf=$(mktemp); msgf=$(mktemp)

WARNAGE=10	### If a single file is older than this, go yellow
ALARMAGE=30	### If a single file is older than this, go red

touch -d "-${WARNAGE} minutes" $warnf
touch -d "-${ALARMAGE} minutes" $alarmf

warnlist="" ; alarmlist=""
BASEDIR=/shared/maximo/AppTier/integration/oraoutflat

{

echo "Oracle Integration File Check"

checkdirs="oragr orapo oragltxn"

for checkdir in $checkdirs ; do

	echo
	echo
	echo $BASEDIR/$checkdir
	echo

	find $BASEDIR/$checkdir -maxdepth 1 -type f -ls > $tmpf

	if [[ -s $tmpf ]] ; then
		sed -e 's/^/    /' $tmpf
		singles=$(awk '{print $NF}' $tmpf | sed -e 's/\.go$//' | sort | uniq -c | awk '$1==1 {print $2}')

		for single in $singles ; do

			warnmatch=$(find ${single}* ! -newer $warnf)
			alarmmatch=$(find ${single}* ! -newer $alarmf)

			if [[ -n ${alarmmatch} ]] ; then
				alarmlist="$alarmlist $alarmmatch"
			elif [[ -n ${warnmatch} ]] ; then
				warnlist="$warnlist $warnmatch"
			fi
		done


	else
		echo "    No files"
	fi

done


### Xymon output

COLOR="green"

if [[ -n ${warnlist} ]] ; then 
	echo ; echo
	echo "The following file(s) are over $WARNAGE minutes old, and have no partner file (warning):"
	for f in $warnlist ; do echo $f ; done
	COLOR="yellow"
fi

if [[ -n ${alarmlist} ]] ; then 
	echo ; echo
	echo "The following file(s) are over $ALARMAGE minutes old, and have no partner file (alarm):"
	for f in $alarmlist ; do echo $f ; done
	COLOR="red"
fi

} > $msgf

# Tell Xymon about it
$XYMON $XYMSRV "status $MACHINE.$COLUMN $COLOR `date`

$(cat $msgf)
"


rm -f $tmpf $warnf $alarmf $msgf
