#!/bin/bash

cd /tmp
tmpf=$(mktemp)
expand /cloudinit/etc/host_table /cloudinit/etc/host_table_monitoronly | grep "^[a-z]" | sed -e 's/  */ /g' | \
  sort -k 5,5 -k 4,4 -k 2,2 -k 6,6 > $tmpf

cd /usr/share/xymon/etc

getclientname()
{
    myclient="$1"
    while read client vpnint vpnext vpnport vpnpub fullname subnet region awsid; do
        [[ "$client" == "$myclient" ]] && echo "$fullname"
    done < /cloudinit/etc/infrastructure_table

}

getenvname()
{
    myenv="$1"
    while read env fullname ; do
    if [[ "$env" == "$myenv" ]] ; then
        echo "$fullname"
        return
    fi
    done < /cloudinit/etc/env_table

    echo $myenv

}


### hosts.cfg

outf=/etc/xymon/hosts.cfg
echo "title Clients" > $outf


### By client/env

currentenv="none"
currentclient="none"

while read host servertype ip env client instance subnet zone version owner monitor url; do

    if [[ "$monitor" != "ignore" ]] ; then

        CLIENT=$(echo $client | tr [a-z] [A-Z])
        if [[ $currentclient != $client ]] ;then
            echo "page $client $(getclientname $client) ($CLIENT)" >> $outf
            echo "title $(getclientname $client) ($CLIENT)" >> $outf
        fi
        if [[ $currentenv != $env || $currentclient != $client ]] ; then
            ENV=$(echo $env | tr [a-z] [A-Z])
    #        echo "group $CLIENT-$ENV" >> $outf
            echo "group $(getenvname $env)" >> $outf
            currentenv=$env
            currentclient=$client
        fi

        tests=""
        dialup="" ; [[ ${monitor} == "no" ]] && dialup="dialup"
        [[ -z "$url" ]] && url="https://$host.$zone/"
        [[ $servertype == "proxy" ]] && tests="$url $tests"

        ### Subnet = -  is true when the host is from host_table_monitoronly
        if [[ "$subnet" == "-" ]] ; then
            echo "$ip $host.$zone # $dialup $tests NAME:\"$servertype-$instance.$env.$client ($host)\""  >> $outf
        elif [[ ( $CLIENT == $CLOUDCLIENT || $CLOUDCLIENT == "EMS" ) ]] ; then
            echo "$ip $host.$env.$client.$zone ssh # $dialup $tests NAME:\"$servertype-$instance.$env.$client ($host)\" CLASS:$servertype"  >> $outf
        fi
    fi

done < $tmpf


tmpf2=$(mktemp)
sort -k 4,4 -k 5,5 -k 2,2 -k 6,6 $tmpf > $tmpf2


### by env, client

echo "title Environments" >> $outf

currentenv="none"
currentclient="none"
while read host servertype ip env client instance subnet zone version owner monitor url; do

    if [[ "$monitor" != "ignore" ]] ; then

        CLIENT=$(echo $client | tr [a-z] [A-Z])
        if [[ $currentenv != $env ]] ;then
            echo "page $env $(getenvname $env)" >> $outf
            echo "title $(getenvname $env) Environments" >> $outf
        fi
        if [[ $currentenv != $env || $currentclient != $client ]] ; then
            ENV=$(echo $env | tr [a-z] [A-Z])
            echo "group $(getclientname $client) ($CLIENT)" >> $outf
    #        echo "group $CLIENT-$ENV" >> $outf
            currentenv=$env
            currentclient=$client
        fi

        tests=""
        dialup="" ; [[ ${monitor} == "no" ]] && dialup="dialup"
        [[ -z "$url" ]] && url="https://$host.$zone/"
        [[ $servertype == "proxy" ]] && tests="$url $tests"

        ### Subnet = -  is true when the host is from host_table_monitoronly
        if [[ "$subnet" == "-" ]] ; then
            echo "$ip $host.$zone # $dialup $tests NAME:\"$servertype-$instance.$env.$client ($host)\""  >> $outf
        elif [[ ( $CLIENT == $CLOUDCLIENT || $CLOUDCLIENT == "EMS" ) ]] ; then
            echo "$ip $host.$env.$client.$zone ssh # $dialup $tests NAME:\"$servertype-$instance.$env.$client ($host)\" CLASS:$servertype"  >> $outf
        fi

    fi


done < $tmpf2

chown root:xymon $outf





### analysis.d

sourcedir=/usr/share/xymon/mkconf/analysis

cp $sourcedir/analysis.cfg /etc/xymon/analysis.cfg
chown root:xymon /etc/xymon/analysis.cfg

while read host servertype ip env client instance subnet zone version owner monitor url ; do

    if [[ "$monitor" == "yes" ]] ; then

        fqdn=$host.$env.$client.$zone
        hostf=/etc/xymon/analysis.d/$fqdn.cfg

        ### Only include auto-build servers
        if [[ "$subnet" != "-" ]] ; then
            excludef=$(mktemp)
            outf=$(mktemp)
                echo "HOST=$fqdn" > $outf
                sed -e 's/^/    /' $sourcedir/all.cfg >> $outf
                for f in $host $fqdn $servertype $env $client $env.$client $servertype.$env $servertype.$client $servertype.$env.$client ; do

                    [[ -f $sourcedir/$f.exclude ]] && cat $sourcedir/$f.exclude >> $excludef
                    [[ -f $sourcedir/$f.cfg ]] && sed -e 's/^/    /' $sourcedir/$f.cfg >> $outf

                done >> $outf

            if [[ -s $excludef ]] ; then
                egrep -v "$(cat $excludef | awk '{printf "%s|", $0}' | sed -e 's/|$//')" $outf > $hostf
            else
                cp $outf $hostf
            fi
            echo >> $hostf
            rm $outf $excludef
        fi
    fi

done < $tmpf
rm $tmpf $tmpf2

chown -R root:xymon /etc/xymon/analysis.d
chmod -R a+r /etc/xymon/analysis.d

### client-local.cfg is currently just a straight copy
### Add to this script, and remove from puppet control (local_xymon module) if it needs
### to be generated.


### alerts.cfg



## Don't need this, as puppet should do it
#systemctl restart xymonlaunch
