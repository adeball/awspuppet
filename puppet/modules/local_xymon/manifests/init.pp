
class local_xymon (
  $xymon_type = 'client',
) {

  $releasever=hiera('common::releasever')

  ### Client and server

  group { 'xymon': gid => 2005 } ->
  user { 'xymon':
    ensure => present,
    uid => 2203,
    gid => xymon,
    groups => ['adm', 'ossec', 'puppet', 'admin', ],
    home => '/usr/share/xymon-client',
    comment => "MYCO Xymon monitor ($hostname)",
    managehome => true,
  } ->

  file { '/etc/yum.repos.d/xymon.repo':
    ensure => present,
    mode => 0755,
    content => template("local_xymon/etc/yum.repos.d/xymon.repo"),
  } ->

  package { 'xymon-client': ensure => present }


  ### Server

  if $xymon_type == 'server' {

    $xroot = '/usr/share/xymon'

    package { ['xymon']: ensure => present }
    file {'/etc/httpd/conf.d/xymon.conf': ensure => present, source => 'puppet:///modules/local_xymon/etc/httpd/conf.d/xymon.conf' }
    file {"$xroot/www/gifs": ensure => link, target => "$xroot/static/gifs"}
    file {"$xroot/www/help": ensure => link, target => "$xroot/static/help"}
    file {"$xroot/www/menu": ensure => link, target => "$xroot/static/menu"}
    file {"$xroot/www/rep": ensure => link, target => "/var/cache/xymon/rep"}
    file {"$xroot/www/snap": ensure => link, target => "/var/cache/xymon/snap"}

    file {'/run': ensure => directory, owner => root, group => root, mode => 0755,}
    file {'/run/xymon': ensure => directory, owner => xymon, mode => 0755,}

    file {"$xroot/mkconf":
      ensure => directory,
      source => "puppet:///modules/local_xymon/$xroot/mkconf",
      recurse => true,
      purge => true,
    } ->
    file {"$xroot/mkconf/mk_hosts_config.sh":
      owner => 'xymon',
      mode => 0755,
      source => "puppet:///modules/local_xymon/$xroot/mkconf/mk_hosts_config.sh",
    } ->
    exec {'Create xymon hosts config':
      command => "$xroot/mkconf/mk_hosts_config.sh",
      path => '/usr/bin:/usr/sbin',
      notify => Service['xymonlaunch'],
      subscribe => [
          File["$xroot/mkconf"],
          File['/cloudinit/etc/host_table'],
          File['/cloudinit/etc/host_table_monitoronly'],
          File["$xroot/mkconf/mk_hosts_config.sh"],
        ],
      refreshonly => true,
    } ->
      file {'/usr/share/xymon/static/gifs':
        ensure => directory,
        purge => false,
        recurse => true,
        sourceselect => all,
        source => ['puppet:///modules/local_xymon/usr/share/xymon/themes/ModernLED',
          'puppet:///modules/local_xymon/usr/share/xymon/themes/Common',]
      }

    file {"/etc/xymon":
      ensure => directory,
      owner => root,
      recurse => true,
      purge => false,
      source => "puppet:///modules/local_xymon/$xroot/etc",
      notify => Service['xymonlaunch'],
    } ->
    file {"$xroot/etc":
      ensure => link,
      target => '/etc/xymon',
    }

    file {'/usr/local/man':               ensure => directory, owner => root, mode => 755, } ->
    file {'/usr/local/man/man1':          ensure => directory, owner => root, mode => 755, } ->
    file {'/usr/local/man/man1/xymonq.1': ensure => present, owner => root, mode => 644, source => 'puppet:///modules/local_xymon/usr/local/man/xymonq.1', }
    file {'/usr/local/bin/xymonq': ensure => present, owner => root, mode => 755, source => 'puppet:///modules/local_xymon/usr/local/bin/xymonq', }
    file {'/etc/xymonq.cfg':       ensure => present, owner => root, mode => 644, source => 'puppet:///modules/local_xymon/etc/xymonq.cfg', }

    ### This has the same content as the xymon-client file, and causing duplicate errors if present
    file {'/etc/logrotate.d/xymon':
      ensure => absent,
    }

    file{'/usr/share/xymon/bin/bb-hostsvc.cgi':
      ensure => link,
      target => '/usr/share/xymon/bin/svcstatus.cgi',
    }
    ### This one needs to be a hard link
    exec {'Create hard link /usr/share/xymon/cgi-bin/bb-hostsvc.sh':
      command => 'ln /usr/share/xymon/cgi-bin/svcstatus.sh /usr/share/xymon/cgi-bin/bb-hostsvc.sh',
      path => '/bin',
      creates => '/usr/share/xymon/cgi-bin/bb-hostsvc.sh',
    }

  }

  ### Client

  file { '/etc/sysconfig/xymon-client':
    ensure => present,
    content => "XYMONSERVERS=xymon\n"
  }

  service { ['xymonlaunch']: ensure => 'running', enable => 'true', }

  firewall { '01984 Allow inbound Xymon':
    dport    => 1984,
    proto    => tcp,
    action   => accept,
  }

  ### Xymon needs to read /var/log/messages
  exec { 'Set ACL on /var/log/messages':
    command => 'setfacl -m g:adm:r /var/log/messages',
    path => '/bin:/usr/bin',
    unless => 'getfacl /var/log/messages 2>&1 | grep -q group:adm',
  }

  ### Client extension scripts
  $xcbase = '/usr/share/xymon-client'
  file {"$xcbase/ext":
    ensure => directory,
    source => "puppet:///modules/local_xymon/$xcbase/ext",
    recurse => true,
    purge => true,
    mode => 0755,
  } ->
  file { "$xcbase/ext/mk_clientext_config.sh":
    ensure => present,
    source => "puppet:///modules/local_xymon/$xcbase/ext/mk_clientext_config.sh",
    owner => root,
    mode => 755,
  } ->
  exec {'Setup xymon client extension scripts':
    command => "$xcbase/ext/mk_clientext_config.sh",
    path => '/usr/bin:/usr/sbin',
    notify => Service['xymonlaunch'],
    subscribe => [
      File["$xcbase/ext"],
      File["$xcbase/ext/mk_clientext_config.sh"],
    ],
    refreshonly => true,
  }

  ### For upstart
  file {'/etc/init': ensure => directory, mode => 0755} ->
  file {'/etc/init/xymonlaunch.conf':
    ensure => present,
    mode => 0755,
    source => 'puppet:///modules/local_xymon/etc/init/xymonlaunch.conf',
  }
  file {'/etc/init.d/xymonlaunch':
    ensure => link,
    target => '/etc/init.d/xymon-client',
  }


}

