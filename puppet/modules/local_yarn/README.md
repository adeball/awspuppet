# local_yarn

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [local_yarn]](#setup)
    * [What [local_yarn] affects](#what-[local_yarn]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [local_yarn]](#beginning-with-[local_yarn])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up the repository and installs yarn.

## Setup

### Beginning with [local_yarn]	

include local_yarn

## Usage

## Reference

* Installs: yarn

## Limitations

Tested on RHEL7

## Development
