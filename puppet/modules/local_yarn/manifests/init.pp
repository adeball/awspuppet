
class local_yarn
  {

    exec { 'Configure repository and install yarn':
      command => "wget https://dl.yarnpkg.com/rpm/yarn.repo -O /etc/yum.repos.d/yarn.repo; yum -y install yarn",
      path => '/bin:/usr/bin:/usr/sbin',
      unless => "rpm -q yarn",
    }

  }
