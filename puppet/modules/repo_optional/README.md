# repo_optional

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [repo_optional]](#setup)
    * [What [repo_optional] affects](#what-[repo_optional]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [repo_optional]](#beginning-with-[repo_optional])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up the yum RHEL Optional repository.

This enables clients to install packages from the RHEL optional repository.

## Setup

Requires a subscription to RHN (provided as part of the RHEL7 AMI)

### Beginning with [repo_optional]	

include repo_optional

## Usage

## Reference

## Limitations

Tested on RHEL7.  Do not use on Amazon Linux

## Development

