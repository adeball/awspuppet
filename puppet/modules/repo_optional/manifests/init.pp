
class repo_optional {

  exec {'Add RHEL7 optional repository':
    command => 'yum-config-manager --enable rhui-REGION-rhel-server-optional',
    path    => '/usr/bin:/usr/sbin',
    unless  => 'yum repolist enabled | grep -q rhel-server-optional',
  }

}
