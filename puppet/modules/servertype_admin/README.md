# servertype_admin

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_admin]](#setup)
    * [What [servertype_admin] affects](#what-[servertype_admin]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_admin]](#beginning-with-[servertype_admin])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up an EC2 instance as an 'admin' server.  

The admin servertype is applied to systems which act as OS deployment, monitoring and alerting servers.  An admin server does not provide any application services required by clients.

## Setup

### What [servertype_admin] affects 

* Sets up Apache
* Sets up an overview web page
* Installs and configures postfix for sending mail
* (From Hiera): Xymon & Munin are configured as servers
* Configures an admin account which may be used for AWS access to client accounts

### Setup Requirements 

The server must have been built as an admin server (see https://confluence.myco-group.com/display/EC/scripts) - script 'install_admin_server.sh' should have been run at initial installation time, which sets up an admin server as a Puppet master.

### Beginning with [servertype_admin]	

include servertype_admin

## Usage

One parameter may be changed, which is the root HTML directory (defaults to /var/www/html)

## Reference

### Classes

#### ::aws
  * Sets up the admin account (probably 'puppetaws')
  * configures access to the code repository
  * Sets up cron to update AWS info hourly
  
#### ::firewall
  * Configures access to the Puppet master in IPTables
  
#### ::storage
  * Adds swap
  * Adds a separate filesystem for /var/jenkins

## Limitations

Tested on RHEL7.  Amazon Linux is 95% compatible.  Amazon Linux 2 to be tested.

## Development

