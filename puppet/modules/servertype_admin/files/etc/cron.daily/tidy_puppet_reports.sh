#!/bin/bash
cd /var/lib/puppet/reports || exit 1
find . -type f -name *yaml -mtime +4 -exec rm {} \;

