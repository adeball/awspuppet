#!/bin/bash
#
# cloudinitsynchro.sh
#
# Puppet: servertype_admin module
#
# Run by cron

aws s3 sync --delete s3://myco-cloud-puppet/ /cloudinit

### Probably a better way to do this in puppet natively
### but for now, this removes the files left by xymon and munin, causing them
### to re-create their configs if the host_table has changed.

for semaphore in /etc/munin/munin_config_created ; do
   [[ "/cloudinit/etc/host_table" -nt $semaphore ]] && rm -f $semaphore
done
