#!/usr/bin/env bash


. /usr/local/admin_server/etc/admin.cfg

cd
REPORONAME=$(echo $REPORO | sed -e 's-^.*/--;s/\.git$//')

if [[ ! -d $REPORONAME ]] ; then
    ### Force add to ~/.ssh/known_hosts
    REPOSSH=$(echo $REPORO | awk -F: '{print $1}')
    ssh -o StrictHostKeyChecking=no $REPOSSH 2> /dev/null

    git clone $REPORO
    cd $REPORONAME
else
    cd $REPORONAME
    git pull | grep -v "Already up-to-date"
fi

git checkout $ECPREPOBRANCH

cd
chmod 755 $REPORONAME/scripts/common.sh


. $REPORONAME/scripts/common.sh
export ECPETCDIR=~/$REPORONAME/etc
export ECPSCRIPTDIR=~/$REPORONAME/scripts

mkdir -p ~/aws/state && cd ~/aws/state || fail "Cannot mkdir ~aws/state"
for client in $(getclients);
    do mkdir -p $client
    cd $client

    export AWS_DEFAULT_PROFILE=$client
    export AWS_DEFAULT_REGION=$(getregion $client)
    for command in "ec2 describe-instances" "ec2 describe-vpcs" "ec2 describe-subnets" "elbv2 describe-load-balancers" \
    ; do
        out=$(echo $command | sed -e 's/ /./g')
        for format in text json ; do
            aws $command --output=$format > ${out}.$format
        done
     done

    cd ..
done


### DB2 info - no need to run every hour as it's relatively intensive, and the output will barely change

if [[ "$(date +%H)" == "09" ]] ; then
    zone=$(getmasterzone)
    for client in $(getclients) ; do
        cd ~/aws/state/$client
        for server in $(getservers $client) ; do
            stype=$(getservertype $server)
            if [[ "$stype" == "db2" || "$stype" == "cognos" ]] ; then
                ssh -n -o ConnectTimeout=1 -o StrictHostKeyChecking=no $server.$zone \
                    "sudo /usr/local/bin/db2info.sh" \
                    > db2info.$server.text
            fi
        done
    done
fi
