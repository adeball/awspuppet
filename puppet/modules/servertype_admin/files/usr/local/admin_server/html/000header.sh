#!/bin/bash

description=$(getmasterdescription)

cat <<EOF

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="refresh" content="60">
<base target="_blank">
<title>$description Puppet server</title>
<script src="sorttable.js"></script>
EOF
