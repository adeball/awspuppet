#!/bin/bash

cat <<EOF

<style>

* { font-family: helvetica,Arial,sans-serif; }

p,iframe {
    margin-top: 20px;
    margin-bottom: 5px;
    margin-left: 20px;
    margin-right: 20px;
    overflow:hidden;
}

#wrapper {
    overflow:hidden;
    height:240px;
    width:160px;
}

iframe.small {
    position: relative;
    top: -480px;
    left: -320px;
    width: 800px;
    height: 1200px;
    padding: 0px;
    margin: 0px;
    border: 0px;
    frameborder: 0px;
    -ms-transform:scale(0.16);
    -moz-transform:scale(0.16);
    -o-transform: scale(0.16);
    -webkit-transform: scale(0.16);
    transform:scale(0.16);
    border:1px solid black;
}



h1 { margin-top: 30px; padding: 10px; border-radius: 10px; background-color: #a0ddfa; color: black }
h2 { margin-top: 75px; padding: 15px; border-radius: 10px; background-color: #003d7a; color: white ; width 50%}
h3 { margin-top: 25px; color: #4c177c; }
h4 { margin-top: 25px; color: #7122b9; }
table.wide { width: 100%; align: center; }
table.left { width: 60%; align: left; }


table,th,td {
	border: 1px solid black; 
	text-align: left;
	vertical-align: top;
	padding: 5px;
}

th {background-color: #aaaaaa}

table {
	border-collapse: collapse;
}

tr:nth-child(even) {background-color: #f2f2f2;} 

</style>
EOF
