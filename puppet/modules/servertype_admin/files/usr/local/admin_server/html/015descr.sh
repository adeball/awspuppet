#!/bin/bash

description=$(getmasterdescription)

cat << EOF
<h1>$description Puppet Server</h1>

<p>This is $(hostname), the $description Puppet server. <i>[page last updated: $(date)]</i></p>

<ul>
<li>Automated-build AWS/Linux client instances are documented here.
<li>For non-automated build systems, please refer to the <a href=$(getmastermaindoc)>$description main documentation</a>.
<li>For detailed documentation on the AWS/Linux automated-builds, please go to the
<a href=$(getmastersysdoc)>Technical documentation</a>.
</ul>

EOF
