#!/bin/bash

description=$(getmasterdescription)
master=$(getmasterclient)

echo "<h1>Services</h1>"

echo "<table width=100%>"

echo "<tr>"
while read h ; do
	echo "<th>$h</th>"	
done << EOF
AWS/Linux
Documentation
$description Services
Client AWS account & instances
EOF
echo "</tr>"



### For the doc table, build up a file with blank lines to separate the sections

urlf=$(mktemp)

## AWS/Linux
cat > $urlf <<EOF
/xymon/xymon.html     Xymon monitoring
/xymon/nongreen.html  Xymon non-green view
/munin                Munin detailed performance metrics
https://$(getawsid $master).signin.aws.amazon.com/console  $description AWS Console
$(getmasterpuppetrepo)  Code repository

EOF

## Documentation
cat >> $urlf <<EOF
$(getmastermaindoc) $description Documentation
$(getmastersysdoc) $description Technical System Documentation

EOF

getmasterlinks | while read link ; do echo $link >> $urlf ; done


echo "<tr>"
echo "<td>"
echo "<table style=\"border: 0px\">"
echo "<tr>"
while read url title ; do
	
	if [[ -z "$url" ]] ; then
		echo "</table></td><td><table style=\"border: 0px\">"
	else
		echo "<tr><td style=\"border: 0px;padding: 0px\"><a href=$url>$title</a></td></tr>"
	fi

done < $urlf
rm $urlf

echo "</table></td><td><table style=\"border: 0px\">"
for client in $(getclients) ; do
	CLIENT=${client^^}
	fullname=$(getclientfullname $client)
    echo "<tr><td style=\"border: 0px;padding: 0px\"><a href=\"${client}.html\">$CLIENT ($fullname)</a></td></tr>"
done

echo "</td>"
echo "</tr>"
echo "</table>"



echo "</td>"
echo "</tr>"
echo "</table>"

cat<<EOF
<h1>Status</h1>
<p>
NB: To follow links, you probably want to use the full page in the
<a href=/xymon/nongreen.html>Xymon non-green view</a>.
</p>
<table width=100% bgcolor=#333333><tr><td>
<iframe src="/xymon/nongreen.html" width="97%" height="200">
  <p>Your browser does not support iframes.</p>
</iframe>
</td></tr></table>
EOF

echo "<p></p>"

