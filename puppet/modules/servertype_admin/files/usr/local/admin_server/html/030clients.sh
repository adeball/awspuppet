#!/bin/bash

cat<<EOF
<h1>Client Accounts</h1>
<p>
NB: The ssh links work directly on Macs, using 'terminal'.  To set up on Windows, please refer to: https://kb.op5.com/pages/viewpage.action?pageId=688361#sthash.6VkpBy68.dpbs (not tested, YMMV).
</p>
EOF

for client in $(getclients) ; do

	CLIENT=${client^^}

    clientf=$outdir/$client.html
    cat << EOF > $clientf

    <!DOCTYPE html>
    <html>
    <head>
    <meta http-equiv="refresh" content="60">
    <base target="_blank">
    <script src="sorttable.js"></script>
    <title>$CLIENT</title>

EOF
    ./001style.sh >> $clientf

	fullname=$(getclientfullname $client)
	awsid=$(getawsid $client)
	vpnint=$(getvpnint $client)
	vpnext=$(getvpnext $client)
	vpnport=$(getvpnport $client)
	vpnpub=$(getvpnpub $client)
	subnet=$(getsubnet $client)
	region=$(getregion $client)
	regionname=$(getregionfullname $region)

	### Sign-in is different for the main account
	if [[ "$client" == "ems" ]] ; then
	    awsconsole="https://${awsid}.signin.aws.amazon.com/console"
	else
        awsconsole="https://signin.aws.amazon.com/switchrole?account=${awsid}&roleName=amazon-administrators&displayName=${CLIENT}"
    fi

    xymonpage="/xymon/$client/$client.html"

cat << EOF | tee -a $clientf
	<h2 id="$client">$fullname ($CLIENT)</h2>

	<h3>Account</h3>

	<table width=50%>

	<tr>
	<th width=20%>Parameter</th>
	<th width=20%>Value</th>
	<th width=10%>Status</th>
	</tr>


	<tr>
	<td>AWS ID</td>
	<td>$awsid</td>

    <td rowspan=8>
        <div id="wrapper">
                <iframe class="small" src=/xymon/$client/$client.html name="$client">
                    <p>Your browser does not support iframes.</p>
                </iframe>
        </div>
                <a href=/xymon/$client/$client.html>Link</a>
    </td>


	</tr>

	<tr>
	<td>AWS Console</td>
	<td><a href=${awsconsole}>${awsconsole}</a></td>
	</tr>

	<tr>
	<td>OpenVPN Internal IP</td>
	<td>${vpnint}</td>
	</tr>

	<tr>
	<td>OpenVPN External IP</td>
	<td>${vpnext}</td>
	</tr>

	<tr>
	<td>OpenVPN Public IP</td>
	<td>${vpnpub}</td>
	</tr>

	<tr>
	<td>OpenVPN Port</td>
	<td>${vpnport}</td>
	</tr>

	<tr>
	<td>VPC Subnet</td>
	<td>${subnet}</td>
	</tr>

	<tr>
	<td>AWS region</td>
	<td>${region} ($regionname)</td>
	</tr>

	</table>

	<h3>Environments</h3>
EOF

    for environment in $(getenvironments $client) ; do

        cat << EOF | tee -a $clientf

        <h4>$(getenvfullname $environment)</h4>

        <table width=100% class="sortable">
        <tr>
        <th>Name</th>
        <th>Type</th>
        <th>FQDN (ssh link if up)</th>
        <th>Internal IP</th>
        <th>External IP</th>
        <th>Xymon status<br>(links to trends)</th>
        <th>Performance</th>
        <th>URLs/info</th>
        </tr>

EOF

        for server in $(getservers $client $environment) ; do

            servertype=$(getservertype $server)
            instance=$(getinstance $server)
            env=$(getenvironment $server)
            environment=$(getenvfullname $env)
            zone=$(getmasterzone)
            fqdnfunc=${servertype}-${instance}.${env}.${client}.${zone}
            fqdnname=${server}.${env}.${client}.${zone}
            ip=$(getip $server)
            extip=$(getextip $server)
            urls=$(geturls $server)
            xymontrends="/xymon-cgi/svcstatus.sh?HOST=${fqdnname}&SERVICE=trends"
            munintrends="/munin/${env}.${client}.${zone}/${fqdnname}/index.html"

            instanceid=


            status=$(xymon xymon "query ${fqdnname}.conn" | awk '{print $1}')
            allstatus=$(xymon xymon "xymondboard host=$fqdnname" | awk -F\| '{print $3}' | sort -u)
            for col in blue purple red yellow ; do
                echo $allstatus | grep -q $col
                if [[ $? -eq 0 ]] ; then
                    status=$col
                    break
                fi
            done


            img="/xymon/gifs/${status}.gif"
            if [[ "$status" == "clear" || -z "$status" ]] ; then
                fqdnentry="<a href=/xymon-cgi/svcstatus.sh?HOST=${fqdnname}&SERVICE=info>$fqdnfunc</a>"
            else
                fqdnentry="<a href=/xymon-cgi/svcstatus.sh?HOST=${fqdnname}&SERVICE=info>$fqdnfunc</a>
                (<a href=ssh://${fqdnfunc} target=_self>SSH</a> /
                <a href=sftp://${fqdnfunc} target=_self>SFTP</a>)"
            fi

            case $servertype in
                proxy | nginx | cognos )
                    awstats="<a href=http://${fqdnname}:81/awstats/awstats.pl>Web stats</a>"
                ;;
                *)
                    awstats=""
                ;;
            esac

            case $servertype in
                db2 | cognos )
                   db2info="$(echo 'DB2 INFO<br>';getdb2info $server|sed -e 's/$/<br>/')"
                ;;
                *)
                   db2info=""
                ;;
             esac

            cat << EOF | tee -a $clientf
            <tr>
            <td>$server</td>
            <td>$servertype</td>
            <td>$fqdnentry</td>
            <td>$ip</td>
            <td>$extip</td>
            <td style="text-align: center"><a href=$xymontrends><img src="$img" alt="$status"></a></td>
            <td><a href=$munintrends>munin</a></td>
EOF
            echo "<td>" | tee -a $clientf
            if [[ -z "$urls" && -z "$db2info" && -z "$awstats" ]] ; then
                echo "-" | tee -a $clientf
            else
                if [[ -n "$urls" ]] ; then
                    geturls $server | while read addr descr ; do
                        echo "<a href=$addr>$descr</a><br>" | tee -a $clientf
                    done
                fi

                [[ -n "$urls" && -n "$db2info" ]] && echo "<br>" | tee -a $clientf ### add separator if both, e.g. Cognos server
                [[ -n "$db2info" ]] && echo "$db2info" | tee -a $clientf
                [[ -n "$awstats" && -n "$db2info" ]] && echo "<br>" | tee -a $clientf ### add separator if both, e.g. Cognos server
                [[ -n "$awstats" ]] && echo "$awstats" | tee -a $clientf

            fi
            echo "</td>" | tee -a $clientf

            echo "</tr>" | tee -a $clientf

        done

        echo "</table>" | tee -a $clientf

	done


	### Unmanaged instances

   instances=$(awk -F\" '/InstanceId/ {print $4}' /home/puppetaws/aws/state/$client/ec2.describe-instances.json)

    if [[ -n "$instances" ]] ; then
        cat << EOF | tee -a $clientf

        <h4>Unmanaged Instances</h4>

        <table width=100% class="sortable">
        <tr>
        <th>Name</th>
        <th>Server Name</th>
        <th>Description</th>
        <th>Internal IP</th>
        <th>External IP</th>
        </tr>

EOF

        for instanceid in $instances ; do

                eval $(
                echo "server="
                echo "name="
                echo "description="
                echo "ip="
                echo "extip="
                awk -v i=$instanceid  '/INSTANCES/ && $0 ~ i             {me=1}
                                       /^INSTANCES/ && !($0 ~ i)         {me=0;}
                                       me==1 && /^TAGS\tServer Name/     {printf "server=%s\n", $NF}
                                       me==1 && /^TAGS\tName/            {printf "name=%s\n", $NF}
                                       me==1 && /^TAGS\tDescription/     {$1=""; $2="" ; printf "description=\"%s\"\n", $0}
                                       me==1 && /^PRIVATEIPADDRESS/      {printf "ip=%s\n", $4}
                                       me==1 && /^ASSOCIATION/           {printf "extip=%s\n", $4}
                ' /home/puppetaws/aws/state/$client/ec2.describe-instances.text | sed -e 's/="  /="/')

                [[ -z "$server" ]] && server="None"

                getservers "$client" | grep -q "$server"

                if [[ $? -ne 0 ]] ; then
                    cat << EOF | tee -a $clientf
                    <tr>
                    <td>$server</td>
                    <td>$name</td>
                    <td>$description</td>
                    <td>$ip</td>
                    <td>$extip</td>
                    </tr>
EOF
                    fi

            done

            echo "</table>"
        fi


done


