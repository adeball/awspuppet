#!/usr/bin/env bash

#!/bin/bash

cd /tmp
tmpf=$(mktemp)
grep "^[a-z]" /cloudinit/etc/host_table | sort -k4 > $tmpf

cd /var/ossec/etc



outf=/var/ossec/etc/client.keys
> $outf

while read host servertype ip env client instance subnet zone owner monitor; do

        uniqid=$(echo $ip | awk -F. '{printf "%d%03d%03d%03d", $1, $2, $3, $4}')
        echo "$uniqid $host $ip 9ecdc6d8d2fbaa5f1fd822f9d8d8b814a497972e820e3d68270d5bfddc164b97" >> $outf

done < $tmpf

touch /var/ossec/client_keys_created
