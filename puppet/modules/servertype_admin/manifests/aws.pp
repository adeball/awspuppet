
class servertype_admin::aws (
){

  $adminacct = hiera('users_admin::adminacct')
  $reporo = hiera('reporo')
  $reporokey = hiera('reporokey', 'dummy')

  # file { '/usr/local/admin_server/bin/refresh_puppet_and_aws_info.sh':
  #   ensure => present,
  #   mode => 0755,
  #   owner => root,
  #   source => 'puppet:///modules/servertype_admin/files/usr/local/admin_server/bin/refresh_puppet_and_aws_info.sh',
  # }

  file { '/usr/local/admin_server/bin/refresh_puppet_and_aws_info.sh':
    require => File['/usr/local/admin_server/bin'],
    ensure => present,
    mode   => 0755,
    source => 'puppet:///modules/servertype_admin/usr/local/admin_server/bin/refresh_puppet_and_aws_info.sh',
  }

  $repobranch=hiera(common::repobranch)
  file { '/usr/local/admin_server/etc/admin.cfg':
    ensure => present,
    owner => root,
    mode => 0644,
    content => template("servertype_admin/admin.cfg.erb"),
  }

  cron{'Refresh puppet repository, update AWS info':
    command => '/usr/local/admin_server/bin/refresh_puppet_and_aws_info.sh',
    user => "$adminacct",
    hour => "*",
    minute => 0,
  }

  file{"/home/$adminacct/.ssh/reporo.rsa":
    mode => 0600,
    owner => "$adminacct",
    content => "$reporokey\n",
  }

  file{"/home/$adminacct/aws":
    ensure => directory,
    mode => 0755,
    owner => "$adminacct",
  }
  file { '/etc/profile.d/repo.sh':
    ensure => present,
    owner => root,
    mode => 0755,
    content => template("servertype_admin/repo.sh.erb"),
  }

}
