
class servertype_admin::firewall {

  firewall { '010 allow Puppet clients to connect':
    dport  => ['8140'],
    proto  => 'tcp',
    action => 'accept',
  }

}
