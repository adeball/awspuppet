class servertype_admin (
  $htmldir="/var/www/html",
){

  $version=hiera(common::version)
  case $version {
    '1':       { include servertype_admin::v1 }
    '2':       { include servertype_admin::v2 }
    'recover': { include servertype_admin::v1 }  ### No specific recovery module yet
  }

}
