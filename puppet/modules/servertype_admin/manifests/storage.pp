
class servertype_admin::storage {

  ### Disk device setup

  class { 'lvm':
    volume_groups => {
      'vg_data' => {
        physical_volumes => $common::lvmdisks,
        logical_volumes  => {
          ### 10GB minus one 4MB extent
          'swap'    => { 'size' => '10236M', fs_type => swap,  },
          'jenkins' => { size => 252M, fs_type => xfs, mountpath => '/var/jenkins', }
        },
      },
    },
  }


}
