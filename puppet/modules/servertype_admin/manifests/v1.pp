class servertype_admin::v1 {

  file { '/tmp/version':
    content => '1
',
  }

  include servertype_admin::storage
  include servertype_admin::firewall
  include servertype_admin::aws

  include local_postfix
  include local_jenkinsslave

  class { 'apache': }

  # ### AWS configuration
  # exec { 'Set up AWS default region':
  #   command => 'aws configure set region eu-west-2',
  #   unless  => 'test -f /root/.aws/config',
  #   path    => '/usr/bin:/usr/sbin:/bin',
  # }

  ### Admin
  file { '/usr/local/admin_server':
    ensure => directory,
    mode   => 0755,
  }
  file { '/usr/local/admin_server/bin':
    require => File['/usr/local/admin_server'],
    ensure  => directory,
    mode    => 0755,
  }
  file { '/usr/local/admin_server/html':
    require => File['/usr/local/admin_server'],
    ensure  => directory,
    mode    => 0755,
    recurse => true,
    source  => 'puppet:///modules/servertype_admin/usr/local/admin_server/html',
  }
  file { '/usr/local/admin_server/etc':
    require => File['/usr/local/admin_server'],
    ensure  => directory,
    mode    => 0755,
  }

  file { '/usr/local/admin_server/bin/make_admin_page.sh':
    require => File['/usr/local/admin_server/bin'],
    ensure  => present,
    mode    => 0755,
    content => template("servertype_admin/make_admin_page.sh.erb"),
  }
  file { '/usr/local/admin_server/bin/gitsshro.sh':
    require => File['/usr/local/admin_server/bin'],
    ensure  => present,
    mode    => 0755,
    source  => 'puppet:///modules/servertype_admin/usr/local/admin_server/bin/gitsshro.sh',
  }
  $htmldir=$::servertype_admin::htmldir
  file { "$htmldir/sorttable.js":
    ensure => present,
    source => 'puppet:///modules/servertype_admin/sorttable.js',
    mode   => 0644,
  }


  cron { 'Update Admin server page':
    require => File['/usr/local/admin_server/bin/make_admin_page.sh'],
    command => '/usr/local/admin_server/bin/make_admin_page.sh',
    user    => root,
    minute  => '*/15',
  }

  # tidy { "/var/lib/puppet/reports":
  #   age => "1w",
  #   recurse => true,
  # }

  ### 'tidy' is buggy, so do it by cron
  file { '/etc/cron.daily/tidy_puppet_reports.sh':
    ensure => present,
    mode   => 0755,
    source => 'puppet:///modules/servertype_admin/etc/cron.daily/tidy_puppet_reports.sh',
  }


  # file {'/root/cloudinitsynchro.sh':
  #   owner => root,
  #   group => root,
  #   mode => '0755',
  #   source => 'puppet:///modules/servertype_admin/root/cloudinitsynchro.sh'
  # } ->
  # cron {'cloudinit synchronisation':
  #   command => '/root/cloudinitsynchro.sh',
  #   hour => '*',
  #   minute => '15',
  # }

  # file { '/var/ossec/mk_client_keys.sh':
  #   source   => 'puppet:///modules/servertype_admin/var/ossec/mk_client_keys.sh',
  #   owner    => root,
  #   mode     => 755,
  # } ->
  # exec { 'Create OSSEC client keys':
  #   command => '/var/ossec/mk_client_keys.sh',
  #   creates => '/var/ossec/client_keys_created',
  #   path => '/usr/bin:/usr/sbin',
  # }

  ensure_packages(['mod_ssl', 'openssl', 'mariadb-server', 'mariadb', 'php', 'php-mysql', 'php-gd', 'php-mbstring'])

}

