# servertype_cognos

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_cognos]](#setup)
    * [What [servertype_cognos] affects](#what-[servertype_cognos]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_cognos]](#beginning-with-[servertype_cognos])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up a server for Cognos installation.

See the 'local-cognos' module for details of the Cognos installation itself.  This module sets up storage and firewall rules required for Cognos.

## Setup

### What [servertype_cognos] affects 

* Storage
* IPTables

### Beginning with [servertype_cognos]	

include servertype_cognos

These modules should be included (use Hiera)

 - users-ibm
 - local-ibmsupport
 - local-ibminstall
 - local-db2server
 - local-db2backup
 - local-cognos
 - local-nfssharedclient

## Usage

## Reference

## Limitations

RHEL7 only

## Development

