class servertype_cognos::firewall {

  firewall { '500 DB2':
    dport  => ['50005'],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '080 HTTP':
    dport  => ['80', '81', '443'],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '930 Cognos':
    dport  => ['9300'],
    proto  => 'tcp',
    action => 'accept',
  }

}
