

class servertype_cognos {

  $version=hiera(common::version)
  case $version {
    '1':        { include servertype_cognos::v1 }
    '2':        { include servertype_cognos::v2 }
    'recover':  { include servertype_cognos::recover }
  }

}
