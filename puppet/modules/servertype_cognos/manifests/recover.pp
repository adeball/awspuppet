
class servertype_cognos::recover {

  include servertype_cognos::storage
  include local_nfssharedclient


  ### Needed for apache
  file {'/run/httpd':
    ensure => directory,
    owner => root,
  }


  ### Recover, then clean out the DB2 installation and reinstall:-

  class { local_recover:
    require => Mount[['/data/vol01', '/opt/IBM']]
  } ->
  exec {'Wipe DB2':
    path => '/usr/bin:/usr/sbin',
    command => 'bash /shared/install/scripts/wipe_db2.sh',
    unless => 'test -d /data/vol01/db',
    require => Mount['/shared/install'],
  } ->
  class {users_ibm:} ->
  class {local_ibmsupport:} ->
  exec {'Reinstall DB2':
    path => '/usr/bin:/usr/sbin',
    command => 'bash /shared/install/scripts/install_db2_server.sh',
    creates => "/data/vol01/db",
    timeout => '900',
    require => Mount['/shared/install'],
  } ->
  notify { 'Recovery complete':
    name => 'Recovery complete - a blank content manager database has been created.  You may want to recover from the latest export.  Once done (if needed), change the build version from recover to 1 or 2 etc...'
  }

}

