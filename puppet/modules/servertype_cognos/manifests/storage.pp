class servertype_cognos::storage {

  ### Disk device setup

  file { [ '/opt/IBM', '/data', ]: ensure => directory } ->
    file { [ '/data/vol01', ]: ensure => directory } ->
  class { 'lvm':
    volume_groups => {
      'vg_data' => {
        physical_volumes => $common::lvmdisks,
        #physical_volumes => ['/dev/xvdd',],
        logical_volumes  => {
          'swap' => { 'size' => '4GB', fs_type => swap, },
          'IBM'  => { 'size' => '40G', 'mountpath' => '/opt/IBM', 'mountpath_require' => true },
          'vol01'  => { 'size' => '40G', 'mountpath' => '/data/vol01', 'mountpath_require' => true },
        },
      },
    },
  }

}

