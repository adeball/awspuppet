class servertype_cognos::v1 {

  file { '/tmp/version':
    content => '1
',
  }

  include servertype_cognos::firewall
  include servertype_cognos::storage

  include local_nfssharedclient
  include users_ibm
  include local_ibmsupport
  include local_ibminstall
  include local_db2server
  include local_db2backup
  include local_cognos

}

