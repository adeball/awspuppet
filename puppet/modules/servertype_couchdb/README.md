# servertype_couchdb

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_couchdb]](#setup)
    * [What [servertype_couchdb] affects](#what-[servertype_couchdb]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_couchdb]](#beginning-with-[servertype_couchdb])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up a CouchDB server instance.

See also the local-couchdb module, which actually installs and configures the software.

## Setup

### What [servertype_couchdb] affects 

* Disables SELinux (permissive mode)
* Inherits local-couchdb

### Setup Requirements 

### Beginning with [servertype_couchdb]	

include servertype_couchdb

## Usage

## Reference

### Classes

#### ::firewall

No rules required

#### ::storage

* Configures swap and /home filesystem
* Copies original /home to /home filesystem

NB: The original /home contents are not deleted (so will sit under the mountpoint - they can be accessed by using bind mount).  In normal operation of the module, the /home migration will take place during system build, so the contents will be miniscule.  Only if you apply this at a later time will you need to clear out the original /home.

## Limitations

Tested on RHEL7

## Development

