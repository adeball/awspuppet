class servertype_couchdb {

  $version=hiera(common::version)
  case $version {
    '1':        { include servertype_couchdb::v1 }
    '2':        { include servertype_couchdb::v2 }
    'recover':  { include servertype_couchdb::recover }
  }

}
