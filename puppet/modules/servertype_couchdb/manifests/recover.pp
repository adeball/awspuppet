
class servertype_couchdb::recover {

  include servertype_couchdb::storage

  class { local_recover:
    require => Mount[['/home',]]
  } ->
  notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

