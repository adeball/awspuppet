
class servertype_couchdb::storage {

  ### Disk device setup
  file { '/home': ensure => directory } ->

    class { 'lvm':
      volume_groups => {
        'vg_data' => {
          physical_volumes => $common::lvmdisks,
          logical_volumes  => {
            'home'       => { 'size' => '80G', 'mountpath' => '/home', 'mountpath_require' => true },
            'swap'       => { 'size' => '10G', fs_type => swap,  },
          },
        },
      },
    }

  exec {'Copy original /home contents':
    path => '/bin:/usr/bin',
    unless => 'test -d /home/ec2-user',
    command => 'mkdir -p /tmp/cdb; mount -o bind / /tmp/cdb; cd /tmp/cdb/home; rsync -avx . /home; cd / ; umount /tmp/cdb',
  }

}
