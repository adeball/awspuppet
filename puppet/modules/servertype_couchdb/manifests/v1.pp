
class servertype_couchdb::v1 {

	file {'/tmp/version': 
		content => '1
',
	}

	### Revisit this when we can stop selinux being a pain.
	### Moved /home means ssh stops working.  Need to sort
	exec { 'Set SELinux to permissive mode':
		command => 'setenforce 0',
		path    => '/usr/bin:/usr/sbin:/bin',
		unless  => 'test $(getenforce) == "Permissive" -o $(getenforce) == "Disabled" ',
	}

	include local_couchdb
	include servertype_couchdb::storage
	include servertype_couchdb::firewall


}

