# servertype_db2

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_db2]](#setup)
    * [What [servertype_db2] affects](#what-[servertype_db2]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_db2]](#beginning-with-[servertype_db2])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Installs DB2 server software and sets up a Maximo database.

This was an early iteration of the DB2 setup, and it requires rewriting, along the lines of the servertype-cognos + local-db2server + local-cognos configuration.  i.e. the DB2 installation and Maximo DB2 configuration need to be separated.  

## Setup

### What [servertype_db2] affects 

* Sets up IPTtables for DB2 (see ::firewall)
* Configures LVM volumes (see ::storage)
* Inherits local-db2backup (daily schedule for production, weekly for non-production)
* Sets up DB2 as a service
* Inherits local-ibmsupport and local-ibminstall
* Copies and executes DB2 installation scripts
* Sets up /shared/maximo synchronisation (configures to run in cron on production systems)


### Setup Requirements

Requires users-ibm (currently configured in the common module).

### Beginning with [servertype_db2]	

include servertype_db2

## Usage

The module, being an early iteration, is very much a "one size fits all" module.  This has been sufficient so far, but requires improvement and parameterisation/abstraction.  For the moment, whilst the module is still used, any specific configurations will need to be coded into the module itself using conditionals.

## Reference

### Classes

#### ::firewall

* Allows TCP ports 50005 & 50006 for DB2 connections
* Allows TCP ports 55001 & 55002 for HADR connections
* Allows TCP port 2049 for NFSv4 client connections

#### ::storage

Configures multiple volumes:

* /data/vol01
* /data/vol02
* /data/vol03
* /opt/maxapps
* /shared/maximo
* swap

Sets up NFSv4 server:

* /shared/maximo - used to enable DB2 & WAS servers to access Maximo documents

Creates documents directory structure under /shared/maximo:

* Creates the structure
* Sets ownerships
* Set permissions

### Scripts

* /usr/local/bin/sync_shared_maximo: Runs on production DB2 servers, every ten minutes (configured in cron).  rsyncs from db2-001 to db2-002.  NB: If failover is used and the DB2 servers are swapped, this script will automatically stop syncing to preserver the safety of the /shared/maximo contents.  Once the 'db2' hostname is pointing to db2-002 AND this node is marked as the active one, synchronisation will commence (from db2-002 to db2-001).  To mark db2-002 as the active node, you edit this script (change the ACTIVEINSTANCE and BACKUPINSTANCE variables over from 001/002 to 002/001) - and deploy.
* /data/install/scripts/create_mf_db.sh: Not executed automatically, but may be used on the DB2 server later to install a Mobile First database.
* /data/install/scripts/create_mf_schema.sh: See comments in the script - do not use as-is - keeping it for reference only.
* /data/install/scripts/db2_maximo_config.sh: This is executed as part of the installation (by install_db2_server.sh).  Run as the instance owner, it sets up a Maximo database with all the required permissions and configuration parameters needed to support Maximo.
* /data/install/scripts/install_db2_server.sh: Installs and configures a base DB2 installation, sets up users, groups, paths, permissions etc.  At the end, calls db2_maximo_config.sh
* wipe_db2.sh: Not used by the module - totally removes DB2 files/users from a server - useful for when developing installation code.
* /etc/init.d/db2: Start/stop script used by the DB2 service which the module configures.

## Limitations

RHEL7 only

## Development

This module should only be changed for required updates.  The module needs restructuring with parameters and more abstraction of components, which is where future development effort should be aimed.

## Release Notes/Contributors/Etc.

