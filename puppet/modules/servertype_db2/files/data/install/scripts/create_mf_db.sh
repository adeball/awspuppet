#!/usr/bin/env bash
#
# db2_${DBNAME}_config.sh
#
# Based on https://confluence.myco-group.com/display/HD/Installing+DB2+on+Amazon+Linux
#



######
###### This needs a rewrite - modified/run in one hour before going on holiday,
###### with no DB2 knowledge for the production installation for GTR.
###### Also no design, information other than a hipchat session with James K
######
###### Crazy!
######
###### Do not use this as-is
######


if [[ ${USER} != "mfdbadm" ]] ; then
    echo
    echo "This script must be executed as the mfdbadm user"
    echo
fi

DBNAME=${CLOUDCLIENT}MF${CLOUDENV}

cd

db2start
db2 update dbm config using SVCENAME 50006 DEFERRED

### Oracle compatibility mode - has to be set before creating the DB
db2set DB2_COMPATIBILITY_VECTOR=ORA
db2set DB2_DEFERRED_PREPARE_SEMANTICS=YES
db2set DB2_WORKLOAD=MAXIMO
db2set DB2AUTOSTART=YES

db2stop
db2set DB2COMM=tcpip
db2start

### Setup the database

mkdir -p /data/vol01/${DBNAME}

db2 create db ${DBNAME} on /data/vol02/db ALIAS ${DBNAME} using codeset UTF-8 territory US pagesize 32 K

db2 update db cfg for ${DBNAME} using SELF_TUNING_MEM ON
db2 update db cfg for ${DBNAME} using APPGROUP_MEM_SZ 16384 DEFERRED
db2 update db cfg for ${DBNAME} using APPLHEAPSZ 2048 AUTOMATIC DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_MAINT ON DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_TBL_MAINT ON DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_RUNSTATS ON DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_REORG ON DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_DB_BACKUP ON DEFERRED
db2 update db cfg for ${DBNAME} using CATALOGCACHE_SZ 800 DEFERRED
db2 update db cfg for ${DBNAME} using CHNGPGS_THRESH 40 DEFERRED
db2 update db cfg for ${DBNAME} using DBHEAP AUTOMATIC
db2 update db cfg for ${DBNAME} using LOCKLIST AUTOMATIC DEFERRED
db2 update db cfg for ${DBNAME} using LOGBUFSZ 1024 DEFERRED
db2 update db cfg for ${DBNAME} using LOCKTIMEOUT 300 DEFERRED
db2 update db cfg for ${DBNAME} using LOGPRIMARY 156 DEFERRED
db2 update db cfg for ${DBNAME} using LOGSECOND 100 DEFERRED
db2 update db cfg for ${DBNAME} using LOGFILSIZ 4096 DEFERRED
db2 update db cfg for ${DBNAME} using SOFTMAX 1000 DEFERRED
db2 update db cfg for ${DBNAME} using MAXFILOP 65535 DEFERRED
db2 update db cfg for ${DBNAME} using PCKCACHESZ AUTOMATIC DEFERRED
db2 update db cfg for ${DBNAME} using STAT_HEAP_SZ AUTOMATIC DEFERRED
db2 update db cfg for ${DBNAME} using STMTHEAP AUTOMATIC DEFERRED
db2 update db cfg for ${DBNAME} using UTIL_HEAP_SZ 200000 DEFERRED
db2 update db cfg for ${DBNAME} using DATABASE_MEMORY AUTOMATIC DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_STMT_STATS OFF DEFERRED
db2 update db cfg for ${DBNAME} using STMT_CONC LITERALS DEFERRED
db2 update db cfg for ${DBNAME} using TRACKMOD YES DEFERRED


### Logs - path TBD
db2 update db cfg for ${DBNAME} using LOGARCHMETH1 DISK:/data/vol02/log DEFERRED
db2 update db cfg for ${DBNAME} using LOGARCHMETH1 DISK:/data/vol03/log DEFERRED
db2 update db cfg for ${DBNAME} using FAILARCHPATH /data/vol01/log DEFERRED
mkdir -p /data/vol01/logs
mkdir -p /data/vol02/logs
mkdir -p /data/vol03/logs

db2 update db cfg for ${DBNAME} using LOGINDEXBUILD ON DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_DB_BACKUP OFF DEFERRED
db2 update db cfg for ${DBNAME} using AUTO_STMT_STATS ON DEFERRED


### Update alerts
db2 update alert cfg for database on ${DBNAME} using db.db_backup_req SET THRESHOLDSCHECKED YES
db2 update alert cfg for database on ${DBNAME} using db.tb_reorg_req SET THRESHOLDSCHECKED YES
db2 update alert cfg for database on ${DBNAME} using db.tb_runstats_req SET THRESHOLDSCHECKED YES

### Update the config
db2 update dbm cfg using PRIV_MEM_THRESH 32767 DEFERRED
db2 update dbm cfg using KEEPFENCED NO DEFERRED
db2 update dbm cfg using NUMDB 2 DEFERRED
db2 update dbm cfg using RQRIOBLK 65535 DEFERRED
db2 update dbm cfg using HEALTH_MON OFF DEFERRED

### Added after review of existing production DBs
db2 update dbm cfg using DIAGSIZE 100 DEFERRED
db2 update dbm cfg using DFT_MON_BUFPOOL ON DEFERRED
db2 update dbm cfg using DFT_MON_LOCK ON DEFERRED
db2 update dbm cfg using DFT_MON_SORT ON DEFERRED
db2 update dbm cfg using DFT_MON_STMT ON DEFERRED
db2 update dbm cfg using DFT_MON_TABLE ON DEFERRED
db2 update dbm cfg using DFT_MON_TIMESTAMP ON DEFERRED
db2 update dbm cfg using DFT_MON_UOW ON DEFERRED

### Update settings
db2set DB2_SKIPINSERTED=ON
db2set DB2_INLIST_TO_NLJN=YES
db2set DB2_MINIMIZE_LISTPREFETCH=Y
db2set DB2_EVALUNCOMMITTED=YES
db2set DB2_FMP_COMM_HEAPSZ=65536
db2set DB2_SKIPDELETED=ON
db2set DB2_USE_ALTERNATE_PAGE_CLEANING=ON

### Restart and reconnect
db2stop force
db2start

db2 backup database ${DBNAME} to /data/vol02/backup compress

db2 connect to ${DBNAME}


for user in APPCNTR MFADMIN ;do

	### Setup tablespaces
	db2 CREATE BUFFERPOOL MAXBUFPOOL IMMEDIATE SIZE 4096 AUTOMATIC PAGESIZE 32 K
	db2 CREATE REGULAR TABLESPACE MAXDATA PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE INITIALSIZE 2500 M BUFFERPOOL MAXBUFPOOL
	db2 CREATE TEMPORARY TABLESPACE MAXTEMP PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE BUFFERPOOL MAXBUFPOOL
	db2 CREATE REGULAR TABLESPACE MAXINDEX PAGESIZE 32 K MANAGED BY AUTOMATIC STORAGE INITIALSIZE 2500 M BUFFERPOOL MAXBUFPOOL
	db2 GRANT USE OF TABLESPACE MAXDATA TO USER $user
	db2 create schema $user authorization mfdbadm
	db2 GRANT DBADM,CREATETAB,BINDADD,CONNECT,CREATE_NOT_FENCED_ROUTINE,IMPLICIT_SCHEMA, LOAD,CREATE_EXTERNAL_ROUTINE,QUIESCE_CONNECT,SECADM ON DATABASE TO USER $user
	db2 GRANT USE OF TABLESPACE MAXDATA TO USER $user
	db2 GRANT CREATEIN,DROPIN,ALTERIN ON SCHEMA $user TO USER $user

done

db2 grant DBADM on ${DBNAME} to user mfdbadm
db2 grant SECADM on ${DBNAME} to user mfdbadm

sed -i "s/DB2AUTOSTART='NO'/DB2AUTOSTART='YES'/" $HOME/sqllib/profile.env

### On production systems
#db2 grant use of tablespace MAXDATA to user mxsibui
#db2 grant use of tablespace MAXDATA to user mxsibmif
#db2 create schema MXSIBUI authorization mxsibui
#db2 create schema MXSIBMIF authorization mxsibmif
#db2 grant createin, dropin, alterin on schema mxsibui to mxsibui
#db2 grant createin, dropin, alterin on schema mxsibmif to mxsibmif


### Final stop/start - seems to resolve some problems.
db2stop force
db2start


