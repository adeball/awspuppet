#!/usr/bin/env bash
#


######
###### This needs a rewrite - modified/run in one hour before going on holiday,
###### with no DB2 knowledge for the production installation for GTR.
###### Also no design, information other than a hipchat session with James K
######
###### Crazy!
######
###### Do not use this as-is
######





export base=/opt/maxapps       # Installation and application homes

fail()
{
    echo
    echo "Failure: $@"
    echo
    exit 1
}



export PATH=$PATH:$base/ibm/db2/V10.5/bin:$base/ibm/db2/V10.5/instance:$base/ibm/db2/V10.5/adm

db2icrt -s ese -u mfdbadm -p 50006 mfinst1



. /opt/mfinst1/sqllib/db2profile

### Give mfdbadm privileges
f=$(mktemp)
cat > $f << EOF
#!/bin/bash
for g in adm ctrl maint mon ; do
    echo \$g
    db2 update dbm cfg using sys\${g}_group db2iadm1
done
EOF
chmod 755 $f
su - mfinst1 $f

### And copy the sqllib contents
cd /opt/mfinst1
find sqllib | cpio -pdmv /opt/mfdbadm
cd /opt/mfdbadm
find . -user mfinst1 -exec chown mfdbadm {} \;
cat sqllib/db2profile >> .bashrc

