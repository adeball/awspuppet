#!/usr/bin/env bash
#
# install_db2_server.sh
#
# Ade 2017-02
#
# See: https://confluence.myco-group.com/display/HD/Installing+DB2+on+Amazon+Linux for the source of the DB2/Maximo
#       commands.



export base=/opt/maxapps       # Installation and application homes
export insdir=/data/install    # Installation source
export datadir=/data/vol01

fail()
{
    echo
    echo "Failure: $@"
    echo
    exit 1
}


### DB2 already running - abort - only run this on a newly installed system
ps -ef | grep -v grep | grep db2wdog > /dev/null
if [[ $? -eq 0 ]] ; then
    fail "DB2 is already running!"
fi

### And again - belt and braces
if [[ -f /opt/maxapps/ibm/db2 || -f /opt/ibm/db2 ]] ; then
     fail "It looks like DB2 is already installed, please check /opt"
fi



if [[ ! -f /etc/profile.d/ibmsupport.sh ]] ; then
    fail "/etc/profile.d/ibmsupport.sh does not exist yet, this should created by puppet.
    Will not run until this is available"
fi

. /etc/profile.d/ibmsupport.sh

### Switch SELinux to permissive mode, as DB2 doesn't like this... :-(
setenforce Permissive
sed -i 's/^SELINUX=enforcing/SELINUX=permissive/' /etc/sysconfig/selinux
sestatus

if [[ ! -f $insdir/SilentResponseFiles/Installer/Unix/ResponseFile_DB2_Install_Unix.xml ]] ; then
    fail "$insdir/SilentResponseFiles/Installer/Unix/ResponseFile_DB2_Install_Unix.xml does not exist
    it will be put into place by puppet/servertype_db2, will execute when the file is there"
fi


### Clear some space - the DB2 installer is *very* fussy (and also gets its calculations wrong,
### claiming /tmp needs 2GB, when it claims it has 1.95GB, but it really has 6GB free!!
### Clear the yum cache, and unneeded locales

yum clean all
cd /usr/share/locale; rm -rf $(ls | egrep -v "en_GB")

### Install DB2
cd /opt/IBM/InstallationManager/eclipse/tools
./imcl -input $insdir/SilentResponseFiles/Installer/Unix/ResponseFile_DB2_Install_Unix.xml -log $insdir/log_$(date +%Y-%m-%d).xml -acceptLicense


export PATH=$PATH:$base/ibm/db2/V10.5/bin:$base/ibm/db2/V10.5/instance:$base/ibm/db2/V10.5/adm

#db2icrt -s ese -u db2fenc1 -p 50005 ctginst1
db2icrt -s ese -u db2admin -p 50005 ctginst1
. /opt/ctginst1/sqllib/db2profile
ln -s /opt/ctginst1/sqllib/db2profile /etc/profile.d/db2.sh

### Give db2admin privileges
f=$(mktemp)
cat > $f << EOF
#!/bin/bash
for g in adm ctrl maint mon ; do
    echo \$g
    db2 update dbm cfg using sys\${g}_group db2iadm1
done
EOF
chmod 755 $f
su - ctginst1 $f

### And copy the sqllib contents
cd /opt/ctginst1
find sqllib | cpio -pdmv /opt/db2admin
cd /opt/db2admin
find . -user ctginst1 -exec chown db2admin {} \;


### Set up and configure the database for Maximo (must be done as a privileged user)
chmod a+x $insdir/scripts/db2_maximo_config.sh

paths="/data/vol01 /data/vol02 /data/vol03"
subpaths="db log archive backup"
for p in $paths ; do
    for s in $subpaths ; do
        mkdir -p ${p}/${s}
        chown db2admin:db2iadm1 ${p}/${s}
        chmod 775 ${p}/${s}
    done
done
chmod a+x $insdir/scripts/db2_maximo_config.sh
su db2admin $insdir/scripts/db2_maximo_config.sh

