#!/bin/bash

### Log just the last output here
exec > /var/log/sync_shared_maximo.log 2>&1


. /etc/profile.d/cloud.sh
export PATH=/bin:/sbin:/usr/bin:/usr/sbin

fail()
{
	echo "Failed: $@"
	exit 1
}


ACTIVEINSTANCE=001
BACKUPINSTANCE=002


### -- code here to switch these over if we're syncing back -- ###
### -- use a -r switch -- ###



### Work out if we have the 'db2' record assigned
### and whether we're the primary server


### ACTIVE = "this host has the db2 alias pointing to it, so is the active DB2 server"
### PRIMARY = "this host is DB2 instance 001"

myip=$(getent hosts $CLOUDSERVERNAME | awk 'NR==1 {print $1}')
db2ip=$(getent hosts db2 | awk 'NR==1 {print $1}')

[[ "$myip" == "$db2ip" ]]                   && ACTIVE=1  || ACTIVE=0
[[ "$CLOUDINSTANCE" == "$ACTIVEINSTANCE" ]] && PRIMARY=1 || PRIMARY=0


SOURCE=db2-$ACTIVEINSTANCE
DEST=db2-$BACKUPINSTANCE

if [[ $ACTIVE -eq 1 && $PRIMARY -eq 1 ]] ; then
	echo "All ok - syncing to $DEST"
elif [[ $PRIMARY -eq 0 ]] ; then
	echo "We are the secondary node, not syncing"
	exit
elif [[ $ACTIVE -eq 0 ]] ; then
	echo "We are the primary node, but the db2 alias does not point here, not syncing"
	exit
fi


### If we reached here, we're good to go


### Well, maybe not - let's check the other host is up
ping -q -c 1 -W 10 $DEST
if [[ $? -ne 0 ]] ; then
	fail "$DEST is not responding to pings"
fi
echo "$DEST is up"

### Since we have NFS, let's use that rather than having to set up SSH keys

mpt=/tmp/$DEST/maximo
mkdir -p $mpt
echo "Mounting $DEST:/maximo on $mpt"
mount -t nfs4 $DEST:/maximo $mpt || fail "Cannot mount the secondary maximo filesystem"



### Sync
echo "Syncing"

touch /shared/maximo/synced_${SOURCE}_to_${DEST}
rsync -avx --delete /shared/maximo/ $mpt

echo "Unmounting $mpt"
umount $mpt || fail "Cannot unmount $mpt"


