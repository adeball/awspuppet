
class servertype_db2::firewall {

  firewall { '5000 allow DB2 clients to connect':
    dport   => ['50005', '50006'],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '5501 DB2 HADR':
    dport   => ['55001', '55002',],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '02049 Allow NFSv4':
    dport    => 2049,
    proto    => tcp,
    action   => accept,
  }

  firewall { '02004 Allow mountd':
    dport    => 20048,
    proto    => tcp,
    action   => accept,
  }

  firewall { '00111 Allow tcp rpcbind':
    dport    => 111,
    proto    => tcp,
    action   => accept,
  }

  firewall { '00111 Allow udp rpcbind':
    dport    => 111,
    proto    => udp,
    action   => accept,
  }

}
