class servertype_db2
  (
    $basedir                    = '/opt',
    $wasuser                    = 'wasadmin',
    $waspass                    = 'password',
    $wasjvmignore               = 'none',
    $wasjvmdonotrun             = 'none',
    $waslogageindays            = 30,
    $wasdumpageindays           = 2,
    $oraint_housekeep           = false,
    $oraint_housekeep_ageindays = 999999,
  )
  {

    $version = hiera(common::version)
    case $version {
      '1': { include servertype_db2::v1 }
      '2': { include servertype_db2::v2 }
      'recover': { include servertype_db2::recover }
    }

  }
