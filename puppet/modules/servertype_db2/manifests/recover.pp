class servertype_db2::recover {

  include servertype_db2::storage

  ### Recover, then clean out the DB2 installation and reinstall:-

  class { local_recover: require => Mount[['/data/vol01', ]] } ->
  class { 'users_ibm': } ->
  class { 'local_ibmsupport': } ->
  class { 'local_ibminstall': } ->
  file { '/data/install/scripts':
    source  => 'puppet:///modules/servertype_db2/data/install/scripts',
    owner   => root,
    recurse => true,
    purge   => true,
  } ->
  file { '/data/install/SilentResponseFiles':
    source  => 'puppet:///modules/servertype_db2/data/install/SilentResponseFiles',
    owner   => root,
    recurse => true,
  } ->
  exec { 'Install DB2':
    path    => '/usr/bin:/usr/sbin',
    command => 'bash /data/install/scripts/install_db2_server.sh',
    creates => '/opt/maxapps/ibm/db2',
    timeout => '900',
  } ->
  notify { 'Recovery complete':
    name =>
      'Recovery complete - a blank maximo database has been created.  You may want to recover from the latest export/backup.  Once done (if needed), change the build version from recover to 1 or 2 etc...'
  }

}

