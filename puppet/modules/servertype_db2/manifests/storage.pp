
class servertype_db2::storage (
  $vol01="40G",
  $vol02="100G",
  $vol03="100G"
){

  ### Disk device setup

  if $cloudenv == "DEV" {
    $vol01="20G"
    $vol02="20G"
    $vol03="20G"
  }

  file { ['/data', '/opt' ]: ensure => directory } -> ### /shared is defined in the common module
    file { [ '/data/vol01', '/data/vol02', '/data/vol03', '/data/install', '/opt/maxapps', '/shared/maximo', ]: ensure => directory } ->


    class { 'lvm':
      volume_groups => {
        'vg_data' => {
          physical_volumes => $common::lvmdisks,
          logical_volumes  => {
            'install' => { 'size' => '25G', 'mountpath' => '/data/install', 'mountpath_require' => true },
            'vol01'   => { 'size' => "$vol01", 'mountpath' => '/data/vol01', 'mountpath_require' => true },
            'vol02'   => { 'size' => "$vol02", 'mountpath' => '/data/vol02', 'mountpath_require' => true },
            'vol03'   => { 'size' => "$vol03", 'mountpath' => '/data/vol03', 'mountpath_require' => true },
            'maxapps' => { 'size' => '20G', 'mountpath' => '/opt/maxapps', 'mountpath_require' => true },
            'shared_maximo'  => { 'size' => '40G', 'mountpath' => '/shared/maximo', 'mountpath_require' => true },
            'swap'    => { 'size' => '10G', fs_type => swap,  },
          },
        },
      },
    }

  class { '::nfs':
    server_enabled => true,
    nfs_v4 => true,
    nfs_v4_idmap_domain => 'myco.cloud',
    nfs_v4_export_root  => '/export',
    nfs_v4_export_root_clients => '*(rw,fsid=0,insecure,no_subtree_check,async,no_root_squash)',
  }


  ### Shared documents area for Maximo

  $sm = '/shared/maximo'
  $d1 = "$sm/AppTier"
  $d2 = "$sm/AppDocs"
  $ce = downcase($cloudenv)
  nfs::server::export { $sm:
    ensure  => "mounted",
    owner => "httpd",
    group => "httpd",
    clients => "*(rw,insecure,async,no_root_squash,no_subtree_check)",
  } ->
    file {[ "$d1", "$d2",]: ensure => directory, owner => httpd, group => httpd } ->
    file {["$d1/bim", "$d1/integration", "$d2/DOCLINKS",]: ensure => directory, owner => httpd, group => httpd } ->
    file {[
      "$d1/bim/docs",
      "$d1/bim/import",
      "$d1/bim/models",
      "$d1/integration/PM_log",
      "$d1/integration/view",
      "$d2/DOCLINKS/attachments",
      "$d2/DOCLINKS/default",
      "$d2/DOCLINKS/diagrams",
      "$d2/DOCLINKS/images",
      "$sm/local_smtp_$ce",
    ]: ensure => directory, owner => httpd, group => httpd
    } ->
    file {[
      "$d1/integration/oraoutflat",
      "$d1/integration/oraload",
    ]: ensure => directory, owner => oraint, group => httpd, mode => 0775,
    } ->
    file {[
      "$d1/integration/oraoutflat/oragltxn",
      "$d1/integration/oraoutflat/oragr",
      "$d1/integration/oraoutflat/orapo",
      "$d1/integration/oraload/in",
    ]: ensure => directory, owner => oraint, group => httpd, mode => 0775, }



}
