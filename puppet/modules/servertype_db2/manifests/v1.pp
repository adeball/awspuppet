class servertype_db2::v1 {

  file { '/tmp/version':
    content => '1
',
  }

  include servertype_db2::firewall
  include servertype_db2::storage
  include users_ibm


  ### Workaround until the oracle integration housekeeping runs cleanly on the WAS
  ### manager nodes.
  include servertype_websphere::websphere_cfg
  include servertype_websphere::oraint_housekeep



  if $cloudenv == 'PRD' {
    class { 'local_db2backup':
      schedule => 'daily',
      online   => 'true',
    }
  } else {
    class { 'local_db2backup':
      schedule => 'weekly',
      online   => 'false',
    }
  }


  ### Stop/start script

  file { '/etc/init.d/db2':
    ensure => present,
    owner  => root,
    mode   => 700,
    source => 'puppet:///modules/servertype_db2/etc/init.d/db2',
  }
  file { '/etc/rc3.d/S99db2': ensure => link, target => '/etc/init.d/db2', }
  file { '/etc/rc2.d/K01db2': ensure => link, target => '/etc/init.d/db2', }


  class { 'local_ibmsupport': } ->
    class { 'local_ibminstall': } ->
    file { '/data/install/scripts':
      source  => 'puppet:///modules/servertype_db2/data/install/scripts',
      owner   => root,
      recurse => true,
      purge   => true,
    } ->
    file { '/data/install/SilentResponseFiles':
      source  => 'puppet:///modules/servertype_db2/data/install/SilentResponseFiles',
      owner   => root,
      recurse => true,
    } ->
    exec { 'Install DB2':
      path    => '/usr/bin:/usr/sbin',
      command => 'bash /data/install/scripts/install_db2_server.sh',
      creates => '/opt/maxapps/ibm/db2',
      timeout => '900',
    }


  ### /shared/maximo replication

  file { '/usr/local/bin/sync_shared_maximo.sh':
    ensure => present,
    source => 'puppet:///modules/servertype_db2/usr/local/bin/sync_shared_maximo.sh',
    owner  => root,
    mode   => 0755,
  }

  if $cloudenv == "PRD" {
    cron { 'Synchronise /shared/maximo':
      command => '/usr/local/bin/sync_shared_maximo.sh',
      user    => 'root',
      minute  => '*/10',
    }
  }

  file { '/opt/db2admin': ensure => directory, owner => db2admin, group => db2iadm1, mode => 0750 }
  file { '/opt/db2fenc1': ensure => directory, owner => db2fenc1, group => db2iadm1, mode => 0750 }
  file { '/opt/maximo': ensure => directory, owner => maximo, group => db2user, mode => 0755 }
  file { '/opt/mxsibmif': ensure => directory, owner => mxsibmif, group => db2user, mode => 0755 }
  file { '/opt/mxsibui': ensure => directory, owner => mxsibui, group => db2user, mode => 0755 }

}

