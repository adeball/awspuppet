class servertype_icinga2 {

  $version = hiera(common::version)
  case $version {
    '1': { include servertype_icinga2::v1 }
    '2': { include servertype_icinga2::v2 }
    'recover': { include servertype_icinga2::recover }
  }

}
