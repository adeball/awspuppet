class servertype_icinga2::v1 {

        file {'/tmp/version':
                content => '1
',
        }

        include servertype_icinga2::storage
        include servertype_icinga2::firewall

        class { '::icinga2':
          manage_repo => true,
        }
        # class {'::icingaweb2': }

}
