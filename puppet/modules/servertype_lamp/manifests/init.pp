class servertype_lamp {

  $version = hiera(common::version)
  case $version {
    '1': { include servertype_lamp::v1 }
    '2': { include servertype_lamp::v2 }
    'recover': { include servertype_lamp::recover }
  }

}
