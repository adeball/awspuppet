class servertype_lamp::storage {

  ### Disk device setup

  class { 'lvm':
    volume_groups => {
      'vg_data' => {
        physical_volumes => $common::lvmdisks,
        logical_volumes  => {
          ### 4GB minus one 4MB extent
          'swap' => { 'size' => '4092M', fs_type => swap, },
        },
      },
    },
  }

}
