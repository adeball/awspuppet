# servertype_maximo

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_maximo]](#setup)
    * [What [servertype_maximo] affects](#what-[servertype_maximo]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_maximo]](#beginning-with-[servertype_maximo])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up a 'Maximo administration server', which is used to deploy Maximo to Websphere, perform builds and provide file services to WAS, DB2 and IHS nodes.

A Maximo admin server is not required to be running to support normal operation of Maximo, i.e. it may be rebooted safely.  It does provide (amongst other things) NFS file services for installations and tempoarary sharing.  Scheduled tasks, however, may rely on these file services, so the Maximo admin server should remain running for most of time.  One Maximo admin server is sufficient for all client environments.
For the avoidance of doubt: NFS services which are required permanently (i.e. /shared/maximo) are configured on the relevant DB2 server in each environment.

## Setup

### What [servertype_maximo] affects 

* Development/build packages are installed
* IBM users/support configuration is deployed
* DB2 client software is deployed
* NodeJS is deployed
* A Jenkins slave client is deployed
* In RWG, local-connect and local-ohubbuild are also deployed
* NFSv4 server components are installed

### Setup Requirements

Use Hiera to install:

* local-nfssharedclient
* local-yarn

(These will be joined in Hiera by the other modules in due course - currently the other modules (local-jenkinsslave etc) are deployed directly by this one).

### Beginning with [servertype_maximo]	

include servertype_maximo

## Usage

## Reference

### Classes

#### ::firewall

* Allows TCP port 2049 (NFSv4)
* Allows TCP ports 9001-9004 & 9017-9022 (Docker)

#### ::storage

* Configures multiple volumes:
  * /shared/install
  * /shared/tmp
  * /data/deployment
  * /data/vol01
  * /opt/maxapps
  * /opt/IBM
  * /var/jenkins
  * swap
* Sets up NFSv4 and shares /shared/install + /shared/tmp
* Deploys and schedules a regular clean-up of /shared/tmp

## Limitations

RHEL7 only

## Development

The module needs more abstraction, with more definitions moved into Hiera.

