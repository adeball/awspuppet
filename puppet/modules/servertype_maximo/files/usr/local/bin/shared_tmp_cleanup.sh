#!/bin/bash
#
# Managed by Puppet
#

FS=/shared/tmp
MAXTIME=7

log=$(mktemp)
cat > $log << EOF

LATEST LOG: $(date)

Files deleted

EOF

### Remove files over MAXTIME days old
find $FS -type f -mtime +${MAXTIME} ! -wholename "${FS}/README" -ls -exec rm {} \; >> $log 2>&1

### Remove empty directories (NB needs to iterate as a directory might contain empty directories)
cat >> $log << EOF

Empty directories deleted

EOF

edirs=$(find $FS -type d -empty ! -name "lost+found")
while [[ -n "$edirs" ]] ; do
        echo $edirs >> $log
        find $FS -type d ! -name "lost+found" -empty -print0 | xargs -0 rmdir
        edirs=$(find $FS -type d -empty ! -name "lost+found")
done

cat > $FS/README << EOF

$FS - NFS mounted from ${CLOUDSERVERTYPE}-${CLOUDINSTANCE}.${CLOUDENV,,}.${CLOUDCLIENT,,}.$CLOUDDOMAIN

Use this area to share transient files between $CLOUDCLIENT systems.  The filesystem is
available to all environments, i.e. test and production servers will have the same content.

NB: To keep space clear, FILES OVER $MAXTIME DAYS OLD (and empty directories) ARE AUTOMATICALLY
	DELETED EACH NIGHT.  You can circumvent this by touching files automatically, but please
	don't - if you need to do this, there's probably a better way to achieve your purpose!


If you're looking for somewhere to save semi-permanent files for installation purposes, 
you probably want to use /shared/install instead.

Ade.


----------------------------------------------------
$(cat $log)

EOF

rm $log
