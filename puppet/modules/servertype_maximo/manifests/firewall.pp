class servertype_maximo::firewall {

  firewall { '02049 Allow NFSv4':
    dport    => 2049,
    proto    => tcp,
    action   => accept,
  }

  firewall { '02004 Allow mountd':
    dport    => 20048,
    proto    => tcp,
    action   => accept,
  }

  firewall { '00111 Allow tcp rpcbind':
    dport    => 111,
    proto    => tcp,
    action   => accept,
  }

  firewall { '00111 Allow udp rpcbind':
    dport    => 111,
    proto    => udp,
    action   => accept,
  }


  firewall {'09001 Docker':
    dport    => ['9001','9002','9003','9004','9017','9018','9019','9020','9021','9022'],
    proto    => tcp,
    action   => accept,
  }

}
