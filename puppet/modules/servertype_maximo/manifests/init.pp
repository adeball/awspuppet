class servertype_maximo {

  $version=hiera(common::version)
  case $version {
    '1':       { include servertype_maximo::v1 }
    '2':       { include servertype_maximo::v2 }
    'recover': { include servertype_maximo::recover }
  }

}
