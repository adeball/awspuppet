
class servertype_maximo::recover {

  include servertype_maximo::storage

  class { local_recover:
    require => Mount[['/data/vol01', '/opt/IBM',]]
  } ->
  notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

