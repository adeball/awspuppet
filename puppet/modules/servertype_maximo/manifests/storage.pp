
class servertype_maximo::storage {

  ### Disk device setup
  file { ['/data', ]: ensure => directory } -> ### /shared is defined in the common module
    file { [ '/data/vol01', '/opt/maxapps', '/shared/install', '/data/deployment',
      '/data/fixpacks', '/opt/IBM', '/shared/tmp', ]: ensure => directory } ->

    class { 'lvm':
      volume_groups => {
        'vg_data' => {
          physical_volumes => $common::lvmdisks,
          logical_volumes  => {
            'install'    => { 'size' => '50G', 'mountpath' => '/shared/install', 'mountpath_require' => true },
            'shared_tmp' => { 'size' => '24G', 'mountpath' => '/shared/tmp', 'mountpath_require' => true },
            'deployment' => { 'size' => '50G', 'mountpath' => '/data/deployment', 'mountpath_require' => true },
            'vol01'      => { 'size' => '70G', 'mountpath' => '/data/vol01', 'mountpath_require' => true },
            'maxapps'    => { 'size' => '50G', 'mountpath' => '/opt/maxapps', 'mountpath_require' => true },
            'IBM'        => { 'size' => '5G', fs_type => xfs, mountpath => '/opt/IBM', },
            'jenkins'    => { 'size' => '25G', fs_type => xfs, mountpath => '/var/jenkins', },
            'docker'     => { 'size' => '15G', fs_type => xfs, mountpath => '/var/lib/docker', },
            'swap'       => { 'size' => '10G', fs_type => swap,  },
          },
        },
      },
    }

  class { '::nfs':
    server_enabled => true,
    nfs_v4 => true,
    nfs_v4_idmap_domain => 'myco.cloud',
    nfs_v4_export_root  => '/export',
    nfs_v4_export_root_clients => '*(rw,fsid=0,insecure,no_subtree_check,async,no_root_squash)',
  }


  ### Shared installation code/scripts area

  $si = '/shared/install'
  nfs::server::export { $si:
    ensure  => "mounted",
    owner => "httpd",
    group => "httpd",
    clients => "*(rw,insecure,async,no_root_squash,no_subtree_check)",
  }


  ### Shared temporary storage

  $st = '/shared/tmp'
  nfs::server::export { $st:
    ensure  => "mounted",
    owner => "root",
    group => "root",
    clients => "*(rw,insecure,async,no_root_squash,no_subtree_check)",
  } ->
  file {'/usr/local/bin/shared_tmp_cleanup.sh':
    source => 'puppet:///modules/servertype_maximo/usr/local/bin/shared_tmp_cleanup.sh',
    owner  => root,
    mode   => 0755,
  } ->
  cron { 'Clean up /shared/tmp':
    command => '/usr/local/bin/shared_tmp_cleanup.sh',
    user    => 'root',
    hour    => '22',
    minute  => '55',
  }

}
