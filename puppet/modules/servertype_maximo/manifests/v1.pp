
class servertype_maximo::v1 {

	file {'/tmp/version': 
		content => '1
',
	}

	include servertype_maximo::storage
	include servertype_maximo::firewall
	include users_ibm

	ensure_packages(['ant', 'mercurial', 'lftp', 'docker',])

	file {'/etc/sysconfig/docker':
		ensure => present,
		source => 'puppet:///modules/servertype_maximo/etc/sysconfig/docker',
		owner  => root,
	} ->
	service {['docker']: ensure => 'running'}

	include local_ibminstall
	include local_ibmsupport

	### For Jenkins/build purposes
	include local_db2client
	include local_nodejs
	include local_jenkinsslave

	if $cloudclient == 'RWG' {
		include local_ohubbuild
		include local_connect
	}

}

