# servertype_nginx

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_nginx]](#setup)
    * [What [servertype_nginx] affects](#what-[servertype_nginx]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_nginx]](#beginning-with-[servertype_nginx])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up an instance as an NGINX server.

NGINX is used as the web server/load balancer in the RWG OHub configuration (see https://confluence.myco-group.com/pages/viewpage.action?pageId=30769974 & https://confluence.myco-group.com/display/RWGMROSI/OHub+Infrastructure).  This module deploys NGINX and configures it to point to the NodeJS & CouchDB services.

## Setup

### What [servertype_nginx] affects 

* Installs nginx package
* IPTables configured
* Installs AWStats 


### Setup Requirements 

This module will install without any pre-requisites in place, but it does require the nodejs/couchdb servers in the same client/environment to be available as it sets up nginx as a proxy to these services.

Security groups will need to be configured to match the firewall rules (see reference) where applicable.

### Beginning with [servertype_nginx]	

include servertype_nginx

## Usage

One parameter is available: $installawsstats, this should be either 'yes' (default) or any other value.  If set to 'yes', the AWStats package will be installed and configured to listen on port 81 to provide basic web server statistics.

## Reference

### Classes

#### ::firewall

* Allows access to TCP ports 80 & 443 (NGINX)
* Allows access to TCP port 81 (AWStats)

#### ::storage

* Configures swap

## Limitations

Tested on RHEL7 and also Amazon Linux (but not deployed to the latter, preliminary testing only)

## Development

Requires some abstraction - i.e. have NGINX itself installed using another module (e.g. local-nginx, which would need writing), and then configure here using Hiera.

