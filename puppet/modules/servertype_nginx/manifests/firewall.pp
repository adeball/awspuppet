class servertype_nginx::firewall {

  firewall { '80 Nginx HTTP/HTTPS ':
    dport  => ['80', '81', '443'],
    proto  => 'tcp',
    action => 'accept',
  }

}
