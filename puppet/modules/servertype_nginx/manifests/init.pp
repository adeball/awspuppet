class servertype_nginx (
  $installawstats = 'yes',
)
{
  $version = hiera(common::version)
  case $version {
    '1': { include servertype_nginx::v1 }
    '2': { include servertype_nginx::v2 }
    'recover': { include servertype_nginx::recover }
  }

}
