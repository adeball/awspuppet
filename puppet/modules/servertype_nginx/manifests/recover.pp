
class servertype_nginx::recover {

  include servertype_nginx::storage

  ### Needed for apache
  file {'/run/httpd':
    ensure => directory,
    owner => root,
  }

  class { local_recover: } ->
  notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

