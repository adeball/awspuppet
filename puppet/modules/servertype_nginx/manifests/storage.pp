class servertype_nginx::storage {

  ### Disk device setup

  class { 'lvm':
    volume_groups => {
      'vg_data' => {
        physical_volumes => $common::lvmdisks,
        logical_volumes  => {
          'swap'       => { 'size' => '10G', fs_type => swap,  },
        },
      },
    },
  }
}
