class servertype_nginx::v1 {

  file { '/tmp/version':
    content => '1
',
  }

  include servertype_nginx::firewall
  include servertype_nginx::storage

  exec { 'Set SELinux to permissive mode':
    command => 'setenforce 0',
    path    => '/usr/bin:/usr/sbin:/bin',
    unless  => 'test $(getenforce) == "Permissive" -o $(getenforce) == "Disabled" ',

  }


  ### NGINX

  # Puppet doesn't have variables, so $myenv must be declared only once
  if $cloudenv == "PRD" { $myenv = "" }
  if $cloudenv == "TST" { $myenv = "tst" }
  if $cloudenv == "TRN" { $myenv = "trn" }
  $server = "nginx-$cloudinstance.$cloudenv.$cloudclient.$clouddomain ${cloudclient}ohub${myenv}.mydomain.com"
  $couchdbserver = "${cloudclient}ohubdb${myenv}.mydomain.com"

  ### Redirect back out to ELB https port
  file { '/etc/nginx/redirect.inc':
    ensure  => present,
    owner   => root,
    content => '
    if ($http_x_forwarded_proto != \'https\') {
      rewrite ^ https://$host$request_uri? permanent;
    }
  ',
  }

  ### Declare main NGINX class
  class { 'nginx':

    ### Took some sorting out - this is needed when running behind a load-balancer.
    ### This ensures that the real ip address of the client is logged, rather than the load balancer IP
    http_cfg_prepend => {
      real_ip_header   => 'X-Forwarded-For',
      set_real_ip_from => '0.0.0.0/0',
    }

  }

  $staticd = '/var/www'

  if $cloudenv != 'PRD' {
    nginx::resource::server { "$server":
      ensure        => present,
      listen_port   => 80,
      www_root      => "$staticd",
      include_files => ['redirect.inc', ],
    }
  } else {
    nginx::resource::server { "$server":
      ensure      => present,
      listen_port => 80,
      www_root    => "$staticd",
      #include_files => ['redirect.inc',],
    }
  }


  nginx::resource::server { "$couchdbserver":
    ensure      => present,
    listen_port => 80,
    proxy       => 'http://couchdb/',
  }

  nginx::resource::location { "/couchdb/":
    proxy  => 'http://couchdb/',
    server => "$server",
    location_cfg_prepend => {
      proxy_redirect => 'off',
      proxy_buffering => 'off',
    }
  }


  nginx::resource::location { "/nodejs/":
    proxy  => 'http://nodejs/',
    server => "$server",
  }




  ## CouchDB

  nginx::resource::upstream { 'couchdb':
    members => [
      'couchdb-001:5984',
      #'couchdb-002:5984',
    ],
  }


  ## NodeJS

  if $cloudenv == "PRD" {
    nginx::resource::upstream { 'nodejs':
      members => [
        'nodejs-001:8181',
        'nodejs-002:8181',
      ],
    }
  } else {
    nginx::resource::upstream { 'nodejs':
      members => [
        'nodejs-001:8181',
      ],
    }
  }


  ## OHub static files

  group { 'connect': ensure => present, gid => 4001 } ->
    user { 'connect': ensure => present, uid => 4001, gid => connect, home => '/home/connect', managehome => true, } ->
    file { "$staticd":
      ensure => directory,
      mode   => 0755,
      owner  => connect,
    } ->
    nginx::resource::location { "/static/":
      server        => "$server",
      rewrite_rules => ['^(.*)/static(.*)$ $1$2', ],
      www_root      => "$staticd",
    }

  $key = hiera('local_jenkinsslave::sshpublickey')
  ssh_authorized_key { "connect@jenkins": user => "connect", type => 'ssh-rsa', key => "$key", }


  ### awstats
  if $servertype_nginx::installawstats == 'yes' {

    class { 'apache':
      default_vhost => false,
    }

    $loadplugin = ['decodeutfkeys', 'geoip GEOIP_STANDARD /usr/share/GeoIP/GeoIP.dat']
    $internal_hosts = 'REGEX[^192\.168\.] REGEX[^172\.16\.] REGEX[^10\.]'

    apache::vhost { ["$fqdn", "localhost", "$hostname", ]:
      port          => '81',
      docroot       => '/usr/share/awstats/wwwroot',
      serveraliases => ['awstats'],
      aliases       => [
        { alias => '/awstatsclasses', path => '/usr/share/awstats/wwwroot/classes/' },
        { alias => '/awstatscss', path => '/usr/share/awstats/wwwroot/css/' },
        { alias => '/awstatsicons', path => '/usr/share/awstats/wwwroot/icon/' },
      ],
      scriptaliases => [
        { alias => '/awstats/', path => '/usr/share/awstats/wwwroot/cgi-bin/' },
      ],
      directories   => [{
        path     => '/usr/share/awstats/wwwroot',
        provider => 'directory',
        options  => 'None',
        order    => 'Allow,Deny',
        allow    => 'from all',
        #deny    => 'from all',
      }],
      setenv        => ['PERL5LIB /usr/share/awstats/lib:/usr/share/awstats/plugins'],
    }

    class { '::awstats':
      config_dir_purge => true,
      enable_plugins   => [ 'DecodeUTFKeys', 'GeoIP' ],
    }

    # this ordering is needed for both the docroot path and so that the
    # awstats package provided apache configuration snippet is purged on the
    # first run
    Class['::awstats'] -> Class['::apache']

    # apache log
    awstats::conf { "$fqdn":
      options => {
        'SiteDomain'        => "${cloudservertype}-${cloudinstance}.${cloudenv}.${cloudclient}.${clouddomain}",
        'LogFile'           => "cat /var/log/nginx/${cloudservertype}-${cloudinstance}.${cloudenv}.${cloudclient}.${
          clouddomain}_${cloudclient}ohub${myenv}.mydomain.com.access.log |",
        'AllowFullYearView' => 3,
        'DNSLookup'         => 1,
        'LogFormat'         => '%host %other %other %time1 %methodurl %code %bytesd %otherquot %uaquot',
        #'SkipHosts'         => $internal_hosts,
        'LoadPlugin'        => $loadplugin,
      },
    }

    ### Update every hour
    cron { 'Update awstats':
      command => "/usr/share/awstats/wwwroot/cgi-bin/awstats.pl -config=$fqdn -update",
      user    => 'root',
      hour    => '*',
      minute  => '50',
    }

  }


}

