# servertype_nodejs

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_nodejs]](#setup)
    * [What [servertype_nodejs] affects](#what-[servertype_nodejs]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_nodejs]](#beginning-with-[servertype_nodejs])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up an instance as a NodeJS server, and installs the connect NodeJS application.

This module is very simple, most of the work is carried out by local-nodejs and local-connect.

## Setup

### What [servertype_nodejs] affects 

* This module inherits local-nodejs & local-connect, refer to those modules for further information.
* Sets up IPTables
* Configures SELinux to permissive mode
* Installs a Jenkins slave (see local-jenkinsslave) for automated deployments

### Setup Requirements 

### Beginning with [servertype_nodejs]	

include servertype_nodejs

## Usage

## Reference

### Classes

#### ::firewall

Placeholder only.  Firewall rules are applied by the local-connect module.

#### ::storage

Configures:

* /var/jenkins
* swap

## Limitations

Tested on RHEL7.

## Development

