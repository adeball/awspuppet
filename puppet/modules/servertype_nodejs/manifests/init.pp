class servertype_nodejs {

  $version = hiera(common::version)
  case $version {
    '1': { include servertype_nodejs::v1 }
    '2': { include servertype_nodejs::v2 }
    'recover': { include servertype_nodejs::recover }
  }

}
