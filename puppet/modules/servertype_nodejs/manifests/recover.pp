class servertype_nodejs::recover {

  include servertype_nodejs::storage

  class { local_recover:
    require => Mount[['/var/jenkins', ]]
  } ->
    notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

