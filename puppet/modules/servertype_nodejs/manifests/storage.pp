class servertype_nodejs::storage {

  if $cloudos == "linux" { $fs = "ext4" } else { $fs = "xfs" }
  ### Disk device setup
  file { '/home': ensure => directory } ->

    class { 'lvm':
      volume_groups => {
        'vg_data' => {
          physical_volumes => $common::lvmdisks,
          logical_volumes  => {
            'swap'    => { 'size' => '10G', fs_type => swap, },
            'jenkins' => { size => 5G, fs_type => $fs, mountpath => '/var/jenkins', },
          },
        },
      },
    }
}
