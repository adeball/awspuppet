
class servertype_nodejs::v1 {

	file {'/tmp/version': 
		content => '1
',
	}

	include servertype_nodejs::firewall
	include servertype_nodejs::storage

	include local_nodejs
	include local_connect
	include local_jenkinsslave

	exec { 'Set SELinux to permissive mode':
		command => 'setenforce 0',
		path    => '/usr/bin:/usr/sbin:/bin',
		unless  => 'test $(getenforce) == "Permissive" -o $(getenforce) == "Disabled" ',
	}

}

