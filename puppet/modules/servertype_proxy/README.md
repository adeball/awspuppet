# servertype_proxy

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_proxy]](#setup)
    * [What [servertype_proxy] affects](#what-[servertype_proxy]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_proxy]](#beginning-with-[servertype_proxy])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up an instance to run IBM's IHS proxy server (Apache, with IBM extensions).

## Setup

### What [servertype_proxy] affects

* Inherits local-ibminstall & local-ibmsupport 
* Installs start/stop script for IHS proxy
* Does NOT install IHS proxy software itself (this still needs to be added to the module)
* Installs/updates SSL certificates, where applicable
* Installs AWStats (web server statistics) if requested
* Sets up IBM HTTPD as a service


### Setup Requirements

To install/update SSL certificates, the various parameters need to be set up in Hiera (see usage).

### Beginning with [servertype_proxy]	

include servertype_proxy

## Usage

The following parameters are available (with default values shown):

* $installawstats='yes'
  * set to any other value ('no' would seem the obvious one) to not install AWSstats
* $sslrenewdir='/opt/maxapps/IBM/HTTPServer/bin/ssl/renewal'
  * temporary directory used for SSL certificate renewals
* $sslcert='none'
  * SSL certificate to be used on this Proxy
* $sslkey='none'
  * SSL Private key for $sslcert (use eyaml to store)
* $sslchain='none'
  * SSL chain, if provided
* $sslroot='none'
  * SSL root certificate for the CA, which may be needed
* $passout='none'
  * Output password, used to create the PKCS12 file, and then import into IHS
* $applysslrenewal='false'
  * Apply SSL certificate renewals, use to control deployment into different clients/environments as required
* $ssllabel='newcert'
  * This should be set to something meaningful e.g. 'mydomain.com_expires_2020-12-17'
* $sslkeyfile='key.kdb'
  * Arbitrary name for the key database, leaving it as-is should work

## Reference

### Classes

#### ::firewall

* Allows access to TCP ports 80, 81, 443 and 8008

#### ::storage

Configures:

* /data/install
* /opt/maxapps
* swap

## Limitations

RHEL7 only.

## Development

Requires automatic deployment/configuration of the IHS application adding.

