
class servertype_proxy::firewall {

  firewall { '020 HTTP':
    dport  => [80, 81, 443, 8008],
    proto  => 'tcp',
    action => 'accept',
  }

}
