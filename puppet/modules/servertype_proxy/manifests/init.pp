class servertype_proxy (
  $installawstats='yes',
  $sslrenewdir='/opt/maxapps/IBM/HTTPServer/bin/ssl/renewal',
  $sslcert='none',
  $sslkey='none',
  $sslchain='none',
  $sslroot='none',
  $passout='none',
  $applysslrenewal='false',
  $ssllabel='newcert',
  $sslkeyfile='key.kdb',
)
{

  $version = hiera(common::version)
  case $version {
    '1': { include servertype_proxy::v1 }
    '2': { include servertype_proxy::v2 }
    'recover': { include servertype_proxy::recover }
  }

}
