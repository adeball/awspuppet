class servertype_proxy::recover {

  include servertype_proxy::storage

  class { local_recover:
    require => Mount[['/opt/maxapps', ]]
  } ->
    notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

