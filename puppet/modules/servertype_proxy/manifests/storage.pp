class servertype_proxy::storage {

  ### Disk device setup

  file { '/data': ensure => directory } ->
    file { [ '/data/vol01', '/data/vol02', '/data/install', '/opt/maxapps', ]: ensure => directory } ->

    class { 'lvm':
      volume_groups => {
        'vg_data' => {
          physical_volumes => $common::lvmdisks,
          logical_volumes  => {
            'install' => { 'size' => '25G', 'mountpath' => '/data/install', 'mountpath_require' => true },
            'maxapps' => { 'size' => '20G', 'mountpath' => '/opt/maxapps',  'mountpath_require' => true },
            'swap'    => { 'size' => '10G', fs_type => swap,  },
          },
        },
      },
    }

}
