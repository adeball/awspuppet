
class servertype_proxy::v1 {

	file {'/tmp/version': 
		content => '1
',
	}
	### Ade 2017-03

	include servertype_proxy::firewall
	include servertype_proxy::storage

	include local_ibminstall
	include local_ibmsupport
	include users_ibm


	### SSL cert renewal
	if $servertype_proxy::applysslrenewal == 'true' {

		file { "$servertype_proxy::sslrenewdir": ensure => directory, owner => root, mode => 0700 } ->
			file { "$servertype_proxy::sslrenewdir/new.crt": ensure => present, owner => root, content => "$servertype_proxy::sslcert\n" } ->
			file { "$servertype_proxy::sslrenewdir/key.crt": ensure => present, owner => root, content => "$servertype_proxy::sslkey\n" } ->
			file { "$servertype_proxy::sslrenewdir/chain.crt": ensure => present, owner => root, content => "$servertype_proxy::sslchain\n" } ->
			file { "$servertype_proxy::sslrenewdir/root.crt": ensure => present, owner => root, content => "$servertype_proxy::sslroot\n" } ->

			exec { 'Create combined certificate':
				path    => '/sbin:/bin:/usr/sbin:/usr/bin',
				creates => "$servertype_proxy::sslrenewdir/all.crt",
				command => "cd $servertype_proxy::sslrenewdir ; cat new.crt chain.crt root.crt > all.crt",
			} ->
			exec { 'Create keystore database for IHS SSL certs':
				path    => '/sbin:/bin:/usr/sbin:/usr/bin',
				creates => "$servertype_proxy::sslrenewdir/keys_temp.p12",
				command => "cd $servertype_proxy::sslrenewdir; openssl pkcs12 -inkey key.crt -in all.crt -export -out keys_temp.p12 -name \"$servertype_proxy::ssllabel\" -passout pass:$servertype_proxy::passout",
			} ->
			exec {'Backup existing SSL files':
				path    => '/sbin:/bin:/usr/sbin:/usr/bin',
				creates => ["$servertype_proxy::sslrenewdir/../backup.tar", "$servertype_proxy::sslrenewdir/../${servertype_proxy::ssllabel}.imported", ],
				command => "cd $servertype_proxy::sslrenewdir/..; outf=\$(date +%Y-%m-%d-%H-%M).tar; \n
      find . -maxdepth 1 -type f | grep -v tar$ | cpio -oHtar > \$outf ; ln -s \$outf backup.tar",
			} ->
			exec {'Import certificate into renewal key DB':
				path    => '/sbin:/bin:/usr/sbin:/usr/bin',
				creates => "$servertype_proxy::sslrenewdir/$servertype_proxy::sslkeyfile",
				command => "cd $servertype_proxy::sslrenewdir; cp ../$servertype_proxy::sslkeyfile . ; \n
      ../../gskcmd -cert -import -file keys_temp.p12 -pw $servertype_proxy::passout -type pkcs12 -target $servertype_proxy::sslkeyfile -target_type cms -target_pw $servertype_proxy::passout"
			} ->
			exec { "Validate renewal certificate, make default and copy to $servertype_proxy::sslrenewdir":
				path    => '/sbin:/bin:/usr/sbin:/usr/bin',
				creates => "$servertype_proxy::sslrenewdir/../${servertype_proxy::ssllabel}.imported",
				command => "cd $servertype_proxy::sslrenewdir ; \n
      ../../gskcmd -cert -validate -db $servertype_proxy::sslkeyfile -label \"$servertype_proxy::ssllabel\" -pw $servertype_proxy::passout | egrep -vi \"warning|error\" ; \n
      if [[ \$? -eq 0 ]] ; then \n
        ../../gskcmd -cert -setdefault -db $servertype_proxy::sslkeyfile -label \"$servertype_proxy::ssllabel\" -pw $servertype_proxy::passout ; \n
        cp $servertype_proxy::sslkeyfile .. \n
        touch ../${servertype_proxy::ssllabel}.imported \n
        /etc/init.d/apachectl stop ;sleep 2 ; /etc/init.d/apachectl start \n
      else \n
        exit 1 \n
      fi \n
      "
			}

	} else {
		file {"$servertype_proxy::sslrenewdir/backup.tar": ensure => absent }
	}

	### Stop/start script for HTTP server (only if it has been installed)
	exec { 'Create IBM HTTPServer start script':
		command => 'sed "2i#\n# chkconfig: 345 80 20\n# description: IBM HTTPD Server" /opt/maxapps/IBM/HTTPServer/bin/apachectl > /etc/init.d/apachectl;
                chmod a+x /etc/init.d/apachectl;
                chkconfig --add apachectl;
                systemctl enable apachectl;
                systemctl start apachectl',
		onlyif  => 'test -f /opt/maxapps/IBM/HTTPServer/bin/apachectl',
		unless  => 'test -f /etc/init.d/apachectl',
		path    => '/usr/bin:/usr/sbin:/bin',
	}

	### Stop/start script for HTTP admin server (only if it has been installed (i.e. on the deployment manager))
	exec { 'Create IBM HTTPServer admin start script':
		command => 'sed "2i#\n# chkconfig: 345 70 30\n# description: IBM HTTPD Admin Server" /opt/maxapps/IBM/HTTPServer/bin/adminctl > /etc/init.d/adminctl;
                chmod a+x /etc/init.d/adminctl;
                chkconfig --add adminctl;
                systemctl enable adminctl;
                systemctl start adminctl',
		onlyif  => 'test -f /opt/maxapps/IBM/HTTPServer/bin/adminctl',
		unless  => 'test -f /etc/init.d/adminctl',
		path    => '/usr/bin:/usr/sbin:/bin',
	}
	### awstats
	if $servertype_proxy::installawstats == 'yes' {

		class { 'apache':
			default_vhost => false,
		}

		$loadplugin     = ['decodeutfkeys', 'geoip GEOIP_STANDARD /usr/share/GeoIP/GeoIP.dat']
		$internal_hosts = 'REGEX[^192\.168\.] REGEX[^172\.16\.] REGEX[^10\.]'

		apache::vhost { ["$fqdn","localhost","$hostname",]:
			port          => '81',
			docroot       => '/usr/share/awstats/wwwroot',
			serveraliases => ['awstats'],
			aliases => [
				{ alias => '/awstatsclasses', path => '/usr/share/awstats/wwwroot/classes/' },
				{ alias => '/awstatscss', path => '/usr/share/awstats/wwwroot/css/' },
				{ alias => '/awstatsicons', path => '/usr/share/awstats/wwwroot/icon/' },
			],
			scriptaliases => [
				{ alias => '/awstats/', path => '/usr/share/awstats/wwwroot/cgi-bin/' },
			],
			directories   => [{
				path     => '/usr/share/awstats/wwwroot',
				provider => 'directory',
				options  => 'None',
				order    => 'Allow,Deny',
				allow    => 'from all',
				#deny    => 'from all',
			}],
			setenv        => ['PERL5LIB /usr/share/awstats/lib:/usr/share/awstats/plugins'],
		}

		class { '::awstats':
			config_dir_purge => true,
			enable_plugins   => [ 'DecodeUTFKeys', 'GeoIP' ],
		}

		# this ordering is needed for both the docroot path and so that the
		# awstats package provided apache configuration snippet is purged on the
		# first run
		Class['::awstats'] -> Class['::apache']

		# apache log
		awstats::conf { "$fqdn":
			options => {
				'SiteDomain'        => "${cloudservertype}-${cloudinstance}.${cloudenv}.${cloudclient}.${clouddomain}",
				'LogFile'           => "cat /opt/maxapps/IBM/HTTPServer/logs/access_log |",
				'AllowFullYearView' => 3,
				'DNSLookup'         => 1,
				'LogFormat'         => '%host %other %other %time1 %methodurl %code %bytesd',
				#'SkipHosts'         => $internal_hosts,
				'LoadPlugin'        => $loadplugin,
			},
		}

		### Update every hour
		cron {'Update awstats':
			command => "/usr/share/awstats/wwwroot/cgi-bin/awstats.pl -config=$fqdn -update",
			user    => 'root',
			hour    => '*',
			minute  => '50',
		}

	}
}

