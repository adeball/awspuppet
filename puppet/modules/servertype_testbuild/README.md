# servertype_testbuild

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_testbuild]](#setup)
    * [What [servertype_testbuild] affects](#what-[servertype_testbuild]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_testbuild]](#beginning-with-[servertype_testbuild])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Minimalist servertype, deploys only common software.

This servertype is only used for testing the build process itself

## Setup

### Beginning with [servertype_testbuild]	

include servertype_testbuild

## Usage

## Reference

### Classes

#### ::firewall

Does nothing

#### ::storage

Deploys swap

## Limitations

RHEL7 & Amazon Linux

## Development

