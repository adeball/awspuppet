

class servertype_testbuild {

  $version=hiera(common::version)
  case $version {
    '1': { include servertype_testbuild::v1 }
    '2': { include servertype_testbuild::v2 }
  }

  include servertype_testbuild::firewall
  include servertype_testbuild::storage


}
