class servertype_testbuild::storage {

  ### Disk device setup

  $disks=hiera('common::lvmdisks')

  class { 'lvm':
    volume_groups => {
      'vg_data' => {
        physical_volumes => $disks,
        logical_volumes  => {
          ### 4GB minus one 4MB extent
          'swap' => { 'size' => '4092M', fs_type => swap, },
        },
      },
    },
  }
}
