# servertype_vpn

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_vpn]](#setup)
    * [What [servertype_vpn] affects](#what-[servertype_vpn]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_vpn]](#beginning-with-[servertype_vpn])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Deploys an instance configured with OpenVPN.

OpenVPN is configured to connect to an OpenVPN server at the other end of the link, using the parameters provided.

## Setup

### What [servertype_vpn] affects 

* Ensures SELinux is set to permissive
* Configures IPTables
* Adds tcpdump
* Sets up IPv4 forwarding
* Deploys and configures OpenVPN, using the PuppetForge OpenVPN module

### Setup Requirements 

The instance should be configured with "Source/dest check" = false in AWS.

### Beginning with [servertype_vpn]	

include servertype_vpn

## Usage

Currently, the module has various configuration data coded in.  It needs abstracting into Heira.  For the moment, if changes are needed, code them into the module.

## Reference

### Classes

#### ::firewall

* Allows UDP <$cloudvpnport> (where this is defined in the infrastructure table)
* Allows TCP ports 8140 (puppet), 4949 (munin) and 1984 (Xymon).  This enables that traffic to pass over the VPN
* Allows UDP ports 1514 & 1515 - this was for OSSEC HIDS server access, which is not now required, this may be removed.
* Sets up NAT masquerading using generic subnets (needs making more precise, using Hiera)

#### ::storage

* Configures swap

## Limitations

Tested on RHEL7 and Amazon Linux

## Development

To be parameterised (Hiera).

