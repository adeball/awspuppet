class servertype_vpn::firewall {

  firewall { "$cloudvpnport OpenVPN":
    dport  => ["$cloudvpnport"],
    proto  => 'udp',
    action => 'accept',
  }

  firewall {'8140 Admin TCP ports via VPN':
    dport  => [8140, 4949, 1984],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall {'1514 Admin UDP ports via VPN':
    dport  => [1514, 1515],
    proto  => 'udp',
    action => 'accept',
  }

  firewall {'10 NAT':
    chain => 'POSTROUTING',
    jump  => 'MASQUERADE',
    proto => 'all',
    outiface => 'eth0',
    source => '10.0.0.0/8',  ### Can be set to the VPC, but this makes it universal
    table  => 'nat',
  }
  firewall {'169 NAT':
    chain => 'POSTROUTING',
    jump  => 'MASQUERADE',
    proto => 'all',
    outiface => 'eth0',
    source => '169.254.255.0/24',  ### Can be set to the remote IP
    table  => 'nat',
  }

}
