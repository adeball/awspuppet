
### To be done: make this take port/IP parameters

class servertype_vpn {

  $version = hiera(common::version)
  case $version {
    '1': { include servertype_vpn::v1 }
    '2': { include servertype_vpn::v2 }
    #'recover': { include servertype_vpn::recover } - don't bother, just rebuild
  }


}
