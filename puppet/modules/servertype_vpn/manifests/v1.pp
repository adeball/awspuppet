
class servertype_vpn::v1 {

	file {'/tmp/version': 
		content => '1
',
	}

	include servertype_vpn::firewall
	include servertype_vpn::storage

	### Revisit this when we can stop selinux being a pain.
	### Non standard port causes problems
	exec { 'Set SELinux to permissive mode':
		command => 'setenforce 0',
		path    => '/usr/bin:/usr/sbin:/bin',
		unless  => 'test $(getenforce) == "Permissive" -o $(getenforce) == "Disabled" '
	}

	ensure_packages(['tcpdump',])

	include '::openvpn'
	openvpn::secret { 'ovpn.key':
		source => 'puppet:///modules/servertype_vpn/etc/openvpn/ovpn.key',
	}

	openvpn::conftemplate { $cloudclient:
		dev              => 'tun0',
		remote           => '54.77.244.33',
		ipaddress_local  => "$cloudvpnint",
		ipaddress_remote => "$cloudvpnext",
		routes           => [ '14.1.0.0 255.255.0.0' ],
		secret           => 'ovpn.key',
		port             => $cloudvpnport,
		log              => "/var/log/openvpn-$cloudclient.log",
	}

	### IPV4 forwarding (possibly OTT, but it shows what the state should be)
	file {'/etc/sysctl.d/S90net.ipv4.ip_forward.conf':
		ensure => present,
		source => 'puppet:///modules/servertype_vpn/etc/sysctl.d/S90net.ipv4.ip_forward.conf',
		owner => root,
	}

	exec { 'Set IPV4 forwarding, no reboot needed':
		command => 'echo 1 > /proc/sys/net/ipv4/ip_forward',
		path    => '/usr/bin:/usr/sbin:/bin',
		unless  => 'test $(cat /proc/sys/net/ipv4/ip_forward) == 1',
	}

	### Amazon Linux 2 thinks it is CentOS7, but it isn't :-(
	### No service start/stop is configured
	if $::operatingsystem == "Amazon" {
		file { '/etc/init.d/openvpn':
			source => 'puppet:///modules/servertype_vpn/etc/init.d/openvpn',
			owner  => root,
			group  => root,
			mode   => 0755,
		}
		file {'/var/run/openvpn':
			ensure => directory,
			owner => root,
			group => root,
			mode => 710,
		}
	}

}

