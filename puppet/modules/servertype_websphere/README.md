# servertype_websphere

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_websphere]](#setup)
    * [What [servertype_websphere] affects](#what-[servertype_websphere]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_websphere]](#beginning-with-[servertype_websphere])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up an instance for Websphere Application Server installation/use.

Applies all pre-requisites, user accounts, filesystems, system configuration etc required to support IBM WAS.  Also installs the IBM installation manager.

## Setup

### What [servertype_websphere] affects 

* Inherits local-ibmsupport
* Inherits local-ibminstall
* Installs TrueType fonts
* Adds the FTP client for integration purposes
* Configures Websphere Application Server as a service

NB: This module *DOES NOT* install WAS itself.  This functionality needs to be added.

### Setup Requirements 

### Beginning with [servertype_websphere]	

include servertype_websphere

## Usage

There is currently no customisation required or available.
AWS security groups need to be configured to allow access to all the TCP ports in the ::firewall class.

## Reference

### Classes

#### ::firewall

* Enables access to multiple TCP ports required for WAS operation.  Refer to the module.

#### ::storage

Configures:

* /data/vol01
* /opt/maxapps
* swap

## Limitations

RHEL7 only

## Development

The module requires parameterisation (using Hiera).  Once done, it should be possible to include local-websphere (to be written) which will install and configure WAS.

