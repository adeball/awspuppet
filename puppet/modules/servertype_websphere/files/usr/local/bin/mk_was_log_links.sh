#!/bin/bash
#
# This file is managed by Puppet
#
# -l : list logs
#

. /usr/local/etc/websphere.cfg

LOGBASE=/var/log/websphere
PROFILEDIR=$BASEDIR/IBM/WebSphere/AppServer/profiles

cd $PROFILEDIR

getopts "l" opt

if [[ "$opt" == "l" ]] ; then

    ### Use this form as we need a list of full paths
    ls -1 $LOGBASE/*

else

mkdir -p $LOGBASE


    PROFILES=$(ls)

    for profile in $PROFILES ; do

        find $profile -type f | grep SystemOut.log$ | while read log; do

            echo $log

            shortname=$(echo $log | sed -e 's/\.\///;s/\//_/g' | sed -e 's/_logs//;s/_SystemOut//')
            ln -s $PROFILEDIR/$log $LOGBASE/$shortname

        done

    done

fi
