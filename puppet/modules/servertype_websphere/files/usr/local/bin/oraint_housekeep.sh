#!/bin/bash
#
# This file is managed by Puppet
#
#

### Load configuration (BASEDIR, ORAINT_HOUSEKEEP and ORAINT_HOUSEKEEP_AGEINDAYS are relevant here)
. /usr/local/etc/websphere.cfg

### Check if we should run here
if [[ ${ORAINT_HOUSEKEEP} != "true" ]] ; then
	echo "ORAINT_HOUSEKEEP is not set to true, please check /usr/local/etc/websphere.cfg"
	echo "and/or settings in Hiera"
	echo "Aborting run..."
	exit 1
fi

NFSBASE=/shared/maximo
INTBASE=$NFSBASE/AppTier/integration


grep -q $NFSBASE /etc/mtab
if [[ $? -ne 0 ]] ; then
    echo "$NFSBASE does not appear to be mounted"
    exit 1
fi

cd $INTBASE
if [[ $? -ne 0 ]] ; then
    echo "Cannot cd $INTBASE"
    exit 1
fi


targetdirs=$(find . -type d | egrep "/backup$|/processed$")

for targetdir in $targetdirs ; do
    echo
    echo "Archiving $INTBASE/$targetdir"
    echo
    archivedir=${targetdir}_archive
    mkdir -p $archivedir

    oldfiles=$(find $targetdir -type f -mtime +${ORAINT_HOUSEKEEP_AGEINDAYS})
    for f in $oldfiles; do
        echo "Archiving $f"
        mv $f $archivedir
    done

    ### We're using NFS, so some files will not be cpio-ed on the first run as the
    ### directory entry is cached.  After 60 seconds (i.e. the next run probably) the files will be
    ### put into archives.

    cd $archivedir
    ### Find the oldest non-cpio file
    oldestfile=$(ls -t | grep -v cpio | head -1)
    if [[ -n "$oldestfile" ]] ; then
        startyear=$(stat $oldestfile | awk '/Modify/ {split($2,a,"-"); print a[1]}')
        currentyear=$(date +%Y)
        for year in $(seq $startyear $currentyear) ; do
            ### cpio_del_month is quick for empty months, so just do them all, even for the start year
            for month in $(seq 1 12) ; do
                /usr/local/bin/cpio_del_month $year $month
            done
        done
    fi

    cd $INTBASE

done
