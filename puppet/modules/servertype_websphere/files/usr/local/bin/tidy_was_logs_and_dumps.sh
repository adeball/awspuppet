#!/bin/bash
#
# This file is managed by Puppet
#
# Options: -l : list files which would be (or, if with -d, are being) deleted
#          -d : delete files
#

list=0 ; delete=0

while getopts "ld" opt; do
    case $opt in
        l)
            list=1
            ;;
        d)
            delete=1
            ;;
        \?)
            echo "Usage: $0 [-l] [-d] (-l long lists, -d deletes/gzips)"
            exit 1
            ;;
    esac
done


[[ $list -eq 0 && $delete -eq 0 ]] && echo "No switches given, you probably want to use -l (list) and/or -d (delete/gzip)"


### Load configuration (BASEDIR, WASLOGAGEINDAYS & WASDUMPAGEINDAYS are relevant here)
. /usr/local/etc/websphere.cfg

PROFILEDIR=$BASEDIR/IBM/WebSphere/AppServer/profiles

cd $PROFILEDIR

for p in $(ls) ; do

    cd $p

    if [[ $list -eq 1 ]] ; then
        echo
        echo "Dumps found in $PROFILEDIR/$p over ${WASDUMPAGEINDAYS} days old"
        find . -mtime +${WASDUMPAGEINDAYS} -ls | egrep "./Snap.*trc$|./javacore.*txt$|./core.*dmp$"
        echo
        echo "Logs found in $PROFILEDIR/$p over ${WASLOGAGEINDAYS} days old"
        find logs -mtime +${WASLOGAGEINDAYS} -ls | egrep "/SystemOut_.*log$|/SystemErr_.*log$"
    fi

    if [[ $delete -eq 1 ]] ; then
        echo "Deleting dumps in $PROFILEDIR/$p over ${WASDUMPAGEINDAYS} days old"
        find . -mtime +${WASDUMPAGEINDAYS} | egrep "./Snap.*trc$|./javacore.*txt$|./core.*dmp$" | xargs rm -f
        echo "Gzipping logs in $PROFILEDIR/$p over ${WASLOGAGEINDAYS} days old"
        logfiles=$(find logs -mtime +${WASLOGAGEINDAYS} | egrep "/SystemOut_.*log$|/SystemErr_.*log$")
        [[ -n "$logfiles" ]] && echo $logfiles | xargs gzip
    fi

    cd $PROFILEDIR

done
