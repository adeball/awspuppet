class servertype_websphere::firewall {

  ### FIREWALL

  firewall { '200 Websphere Node':
    dport  => [ 2810, 5000-5001, 5060-5063, 5558-5559, 5578-5579, 7061, 7272, 7276, 7278, 7286-7287, ],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '201 Websphere Node':
    dport  => [ 8878, 8880-8881, 9044-9045, 9061-9062, 9080-9081, 9101-9103, 9201-9202, ],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '202 Websphere Node':
    dport  => [ 9353-9355, 9443-9444, 9626, 9633-9634, 9810-9811, 9901, 11001-11002, 11007-11010, ],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '203 Websphere Node (RMI)':
    dport  => [ 13400-13429, ],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '206 Websphere (MobileFirst)':
    dport  => [ 5064-5065, 5560, 5580, 7279, 7288, 8882, 9046, 9063, ],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '207 Websphere (MobileFirst)':
    dport  => [ 9063, 9082, 9104, 9356, 9445, 9635, 9812, 11011-11012, ],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '300 WWW':
    dport  => [80, 443, 8008],
    proto  => 'tcp',
    action => 'accept',
  }

  firewall { '1340 RMI registry':
    dport  => [13400-13402],
    proto  => 'tcp',
    action => 'accept',
  }

}
