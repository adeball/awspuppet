
class servertype_websphere::oraint_housekeep {

  ### Oracle Integration housekeeping
  if $servertype_websphere::oraint_housekeep or $servertype_db2::oraint_housekeep {
    file { '/usr/local/bin/oraint_housekeep.sh':
      ensure => present,
      owner  => root,
      mode   => 0755,
      source => 'puppet:///modules/servertype_websphere/usr/local/bin/oraint_housekeep.sh',
    }
    file { '/etc/cron.daily/oraint_housekeep.sh':
      ensure => link,
      target => '/usr/local/bin/oraint_housekeep.sh',
    }
  } else {
    file { '/etc/cron.daily/oraint_housekeep.sh':
      ensure => absent,
    }
  }

}