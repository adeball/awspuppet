class servertype_websphere::recover {

  include servertype_websphere::storage

  class { local_recover:
    require => Mount[['/data/vol01', ]]
  } ->
    notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

