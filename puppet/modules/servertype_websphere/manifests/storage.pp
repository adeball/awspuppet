class servertype_websphere::storage {

  ensure_packages(['lvm2',])

  ### Disk device setup

  file { '/data': ensure => directory } ->
    # file { [ '/data/vol01', '/data/vol02', '/opt/maxapps', '/data/install' ]: ensure => directory } ->
    file { [ '/data/vol01', '/opt/maxapps', ]: ensure => directory } ->

    class { 'lvm':
      volume_groups => {
        'vg_data' => {
          physical_volumes => $common::lvmdisks,
          logical_volumes  => {
            # 'install' => { 'size' => '25G', 'mountpath' => '/data/install', 'mountpath_require' => true },
            'vol01'   => { 'size' => '40G', 'mountpath' => '/data/vol01', 'mountpath_require' => true },
            # 'vol02'   => { 'size' => '140G', 'mountpath' => '/data/vol02', 'mountpath_require' => true },
            'maxapps' => { 'size' => '50G', 'mountpath' => '/opt/maxapps', 'mountpath_require' => true },
            'swap'    => { 'size' => '10G', fs_type => swap, },
          },
        },
      },
    }
}
