
class servertype_websphere::v2 {

	file {'/tmp/version': 
		content => '2
',
	}

	### Ade 2017-03

	include local_ibminstall
	include local_ibmsupport
	include local_ibmsupport::truetype
	include servertype_websphere::storage
	include servertype_websphere::firewall
	include users_ibm

	ensure_packages('ftp')


	### Log links

	file {'/usr/local/bin/mk_was_log_links.sh':
		owner => root,
		mode => 0755,
		source => 'puppet:///modules/servertype_websphere/usr/local/bin/mk_was_log_links.sh',
	} ->
	exec {'Make Websphere log links':
		path => '/usr/bin:/usr/bin:/bin',
		command => '/usr/local/bin/mk_was_log_links.sh',
		onlyif => 'test -d /opt/maxapps/IBM/WebSphere/AppServer/profiles',
		creates => '/var/log/websphere',
	}


	### Stop/start script

	file { '/etc/init.d/websphere':
		ensure => present,
		owner  => root,
		mode   => 700,
		source => 'puppet:///modules/servertype_websphere/etc/init.d/websphere',
	}
	file { '/etc/rc3.d/S99websphere': ensure => link, target => '/etc/init.d/websphere', }
	file { '/etc/rc2.d/K01websphere': ensure => link, target => '/etc/init.d/websphere', }


	### Stop/start script for HTTP admin server (only if it has been installed (i.e. on the deployment manager))
	exec { 'Link to IBM HTTPServer admin start script':
		command => 'cd /etc/init.d ; ln -s /opt/maxapps/IBM/HTTPServer/bin/adminctl ;
                cd /etc/rc3.d ; ln -s ../init.d/adminctl ./S99adminctl
                cd /etc/rc2.d ; ln -s ../init.d/adminctl ./K01adminctl',
		onlyif  => 'test -f /opt/maxapps/IBM/HTTPServer/bin/adminctl',
		unless  => 'test -f /etc/init.d/adminctl',
		path    => '/usr/bin:/usr/sbin:/bin',
	}

	### Stop/start script for HTTP server (only if it has been installed)
	exec { 'Link to IBM HTTPServer start script':
		command => 'cd /etc/init.d ; ln -s /opt/maxapps/IBM/HTTPServer/bin/apachectl ;
                cd /etc/rc3.d ; ln -s ../init.d/apachectl ./S99apachectl
                cd /etc/rc2.d ; ln -s ../init.d/apachectl ./K01apachectl',
		onlyif  => 'test -f /opt/maxapps/IBM/HTTPServer/bin/apachectl',
		unless  => 'test -f /etc/init.d/apachectl',
		path    => '/usr/bin:/usr/sbin:/bin',
	}

}

