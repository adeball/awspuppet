
class servertype_websphere::websphere_cfg {

  ### Config file

  file {'/usr/local/etc/websphere.cfg':
    owner => root,
    group => root,
    mode => 0700,
    content => template('servertype_websphere/websphere.cfg.erb'),
  }

}