# servertype_webspheremgr

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [servertype_webspheremgr]](#setup)
    * [What [servertype_webspheremgr] affects](#what-[servertype_webspheremgr]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [servertype_webspheremgr]](#beginning-with-[servertype_webspheremgr])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up an instance for Websphere Application Server (manager node) installation/use.

This module inherits servertype-websphere, as the manager node requires everything a normal WAS node requires.  It additionally adds firewall rules.

## Setup

### What [servertype_webspheremgr] affects 

* Inherits servertype-websphere
* Adds firewall rules (in init.pp) for WAS manager use

### Setup Requirements 

### Beginning with [servertype_webspheremgr]	

include servertype_webspheremgr

## Usage

There is currently no customisation required or available.
AWS security groups need to be configured to allow access to all the TCP ports in the servertye-websphere:firewall class, and those in init.pp.

## Reference

### Classes

#### ::firewall

Does nothing

#### ::storage

Does nothing

## Limitations

RHEL7 only

## Development

The module requires parameterisation (using Hiera).  Once done, it should be possible to include local-webspheremgr (to be written) which will install and configure a WAS manager node.

The firewall rules need moving into the ::firewall class.

