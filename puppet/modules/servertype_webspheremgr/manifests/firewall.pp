class servertype_webspheremgr::firewall {

  firewall { '204 Websphere Manager':
    dport => [ 5555, 7060, 7277, 8879, 9043, 9060, 9083, 9100, ],
    proto  => 'tcp',
    action => 'accept',
  }
  firewall { '205 Websphere Manager':
    dport => [ 9352, 9401-9403, 9420, 9632, 9809, 11005-11006, ],
    proto  => 'tcp',
    action => 'accept',
  }

}
