
class servertype_webspheremgr {

  $version = hiera(common::version)
  case $version {
    '1': { include servertype_webspheremgr::v1 }
    '2': { include servertype_webspheremgr::v2 }
    'recover': { include servertype_webspheremgr::recover }
  }



}
