class servertype_webspheremgr::recover {

  ### NB: webspheremgr::storage is blank
  include servertype_websphere::storage

  class { local_recover:
    require => Mount[['/data/vol01', ]]
  } ->
    notify { 'Recovery complete.  Now change the build version from recover to 1 or 2 etc...': }

}

