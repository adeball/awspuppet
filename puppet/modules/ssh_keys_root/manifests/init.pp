
class ssh_keys_root {
  ssh_authorized_key {'ade@root':
    ensure  => present,
    type    => 'ssh-rsa',
    key     => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCgHH7vHKUPf1PI9XF2aDNfnp/qzpLnk5iEvnc9XWX6H+6Tnvajb7PgKPq/mnM7yFgH3DRd3j43A06jFeRFI/sUQOejThK2P8PXzNGfGG11OKBVvaJrg21vJj8yXV/gflKpv0NWmip6z4mPDj0JNcPSJVlCFpgTxbfdL849aTjhlW5dWl4XMrH1uNIKymY0lifb3GxQvg6s586sr3oTWkwr6tpczKEVFWTrbCmAg2Jr5LA3iAYkfbDYl66/sFHHqCaT5gLwfeOLxcwrjjGgXMBB4pqOIfnnUExX7nCoYQ5NroYs7neq68sDTUMQ+fce+R141X2AU+6eBui0uV7zNEPf',
    user    => 'root',
  }
}

