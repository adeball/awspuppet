# users_admin

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [users_admin]](#setup)
    * [What [users_admin] affects](#what-[users_admin]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [users_admin]](#beginning-with-[users_admin])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up accounts for privileged administration users (Unix login accounts).

A number of users need login access to servers, and with privileges.  This may be for support or project implementation work.  It is possible to use the ec2-user & key, but this (whilst essential for initial build troubleshooting and potentially for emergency access) is not easily auditable.  It also requires every user to have access to the ec2-user key for each client account.

This module sets up user accounts, with sudo rights, using their own user account and SSH public key.  The private key is the user's responsibility.

## Setup

### What [users_admin] affects 

User accounts

### Beginning with [users_admin]	

include users_admin

## Usage

All instances should inherit this module (so include in the common list, either as a class in Hiera, or by adding to the common module).

## Reference

The current mechanism for creating user accounts is somewhat simplistic.  Users are configured directly in the module code.  To add a new user, you will need to add them to init.pp using the existing users as a reference.  

You must also ensure no UID/GID clashes - the mechanism for this is simply by manual addition to the user_table file. This is purely used as a reference/record of UIDs/GIDs/names used.

NB: Sometimes developers need access to production systems.  In these cases, use 'adm' as a suffix to the username (this and the normal account can use the same SSH key of course).

## Limitations

## Development

This module, and the other users-* modules need redeveloping to obtain their data from Hiera which will define usernames, groupnames, UIDs & GIDs along with where they should be instantiated.  It is anticipated that a single 'users' module will achieve this.

