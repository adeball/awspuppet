
class users_admin (
  $adminacct='none',
  $adminacctpublickey='none',
  $adminacctprivatekey='none',
  $adminacctuid='none',
){

  ### Any user in the admin group gets sudo rights
  group { 'admin': ensure => present, gid => 5000 }


  if $adminacct != 'none' {

    user { "$adminacct": ensure => present, uid => $adminacctuid, gid => 'admin', home => "/home/$adminacct", managehome => true }
    ssh_authorized_key { "$adminacct@admin":
      ensure => present,
      user   => "$adminacct",
      type   => 'ssh-rsa',
      key    => "$adminacctpublickey",
    }
    file {"/home/$adminacct": mode => 0755}
    if $adminacctkey != 'none' {
      file { "/home/$adminacct/.ssh":
        ensure => directory,
        owner  => "$adminacct",
        mode   => 0700,
      } ->
        file { "/home/$adminacct/.ssh/id_rsa":
          ensure  => present,
          owner   => "$adminacct",
          mode    => 0600,
          content => "$adminacctprivatekey\n"
        }
    }
  }



  ### Ade

  user { 'ade': ensure => present, uid => 5001, gid => 'admin', home => '/home/ade', managehome => true }
  ssh_authorized_key {'ade@admin':
    ensure  => present,
    type    => 'ssh-rsa',
    key     => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCgHH7vHKUPf1PI9XF2aDNfnp/qzpLnk5iEvnc9XWX6H+6Tnvajb7PgKPq/mnM7yFgH3DRd3j43A06jFeRFI/sUQOejThK2P8PXzNGfGG11OKBVvaJrg21vJj8yXV/gflKpv0NWmip6z4mPDj0JNcPSJVlCFpgTxbfdL849aTjhlW5dWl4XMrH1uNIKymY0lifb3GxQvg6s586sr3oTWkwr6tpczKEVFWTrbCmAg2Jr5LA3iAYkfbDYl66/sFHHqCaT5gLwfeOLxcwrjjGgXMBB4pqOIfnnUExX7nCoYQ5NroYs7neq68sDTUMQ+fce+R141X2AU+6eBui0uV7zNEPf',
    user    => 'ade',
  }

  }


  include sudo
  sudo::conf { 'admin':
    priority => 500,
    content  => '%admin ALL=(ALL) NOPASSWD: ALL',
  }
  sudo::conf { 'ec2-user':
    priority => 510,
    content  => '%ec2-user ALL=(ALL) NOPASSWD: ALL',
  }

}
