
class users_developers {

  group { 'devs': ensure => present, gid => 3000 }

  ### User1
  user { 'user1': ensure => present, uid => 3001, gid => devs, home => '/home/user1', managehome => true }
  ssh_authorized_key { 'user1@devs': user => 'user1', type => 'ssh-rsa', key =>
    'AAAAB3NzaC1yc2EAAAADAQABAAABAQDcdrmIjtD5nVAKRz5PAgK/x5bG5uM+sJkBglKnhfqvUydT6oLjzTAY6Ep8mQf3cwRZ+g6LAbD9ji79LW+S0vpxA0RT1okKITrvbrn17VwwdGfUkqfEpxEGmqgvRiohCNkXH2wnlGVOFQ2cjV1Mjds4+UO9Jwve7FmaaMo+nvHVQ7l5IUSkz5RiiwIRj4rgKCKHMEKIXSJ7tEzgN2aMZzLSL8kmv0nfbLl2EfT5UMqmzXoex+WYbya9Ut6s/HHow7gvPMy8jCpx0z4m3Z1TwGMvn36lLHuJKDIWyXU2/2hsSEbnYAm5RNQPBJwyMRkVTQdmcaJExiAn52QaLGfYDCb3'
  }


  ### NB: This should be restricted to dev/tst etc in the common module
  include sudo
  sudo::conf { 'devs':
    priority => 300,
    content  => '%devs ALL=(ALL) NOPASSWD: ALL',
  }

}
