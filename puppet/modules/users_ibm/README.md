# users_ibm

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with [users_ibm]](#setup)
    * [What [users_ibm] affects](#what-[users_ibm]-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with [users_ibm]](#beginning-with-[users_ibm])
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

Sets up accounts for IBM application users (Unix accounts).

A number of accounts are needed to support an IBM stack installation.  Rather than risk mismatched UIDs (particularly in the case of NFS shares), they are all defined on all IBM stack servers, even where not used.  Where they are not used, there will be no login access at all.

## Setup

### What [users_ibm] affects 

User accounts

### Beginning with [users_ibm]	

include users_ibm

## Usage

## Reference

The module also configures some additional directories/ownerships:
* Several accounts are homed under /opt rather than /home
* The /shared/maximo/AppTier/integration tree is configured here

SFTP is set up for Oracle integration.

The current mechanism for creating user accounts is somewhat simplistic.  Users are configured directly in the module code.  To add a new user, you will need to add them to init.pp using the existing users as a reference.  

You must also ensure no UID/GID clashes - the mechanism for this is simply by manual addition to the user_table file. This is purely used as a reference/record of UIDs/GIDs/names used.

NB: Sometimes developers need access to production systems.  In these cases, use 'adm' as a suffix to the username (this and the normal account can use the same SSH key of course).

## Limitations

## Development

This module, and the other users-* modules need redeveloping to obtain their data from Hiera which will define usernames, groupnames, UIDs & GIDs along with where they should be instantiated.  It is anticipated that a single 'users' module will achieve this.

NB: The Oracle integration setup needs to extracted from this module, or at least moved to a separate class.
