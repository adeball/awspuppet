
class users_ibm {

  group { 'db2iadm1': ensure => present, gid => 2001 }
  group { 'db2user':  ensure => present, gid => 2002 }
  group { 'httpd':    ensure => present, gid => 2003 }
  group { 'sftp':     ensure => present, gid => 2006 }
  group { 'cognos':   ensure => present, gid => 2007 }

  user { 'dasusr1':  ensure => present, uid => 2001, gid => db2iadm1, home => '/home/dasusr1', managehome => true, }
  user { 'ctginst1': ensure => present, uid => 2002, gid => db2iadm1, home => '/opt/ctginst1', managehome => true, }
  user { 'db2admin':
    ensure => present, uid => 2003, gid => db2iadm1, home => '/opt/db2admin', managehome => true,
    password => '$1$9bRFeiIQ$q7ducpLftKEKJiVyo2wSV/',
  }
  user { 'mfinst1':  ensure => present, uid => 2004, gid => db2iadm1, home => '/opt/mfinst1',  managehome => true, }
  user { 'mfdbadm':  ensure => present, uid => 2005, gid => db2iadm1, home => '/opt/mfdbadm',  managehome => true, }
  user { 'db2inst1': ensure => present, uid => 2006, gid => db2iadm1, home => '/opt/db2inst1', managehome => true, }

  user { 'db2fenc1': ensure => present, uid => 2101, gid => db2user,  home => '/opt/db2fenc1', managehome => true, }
  user { 'mxsibui':  ensure => present, uid => 2102, gid => db2user,  home => '/opt/mxsibui',  managehome => true, }
  user { 'mxsibmif': ensure => present, uid => 2103, gid => db2user,  home => '/opt/mxsibmif', managehome => true, }
  user { 'maximo':   ensure => present, uid => 2104, gid => db2user,  home => '/opt/maximo',   managehome => true, }
  user { 'appcntr':  ensure => present, uid => 2105, gid => db2user,  home => '/opt/appcntr',  managehome => true, }
  user { 'mfadmin':  ensure => present, uid => 2106, gid => db2user,  home => '/opt/mfadmin',  managehome => true, }
  user { 'cognos':
    ensure => present, uid => 2107, gid => cognos,   home => '/home/cognos',  managehome => true,
    password => '$1$FLsqX7n0$eS97JQOqHSXE2b9F.Hxq41',
    groups => ['sftp', ],
  }
  user { 'httpd':    ensure => present, uid => 2201, gid => httpd,    home => '/home/httpd',   managehome => true, }

  user { 'oraint':
    ensure => present,
    uid => 2208,
    gid => httpd,
    home => '/shared/maximo/AppTier/integration',
    groups => ['sftp', ],
    managehome => false,
    comment => "GTR Oracle Integration file transfer",
  }


  ### file_line can't handle multiple lines properly, so this is a bit of a hack
  ### to avoid using augeas.

  $sshconf='/etc/ssh/sshd_config'
  $sshcomment='# Puppet - sftp'
  $sshtitle='Set up SFTP group in sshd'

  $l1="Match Group sftp               $sshcomment 1"
  $l2="  PasswordAuthentication yes   $sshcomment 2"
  $l3="  X11Forwarding no             $sshcomment 3"
  $l4="  ForceCommand internal-sftp   $sshcomment 4"
  $l5="  AllowTCPForwarding no        $sshcomment 5"

  $ml1="^.*$sshcomment 1"
  $ml2="^.*$sshcomment 2"
  $ml3="^.*$sshcomment 3"
  $ml4="^.*$sshcomment 4"
  $ml5="^.*$sshcomment 5"

  file_line { "$sshtitle 1": path => $sshconf, line => "$l1", match => "$ml1", } ->
  file_line { "$sshtitle 2": path => $sshconf, line => "$l2", match => "$ml2", } ->
  file_line { "$sshtitle 3": path => $sshconf, line => "$l3", match => "$ml3", } ->
  file_line { "$sshtitle 4": path => $sshconf, line => "$l4", match => "$ml4", } ->
  file_line { "$sshtitle 5": path => $sshconf, line => "$l5", match => "$ml5",
    notify => Service['sshd'],
  }

}
