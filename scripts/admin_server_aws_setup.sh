#!/bin/bash
#
# This relies on several things in place
#
# 1) You must have admin credentials in the AWS account you are setting up the
#    puppet account in
# 2) You must have a login on the Puppet server (which is likely to be the same for all
#    accounts, but will resolve to puppet.<client>.<zone>
# 3) Your login must have sudo rights to run shell commands on the Puppet server
# 4) There should be an admin group in the AWS account
# 5) The admin server should have an admin user account
#
# The pre-requisites will be tested, and if any fail, the script will not run
#
# If all pre-requisites are in place, the script will:
# 1) Connect to the AWS client account
# 2) Generate an admin account in the default region
# 3) Add it to the admin group
# 4) Generate an access key ID and secret access key
# 5) Copy these to the admin account on the puppet server, and generate the .aws/credentials and .aws/config files
#
#
# The access key ID + secret access key MUST NOT be stored in any repository, to do so would constitute 
# a large security risk.  They could be stored in LastPass, but it is probably a better idea to simply
# delete and create a new ID/key pair if needed.
#

cd $(dirname $0)
. ./common.sh

[[ -n "$1" ]] || fail "Usage: $0 client"

client=$1
export AWS_DEFAULT_PROFILE=$client
export AWS_DEFAULT_REGION=$(getregion $client)

puppethost=puppet.$client.$(getmasterzone)
adminacct=$(getmasteradminacct)
admingroup=$(getmasteradmingroup)
masterclient=$(getmasterclient)

echo
echo "Setting up the puppet AWS account for client: $CLIENT"
echo
echo "Pre-requisite checks..."
echo

### Test that we can connect using the client credentials
echo -n "Checking for AWS credentials and infrastructure stack... "
aws ec2 describe-regions > /dev/null || fail "Cannot connect and run a basic command.  Please check AWS credentials"
echo ok

if [[ $client == $masterclient ]] ; then
    ### Test that the default admin group exists
    echo -n "Checking for the $admingroup group... "
    aws iam get-group --group-name=$admingroup > /dev/null || fail "The $admingroup AWS group is not configured"
    echo ok
fi

### Test that we can ssh to the Puppet server
echo -n "Checking ssh to $puppethost... "
ssh $puppethost hostname > /dev/null || fail "Your account is not able to ssh to $puppethost"
echo ok

### and that we have sudo rights
echo -n "Checking sudo on $puppethost... "
ssh $puppethost "sudo hostname" > /dev/null || fail "Your account is not able to sudo on $puppethost"
echo ok

### finally, that the 'admin' account exists
echo -n "Checking for admin account on $puppethost... "
ssh $puppethost "id $adminacct" > /dev/null || fail "The $adminacct account is not configured on $puppethost"
echo ok


profile=$(mktemp)
### Now create the .aws/config profile entry for this account
cat > $profile << EOF
[profile $client]
region = $(getregion $client)

EOF

echo "Copying config file sub-section"
ssh $puppethost "sudo mkdir -p ~${adminacct}/aws ~${adminacct}/.aws"
scp $profile $puppethost:/tmp/${client}.profile
ssh $puppethost "sudo mv /tmp/${client}.profile ~${adminacct}/aws"
ssh $puppethost "sudo bash -c \"cat ~${adminacct}/aws/*.profile > ~${adminacct}/.aws/config\""


credentials=$(mktemp)


### We need at least one set of credentials if we are setting up the master account
if [[ $client == $masterclient ]] ; then

    echo -n "Checking for AWS admin account... "
    aws iam get-user --user-name $adminacct > /dev/null 2>&1
    if [[ $? -ne 0 ]] ; then
        echo "$adminacct does not exist, creating it..."
        aws iam create-user --user-name $adminacct
    else
        echo "$adminacct already exists"
    fi

    echo -n "Including in $admingroup group... "
    aws iam add-user-to-group --group-name $admingroup --user-name $adminacct || fail "Cannot add to $admingroup group"
    echo "done"


    ### Check to see if there is already an access key in place

    ak=$(aws iam list-access-keys --user-name $adminacct --output=text)

    if [[ -n "$ak" ]] ; then
        echo "$adminacct already has an access key ID configured"
    else
        result=$(aws iam create-access-key --user-name $adminacct --output=text)
        accesskey=$(echo $result | awk '{print $2}')
        secretkey=$(echo $result | awk '{print $4}')

    cat > $credentials << EOF
[$client]
aws_access_key_id = $accesskey
aws_secret_access_key = $secretkey

EOF

    fi

else
    cat > $credentials << EOF
[$client]
role_arn = arn:aws:iam::$(getawsid $client):role/amazon-administrators
source_profile = $masterclient

EOF

fi


if [[ -s $credentials ]] ; then
    echo "Copying credentials file sub-section"
    scp $credentials $puppethost:/tmp/${client}.credentials
    ssh $puppethost "sudo mv /tmp/${client}.credentials ~${adminacct}/aws"
    ssh $puppethost "sudo bash -c \"cat ~${adminacct}/aws/*.credentials > ~${adminacct}/.aws/credentials\""

    ssh $puppethost "sudo chown -R $adminacct ~${adminacct}/.aws"
fi

rm $profile $credentials
