#!/bin/bash
#
# Common functions for cloud scripts
#


### Find the absolute path name for the common.sh script, and derive the etc directory location
### Works interactively, but may need to be overridden if called from another script.

[[ $0 == "-bash" ]] && mydir=$(dirname ${BASH_ARGV[0]}) || mydir=$(dirname $0)
export ECPSCRIPTDIR=$(cd $mydir ; pwd)
export ECPETCDIR=$ECPSCRIPTDIR/../etc




fail()
{

### Function Name: fail
### Description: Call when something fails terminally, exits with status = 1
### Parameters: $@ = String describing the failure reason
### Output: Text message containing the failure reason to stderr
### Example: In a script use e.g. cd somedir <double vertical bar> fail "Cannot cd somedir"
### Example: would cause the script to terminate and write "Script failed: Cannot cd somedir", then exit with status 1


    echo >&2
    echo "Script failed: $@" >&2
    echo >&2
    exit 1
}


yn()
{

### Function Name: yn
### Description: Prompts for interactive terminal input for yes/no questions
### Parameters: $1 = the prompt, [ $2 = the default answer, valid values are "y" or "n") ]
### Output: Interactively prompts the user, adding " (y/n)? : " plus default if required, outputs 'y' or 'n'
### Example: answer=$(yn "Do you want to run this script")
### Example: if [[ "$answer" == "n" ]] ; then echo "Ok, stopping here"; exit; fi


        prompt="$1"
        default="$2"
        ans="xxx"

        if [[ "$default" == "y" || "$default" == "n" ]] ; then 
                prompt="$prompt (y/n)? [$default]: "
        else
                prompt="$prompt (y/n)? : "
        fi

        while [[ "$ans" != "y" && "$ans" != "n" ]] ; do

                [[ "$ans" != "xxx" ]] && echo "Please answer with y or n"
                read -p "$prompt" ans
		ans=$(echo $ans| tr [YN] [yn])
                if [[ -z "$ans" && -n "$default" ]] ; then
                        ans="$default"
                fi
        done

        echo $ans

}


getenvironments()
{

### Function Name: getenvironments
### Description: Returns a list of environment short codes for a client based on defined hosts
### Parameters: $1 = Client short code
### Output: List of environment short codes where a defined host matches the client code, non-matched clients return an empty list
### Example: getenvironments gtr ->
### Example: tst
### Example: prd
### Example: poc


    myclient=$1
    awk -v c=$myclient '!/^#/ && $5==c {if ($4!="") {print $4}}' $ECPETCDIR/host_table | sort -u

}


getclients()
{

### Function Name: getclients
### Description: Returns a list of client short codes from the infrastructure table
### Parameters: none
### Output: List of client short codes
### Example: getclients ->
### Example: gtr
### Example: rwg
### Example: tfl
### Example: nmd


    awk '!/^#/ {if ($1!="") {print $1}}' $ECPETCDIR/infrastructure_table | sort -u

}


getenvironment()
{

### Function Name: getenvironment
### Description: Given a server short (friendly) name, return the environment
### Parameters: $1 = Server friendly name
### Output: Single short environment name
### Example: getenvironment fuchsia -> tst


    myhost=$1

    ### If not, read from the table
    cat $ECPETCDIR/host_table $ECPETCDIR/host_table_monitoronly | while read host servertype ip env \
        client rest ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${env}
            return
        fi
    done

}


getinstancetype()
{

### Function Name: getinstancetype
### Description: Returns the instance type to use when building a server (NB: this is not necessarily the current instance type)
### Parameters: $1 = Server friendly name
### Output: AWS instance type to build the server with
### Example: getinstancetype fuchsia -> t2.large


    myhost=$1
    myservertype=$(getservertype $myhost)
    myenvironment=$(getenvironment $myhost)
    myclient=$(getclient $myhost)
    while read servertype environment client instancetype xvdd dev os ; do
        if [[ "$servertype" == "$myservertype" && "$environment" == "$myenvironment" && "$client" == "$myclient" ]] ; then
            echo $instancetype
            return
        fi
        if [[ "$servertype" == "$myservertype" && "$environment" == "default" && "$client" == "default" ]] ; then
            echo $instancetype
            return
        fi
    done < $ECPETCDIR/sizing_table

}


getxvdd()
{

### Function Name: getxvdd
### Description: Returns the amount in GB of the additional storage to allocate to an instance at build time
### Parameters: $1 = Server friendly name
### Output: Integer value
### Example: getxvdd fuchsia -> 120


    myhost=$1
    myservertype=$(getservertype $myhost)
    myenvironment=$(getenvironment $myhost)
    myclient=$(getclient $myhost)
    while read servertype environment client instancetype xvdd dev os ; do
        if [[ "$servertype" == "$myservertype" && "$environment" == "$myenvironment" && "$client" == "$myclient" ]] ; then
            echo $xvdd
            return
        fi
        if [[ "$servertype" == "$myservertype" && "$environment" == "default" && "$client" == "default" ]] ; then
            echo $xvdd
            return
        fi
    done < $ECPETCDIR/sizing_table

}


getos()
{

### Function Name: getos
### Description: Returns the OS a server is built with, determines the AMI to use when initially launching
### Parameters: $1 = Server friendly name
### Output: String, currently possible are: "linux|linux2|rhel|win|rancheros" for
### Output: Amazon Linux, Amazon Linux 2, RHEL 7, Windows & RancherOS respectively
### Example: getos fuchsia -> rhel


    myhost=$1
    myservertype=$(getservertype $myhost)
    myenvironment=$(getenvironment $myhost)
    myclient=$(getclient $myhost)
    while read servertype environment client instancetype xvdd dev os ; do
        #echo $servertype $environment $client
        if [[ "$servertype" == "$myservertype" && "$environment" == "$myenvironment" && "$client" == "$myclient" ]] ; then
            echo $os
            return
        fi
        if [[ "$servertype" == "$myservertype" && "$environment" == "default" && "$client" == "default" ]] ; then
            echo $os
            return
        fi
    done < $ECPETCDIR/sizing_table

}


getdev()
{

### Function Name: getdev
### Description: Returns the device name additional storage should be created on, at build time
### Parameters: $1 = Server friendly name
### Output: String, the device file name e.g. xvdd (so storage is created on /dev/xvdd)
### Example: getdev fuchsia -> xvde


    myhost=$1
    myservertype=$(getservertype $myhost)
    myenvironment=$(getenvironment $myhost)
    myclient=$(getclient $myhost)
    while read servertype environment client instancetype xvdd dev os ; do
        #echo $servertype $environment $client
        if [[ "$servertype" == "$myservertype" && "$environment" == "$myenvironment" && "$client" == "$myclient" ]] ; then
            echo $dev
            return
        fi
        if [[ "$servertype" == "$myservertype" && "$environment" == "default" && "$client" == "default" ]] ; then
            echo $dev
            return
        fi
    done < $ECPETCDIR/sizing_table

}


getclient()
{

### Function Name: getclient
### Description: Returns the short code for the client a server belongs to
### Parameters: $1 = Server friendly name
### Output: Client short code
### Example: getclient fuchsia -> rwg


    myhost=$1

    ### If not, read from the table
    cat $ECPETCDIR/host_table $ECPETCDIR/host_table_monitoronly | while read host servertype ip env client rest ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${client}
            return
        fi
    done

}


getservertype()
{

### Function Name: getservertype
### Description: Returns the servertype of an instance, e.g. db2, websphere, admin etc
### Parameters: $1 = Server friendly name
### Output: Server type
### Example: getservertype fuchsia -> webspheremgr


    myhost=$1

    (cat $ECPETCDIR/host_table $ECPETCDIR/host_table_monitoronly) | while read host servertype ip env \
        client rest ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${servertype}
            return
        fi
    done

    ### If there's no direct match, then our unqualified hostname (minus instance#) is probably the servertype
    ### This just validates that is a valid servertype
#    shortname=$(echo $myhost | awk -F. '{split ($1,a,"-"); print a[1]}')
#    cat $ECPETCDIR/host_table $ECPETCDIR/host_table_monitoronly | while read host servertype ip env \
#    client rest ; do
#    if [[ ${servertype} == ${shortname} || ${host} == ${shortname} ]] ; then
#        echo ${servertype}
#        return
#    fi
#    done

}


getsubnet()
{

### Function Name: getsubnet
### Description: Returns the subnet NAME an instance sits in.
### Parameters: $1 = Server friendly name
### Output: subnet name as tagged in AWS, comprising the client short code, environment short code and PRIVATE, PUBLIC[AB}
### Example: getsubnet fuchsia -> RWGTSTPRIVATE


    myhost=$1

    while read host servertype ip env client instance subnet zone version owner monitor; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${subnet}
            return
        fi
    done < $ECPETCDIR/host_table

    ### Didn't match a host, so we can overload this function and return the VPC subnet
    myclient=$myhost
    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $subnet
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getsubnetid()
{

### Function Name: getsubnetid
### Description: Returns the AWS subnet ID an instance sits in
### Parameters: $1 = Server friendly name
### Output: AWS subnet ID
### Example: getsubnetid fuchsia -> subnet-f8c3d8b2


    myhost=$1
    myclient=$(getclient $myhost)

    ### defines variables
    eval $(getsubnets $myclient)

    ### Returns friendly(ish) subnet name
    subnet=$(getsubnet $myhost)

    ### Outputs subnetid of subnet
    eval echo "\${$subnet}"

}


getip()
{

### Function Name: getip
### Description: Returns the instance local IP as defined in the host_table
### Parameters: $1 = Server friendly name
### Output: Dotted quad IPv4 IP address
### Example: getip fuchsia -> 10.6.5.10

    myhost=$1

    while read host servertype ip env client instance subnet zone version owner monitor ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${ip}
            return
        fi
    done < $ECPETCDIR/host_table

}


getextip()
{

### Function Name: getextip
### Description: Returns the instance external IP
### Parameters: $1 = Server friendly name
### Output: Dotted quad IPv4 IP address
### Example: getextip fuchsia -> <empty> (no external IP assigned)
### Example: getextip nineveh -> 52.56.94.190

    myhost=$1

    ### On the Puppet server, we have the info to hand
    if [[ -n "$REPOROHOME" ]] ; then
        infile=$REPOROHOME/../aws/state/$(getclient $myhost)/ec2.describe-instances.text
    else
        ### The slower method
        getextiptmpf=$(mktemp)
        aws ec2 describe-instances --instance-id=$(getinstanceid $myhost) --output=text > $tmpf
        infile=$tmpf
    fi

    awk -v h="$myhost" '
    /ASSOCIATION/            {extip=$NF}
    /TAGS/ && $NF == h       {if (extip != "") {print extip}}
    /INSTANCES/              {extip=""}
    ' $infile

    [[ -n "$getextiptmpf" ]] && rm -f $getextiptmpf

}


getinstance()
{

### Function Name: getinstance
### Description: Returns the instance number (NB: not the AWS instance ID), e.g. 001
### Parameters: $1 = Server friendly name
### Output: Instance number as defined in the host_table
### Example: getinstance fuchsia -> 001

    myhost=$1

    cat $ECPETCDIR/host_table $ECPETCDIR/host_table_monitoronly | while read host servertype ip env \
        client instance subnet zone version owner monitor; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${instance}
            return
        fi
    done

}


getversion()
{

### Function Name: getversion
### Description: Returns the build system version number to use.  This can be used by Puppet to apply different builds.
### Parameters: $1 = Server friendly name
### Output: Build version, e.g. 2
### Example: getversion fuchsia -> 1

    myhost=$1

    while read host servertype ip env client instance subnet zone version owner monitor ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${version}
            return
        fi
    done < $ECPETCDIR/host_table

}


getmonitor()
{

### Function Name: getmonitor
### Description: Returns the 'monitor' field from the host table for a given server
### Parameters: $1 = Server friendly name
### Output: Monitor field, e.g. 'yes', 'no', 'ignore'
### Example: getmonitor fuchsia -> yes

    myhost=$1

    while read host servertype ip env client instance subnet zone version owner monitor ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${monitor}
            return
        fi
    done < $ECPETCDIR/host_table

}


getowner()
{

### Function Name: getowner
### Description: Returns the short code for the nominal server owner, i.e. the point of contact
### Parameters: $1 = Server friendly name
### Output: Short code, which should match an entry in the owner_table
### Example: getowner fuchsia -> ade

    myhost=$1

    cat $ECPETCDIR/host_table $ECPETCDIR/host_table_monitoronly | while read host servertype ip env \
        client instance subnet zone version owner monitor ; do
        if [[ ${myhost} == ${host} ]] ; then
            echo ${owner}
            return
        fi
    done

}


getownerfullname()
{

### Function Name: getownerfullname
### Description: Returns the full name for a short owner name, as defined in the owner_table
### Parameters: $1 = Short owner name
### Output: Full owner name
### Example: getownername ade -> "Ade Ball"
### Example: getownername $(getowner fuchsia) -> "Ade Ball"

    myowner=$1
    while read owner fullname ; do
        if [[ ${myowner} == ${owner} ]] ; then
            echo $fullname
            return
        fi
     done < $ECPETCDIR/owner_table

}


getservers()
{

### Function Name: getservers
### Description: Returns a list of all AWS/Linux managed servers (or those for a specified client)
### Parameters: [ $1 = client short code ]
### Output: Line separated list of server friendly names
### Example: getservers -> <lists all servers defined in the host_table>
### Example: getservers nmd ->
### Example: palantir
### Example: pippin
### Example: merry
### Example: gwahir

    myclient=$1
    myenv=$2

    while read host servertype ip env client instance subnet zone version owner monitor ; do
        [[ -z ${myenv} && -z ${myclient} ]] && echo ${host}
        [[ -z ${myenv} &&  ${myclient} == ${client} ]] && echo ${host}
        [[ ${myclient} == ${client} && ${myenv} == ${env} ]] && echo ${host}
    done < $ECPETCDIR/host_table | grep "^[a-z]"

}


getinstanceid()
{

### Function Name: getinstanceid
### Description: Returns AWS EC2 instance ID, given a short hostname as the argument
### Parameters: $1 = Server friendly name
### Output: AWS Instance ID
### Example: getinstanceid fuchsia -> i-06cccc5ed65013c37

    myhost=$1
    myclient=$(getclient $myhost)

    aws ec2 describe-instances \
        --profile=$myclient \
        --region=$(getregion $myclient) \
        --filters \
            Name="tag:Server Name",Values=$myhost \
            Name=instance-state-name,Values=running,stopped,stopping,pending,shutting-down \
        | awk -F\" '/InstanceId/ {print $4}'

}


### Returns availability zone, given short hostname as the argument
getaz()
{

### Function Name: getaz
### Description: Returns the AWS availability zone an instance sits in
### Parameters: $1 = Server friendly name
### Output: AWS availability zone
### Example: getaz fuchsia -> eu-west-2b

    myhost=$1
    myclient=$(getclient $myhost)
    clientaws $myclient ec2 describe-subnets --subnet-ids $(getsubnetid $1) --output=text | awk '/SUBNETS/ {print $3}'

}


getfg()
{

### Function Name: getfg
### Description: Returns an RGB or named (X11 rgb.txt) foreground colour for a client/env/servertype
### Parameters: $1 = Client short code, $2 = Environment short code, $3 = Server type
### Output: String
### Example: getfg rwg tst websphere -> palegreen1

    myclient="$1"
    myenv="$2"
    myservertype="$3"

    while read client env servertype xtfg xtbg ; do
        if [[ $myclient == $client && $myservertype == $servertype && $myenv == $env ]] ; then
            echo $xtfg
            return
        fi
    done < $ECPETCDIR/xt_table

    echo "ivory"

}


getbg()
{

### Function Name: getbg
### Description: Returns an RGB or named (X11 rgb.txt) background colour for a client/env/servertype
### Parameters: $1 = Client short code, $2 = Environment short code, $3 = Server type
### Output: String
### Example: getbg rwg tst websphere -> #00304d

    myclient="$1"
    myenv="$2"
    myservertype="$3"

    while read client env servertype xtfg xtbg ; do
        if [[ $myclient == $client && $myservertype == $servertype && $myenv == $env ]] ; then
            echo $xtbg
            return
        fi
    done < $ECPETCDIR/xt_table

    echo "black"

}


deployec2()
{

### Function Name: deployec2
### Description: Creates and deploys an EC2 instance into the AWS client account
### Parameters: $1 = friendly server name
### Output: Cloudformation stack details in JSON format
### Example: deployec2 fuchsia

    echo

    servername="$1"


    [[ -z "$ZONENAME" ]] && ZONENAME=$(getmasterzone)
    [[ -z "$ZONEID" ]] && ZONEID=$(getmasterprivatezoneid)

    myclient=$(getclient $servername)
    MYCLIENT=$(echo $myclient | tr [a-z] [A-Z])

    servertype=$(getservertype $servername)
    subnetid=$(getsubnetid $servername)
    ip=$(getip $servername)
    instance=$(getinstance $servername)
    instancetype=$(getinstancetype $servername)
    xvdd=$(getxvdd $servername)
    owner=$(getowner $servername)
    ownerfullname=\\\'$(getownerfullname $owner)\\\'
    env=$(getenvironment $servername)
    ENVIRONMENT=$(echo $env | tr [a-z] [A-Z])
    vpnint=$(getvpnint $myclient)
    vpnext=$(getvpnext $myclient)
    vpnport=$(getvpnport $myclient)
    os=$(getos $servername)
    version=$(getversion $servername)
    repobucket=$(getmasterrepobucketname)

    ## Should return two SGs, one for server type and one common
    sg1=$(getsgs $servername | awk '{print $1}')
    sg2=$(getsgs $servername | awk '{print $2}')

    vpcid=$(getvpc $myclient)
    servertype2=$(getservertype2 $servertype)

    template="file://$ECPSCRIPTDIR/../$myclient/generic.yaml"
    [[ "$servertype" == "rancher" || "$servertype" == "rancherp" ]] && template="file://$(pwd)/rancher.yaml"
#    [[ "$servertype" == "admin" ]] && template="file://$(pwd)/admin.yaml"


    stackname="${MYCLIENT}-${ENVIRONMENT}-${servertype}-${instance}-${servername}"
    action="create-stack"
    clientaws $myclient cloudformation describe-stacks --stack-name "${stackname}" >/dev/null 2>&1
    [[ $? -eq 0 ]] && action="update-stack"

    cmd="clientaws $myclient cloudformation $action \
    --stack-name "${stackname}" \
    --template-body ${template} \
    --parameters ParameterKey=InstanceNumber,ParameterValue="${instance}" \
                 ParameterKey=HostedZone,ParameterValue="${ZONENAME}" \
                 ParameterKey=ZoneId,ParameterValue="${ZONEID}" \
                 ParameterKey=Environment,ParameterValue="${ENVIRONMENT}" \
                 ParameterKey=Client,ParameterValue="${MYCLIENT}" \
                 ParameterKey=ServerType,ParameterValue="${servertype}" \
                 ParameterKey=ServerType2,ParameterValue="${servertype2}" \
                 ParameterKey=VPCID,ParameterValue="${vpcid}" \
                 ParameterKey=Subnet,ParameterValue="${subnetid}" \
                 ParameterKey=SpecifiedPrivateIP,ParameterValue="${ip}" \
                 ParameterKey=ServerName,ParameterValue="${servername}" \
                 ParameterKey=Xvdd,ParameterValue="${xvdd}" \
                 ParameterKey=MyInstanceType,ParameterValue="${instancetype}" \
                 ParameterKey=Owner,ParameterValue="${ownerfullname}" \
                 ParameterKey=SG1,ParameterValue="${sg1}" \
                 ParameterKey=SG2,ParameterValue="${sg2}" \
                 ParameterKey=VpnInt,ParameterValue="${vpnint}" \
                 ParameterKey=VpnExt,ParameterValue="${vpnext}" \
                 ParameterKey=VpnPort,ParameterValue="${vpnport}" \
                 ParameterKey=Os,ParameterValue="${os}" \
                 ParameterKey=Version,ParameterValue="${version}" \
                 ParameterKey=RepoBucket,ParameterValue="${repobucket}" \
    --capabilities CAPABILITY_IAM"

    echo
    echo $cmd | fmt -1
    eval $cmd
    echo

    ### Give AWS a chance to start creating the stack
    sleep 15
    ### Wait for it to be built
    waitforstack $myclient $stackname

    tag_ec2 $servername
    tag_ebs $servername

}


decommissionec2()
{

### Function Name: decommissionec2
### Description: Destroys an EC2 instance stack
### Parameters: $1 = <friendly server name>, optional switch -f forces the run without confirmation
### Output: If -f is not specified, interactive confirmation.  Decommissioning details.
### Example: decommissionec2 -f victim

    local OPTIND f
    question=1
    while getopts "fs:" opt; do
        [[ "$opt" == "f" ]] && question=0
    done

    shift $(( $OPTIND - 1 ))

    if [[ $# -ne 1 ]] ; then
        echo "Usage: decommissionec2 [-f] servername"
        return
    fi

    myserver=$1

    myclient=$(getclient $myserver)
    if [[ -z "$myclient" ]] ; then
        echo "I cannot find records for a server named \"$myserver\""
        return
    fi

    MYCLIENT=$(echo $myclient | tr '[a-z]' '[A-Z]')
    myenv=$(getenvironment $myserver)
    MYENV=$(echo $myenv | tr '[a-z]' '[A-Z]')
    myinstance=$(getinstance $myserver)
    myinstanceid=$(getinstanceid $myserver)
    mystype=$(getservertype $myserver)

    stackname="${MYCLIENT}-${MYENV}-${mystype}-${myinstance}-$myserver"

    echo Checking for CF stack: $stackname
    aws cloudformation describe-stacks --output=text --profile=$myclient --region=$(getregion $myclient) --stack-name="$stackname"
    if [[ $? -ne 0 ]] ; then
        echo "Cannot find stack: $stackname"
        return
    fi

    if [[ $question -eq 1 ]] ; then
        ans=$(yn "You are about to delete $stackname. Please confirm that you wish to do this")
        if [[ "$ans" != "y" ]] ; then
            echo "Ok - not decommissioning $stackname"
            return
        fi
    fi

    echo "Decommissioning $stackname"

    ### Disable termination protection on the instance
    aws ec2 modify-instance-attribute --profile=$myclient --region=$(getregion $myclient) --instance-id=$myinstanceid --no-disable-api-termination

    ### And delete the stack
    aws cloudformation delete-stack --profile=$myclient --region=$(getregion $myclient) --stack-name "$stackname"

    waitforstack $myclient "$stackname"

    ssh $(getmasteradminserver) "sudo puppet cert clean $myserver.$myenv.$myclient.$(getmasterzone)"

    ### If we have an entry in our own known_hosts, remove it (doesn't help anyone else, but it's still handy!)
    [[ -f ~/.ssh/known_hosts ]] && sed -i -e "/^$myserver[ ,]/d" ~/.ssh/known_hosts

    echo "Now remove/comment the host entry from host_table and (optionally) update DNS"
    echo "by running populate_dns.sh"

}



envrc()
{

### Function Name: envrc
### Description: Starts or stops a client environment
### Parameters: $1=client, $2=environment, optional switch -f forces the run without confirmation
### Output: If -f is not specified, interactive confirmation.
### Example: envrc -f rwg tst start

    local OPTIND f
    question=1
    while getopts "fs:" opt; do
        [[ "$opt" == "f" ]] && question=0
    done

    shift $(( $OPTIND - 1 ))

    if [[ $# -ne 3 || $3 != "start" && $3 != "stop" ]] ; then
        echo "Usage: envrc [-f] client environment start|stop"
        return
    fi

    [[ $# -ne 3 ]] && fail "Usage: $0 client environment start|stop (e.g. $0 rwg tst start)"

    client=$1
    env=$2
    action=$3

    echo

    fmt="%-3s %-3s %-12s %-3s (%s)\n"

    for server in $(getservers $client $env) ; do
        printf "$fmt" $(getclient $server) $(getenvironment $server) $(getservertype $server) $(getinstance $server) $server

    done | sort

    echo
    [[ $question -eq 0 ]] && ans="y" || ans=$(yn "You have requested to $action the above servers, continue")

    if [[ $ans == "y" ]] ; then

        echo "Finding instance ids..."
        ids=""
        for server in $(getservers $client $env) ; do
             ids="$ids $(getinstanceid $server)"
        done

        echo "$action: $ids"

        aws ec2 ${action}-instances --profile=$client --region=$(getregion $client) --instance-ids $ids --output=text

    else

        echo "Ok, will not $action the listed servers"

    fi

}


ec2rc()
{

### Function Name: ec2rc
### Description: Starts or stops an ec2 instance
### Parameters: $1=client, optional switch -f forces the run without confirmation, $2=server friendly name
### Output: If -f is not specified, interactive confirmation.
### Example: ec2rc -f fuchsia start

    local OPTIND f
    question=1
    while getopts "fs:" opt; do
        [[ "$opt" == "f" ]] && question=0
    done

    shift $(( $OPTIND - 1 ))

    if [[ $# -ne 2 || $2 != "start" && $2 != "stop" ]] ; then
        echo "Usage: ec2rc [-f] server start|stop"
        return
    fi

    [[ $# -ne 2 ]] && fail "Usage: ec2rc server start|stop (e.g. ec2rc fuchsia start)"

    server=$1
    action=$2
    client=$(getclient $server)

    echo

    fmt="%-3s %-3s %-12s %-3s (%s)\n"

    printf "$fmt" $(getclient $server) $(getenvironment $server) $(getservertype $server) $(getinstance $server) $server


    echo
    [[ $question -eq 0 ]] && ans="y" || ans=$(yn "You have requested to $action $server, continue")

    if [[ $ans == "y" ]] ; then

        echo "Finding instance id..."
        id=$(getinstanceid $server)
        aws ec2 ${action}-instances --profile=$client --region=$(getregion $client) --instance-ids $id --output=text

    else

        echo "Ok, will not $action $server"

    fi

}


getstackstatus()
{

### Function Name: getstackstatus
### Description: Retrieves the status of a Cloudformation stack (e.g. UPDATE_COMPLETE)
### Parameters: $1 = client, $2 = CF Stack name
### Output: Stack status, or 'unknown' if it is not possible to determine it
### Example: getstackstatus rwg RWG_INFRASTRUCTURE

    client=$1
    stack=$2

    status=$(aws cloudformation describe-stacks --profile=$client --region=$(getregion $client) --stack-name $stack | awk -F\" '/StackStatus/ {print $4}')
    [[ -z "$status" ]] && status="unknown"
    echo $status

}



waitforstack()
{

### Function Name: waitforstack
### Description: Waits for a CF stack to reach a completed state.  Use when the next action is dependent on the stack being in place.
### Parameters: $1 = client, $2 = CF Stack name
### Output: Date/status every 15 seconds until complete, then status on the final line
### Example: waitforstack rwg RWG_INFRASTRUCTURE

    client=$1
    stack=$2

    status=$(getstackstatus $client $stack)
    until $(echo ${status} | egrep -q "_COMPLETE|unknown"); do
        sleep 15
        status=$(getstackstatus $client $stack)
        echo "$(date) - ${status}"
    done

    echo "$status"

}


getservertype2()
{

### Function Name: getservertype2
### Description: Given the longer servertype (e.g. websphere), returns the (nominally) three-character uppercase servertype2
### Parameters: $1 = servertype
### Output: String, servertype2
### Example: getservertype websphere -> WAS

    servertype="$1"
    while read s p description ; do
        if [[ "${s}" == "${servertype}" ]] ; then
            echo "$p"
            return
        fi
    done < $ECPETCDIR/servertype_table
    echo "unknown"

}


getvpc()
{

### Function Name: getvpc
### Description: Returns the AWS VPC ID of the current $CLIENT, or the client supplied as a parameter
### Parameters: [ $1 = client short code, optional ]
### Output: AWS VPC ID
### Example: getvpc rwg -> vpc-770ed21e

    [[ -n $1 ]] && myclient=$1 || myclient=$(echo $CLIENT | tr '[A-Z]' '[a-z]')
    if [[ -z ${myclient} || ${myclient} == $(getmasterclient) ]] ; then
        myclient=$(getmasterclient)
        vpcname="$(getmasterstackprefix)-VPC"
        MYCLIENT=$(echo $myclient | tr '[a-z]' '[A-Z]')
    else
        MYCLIENT=$(echo $myclient | tr '[a-z]' '[A-Z]')
        vpcname=${MYCLIENT}-VPC
    fi

	clientaws $myclient ec2 describe-vpcs --output=text | awk -v v=$vpcname '
	    /VPCS/ {id=$NF}
	    /TAGS/ && $3 == v {print id}
	    '
}


getsgs()
{

### Function Name: getsgs
### Description: Returns the two security groups (commonSG + server-specific one) an instance should belong to
### Description: NB - this is not necessarily the groups the server belongs to if changes are made later
### Parameters: $1 = Server friendly name
### Output: Space-separated list of two security group IDs
### Example: getsgs fuchsia -> sg-1405bb7d sg-5907b930

    myhost=$1
    client=$(getclient $myhost)
    CLIENT=$(echo $client | tr [a-z] [A-Z])
    myservertype=$(getservertype $myhost)
    stack=${CLIENT}-INFRASTRUCTURE
    clientaws $myclient cloudformation describe-stacks --stack-name=$stack --output=text | egrep "${myservertype}SG|commonSG" | awk '{printf "%s ", $3} END {print ""}'

}


getclassb()
{

### Function Name: getclassb
### Description: Returns the second octet of the (nominal) Class B subnet assigned to a client
### Parameters: $1 = Client short code
### Output: Integer in the range 0-255
### Example: getclassb rwg -> 6 (So the VPC subnet will be 10.6.0.0/16)

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $subnet | awk -F. '{print $2}'
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getregion()
{

### Function Name: getregion
### Description: Returns the AWS default region a client is configured in
### Parameters: $1 = Client short code
### Output: The AWS region name
### Example: getregion rwg -> eu-west-2

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $region
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getregionfullname()
{

### Function Name: getregionfullname
### Description: Returns the full name for an AWS region (seemingly, there's nothing in the AWS API to do this)
### Parameters: $1 = AWS region
### Output: Long name for the region
### Example: getregionfullname eu-west-2 -> "EU (London)"
### Example: getregionfullname $(getregion nmd) -> "US East (Ohio)"

    myregion="$1"
    while read region description ; do
        if [[ "$myregion" == "$region" && -n "$region" ]] ; then
            echo $description
            return
        fi
    done < $ECPETCDIR/awsregion_table

}


getvpnint()
{

### Function Name: getvpnint
### Description: Returns the internal VPN IP address for a client OpenVPN server (i.e. the one applied on that server)
### Parameters: $1 = Client short code
### Output: Dotted quad IPv4 address
### Example: getvpnint rwg -> 169.254.255.10

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $vpnint
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getvpnext()
{

### Function Name: getvpnext
### Description: Returns the external VPN IP address for a client OpenVPN server (i.e. the one applied on the master account OpenVPN server)
### Parameters: $1 = Client short code
### Output: Dotted quad IPv4 address
### Example: getvpnint rwg -> 169.254.255.9

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $vpnext
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getvpnport()
{

### Function Name: getvpnport
### Description: Returns the UDP port number to use for a client OpenVPN tunnel
### Parameters: $1 = Client short code
### Output: Integer
### Example: getvpnport rwg -> 1998

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $vpnport
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getvpnpub()
{

### Function Name: getvpnpub
### Description: Returns the external public IP address of the client OpenVPN server
### Parameters: $1 = Client short code
### Output: Dotted quad IPv4 address
### Example: getvpnpub rwg -> 35.177.225.86

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $vpnpub
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getawsid()
{

### Function Name: getawsid
### Description: Returns the AWS account ID for the client
### Parameters: $1 = Client short code
### Output: 13-character ID number
### Example: getawsid rwg -> 086401325039

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $awsid
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getsubnets()
{

### Function Name: getsubnets
### Description: Returns AWS subnets for current or specified client (or master account if neither is set)
### Parameters: [ $1 = client short code ]
### Output: Semi-colon separated list of assignment pairs (i.e. name=value) for each defined subnet
### Example: getsubnets rwg -> RWGTSTPRIVATE=subnet-f8c3d8b2 ; RWGPRDPUBLICB=subnet-fbc3d8b1 ; RWGSYSPUBLIC=subnet-34c90b4f ; RWGPRDPUBLICA=subnet-b46eafcf ; RWGTSTPUBLIC=subnet-c4c0db8e ; RWGSYSPRIVATE=subnet-7368aa08 ; RWGPRDPRIVATEA=subnet-c36faeb8 ; RWGPRDPRIVATEB=subnet-c5c0db8f ;
### Example: This can be put in an eval statement to make the assignments, e.g. subnets=$(getsubnets gtr) ; eval $subnets

    if [[ -n "$1" ]] ; then
        myclient=$1
    elif [[ -n "$CLIENT" ]] ; then
        myclient=$(echo $CLIENT | tr '[A-Z]' '[a-z]')
    else
        myclient=$(getmasterclient)
    fi

	VPC=$(getvpc $myclient)

	snets=$(clientaws ${myclient} ec2 describe-subnets --output=text --filters Name=vpc-id,Values=$VPC | awk '
	    /SUBNETS/   	{ id=$9 }
	    /TAGS/ && /Name/    { printf "%s=%s ; ", $3, id}
	    END         { print "\n" }
	    ' | sed -e 's/\([A-Z1-9]\)-\([A-Z]\)/\1\2/g' )

	echo $snets

}


getclientfullname()
{

### Function Name: getclientfullname
### Description: Returns the full name for a client
### Parameters: $1 = Client short code
### Output: String, longer (one line) description for the client
### Example: getclientfullname rwg -> "Rolls Wood Group"

    myclient="$1"

    while read client vpnint vpnext vpnport vpnpub clientfullname subnet region awsid ; do
        if [[ $myclient == $client ]] ; then
            echo $clientfullname
            return
        fi
    done < $ECPETCDIR/infrastructure_table

}


getenvfullname()
{

### Function Name: getenvfullname
### Description: Returns the full name for an environment
### Parameters: $1 = Environment short code
### Output: String, longer (one line) description for the environment
### Example: getenvfullname poc -> "Proof-of-concept"

    myenv="$1"

    while read env envfullname ; do
        if [[ $myenv == $env ]] ; then
            echo $envfullname
            return
        fi
    done < $ECPETCDIR/env_table

}


geturls()
{

### Function Name: geturls
### Description: Returns the defined URLs and descriptions for a given server
### Parameters: $1 = Server friendly name
### Output: Line separated list of URLs associated with that server
### Example: geturls katara ->
### Example: https://myservice.mydomain.com/ My Most Excellent Web Site
### Example: https://myotherservuice/service.html My Not So Good Service
### Example: or e.g. for s in $(getservers ux1) ; do geturls $s ; done (for all URLs for the ux1 client)

    myservername="$1"
    while read servername url descr ; do
        if [[ "$myservername" == "$servername" ]] ; then
            echo "$url $descr"
        fi
    done < $ECPETCDIR/url_table

}


getwhitelistsg()
{

### Function Name: getwhitelistsg
### Description: Returns the IP Whitelist security group for a client
### Parameters: $1 = Client short code
### Output: AWS security group ID
### Example: getwhitelistsg rwg -> sg-728ffb1b

    myclient=$1
    MYCLIENT=$(echo $myclient | tr '[a-z]' '[A-Z]')
    clientaws $myclient cloudformation describe-stacks --stack-name="${MYCLIENT}-WHITELIST" --output=text \
        | awk '$1=="OUTPUTS" && $2=="ElbSg" {print $3}'

}


getmasterclient()
{

### Function Name: getmasterclient
### Description: Returns the 'master' account client name
### Parameters: none
### Output: Client short code
### Example: getmasterclient -> ems

    while read item value ; do
        if [[ "$item" == "client" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterzone()
{

### Function Name: getmasterzone
### Description: Returns the DNS zone name for the master account
### Parameters: none
### Output: DNS zone name
### Example: getmasterzonename -> mydomain.cloud

    while read item value ; do
        if [[ "$item" == "zone" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterpubliczoneid()
{

### Function Name: getmasterpubliczoneid
### Description: Returns the AWS Zone ID for the Route53 master public zone
### Parameters: none
### Output: AWS Zone ID
### Example: getmasterpubliczoneid -> Z15OBW2022GE4G

    while read item value ; do
        if [[ "$item" == "publiczoneid" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterprivatezoneid()
{

### Function Name: getmasterprivatezoneid
### Description: Returns the AWS Zone ID for the Route53 master private zone
### Parameters: none
### Output: AWS Zone ID
### Example: getmasterprivatezoneid -> Z2QAILAAPV4EDQ

    while read item value ; do
        if [[ "$item" == "privatezoneid" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterdescription()
{

### Function Name: getmasterdescription
### Description: Returns the description for the master account (normally, should be the same as getclientfullname $(getmasterclient))
### Parameters: none
### Output: String
### Example: getmasterdescription -> My Co

    while read item value ; do
        if [[ "$item" == "description" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmastermaindoc()
{

### Function Name: getmastermaindoc
### Description: Returns a URL for the master account main documentation web site
### Parameters: none
### Output: String - blank, or a URL
### Example: getmastermaindoc -> http://confluence.mydomain.com/

    while read item value ; do
        if [[ "$item" == "maindoc" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmastersysdoc()
{

### Function Name: getmastersysdoc
### Description: Returns a URL for the master account main system documentation web site
### Parameters: none
### Output: String - blank, or a URL
### Example: getmastersysdoc -> https://confluence.mydomain.com/pages/viewpage.action?pageId=30776670

    while read item value ; do
        if [[ "$item" == "sysdoc" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmastercontact()
{

### Function Name: getmastercontact
### Description: Returns contact details for someone responsible for the master account
### Parameters: none
### Output: String, anything
### Example: getmastercontact -> <Adrian Ball - Ux1 Ltd> ade@ux1.co.uk

    while read item value ; do
        if [[ "$item" == "contact" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterstackprefix()
{

### Function Name: getmasterstackprefix
### Description: Returns the prefix to use for CloudFormation stacks used in the master account,
### Description: normally just the uppercase version of the master account short code - e.g. EMS
### Description: but if set up manually, it may be different.
### Parameters: none
### Output: String
### Example: getmasterstackprefix -> MYCO

    while read item value ; do
        if [[ "$item" == "stackprefix" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasteradminserver()
{

### Function Name: getmasteradminserver
### Description: Returns the friendly name of the master 'admin' server
### Parameters: none
### Output: Server friendly name
### Example: getmasteradminserver -> xray

    while read item value ; do
        if [[ "$item" == "adminserver" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasteradminaliases()
{

### Function Name: getmasteradminaliases
### Description: Returns a space-separated list of aliases applied to the admin server (for other services)
### Parameters: none
### Output: Space separated list of aliases (short names, not FQDNs)
### Example: getmasteradminaliases -> puppet munin xymon

    while read item value ; do
        if [[ "$item" == "adminaliases" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasteradminenv()
{

### Function Name: getmasteradminenv
### Description: Returns the environment the master admin server sits in (usually 'sys' but may not be)
### Parameters: none
### Output: Short environment code
### Example: getmasteradminenv -> sys

    while read item value ; do
        if [[ "$item" == "adminenv" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterbackupbase()
{

### Function Name: getmasterbackupbase
### Description: S3 bucket base name for the EC2 instance daily backups.  Clients will use this with "-<client>" appended.
### Parameters: none
### Output: String
### Example: getmasterbackupbase -> myco-backup

    while read item value ; do
        if [[ "$item" == "backupbase" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterbucketlist()
{

### Function Name: getmasterbucketlist
### Description: List of S3 buckets that client accounts need read access to
### Parameters: none
### Output: Space separated list of S3 bucket names
### Example: getmasterbucketlist -> ux1-media ux1-public

    while read item value ; do
        if [[ "$item" == "bucketlist" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasteradminacct()
{

### Function Name: getmasteradminacct
### Description: Unix user account name created on the admin server.  Used for AWS queries and admin, inc. SSH access
### Parameters: none
### Output: Unix user name
### Example: getmasteradminacct -> puppetaws

    while read item value ; do
        if [[ "$item" == "adminacct" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasteradmingroup()
{

### Function Name: getmasteradmingroup
### Description: AWS IAM group which should exist in all accounts, with admin privileges, used by the admin account
### Parameters: none
### Output: AWS IAM group name
### Example: getmasteradmingroup -> ux1-admin

    while read item value ; do
        if [[ "$item" == "admingroup" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterpuppetrepo()
{

### Function Name: getmasterpuppetrepo
### Description: HTTP(S) link to the repository used to hold the Puppet code
### Parameters: none
### Output: HTTP(S) URL
### Example: getmasterpuppetrepo -> https://github.com/myco/myrepo.git

    while read item value ; do
        if [[ "$item" == "puppetrepo" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterreponame()
{

### Function Name: getmasterreponame
### Description: Repo name, as cloned
### Parameters: none
### Output: Text string (directory name)
### Example: getmasterrepo -> myrepo

    while read item value ; do
        if [[ "$item" == "reponame" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterrepobranch()
{

### Function Name: getmasterrepobranch
### Description: Returns the branch used, e.g. 'master or 'development'
### Parameters: none
### Output: Text string
### Example: getmasterrepobranch -> development

    while read item value ; do
        if [[ "$item" == "repobranch" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterrepobucketname()
{

### Function Name: getmasterrepobucketname
### Description: URL to the S3 bucket used to copy the repo to/from
### Parameters: none
### Output: URL
### Example: getmasterrepobucketname -> s3://ux1-puppet

    while read item value ; do
        if [[ "$item" == "repobucketname" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmastermediabucketname()
{

### Function Name: getmastermediabucketname
### Description: URL to the S3 bucket used to store install media
### Parameters: none
### Output: URL
### Example: getmastermediabucketname -> s3://ux1-media

    while read item value ; do
        if [[ "$item" == "mediabucketname" ]] ; then echo "$value" ; return ; fi
    done < $ECPETCDIR/master_table

}


getmasterlinks()
{

### Function Name: getmasterlinks
### Description: Returns a line-separated list of URLs and descriptions, used for web documentation
### Parameters: none
### Output: Lines, each with a URL, followed by a description
### Example: getmasterlinks ->
### Example: http://www.myco.com/                                Public web site
### Example: https://myco.sharepoint.com/SitePages/Home.aspx     Sharepoint
### Example: https://jira.mydomain.com/secure/Dashboard.jspa     JIRA
### Example: ...

    while read item value ; do
        if [[ "$item" == "link" ]] ; then echo "$value" ; fi
    done < $ECPETCDIR/master_table

}


clientaws()
{

### Function Name: clientaws
### Description: Calls the 'aws' CLI API in a client context
### Parameters: $1 = Client short code, $2... = AWS command
### Output: Same as AWS command
### Example: clientaws gtr ec2 describe-vpcs
### Example: The above command is equivalent to setting up the GTR client environment, then issuing 'aws ec2 describe-vpcs'

    if [[ -n "$CLOUDSERVERTYPE" && $CLOUDSERVERTYPE != "admin" ]] ; then
        echo "Not on an admin server" >&2
        exit 1
    fi

    if [[ $# -lt 2 ]] ; then
            fail "usage: $0 client awscommand (e.g. $0 foo ec2 describe-instances)"
    fi

    myclient="$1"

    clientok=0
    clients=$(getclients)
    for client in $clients ; do
            [[ "$myclient" == "$client" ]] && clientok=1
    done

    if [[ $clientok == 0 ]] ; then
            echo
            echo "Client: \"$1\" is not valid"
            echo
            echo "Valid client codes are as follows:"
            for client in $clients ; do
                    printf "%-8s (%s)\n" $client "$(getclientfullname $client)"
            done
            echo
            fail "Exiting"

    fi

    shift; cmd="$@"
    adminacct=$(getmasteradminacct)

    if [[ $CLOUDSERVERTYPE == "admin" ]] ; then
        echo "export AWS_DEFAULT_PROFILE=$myclient; export AWS_DEFAULT_REGION=$(getregion $myclient) ; aws $cmd" | sudo -i -u $adminacct
    else
         AWS_DEFAULT_PROFILE=$myclient; export AWS_DEFAULT_REGION=$(getregion $myclient) ; eval aws $cmd
    fi

}


htmldoc()
{

### Function Name: htmldoc
### Description: Outputs an HTML table of functions as documented in the comments in this file
### Parameters: none
### Output: HTML table to stdout
### Example: htmldoc

    echo "<table>"
    echo "<tr><th>Function</th><th>Description</th><th>Parameters</th><th>Output</th><th>Example</th>"

    awk '

        BEGIN                                   {doclive=0; desclive=0}

        /^### Function Name/                    {split($0,a,": "); printf "<tr><td>%s\n", a[2]  ; doclive=1}

        /^### Description/ && desclive==1       {split($0,a,": "); printf "<br>%s\n", a[2]}
        /^### Description/ && desclive==0       {split($0,a,": "); printf "</td><td>%s\n", a[2] ; desclive=1}

        /^### Parameters/ && paramlive==1       {split($0,a,": "); printf "<br>%s\n", a[2]}
        /^### Parameters/ && paramlive==0       {split($0,a,": "); printf "</td><td>%s\n", a[2] ; paramlive=1}

        /^### Output/ && outlive==1             {split($0,a,": "); printf "<br>%s\n", a[2]}
        /^### Output/ && outlive==0             {split($0,a,": "); printf "</td><td>%s\n", a[2] ; outlive=1}

        /^### Example/ && exlive==1             {split($0,a,": "); printf "<br>%s\n", a[2]}
        /^### Example/ && exlive==0             {split($0,a,": "); printf "</td><td>%s\n", a[2] ; exlive=1}

        /^$/ && doclive==1                      {print "</td></tr>"; doclive=0; desclive=0; paramlive=0; outlive=0; exlive=0}

    ' $ECPSCRIPTDIR/common.sh

    echo "</table>"


}


wikidoc()
{

### Function Name: wikidoc
### Description: Outputs a table of functions as documented in the comments in this file, in Confluence wiki markup
### Parameters: none
### Output: Wiki table to stdout
### Example: wikidoc

    echo "||Function||Description||Parameters||Output||Example||"

    awk '

        BEGIN                                   {doclive=0; desclive=0}

        /^### Function Name/                    {split($0,a,": "); printf "|%s", a[2]  ; doclive=1}

        /^### Description/ && desclive==1       {split($0,a,": "); printf " \\\\ %s", a[2]}
        /^### Description/ && desclive==0       {split($0,a,": "); printf "|%s", a[2] ; desclive=1}

        /^### Parameters/ && paramlive==1       {split($0,a,": "); printf " \\\\ %s", a[2]}
        /^### Parameters/ && paramlive==0       {split($0,a,": "); printf "|%s", a[2] ; paramlive=1}

        /^### Output/ && outlive==1             {split($0,a,": "); printf " \\\\ %s", a[2]}
        /^### Output/ && outlive==0             {split($0,a,": "); printf "|%s", a[2] ; outlive=1}

        /^### Example/ && exlive==1             {split($0,a,": "); printf " \\\\ %s", a[2]}
        /^### Example/ && exlive==0             {split($0,a,": "); printf "|%s", a[2] ; exlive=1}

        /^$/ && doclive==1                      {print "|"; doclive=0; desclive=0; paramlive=0; outlive=0; exlive=0}

    ' $ECPSCRIPTDIR/common.sh


}


tag_ec2()
{

### Function Name: tag_ec2
### Description: Tags an EC2 instance
### Parameters: $1 = Server friendly name
### Output: Informational, lists the aws CLIs used
### Example: tag_ec2 fuchsia

    host=$1
    instanceid=$(getinstanceid $host)

    owner=$(getowner $host)
    ownerfull=$(getownerfullname $owner)
    stype1=$(getservertype $host)
    stype2=$(getservertype2 $stype1)
    env=$(getenvironment $host)
    instance=$(getinstance $host)
    client=$(getclient $host)
    CLIENT=$(echo $client | tr [a-z] [A-Z])
    env=$(getenvironment $host)
    ENV=$(echo $env | tr [a-z] [A-Z])

    description="$CLIENT $(getenvfullname $env) $stype1 server $instance"

    tmpf=$(mktemp)
    while read var tagname ; do
        printf "aws ec2 create-tags --profile=$client --region=$(getregion $client) --resources=$instanceid --tags=" > $tmpf
        setvalue="value=\${$var}"; eval $setvalue
        printf "%-15s %-25s %-20s\n" var=$var tagname="$tagname" value="$value"
        printf "Key=\"%s\",Value=\"%s\"\n" "$tagname" "$value" >> $tmpf
        cat $tmpf
        bash $tmpf
    done << EOF
host	    Server Name
CLIENT	    Client
ownerfull   Owner
stype1	    Server Type 1
stype2	    Server Type 2
ENV         Environment
instance    Instance
description Description
EOF

    rm $tmpf
    echo
    ### This removes an old tag applied by earlier build scripts, it may not be needed now
    aws ec2 delete-tags --resources=$instanceid --tags=Key="ServerType2"
    echo

}


tag_ebs()
{

### Function Name: tag_ebs
### Description: Tags EBS volumes associated with an EC2 instance
### Parameters: $1 = Server friendly name
### Output: Informational, lists the aws CLIs used
### Example: tag_ebs fuchsia

client=$(getclient $1)
[[ -z ${client} ]] && fail "Cannot find $1 in the host_table"

host=$1
instanceid=$(getinstanceid $host)
owner=$(getowner $host)
ownerfull=$(getownerfullname $owner)
stype1=$(getservertype $host)
stype2=$(getservertype2 $stype1)
env=$(getenvironment $host)
instance=$(getinstance $host)
CLIENT=$(echo $client | tr [a-z] [A-Z])
ENV=$(echo $env | tr [a-z] [A-Z])


### Tag EBS volumes

aws ec2 describe-instances --profile=$client --region=$(getregion $client) --instance-id=$instanceid --output=text \
| awk '/BLOCKDEVICEMAPPINGS/ {printf "%s ", $2} /EBS/ {print $NF}' | while read device volid ; do

    devname=$(echo $device| sed -e 's-/dev/--')
    nametag="${ENV}-${stype2}-${instance} ${host}-$devname"

    echo aws ec2 create-tags --profile=$client --region=$(getregion $client) --resources=$volid --tags=Key="Name",Value="$nametag"
    aws ec2 create-tags --profile=$client --region=$(getregion $client) --resources=$volid --tags=Key="Name",Value="$nametag"
done

}


getdb2info()
{

### Function Name: getdb2info
### Description: Returns a summary of db2 info, db names, accounts... The output from /usr/local/bin/db2info.sh run on a db2 server.  If the data is cached, it will use that (i.e. on the admin/puppet server).  If not, it will ssh to the server and run the script.  Your account will need SSH keys and sudo rights for this to succeed.
### Parameters: $1 = Server friendly name
### Output: Colon-separated list of various db2 information (NB semi-colons used here for ease of parsing docs), e.g.
### Output: Install path ; /opt/maxapps/ibm/db2/V10.5
### Output: Level        ; 10.5.0.4
### Output: Fix pack     ; 4
### Output: tcpport      ; 50005
### Output: db           ; GTRMFPRD (MFDBADM PUBLIC APPCNTR MFADMIN)
### Output: db           ; GTRMXPRD (CTGINST1 DB2ADMIN MAXIMO PUBLIC)
### Example: getdb2info titus

    myhost=$1
    cachef="/home/$(getmasteradminacct)/aws/state/$(getclient $myhost)/db2info.$myhost.text"
    if [[ -f "$cachef" ]] ; then
        cat $cachef
    else
        ssh -n -o ConnectTimeout=2 -o StrictHostKeyChecking=no $myhost /usr/local/bin/db2info.sh 2>/dev/null
        [[ $? -ne 0 ]] && echo "Information not available"
    fi

}
