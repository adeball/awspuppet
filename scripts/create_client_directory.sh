#!/bin/bash
#
# Creates a client directory from the template_client
#
# Most files will be links to the template, if you need to make changes, ensure that
# you convert this to a copy first.
#

cd $(dirname $0)
. ./common.sh

[[ -z "$1" ]] && fail "Usage: $0 clientname" || client=$1

cd ..
[[ -d $client ]] && fail "$client directory already exists"
mkdir $client

cd template_client
find . | cpio -pdmv ../$client

cd ../$client || fail "Cannot cd $client"
while read l ; do 
	rm $l ; ln -s ../template_client/$l
done << EOF
assoc-zone.sh
generic.yaml
infra.yaml
rancher.yaml
servers.sh
EOF

### Set the CLIENT variable in setupenv.sh
CLIENT=$(echo $client | tr '[a-z]' '[A-Z]') 
sed -e "s/CLIENTSHORTCODE/${CLIENT}/" ../template_client/setupenv.sh > setupenv.sh 

