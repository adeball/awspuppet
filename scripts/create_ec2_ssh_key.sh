#!/bin/bash



kp="${CLIENT}-awskey"

aws ec2 create-key-pair --key-name=${kp} --output=text | sed -e 's/.*-----BEGIN/-----BEGIN/; s/END.*$/END PRIVATE RSA KEY-----/' > ~/${kp}.pem 

chmod 600 ~/${kp}.pem


