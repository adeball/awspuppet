#!/bin/bash

cd $(dirname $0) ; here=$(pwd)

cd ~/reponame/scripts
. ./common.sh

domain="mydomain.com"
[[ -n "$domain" ]] && basename="$here/${domain}." || basename="$here/"

for client in $(getclients) ; do
	echo
	echo $client
	export AWS_DEFAULT_PROFILE=$client

	aws acm import-certificate  --certificate       file://${basename}Certificate.pem      \
				    --certificate-chain file://${basename}CertificateChain.pem \
                                    --private-key       file://${basename}PrivateKey.pem
done

