#!/usr/bin/env bash
#
# install_Puppet_server.sh
#


### Puppet specifics

### Installs both server and agent
### And install the Puppet client and basic troubleshooting tools
if [[ "$CLOUDOS" == "linux" ]] ; then
    yum -y install puppet3 puppet3-server
elif [[ "$CLOUDOS" == "rhel" ]] ; then
    yum -y install puppet
fi

### Copy bootstrap puppet.conf and autosign.conf
cd $CLOUDINIT
find puppet | cpio -pdumv /etc

### Start the service
if [[ "$CLOUDOS" == "linux" || "$CLOUDOS" == "linux2" ]] ; then
    service puppetmaster start
elif [[ "$CLOUDOS" == "rhel" ]] ; then
    systemctl enable puppet.service
fi
### Start the master (this will generate the cert)
puppet master --verbose

### Install the init script (the master is not set up as a service by default)
### NB: The default is that it will start in run levels 3-5, and we started it
### above already, so just enable for now.
cp /etc/puppet/initscript /etc/init.d/puppetmaster
chmod a+x /etc/init.d/puppetmaster
chkconfig --add puppetmaster


### Enable the agent
systemctl enable puppet.service

### Start the agent
systemctl start puppetagent.service