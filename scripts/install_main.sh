#!/usr/bin/env bash
#
# install_main.sh
#
# Bootstrap for all RHEL7 instances

fail()
{
    ### General fault occurred - so let's stop
    printf "\n\n****************\nFailure in installation process: $@\n****************\n\n\nSTOPPING INSTALLATION\n\n\n"
    exit 1
}

export CLOUDINIT=/cloudinit
export MYDATA=${CLOUDINIT}/mydata

### Bootstrapping should have copied the ${CLOUDINIT} directory into place
### Along with ${MYDATA}, which contains environment variables
### describing this instance.

if [[ ! -f ${MYDATA} ]] ; then
    echo ""
    echo "Something has failed during the initialisation.  ${MYDATA} does not exist."
    echo "The ${CLOUDINIT} directory should have been copied from S3, and ${MYDATA} created"
    echo "with environment variables pre-populated."
    echo ""
    echo "If you have deleted an existing version of ${MYDATA} by mistake, you may be able to retrieve it using:"
    echo "cp /etc/profile.d/cloud.sh /cloudinit/mydata"
    echo "If the whole /cloudinit directory is not in existence, you can use"
    echo "aws s3 sync s3://ux1cloudscripts ${CLOUDINIT}"
    echo "to recreate it (then retrieve ${MYDATA}) as before."
    echo ""
    fail "See above"
fi

### Copy the host's configuration so it's read at boot time, then
### read it now - this will populate:
### CLOUDENV        e.g. PRD|TST
### CLOUDCLIENT     e.g. GTR
### CLOUDPROJECT    e.g. RPT
### CLOUDSERVERTYPE e.g  Puppet|DB2|Websphere
### CLOUDDOMAIN     e.g. mydomain.com
### CLOUDINSTANCE   e.g. 001
### For details. see the CloudFormation template (under ${CLOUDINIT}/CFTemplates)

cp ${MYDATA} /etc/profile.d/cloud.sh
chmod a+x /etc/profile.d/cloud.sh
. ${MYDATA}

mydomain=$(echo ${CLOUDENV}.${CLOUDCLIENT}.${CLOUDDOMAIN} | tr [A-Z] [a-z])
myfqdn=$(echo ${CLOUDSERVERNAME}.$mydomain | tr [A-Z] [a-z])


### Sets the hostname for this session, and timezone
echo "Setting hostname to: $myfqdn"
if [[ "$CLOUDOS" == "linux" || "$CLOUDOS" == "linux2" || "$CLOUDOS" == "ubuntu" ]] ; then
    hostname ${myfqdn} || fail "Could not set hostname"
    echo ${myfqdn} > /etc/hostname
    ln -sf /usr/share/zoneinfo/Europe/London /etc/localtime
    service network restart
elif [[ "$CLOUDOS" == "rhel" ]] ; then
    hostnamectl set-hostname ${myfqdn} || fail "Could not set hostname"
    timedatectl set-timezone "Europe/London" ### Ultimately, work this out from AWS region...
fi

### Makes the hostname persistent across reboots
### (see: https://aws.amazon.com/premiumsupport/knowledge-center/linux-static-hostname-rhel7-centos7/)
sed -i "s/127.0.0.1/127.0.0.1   $myfqdn/" /etc/hosts
printf "NETWORKING_IPV6=no\nHOSTNAME=$myfqdn\n" >> /etc/sysconfig/network
echo "preserve_hostname: true" >> /etc/cloud/cloud.cfg

if [[ $CLOUDOS == "ubuntu" ]] ; then
    echo "search $mydomain" >> /etc/resolv.conf
    apt-get -y update
    echo
else
    yum -y update
fi

### Now execute the specific bootstrap items for the type of server
### NB: Keep as much of this in Puppet as possible - ideally, these scripts should not be needed
### except for Puppet servers themselves.
### NB2: Source the second script, otherwise the env vars don't propagate
. $CLOUDINIT/scripts/install_${CLOUDSERVERTYPE}_server.sh


### And install the Puppet client and basic troubleshooting tools

case $CLOUDOS in

    "linux")
        yum -y install puppet3
    ;;

    "linux2")
        ### Amazon Linux 2 has a very limited set of packages available by default
        yum -y install https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
        yum -y install puppet
    ;;

    "rhel")
        yum -y install puppet bind-utils nc
    ;;
    "ubuntu")
        apt -y install puppet-common
        puppet agent --enable
    ;;

esac


### Manual run not necessary, but good to have the output in the cloud-init.log
puppet agent -t

### Enable and start the service, it should connect to the server 'puppet' in the same DNS domain
if [[ "$CLOUDOS" == "linux" || "$CLOUDOS" == "linux2" ]] ; then
    chkconfig puppet on
    service puppet start
elif [[ "$CLOUDOS" == "rhel" ]] ; then
    systemctl enable puppet.service
    systemctl start puppet.service
fi


