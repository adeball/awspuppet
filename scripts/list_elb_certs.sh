#!/bin/bash

cd $(dirname $0)
. ../scripts/common.sh

tmpf=$(mktemp)

epoch2date()
{
    case $(uname -s) in
    Darwin)
        date -jf %s $1
        ;;
    Linux)
        date -d @$1
        ;;
    *)
        echo "unsupported OS"
        ;;
    esac

}

for client in $(getclients) ; do

    echo
    echo
    echo "$client"

	export AWS_DEFAULT_PROFILE=$client
    certs=$(aws acm list-certificates --output=text | awk '{print $2}')

    for cert in $certs ; do
        echo
        aws acm describe-certificate --certificate-arn $cert > $tmpf
        inuseby=$(awk -F\" '/arn/ && !/Cert/ {print $2}' $tmpf)
        expiryepoch=$(awk -F\" '/NotAfter/ {print $3}' $tmpf| sed -e 's/: //;s/\..*$//')
        expiry=$(epoch2date $expiryepoch)
        arn=$(awk -F\" '/CertificateArn/ {print $4}' $tmpf)

        echo "ARN=$arn"
        echo "EXPIRY=$expiry"
        echo "USEDBY=$inuseby"
     done

    echo

done

rm $tmpf
