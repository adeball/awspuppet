#!/usr/bin/env bash

cd $(dirname $0)
. ../scripts/common.sh

infile=$(pwd)/../etc/master_table
outfile=../puppet/hieradata/master.yaml

cat > $outfile <<EOF
###
### master.yaml
### Generated from $infile
###

EOF

cat $infile | egrep '^[a-zA-Z]' | while read key value ; do
    if [[ "$key" != "link" ]] ; then
        echo "common::$key: '$value'" >> $outfile
    else
        if [[ $linkstart -eq 0 ]] ; then
            echo "common::$key:" >> $outfile
            linkstart=1
        fi
        echo "  - '$value'" >> $outfile
    fi
done
