#!/bin/bash
#
# populate_dns.sh [client]
#
# Annoying limitation in CF means that different accounts cannot use IAM to assume roles
# within the confines of a template.
# Therefore, create a single template within the master account, from the host_table, and
# apply.  Then use stack update when changes are made.
#
# With a parameter of a client three-character short code (e.g. gtr), just update that client
# and the global aliases
#



cd $(dirname $0)
. ./common.sh

[[ $# -eq 1 ]] && clientlist=$1 || clientlist=$(getclients)



export ZONEID=$(getmasterprivatezoneid)
export zone=$(getmasterzone)
export masterclient=$(getmasterclient)
export MASTERCLIENT=$(echo $masterclient | tr [a-z [A-Z])

export AWS_DEFAULT_PROFILE=$masterclient
export AWS_DEFAULT_REGION=$(getregion $masterclient)


write_primary_rec()
{
  echo "  PrimaryDNS$host:"
  echo "    Type: AWS::Route53::RecordSet"
  echo "    Properties:"
  echo "      Comment: Primary DNS entry for $host"
  echo "      HostedZoneId: $ZONEID"
  echo "      Name: $host.$env.$client.$zone"
  echo "      ResourceRecords: [ '$ip' ] "
  echo "      TTL: 900"
  echo "      Type: A"
}

write_secondary_rec()
{
  echo "  SecondaryDNS$host:"
  echo "    Type: AWS::Route53::RecordSet"
  echo "    Properties:"
  echo "      Comment: Secondary DNS entry for $host"
  echo "      HostedZoneId: $ZONEID"
  echo "      Name: ${servertype}-$instance.$env.$client.$zone"
  echo "      ResourceRecords: [ '$host.$env.$client.$zone' ] "
  echo "      TTL: 900"
  echo "      Type: CNAME"
}

write_tertiary_rec()
{
  echo "  TertiaryDNS$host:"
  echo "    Type: AWS::Route53::RecordSet"
  echo "    Properties:"
  echo "      Comment: Tertiary DNS entry for $host"
  echo "      HostedZoneId: $ZONEID"
  echo "      Name: $host.$zone"
  echo "      ResourceRecords: [ '$host.$env.$client.$zone' ] "
  echo "      TTL: 900"
  echo "      Type: CNAME"
}

write_db2_rec()
{
  echo "  Db2DNS$env:"
  echo "    Type: AWS::Route53::RecordSet"
  echo "    Properties:"
  echo "      Comment: DB2 DNS entry for $client $env"
  echo "      HostedZoneId: $ZONEID"
  echo "      Name: ${servertype}.$env.$client.$zone"
  echo "      ResourceRecords: [ '${servertype}-$instance.$env.$client.$zone' ] "
  echo "      TTL: 900"
  echo "      Type: CNAME"
}

write_admin_aliases()
{

    for function in $(getmasteradminaliases) ; do
      echo "  ${function}DNS$host$myenv$myclient:"
      echo "    Type: AWS::Route53::RecordSet"
      echo "    Properties:"
      echo "      Comment: ${function} alias DNS entry for $host"
      echo "      HostedZoneId: $ZONEID"
      echo "      Name: ${function}.$myenv.$myclient.$zone"
      echo "      ResourceRecords: [ '$host.$zone' ] "
      echo "      TTL: 900"
      echo "      Type: CNAME"
    done

}

write_admin_aliases_noenv()
{

    for function in $(getmasteradminaliases) ; do
      echo "  ${function}DNS$host$myclient:"
      echo "    Type: AWS::Route53::RecordSet"
      echo "    Properties:"
      echo "      Comment: ${function} alias DNS entry for $host"
      echo "      HostedZoneId: $ZONEID"
      echo "      Name: ${function}.$myclient.$zone"
      echo "      ResourceRecords: [ '$host.$zone' ] "
      echo "      TTL: 900"
      echo "      Type: CNAME"
    done

}

tmpf=$(mktemp)
egrep -v "^#|^$" ../etc/host_table > $tmpf


for myclient in $clientlist ; do
    YAML=../etc/dns_$myclient.yaml

    echo
    echo "Creating $YAML"
    echo

    echo "Resources:" > $YAML

    while read host servertype ip env client instance rest ; do
        echo "$host" | grep -q "^[a-z]"
        if [[ $? -eq 0 && $client == $myclient ]] ; then
            echo "Creating DNS records for: $host"
            echo >> $YAML
            echo "### $host.$env.$client.$zone" >> $YAML
            echo >> $YAML
            write_primary_rec >> $YAML
            write_secondary_rec >> $YAML
            write_tertiary_rec >> $YAML

            echo >> $YAML

            if [[ $servertype == "db2" && $instance == "001" ]] ; then
                echo "# Additional CNAME for live DB2 server"
                write_db2_rec >> $YAML
                echo >> $YAML
            fi


        fi

    done < $tmpf

    ### A bit of a hack - add any other dns_<client>_addendum*.yaml files in the etc directory onto the end
    ### This enables specific entries to be hand-crafted.
    [[ -f ../etc/dns_${myclient}_addendum.yaml ]] && cat ../etc/dns_${myclient}_addendum*.yaml >> $YAML

    CLIENT=$(echo $myclient | tr [a-z] [A-Z])
    export AWS_DEFAULT_PROFILE=$(getmasterclient)
    STACK="$(getmasterstackprefix)-DNS-$CLIENT"
    template="file://$(pwd)/$YAML"

    ### Create/update stacks, leave EMS until later, as more content may be added
    if [[ $CLIENT != $MASTERCLIENT ]] ; then
	    aws cloudformation describe-stacks --stack-name "$STACK" > /dev/null 2>&1
	    if [[ $? -eq 0 ]] ; then

            echo "Updating $STACK..."
            aws cloudformation update-stack \
            --stack-name "$STACK" \
            --template-body ${template}

	    else

            echo "Creating $STACK..."
            aws cloudformation create-stack \
            --stack-name "$STACK" \
            --template-body ${template}

	    fi
    fi


done

echo
echo "Creating $(getmasterdescription)/global entries"
echo

### Use this block if you want separate admin servers per client
#for myclient in $(getclients) ; do
#	while read host servertype ip env client instance rest ; do
#		if [[ $myclient == ${client} && ${servertype} == "admin" ]] ; then
#		    echo >> $YAML
#		    echo "### Admin server for $client = $host" >> $YAML
#		    echo >> $YAML
#		    for myenv in $(getenvironments $myclient) ; do
#		        write_admin_aliases >> $YAML
#		    done
#		    write_admin_aliases_noenv >> $YAML
#		fi
#	done < $tmpf
#done

### This forces everything to resolve to the admin server
env=$(getmasteradminenv)
client=$masterclient
CLIENT=$MASTERCLIENT
host=$(getmasteradminserver)
YAML=../etc/dns_$client.yaml
STACK="$(getmasterstackprefix)-DNS-$CLIENT"
template="file://$(pwd)/$YAML"

for myclient in $(getclients) ; do
    for myenv in $(getenvironments $myclient) ; do
       write_admin_aliases >> $YAML
    done
    write_admin_aliases_noenv >> $YAML
done

[[ -f ../etc/dns_${myclient}_addendum*.yaml ]] && cat ../etc/dns_${myclient}_addendum*.yaml >> $YAML


aws cloudformation describe-stacks --stack-name "$STACK" > /dev/null 2>&1
if [[ $? -eq 0 ]] ; then

    echo "Updating $STACK..."
    aws cloudformation update-stack \
    --stack-name "$STACK" \
    --template-body ${template}

else

    echo "Creating $STACK..."
    aws cloudformation create-stack \
    --stack-name "$STACK" \
    --template-body ${template}

fi

rm -f $tmpf
