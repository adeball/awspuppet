#!/bin/bash
#
# setup_buckets.sh
#

cd $(dirname $0)
. ../scripts/common.sh
backupbase=$(getmasterbackupbase)
poltemp=../etc/policy_template.json

### Backup buckets - no special permissions needed for the client, as they are in client account, same region.
### Add permissions for the master account to access from the main account.

for client in $(getclients) ; do

    echo
    echo "Setting up backup bucket for $client"
    export AWS_DEFAULT_PROFILE=$client
    bucketurl=s3://${backupbase}-$client
    aws s3 ls $bucketurl > /dev/null 2>&1
    if [[ $? -ne 0 ]] ; then
        aws s3 mb $bucketurl
    else
        echo "$bucketurl already exists, not creating it"
    fi


    ### Apply permissions for the master account

    pol=$(mktemp)
    masterclient=$(getmasterclient)
    CLIENT=$(echo $client | tr [a-z] [A-Z])
    awsid=$(getawsid $masterclient)
    bucketname=${backupbase}-$client

    if [[ "$awsid" != 0 ]] ; then 
	    echo Applying policy for: client=$client, CLIENT=$CLIENT, awsid=$awsid, bucketname=$bucketname
	    echo '{
	  "Version": "2012-10-17",
	  "Statement": [' > $pol
	    sed -e "s/XXXCLIENT/$CLIENT/g;s/XXXAWSID/$awsid/g;s/XXXBUCKET/$bucketname/g; $ d" $poltemp >> $pol
	    echo '  ]
	}' >> $pol
	    cat $pol
	    aws s3api put-bucket-policy --bucket $bucketname --policy file://$pol
	    rm $pol
    fi

done



### Buckets owned by the master account that clients need access to

export AWS_DEFAULT_PROFILE=$(getmasterclient)

for bucketname in $(getmasterbucketlist) ; do

    echo
    echo "Setting up $bucketname"
    bucketurl=s3://$bucketname
    aws s3 ls $bucketurl > /dev/null 2>&1
    if [[ $? -ne 0 ]] ; then
        aws s3 mb $bucketurl
    else
        echo "$bucketurl already exists, not creating it"
    fi


    ### Apply permissions for clients

    pol=$(mktemp)
    echo '{
  "Version": "2012-10-17",
  "Statement": [' > $pol
    for client in $(getclients) ; do

        CLIENT=$(echo $client | tr [a-z] [A-Z])
        awsid=$(getawsid $client)

        if [[ "$awsid" != 0 ]] ; then
            echo Applying policy for: client=$client, CLIENT=$CLIENT, awsid=$awsid, bucketname=$bucketname
            sed -e "s/XXXCLIENT/$CLIENT/g;s/XXXAWSID/$awsid/g;s/XXXBUCKET/$bucketname/g" $poltemp >> $pol
            echo
        fi

     done
     pol2=$(mktemp)
     sed -e '$ d' $pol > $pol2
     echo '  ]
}' >> $pol2
     aws s3api put-bucket-policy --bucket $bucketname --policy file://$pol2
     cat $pol2
     rm $pol $pol2

done
