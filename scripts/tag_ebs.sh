#!/bin/bash
#
# Sets up tagging based on the host_table entries for arg: $1
#

### Assumes this script is in the scripts directory
### Or at least in a directory at the same level as the client directories

cd $(dirname $0) 
. ../scripts/common.sh

[[ -z ${1} ]] && fail "Usage: $0 servername"
client=$(getclient $1)
[[ -z ${client} ]] && fail "Cannot find $1 in the host_table"

host=$1

### Set up correct AWS credentials
. ../${client}/setupenv.sh
instanceid=$(getinstanceid $host)

owner=$(getowner $host)
ownerfull=$(getownerfullname $owner)
stype1=$(getservertype $host)
stype2=$(getservertype2 $stype1)
env=$(getenvironment $host)
instance=$(getinstance $host)
CLIENT=$(echo $client | tr [a-z] [A-Z])
ENV=$(echo $env | tr [a-z] [A-Z])


### Tag EBS volumes

aws ec2 describe-instances --instance-id=$instanceid --output=text \
| awk '/BLOCKDEVICEMAPPINGS/ {printf "%s ", $2} /EBS/ {print $NF}' | while read device volid ; do

    devname=$(echo $device| sed -e 's-/dev/--')
    nametag="${ENV}-${stype2}-${instance} ${host}-$devname"

    aws ec2 create-tags --resources=$volid --tags=Key="Name",Value="$nametag"
done
