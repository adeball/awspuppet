#!/bin/bash

cd $(dirname $0)/scripts
. ./common.sh
as=$(getmasteradminserver)
cd ..


###
### Use --size-only as the timestamps get messed around when files are pushed/pulled from the repository.
### There's a small chance that a file may not be transferred if it's updated, but the size is the same.
### If you suspect this, you'll need to compare checksums to be 100% and transfer manually.
### This is very unlikely to be a problem, but you need to be aware that it might happen.
###

### Copy to /etc/puppet on the Puppet master
rsync -rlDvx --size-only --delete --exclude="puppet/ssl" --exclude=".git" --delete-excluded puppet root@${as}:/etc

### Copy to /cloudinit on the Puppet master (this can eventually go, but some scripts still use this, e.g. xymon/munin stuff)
### they can be converted to use the local repository copy instead, which is available as
### ~$(getmasteradminacct)/$(getmasterreponame)

rsync -rlDvx --size-only --delete --exclude=".git" --exclude="*.iml" --exclude="mydata" . root@${as}:/cloudinit
