#!/usr/bin/env bash
#
# assoc-zone.sh
#
# Associates a client VPC with a private or public hosted zone in the master account
#
# ./assoc-zone.sh zonename private|public
# e.g. ./assoc-zone.sh mydomain.com private
#

cd $(dirname $0); . ./setupenv.sh

[[ $# -ne 2 ]] && fail "Usage: $0 zonename private|public"

zonename="${1}."  ### Route53 uses the FQDN with the top-level '.'
pubpriv=$2

case $pubpriv in
    private)
        config="True"
        ;;
    public)
        config="False"
        ;;
    *)
        fail "Usage: $0 zonename private|public"
        ;;
esac


### Allow this VPC to create records in the master account private domain
VPC=$(getvpc)

zone=$(clientaws ems route53 list-hosted-zones --output=text | \
        awk -v z="${zonename}" -v c="${config}" '
            $1=="HOSTEDZONES" {zone=$4; split($3,a,"/"); id=a[3] }
            $1=="CONFIG" && $NF==c && zone==z {print id}
        ')

export AWS_DEFAULT_PROFILE=$(getmasterclient) ## Do this with the master account credentials
aws route53 create-vpc-association-authorization --hosted-zone-id $zone --vpc VPCRegion=${REGION},VPCId=${VPC}

export AWS_DEFAULT_PROFILE=$client     ## Do this with client credentials
aws route53 associate-vpc-with-hosted-zone --hosted-zone-id $zone --vpc VPCRegion=${REGION},VPCId=${VPC}


