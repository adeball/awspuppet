#!/usr/bin/env bash

cd $(dirname $0); . ./setupenv.sh 

### Create VPC and public/private subnets in two availability zones

vpnport=$(getvpnport $client)
#zonename=$(getmasterzone)
zonename=mydomain.home  ### Not used, but may be in future

aws cloudformation create-stack \
--stack-name ${VPCSTACK} \
--template-body "file://$(pwd)/infra.yaml" \
--parameters ParameterKey=ClassB,ParameterValue="${CLASSB}" \
             ParameterKey=Client,ParameterValue="${CLIENT}" \
             ParameterKey=VpnPort,ParameterValue="${vpnport}" \
             ParameterKey=TLD,ParameterValue="${zonename}" \
             ParameterKey=ZoneRegion,ParameterValue="${REGION}"

status=$(aws cloudformation describe-stacks --stack-name $VPCSTACK --output=text | awk '/STACKS/ {print $NF}')
while $(echo ${status} | grep -qv "_COMPLETE"); do
    status=$(aws cloudformation describe-stacks --stack-name $VPCSTACK --output=text | awk '/STACKS/ {print $NF}')
    echo "$(date) - ${status}"
    sleep 15
done

### Allow this VPC to create records in the master account private domain
VPC=$(getvpc)

for zone in $ZONEID ; do

	export AWS_DEFAULT_PROFILE=$(getmasterclient) ## Do this with the master account credentials
	aws route53 create-vpc-association-authorization --hosted-zone-id $zone --vpc VPCRegion=${REGION},VPCId=${VPC}

	export AWS_DEFAULT_PROFILE=$client     ## Do this with client credentials
	aws route53 associate-vpc-with-hosted-zone --hosted-zone-id $zone --vpc VPCRegion=${REGION},VPCId=${VPC}

done


### Create an assume-<client>-admin-role policy, <client>-admin group, and attach the policy

export AWS_DEFAULT_PROFILE=$(getmasterclient)
mawsid=$(getawsid $AWS_DEFAULT_PROFILE)

tf=$(mktemp)
cat > $tf << EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "arn:aws:iam::$(getawsid $client):role/amazon-administrators"
  }
}
EOF

aws iam create-policy --policy-name="assume-${client}-admin-role" --policy-document=file:///$tf
arn="arn:aws:iam::${mawsid}:policy/assume-${client}-admin-role"
aws iam create-group --group-name="${client}-admin"
aws iam attach-group-policy --group-name="${client}-admin" --policy-arn=$arn


cat $tf
rm $tf

