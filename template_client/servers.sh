#!/bin/bash

cd $(dirname $0) ; here=$(pwd)
echo $here
cd $here/..
./push_updates

cd $here
. ./setupenv.sh

for server in $(getservers $client) ; do 
	echo
	echo "$server"
	deployec2 "$server"
	echo

done
