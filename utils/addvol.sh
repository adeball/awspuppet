#!/bin/bash
#
# $1=hostname
# $2=size in GB
# $3=device (e.g. xvde)
# $4=type (e.g. gp2)
#

if [[ $# -ne 4 ]] ; then
	echo "Usage: $0 hostname sizeinGB device type (e.g. $0 keda 120 xvdf gp2)"
	exit 1
fi

cd $(dirname $0)
. ../scripts/common.sh

host=$1
size=$2
device=$3
type=$4

### Make IOPs 50 x size if io2 storage
[[ "$type" == "io1" ]] && type="io1 --iops $(echo $size|awk '{print $1*50}')"

az=$(getaz $host)
hostid=$(getinstanceid $host)
env=$(getenvironment $host) ; ENV=$(echo $env | tr '[a-z]' '[A-Z]')
stype=$(getservertype $host) ; STYPE=$(echo $stype | tr '[a-z]' '[A-Z]')
instance=$(getinstance $host)
client=$(getclient $host)
volname="${ENV}-${STYPE}-${instance} ${host}-${device}"

export AWS_DEFAULT_PROFILE=$client
export AWS_DEFAULT_REGION=$(getregion $client)

echo "Adding a ${size}GB volume to $host as device $device (AZ=$az, volname=$volname)"

echo "Creating volume"
volid=$(aws ec2 create-volume --size $size --availability-zone $az \
--volume-type $type | awk -F \" '/VolumeId/ {print $4}')

state="unknown"
until [[ "$state" == "available" ]] ; do
    state=$(aws ec2 describe-volumes --volume-id=$volid --output=text | awk '{print $7}')
    echo $state
    sleep 1
done

echo "Attaching volume"
aws ec2 attach-volume --volume-id $volid --instance-id $hostid --device $device

echo "Tagging volume name as: $volname"
aws ec2 create-tags --resources=$volid --tags=Key="Name",Value="$volname"

