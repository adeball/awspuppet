#!/bin/bash
#
# Creates assume-<client>-admin-role policy
# Attach to an AWS user account or group to enable that user to switch role to 
# to the client account as an administrator
#


. ../scripts/common.sh

tf=$(mktemp)

export AWS_DEFAULT_PROFILE=$(getmasterclient)
mawsid=$(getawsid $AWS_DEFAULT_PROFILE)

for client in $(getclients) ; do
	echo $client
	awsid=$(getawsid $client)
	sed -e "s/XXXAWSIDXXX/$awsid/" > $tf << EOF
{
  "Version": "2012-10-17",
  "Statement": {
    "Effect": "Allow",
    "Action": "sts:AssumeRole",
    "Resource": "arn:aws:iam::XXXAWSIDXXX:role/amazon-administrators"
  }
}
EOF

	aws iam create-policy --policy-name="assume-${client}-admin-role" --policy-document=file:///$tf
	arn="arn:aws:iam::${mawsid}:policy/assume-${client}-admin-role"
	aws iam create-group --group-name="${client}-admin" 
	aws iam attach-group-policy --group-name="${client}-admin" --policy-arn=$arn
	
done

rm $tf
