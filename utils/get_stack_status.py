#
# get_stack_status.py
#

import boto3, sys

if (len(sys.argv)) == 1:
    # No args passed - set to null, otherwise it might match a real stack
    STACK = ""
elif (len(sys.argv)) == 2:
    STACK = sys.argv[1]
else:
    print("Usage: {} [stack]".format(sys.argv[0]))

client = boto3.client('cloudformation')
stacks_dict = client.list_stacks()

stack_list = []
for stack_dict in stacks_dict['StackSummaries']:
    if STACK in stack_dict['StackName']:
        print("Stack name: {0:25}  Stack status: {1}".format(stack_dict['StackName'], stack_dict['StackStatus']))
