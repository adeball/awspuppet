#!/usr/bin/env bash

# Usage get_was_logs.sh client env SERVER (e.g. get_was_logs.sh gtr prd UI)


client=$1
env=$2
server=$3

. ../scripts/common.sh

for h in babylon jericho heliopolis; do
    tf=/tmp/${h}-${server}-logs.tar
    ssh root@$h "cd /opt/maxapps/IBM/WebSphere/AppServer/profiles/ctgAppSrv*/logs ; find *MX${server}Server*/SystemOut* -mtime -1 | cpio -o -Htar > $tf ; gzip $tf"
    scp root@$h:${tf}.gz /tmp
done

