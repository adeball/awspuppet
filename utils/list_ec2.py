#!/usr/bin/env python3
#
# list_ec2.py
#
# Ade - 2017-02
#

import boto3

client_conn = boto3.client('ec2')
ec2_dict = client_conn.describe_instances()

print()
fstring = "{:19} {:15} {:15} {:13} {:6} {:20} {:11} {:15} {:28} {}"
print(fstring.format('Instance ID', 'Private IP', 'Public IP', 'State', 'Client', 'Name', 'Env', 'Owner', 'Stack', "Avlbty Zone"))
print()

### Work down the data structure and pull out the elements we're interested in
### (NB: use aws ec2 describe-instances to see everything)

res_dict = ec2_dict['Reservations']
for instance_dict in res_dict:
    for item in instance_dict.keys():
        if item == 'Instances':
            instance = instance_dict[item]
            for key_dict in instance:
                id = key_dict['InstanceId']
                try:
                    privip = key_dict['PrivateIpAddress']
                except:
                    privip = '---'
                try:
                    pubip = key_dict['PublicIpAddress']
                except:
                    pubip = '---'
                state_dict = key_dict['State']
                state = state_dict['Name']
                env = '---'
                cf = '---'
                owner = '---'
                client = '---'
                project = '---'
                try:
                    tags = key_dict['Tags']
                    for tag in tags:
                        if tag['Key'] == 'Client':
                            client = tag['Value']
                        if tag['Key'] == 'aws:cloudformation:stack-name':
                            cf = tag['Value']
                        if tag['Key'] == 'Owner':
                            owner = tag['Value']
                        if tag['Key'] == 'Environment':
                            env = tag['Value']
                        if tag['Key'] == 'Name':
                            name = tag['Value']
                except:
                    print()
                avz_dict = key_dict['Placement']
                avz = avz_dict['AvailabilityZone']
                print(fstring.format(id, privip, pubip, state, client, name, env, owner, cf, avz))

print()
