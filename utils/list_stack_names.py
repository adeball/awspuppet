#!/usr/bin/env python3
#
# list_stack_names.py
# Python3
# Prints all stack names, sorted & unique
#

import boto3

client = boto3.client('cloudformation')
stacks_dict = client.list_stacks()

stack_list = []
for stack_dict in stacks_dict['StackSummaries']:
    stack_list.append(stack_dict['StackName'])

# 'Set' removes duplicates
for stack in sorted(set(stack_list)):
    print(stack)
