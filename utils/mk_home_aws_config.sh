#!/usr/bin/env bash

cd $(dirname $0)
. ../scripts/common.sh

conf=~/.aws/config
cred=~/.aws/credentials

if [[ -f $conf ]] ;then
    cp $conf $conf.save
else
    mkdir -p ~/.aws
fi

[[ -f $conf.local ]] && cp $conf.local $conf || > $conf
[[ -f $cred.local ]] && cp $cred.local $cred || > $cred

echo "[profile default]" >> $conf
echo "region = eu-west-1" >> $conf

for client in $(getclients) ; do

    echo >> $conf
    echo "[profile $client]" >> $conf
    echo "region = $(getregion $client)" >> $conf

    if [[ $client != $(getmasterclient) ]] ; then

	    echo >> $cred
	    echo "[$client]" >> $cred
	    echo "role_arn = arn:aws:iam::$(getawsid $client):role/amazon-administrators" >> $cred
	    echo "source_profile = $(getmasterclient)" >> $cred
    fi

done 
