#!/bin/bash


while read ip name comment client env stype2 stype instance ; do

    intinstance=$(echo $instance | awk '{printf "%d\n", $1}')
    if [[ "$comment" == "###" && "$intinstance" -gt 0 ]] ; then
        echo
        echo "-------------------------------------"
        echo $name
        ssh -n -o connectTimeout=1 root@$name "$@"
    fi

done < /etc/hosts

echo