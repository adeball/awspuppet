#!/bin/bash
#
# Tags all EC2 instances and EBS volumes
#

cd $(dirname $0)
. ../scripts/common.sh

for client in $(getclients) ; do
	echo
	echo Tagging $client instances 
	for server in $(getservers $client) ; do 
		echo Tagging $server 
		tag_ec2 $server >/dev/null &
		tag_ebs $server 
	done
done 

