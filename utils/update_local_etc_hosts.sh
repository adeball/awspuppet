#!/bin/bash
#
# Update entries in local /etc/hosts based on the current host_table
#

cd $(dirname $0)/../etc
. ../scripts/common.sh

h=/etc/hosts
t=$(mktemp)
MARKER1="### $(getmasterdescription) host entries - start"
MARKER2="### $(getmasterdescription) host entries - end"

awk -v s="$MARKER1" -v e="$MARKER2" '
	BEGIN	{echo=1}
	$0==s	{echo=0}
	echo==1	{print}
	$0==e	{echo=1}
	' $h > $t

echo "$MARKER1" >> $t
echo >> $t
echo "###############################################################" >> $t
echo >> $t

lc="dummy"
grep ^[a-z] host_table | while read host stype ip env client instance subnet domain version owner monitor ; do

	[[ $client != $lc && $lc != "dummy" ]] && echo >> $t
	CLIENT=$(echo $client | tr [a-z] [A-Z])
	ENV=$(echo $env | tr [a-z] [A-Z])
	stype2=$(getservertype2 $stype)
	printf "%-15s %-15s ### %-4s %-4s %-8s %-12s %-3s\n" $ip $host $CLIENT $ENV $stype2 $stype $instance>> $t
	lc=$client

done 

echo >> $t
echo "###############################################################" >> $t
echo >> $t

lc="dummy"
grep ^[a-z] host_table | while read host stype ip env client instance subnet domain version owner monitor ; do

	[[ $client != $lc && $lc != "dummy" ]] && echo >> $t
	CLIENT=$(echo $client | tr [a-z] [A-Z])
	ENV=$(echo $env | tr [a-z] [A-Z])
	printf "%-15s %-37s %-37s\n" $ip $host.$env.$client.$domain ${stype}-$instance.$env.$client.$domain >> $t
	if [[ "$stype" == "admin" ]] ; then
	    for alias in $(getmasteradminaliases) ; do
            printf "%-15s %-37s %-37s\n" $ip $alias.$client.$domain "### Alias of ${stype}-$instance" >> $t
	    done
	fi
	lc=$client

done 

echo >> $t
echo "###############################################################" >> $t
echo >> $t

lc="dummy"
grep ^[a-z] host_table_monitoronly | while read host stype ip env client instance subnet domain version owner monitor ; do

	[[ $client != $lc && $lc != "dummy" ]] && echo >> $t
	CLIENT=$(echo $client | tr [a-z] [A-Z])
	ENV=$(echo $env | tr [a-z] [A-Z])
	stype2=$(getservertype2 $stype)
	printf "%-15s %-15s ### %-4s %-4s %-8s %-12s %-3s\n" $ip $host $CLIENT $ENV $stype2 $stype $instance>> $t
	lc=$client

done 

echo >> $t
echo "$MARKER2" >> $t

cp $t $h
