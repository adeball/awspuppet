#!/usr/bin/env bash

[[ "$0" != "-bash" ]] && cd $(dirname $0)

. ../scripts/common.sh

export CLIENT=UX1

export client=$(echo $CLIENT | tr [A-Z] [a-z])
export AWS_DEFAULT_PROFILE=$client
export VPCSTACK=${CLIENT}-INFRASTRUCTURE
export REGION=$(getregion $client)
export AWS_DEFAULT_REGION=$REGION
export CLASSB=$(getclassb $client)
export ZONENAME=$(getmasterzone)
export ZONEID=$(getmasterprivatezoneid)


