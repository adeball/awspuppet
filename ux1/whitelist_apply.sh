#!/usr/bin/env bash


### There's a 50 rule limit on security groups, so if ports are specified individually, these run out very quickly.
### As we should trust the RWG network (and IPtables will also stop nefarious traffic), it is pragmatic to just use
### a large range of ports to encompass what we need.
### 80-9300 (where 80 = http and 9300 is the Cognos dispatcher service).

cd $(dirname $0)
. ./setupenv.sh

VPC=$(getvpc)
TXT=whitelist.txt
YAML=whitelist.yaml

tmpf=$(mktemp) ; touch $tmpf
while getopts "a" opt; do
        case $opt in
                a)
                        #myip=$(dig +short myip.opendns.com @resolver1.opendns.com)
                        #myip=$(nslookup myip.opendns.com 208.67.222.222 | tail -2 | awk '/Address/ {print $2}')
			myip=$(curl -s https://ipinfo.io/ip)
                        echo "Called with -a, adding ${myip}/32"
                        echo "${myip}/32" > $tmpf
                        ;;
                \?)
                        fail "Invalid option: -$OPTARG  Usage: $0 [-a]"
                        ;;
        esac
done



cat << EOF > $YAML
Description: "$CLIENT Whitelist - Apply to ELBs and IHS instances"
Outputs:
  ElbSg:
    Value: !Ref ElbSg
Resources:
  ElbSg:
    Type: "AWS::EC2::SecurityGroup"
    Properties:
      GroupDescription: "$CLIENT Load balancer security group (whitelist)"
      VpcId: $VPC
      Tags:
        - Key: Name
          Value: "$CLIENT-ELB"
EOF

i=1
sed -e 's/#.*$//; /^$/d' $TXT $tmpf | while read cidr port ; do

    startport=22
    endport=50005
    if [[ -n "$port" ]] ; then
	startport=$port
	endport=$port
    fi

    id=$(echo $i | awk '{printf "%3.3d\n", $1}')

    echo "  ElbSgI${startport}x$id:" >> $YAML
    echo "    Type: \"AWS::EC2::SecurityGroupIngress\"" >> $YAML
    echo "    Properties: { GroupId: !Ref ElbSg, CidrIp: \"$cidr\", IpProtocol: tcp, FromPort: \"$startport\", ToPort: \"$endport\" }" >> $YAML

    i=$(($i + 1))

done



export AWS_DEFAULT_PROFILE=$client
STACK="$CLIENT-WHITELIST"
template="file://$(pwd)/$YAML"

aws cloudformation describe-stacks --stack-name "$STACK" > /dev/null 2>&1
if [[ $? -eq 0 ]] ; then

    echo "Updating $STACK..."
    aws cloudformation update-stack \
    --stack-name "$STACK" \
    --template-body ${template}

else

    echo "Creating $STACK..."
    aws cloudformation create-stack \
    --stack-name "$STACK" \
    --template-body ${template}

fi

stackstat()
{
    aws cloudformation describe-stacks --stack-name="$1" --output=text | awk '/STACKS/ {print $NF}'
}

until stackstat $STACK | grep -q COMPLETE$ ; do
    echo -n .  ; sleep 2
done
echo "Finished, status: $(stackstat $STACK)"

rm $tmpf
